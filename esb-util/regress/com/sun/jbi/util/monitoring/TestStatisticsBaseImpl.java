/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestStatisticsBaseImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util.monitoring;

import com.sun.jbi.monitoring.StatisticsBase;

/**
 * Tests for the StatisticsBaseImpl class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestStatisticsBaseImpl
    extends junit.framework.TestCase
{
    /**
     * Statistics base object.
     */
    private StatisticsBase mStatsBase;

    /**
     * Key value for base object.
     */
    private static final String BASE_KEY = "Base";

    /**
     * Key value for child object.
     */
    private static final String CHILD_KEY_1 = "Child_1";

    /**
     * Key value for child object.
     */
    private static final String CHILD_KEY_2 = "Child_2";

    /**
     * Key value for parent object.
     */
    private static final String PARENT_KEY = "Parent";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestStatisticsBaseImpl(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mStatsBase = new StatisticsBaseImpl(BASE_KEY);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the addChild/getChild methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddGetChild()
        throws Exception
    {
        // Create child objects for test
        StatisticsBaseImpl child1 = new StatisticsBaseImpl(CHILD_KEY_1);
        StatisticsBaseImpl child2 = new StatisticsBaseImpl(CHILD_KEY_2);

        // Retrieve nonexistent child object
        assertNull("getChild returned object: ",
            mStatsBase.getChild(CHILD_KEY_1));

        // Add and retrieve one child object
        mStatsBase.addChild(child1);
        assertSame("getChild returned wrong object: ",
            mStatsBase.getChild(CHILD_KEY_1), child1);

        // Add and retrieve second child object
        mStatsBase.addChild(child2);
        assertSame("getChild returned wrong object: ",
            mStatsBase.getChild(CHILD_KEY_1), child1);
        assertSame("getChild returned wrong object",
            mStatsBase.getChild(CHILD_KEY_2), child2);
    }

    /**
     * Test the set/getParent methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetParent()
        throws Exception
    {
        // Create parent object for test
        StatisticsBaseImpl parent = new StatisticsBaseImpl(PARENT_KEY);

        // Set parent object
        mStatsBase.setParent(parent);
        assertSame("getParent returned wrong object: ",
            mStatsBase.getParent(), parent);

        // Make sure child gets right parent
        StatisticsBaseImpl child = new StatisticsBaseImpl(CHILD_KEY_1);
        mStatsBase.addChild(child);
        assertSame("getParent returned wrong object: ",
            child.getParent(), mStatsBase);
    }

    /**
     * Test the getKey method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetKey()
        throws Exception
    {
        assertEquals("getKey returned wrong key value: ",
            mStatsBase.getKey(), BASE_KEY);
    }

    /**
     * Test the getChildren method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetChildren()
        throws Exception
    {
        int count = 0;

        // Create child objects for test
        StatisticsBaseImpl child1 = new StatisticsBaseImpl(CHILD_KEY_1);
        StatisticsBaseImpl child2 = new StatisticsBaseImpl(CHILD_KEY_2);

        // Retrieve nonexistent children
        assertEquals("getChildren returned wrong number of objects: ",
            new Integer(mStatsBase.getChildren().size()), new Integer(count));

        // Add and retrieve one child object
        mStatsBase.addChild(child1);
        ++count;
        assertEquals("getChildren returned wrong number of objects: ",
            new Integer(mStatsBase.getChildren().size()), new Integer(count));
        assertTrue("getChildren returned wrong objects",
            mStatsBase.getChildren().contains(child1));

        // Add and retrieve second child object
        mStatsBase.addChild(child2);
        ++count;
        assertEquals("getChildren returned wrong number of objects: ",
            new Integer(mStatsBase.getChildren().size()), new Integer(count));
        assertTrue("getChildren returned wrong objects",
            mStatsBase.getChildren().contains(child1));
        assertTrue("getChildren returned wrong objects",
            mStatsBase.getChildren().contains(child2));
    }

    /**
     * Test the setEnabled, setDisabled, and isEnabled methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEnabled()
        throws Exception
    {
        assertTrue("isEnabled() returned wrong value on new object: ",
            mStatsBase.isEnabled());
        mStatsBase.setDisabled();
        assertFalse("isEnabled() returned wrong value after setDisabled(): ",
            mStatsBase.isEnabled());
        mStatsBase.setEnabled();
        assertTrue("isEnabled() returned wrong value after setEnabled(): ",
            mStatsBase.isEnabled());

        // Make sure child gets right setting
        StatisticsBaseImpl child = new StatisticsBaseImpl(CHILD_KEY_1);
        mStatsBase.addChild(child);
        assertTrue("isEnabled() returned wrong value on new object: ",
            child.isEnabled());
        mStatsBase.setDisabled();
        assertFalse("isEnabled() returned wrong value for child after setDisabled(): ",
            child.isEnabled());
        mStatsBase.setEnabled();
        assertTrue("isEnabled() returned wrong value for child after setEnabled(): ",
            child.isEnabled());
    }

    /**
     * Test the registerMBean/unregisterMBean methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterUnregisterMBean()
        throws Exception
    {
        // Not yet implemented
        ;
    }
}
