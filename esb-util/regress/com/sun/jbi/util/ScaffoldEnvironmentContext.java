/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldEnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;

import com.sun.jbi.EnvironmentContext;

/**
 * Scaffolded EnvironmentContext for unit tests.
 *
 * @author Mark S White
 */
public class ScaffoldEnvironmentContext
    implements com.sun.jbi.EnvironmentContext
{    
    /**
     * Get the platform-specific context for this implementation.
     * @return The PlatformContext.
     */
    public com.sun.jbi.platform.PlatformContext getPlatformContext()
    {
        return null;
    }
    
    /**
     * Get the AppServer installation root directory.
     * @return The AppServer install root.
     */
    public String getAppServerInstallRoot()
    {
        return null;
    }

    /**
     * Get the AppServer instance root directory.
     * @return The AppServer instance root.
     */
    public String getAppServerInstanceRoot()
    {
        return null;
    }

    /**
     * Get the ComponentManager handle.
     * @return The ComponentManager instance.
     */
    public com.sun.jbi.ComponentManager getComponentManager()
    {
        return null;
    }

    /**
     * Get the ComponentQuery handle.
     * @return The ComponentQuery instance.
     */
    public com.sun.jbi.ComponentQuery getComponentQuery()
    {
        return null;
    }


    /**
     * Get the ComponentQuery for a specified target
     * @return The ComponentQuery instance.
     * @param targetName - either "domain" or a valid server / cluster name
     */
    public com.sun.jbi.ComponentQuery getComponentQuery(String targetName)
    {
        return null;
    }

    /**
     * Get a reference to the persisted JBI registry.
     * @return Registry instance
     */
    public com.sun.jbi.registry.Registry getRegistry()
    {
        return null;
    }

    /**
     * Indicates whether or not the JBI framework has been fully started.  This
     * method provides clients with a way of determining if the JBI framework
     * started up in passive mode as a result of on-demand initialization.  
     * The 'start' parameter instructs the framework to
     * start completely if it has not already done so.  If the framework has
     * already been started, the request to start again is ignored.
     * @param start requests that the framework start completely before
     *  returning.
     * @return true if the framework is completely started, false otherwise.
     */
    public boolean isFrameworkReady(boolean start)
    {
        return false;
    }

    /**
     * Get the ConnectionManager handle.
     * @return The ConnectionManager instance.
     */
    public com.sun.jbi.messaging.ConnectionManager getConnectionManager()
    {
        return null;
    }

    /**
     * Gets the initial properties specified in the domain.xml file.
     * @return The initial properties from the AppServer.
     */
    public java.util.Properties getInitialProperties()
    {
        return null;
    }

    /**
     * Get the JBI install root directory path.
     * @return The JBI install root directory path.
     */
    public String getJbiInstallRoot()
    {
        return null;
    }

    /**
     * Get the JBI instance root directory path.
     * @return The JBI instance root directory path.
     */
    public String getJbiInstanceRoot()
    {
        return null;
    }

    /**
     * Get a handle to the class implementing management for the named
     * JBI system service.
     * @param aServiceName - the name of the JBI system service.
     * @return The instance implementing management for the service.
     */
    public Object getManagementClass(String aServiceName)
    {
        return null;
    }

    /**
     * Get the management message factory which enables JBI components
     * to construct status and exception messages.
     * @return An instance of ManagementMessageFactory.
     */
    public com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory()
    {
        return null;
    }

    /**
     * Get a handle to the MBeanHelper implementation.
     * @return The MBeanHelper instance.
     */
    public com.sun.jbi.management.MBeanHelper getMBeanHelper()
    {
        return null;
    }

    /**
     * Get a handle to the MBeanNames service for use in creating MBean
     * names.
     * @return Handle to the MBeanNames service.
     */
    public com.sun.jbi.management.MBeanNames getMBeanNames()
    {
        return null;
    }

    /**
     * Get the JMX MBean server used to register all MBeans in the JBI
     * framework.
     * @return The MBean server handle.
     */
    public javax.management.MBeanServer getMBeanServer()
    {
        return null;
    }

    /**
     * Get the JNDI naming context for this implementation. This context
     * is a standard JNDI InitialContext but its content will vary based
     *     on the environment in which the JBI implementation is running.
     * @return The JNDI naming context.
     */
    public javax.naming.InitialContext getNamingContext()
    {
        return null;
    }

    /**
     * Get the Event Notifer MBean instance.
     * @return The EventNotifierMBean instance.
     */
    public com.sun.jbi.framework.EventNotifierCommon getNotifier()
    {
        return null;
    }

    /**
     * Get the ServiceUnitRegistration handle.
     * @return The ServiceUnitRegistration instance.
     */
    public com.sun.jbi.ServiceUnitRegistration getServiceUnitRegistration()
    {
        return null;
    }

    /**
     * Get a StringTranslator for a specific package name.
     * @param packageName - the name of the package for which a StringTranslator
     * is being requested.
     * @return The StringTranslator for the named package.
     */
    public com.sun.jbi.StringTranslator getStringTranslator(String packageName)
    {
        return null;
    }

    /**
     * Get a StringTranslator for a specific object.
     * @param object - the object for which a StringTranslator is being
     * requested, using the name of the package containing the object.
     * @return The StringTranslator for the object's package.
     */
    public com.sun.jbi.StringTranslator getStringTranslatorFor(Object object)
    {
        return null;
    }

    /**
     * Get the TransactionManager for this implementation. The instance
     * returned is an implementation of the standard JTS interface. If none
     * is available, returns null.
     * @return A TransactionManager instance.
     */
    public javax.transaction.TransactionManager getTransactionManager()
    {
        return null;
    }

    /**
     * Get a handle to the VersionInfo implementation.
     * @return The VersionInfo instance.
     */
    public com.sun.jbi.VersionInfo getVersionInfo()
    {
        return null;
    }

    /**
     * Get JBI Init time
     * @return JBI Init time.
     */
    public long getJbiInitTime()
    {
    	return System.currentTimeMillis();
    }

    /**
     * Gets the provider type of JBI.
     * @return The JBI provider.
     */
    public com.sun.jbi.JBIProvider getProvider()
    {
        return null;
    }

    /**
     * Get a copy of the WSDL factory. This needs to be done before
     * any reading, writing, or manipulation of WSDL documents can
     * be performed using the WSDL API.
     *
     * @return An instance of the WSDL factory.
     * @exception WsdlException If the factory cannot be instantiated.
     */
    public com.sun.jbi.wsdl2.WsdlFactory getWsdlFactory() 
        throws com.sun.jbi.wsdl2.WsdlException 
    {
        return null;
    }

    /**
     * This method is used to find out if start-on-deploy is enabled.
     * When this is enabled components are started automatically when 
     * there is deployment for them. 
     * This is controlled by the property com.sun.jbi.startOnDeploy.
     * By default start-on-deploy is enabled. 
     * It is disabled only if com.sun.jbi.startOnDeploy=false.
     */
    public boolean isStartOnDeployEnabled()
    {
        return false;
    }
    
    /**
     * This method is used to find out if start-onverify is enabled.
     * When this is enabled components are started automatically when 
     * an application has to be verified for them 
     * This is controlled by the property com.sun.jbi.startOnVerify.
     * By default start-on-verify is enabled. 
     * It is disabled only if com.sun.jbi.startOnVerify=false.
     */
    public boolean isStartOnVerifyEnabled()
    {
        return false;
    }    
    /**
     * Get a read-only reference to the persisted JBI Registry. A DOM registry document 
     * object is returned. 
     * @return the registry document
     */
    public org.w3c.dom.Document getReadOnlyRegistry()
    {
        // return null, if the DOM registry is needed then more stuff goes here
        return null;
    }
}
