/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EnvironmentAccess.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;


/**
 * This interface defines constants used by the JBI Runtime and Common Client
 *
 * @author Sun Microsystems
 */
public interface Constants 
{
    
    /** The Default Log Level value */
    static final String LOG_LEVEL_DEFAULT = "DEFAULT";
    
    /** Application Configuration Name Key */
    static final String CONFIG_NAME_KEY = "configurationName";
    
    /** File which is created when the Management Runtime is initialized */
    static final String ADMIN_RUNNING_FILE = ".jbi_admin_running";
    
    /** File which is created when the Management Runtime is stopped */
    static final String ADMIN_STOPPED_FILE = ".jbi_admin_stopped";

    /** JBI Instance tmp folder name */
    static final String TMP_FOLDER = "tmp";
}
