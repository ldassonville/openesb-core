/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TextMessageAdaptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import com.sun.jbi.StringTranslator;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import java.util.logging.Logger;

import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.messaging.MessageExchange.Role;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Text message adaptor.
 *
 * @author Sun Microsystems Inc.
 */
public class TextMessageAdaptor
    extends MessageAdaptorImpl
{
    /**
     * Creates a new TextMessageAdaptor object.
     */
    public TextMessageAdaptor()
    {
        mLogger.info(mStringTranslator.getString(JMS_TEXT_ADAPTOR_INVOKED));
    }

    /**
     * Gets name.
     *
     * @return string.
     */
    public String getName()
    {
        return MessageProperties.TEXT_MESSAGE;
    }

    /**
     * Convert JMS to NMR msg.
     *
     * @param msg JMS msg.
     * @param exch NMR exchange.
     */
    public void convertJMStoNMSMessage(
        Message msg,
        MessageExchange exch)
    {
        super.convertJMStoNMSMessage(msg, exch);

        javax.jms.TextMessage textmsg = null;

        try
        {
            Source stream = getSource((TextMessage) msg);
            String status = getJMSStatus(msg);
            NormalizedMessage nm = null;
            Fault flt = null;
            Exception exp = null;

            if (status.trim().equals(MessageProperties.SUCCESS))
            {
                nm = exch.createMessage();
                nm.setContent(stream);
            }
            else if (status.trim().equals(MessageProperties.FAULT))
            {
                flt = exch.createFault();
                flt.setContent(stream);
            }
            else if (status.trim().equals(MessageProperties.ERROR))
            {
                String expstring = getSourceAsString(stream);

                if (expstring == null)
                {
                    expstring = new String(mStringTranslator.getString
                            (JMS_NO_ERROR_RETURNED));
                }

                exp = new Exception(expstring);
            }

            if (exch.getRole().equals(MessageExchange.Role.PROVIDER))
            {
                MessageExchangeHelper.updateOutMessage(exch, nm, exp, flt);
            }
            else
            {
                MessageExchangeHelper.updateInMessage(exch, nm);
            }
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            setError(mStringTranslator.getString
                            (JMS_ERROR_JMS_NMR));
            setError(e.getMessage());
            exch = null;
        }
    }

    /**
     * Convert NMR to JMS msg.
     *
     * @param exch NMR msg.
     * @param msg JMS msg.
     */
    public void convertNMStoJMSMessage(
        MessageExchange exch,
        Message msg)
    {
        super.convertNMStoJMSMessage(exch, msg);

        javax.jms.TextMessage txtmsg = (javax.jms.TextMessage) msg;


        NormalizedMessage nm = null;

        if (exch.getRole().equals(MessageExchange.Role.PROVIDER))
        {
            nm = MessageExchangeHelper.getInMessage(exch);
        }
        else
        {
            nm = MessageExchangeHelper.getOutMessage(exch);
        }

        Fault flt = MessageExchangeHelper.getFault(exch);
        Exception exp = MessageExchangeHelper.getError(exch);

        mLogger.fine("Pattern " + exch.getPattern());
        mLogger.fine("Service " + exch.getEndpoint().getServiceName());

        String txt = null;
        String status = MessageProperties.UNKNOWN;

        if (nm != null)
        {
            /* could be fault or error
             */
            txt = getOutputString(nm);
            status = MessageProperties.SUCCESS;
        }
        else if (flt != null)
        {
            txt = getFaultString(flt);
            status = MessageProperties.FAULT;
        }
        else if (exp != null)
        {
            txt = getErrorString(exp);
            status = MessageProperties.ERROR;
        }
        else
        {
            txt = new String(mStringTranslator.getString
                            (JMS_NO_ERROR_RETURNED));
        }

        setStatus(txtmsg, status);

        try
        {
            txtmsg.setText(txt);
        }
        catch (JMSException je)
        {
            je.printStackTrace();
            setException(je);
        }
    }

    /**
     * Update JMS error msg.
     *
     * @param msg JMS msg.
     * @param obj Object.
     */
    public void updateJMSErrorMessage(
        Message msg,
        Object obj)
    {
        if (obj instanceof java.lang.String)
        {
            String s = (String) obj;
        }
        else
        {
            ;
        }
    }

    /**
     * Get error message.
     *
     * @param e exception.
     *
     * @return string.
     */
    private String getErrorString(Exception e)
    {
        StringBuffer sb = new StringBuffer();

        StackTraceElement [] stckTrElem = e.getStackTrace();

        if (stckTrElem != null)
        {
            for (int i = 0; i < stckTrElem.length; i++)
            {
                String stckTrace = stckTrElem[i].toString();
                sb.append(stckTrace);
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * Get fault string.
     *
     * @param flt fault string.
     *
     * @return string.
     */
    private String getFaultString(Fault flt)
    {
        Source src = flt.getContent();

        return getSourceAsString(src);
    }

    /**
     * Gets the output string.
     *
     * @param nm NM message.
     *
     * @return string.
     */
    private String getOutputString(NormalizedMessage nm)
    {
        Source src = nm.getContent();

        return getSourceAsString(src);
    }

    /**
     * Returns the source.
     *
     * @param textmsg JMS text message.
     *
     * @return source.
     */
    private Source getSource(TextMessage textmsg)
    {
        StreamSource stream = null;

        try
        {
            mLogger.fine("JMS Message *" + textmsg.getText() + "*");

            String s = textmsg.getText();
            StringReader reader = new StringReader(s.trim());
            stream = new StreamSource(reader);
        }
        catch (Exception e)
        {
            ;
        }

        return stream;
    }

    /**
     * Get source as string.
     *
     * @param doc soource doc.
     *
     * @return string.
     */
    private String getSourceAsString(Source doc)
    {
        String s = null;
        StringWriter out = null;

        try
        {
            out = new StringWriter();

            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer trans = tFactory.newTransformer();
            StreamResult result = new StreamResult(out);
            trans.transform(doc, result);
            s = result.getWriter().toString();
            out.close();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            s = new String(mStringTranslator.getString(JMS_OUTPUT_MSG_ERROR,    
                            t.getMessage()));
            setError(t.getMessage());
        }

        return s;
    }
}
