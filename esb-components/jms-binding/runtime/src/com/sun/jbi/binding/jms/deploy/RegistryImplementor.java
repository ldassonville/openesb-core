/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistryImplementor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.jbi.JBIException;

/**
 * This interface isolates the registry implementation from its API so that
 * both of them can vary independently.
 *
 * @author Sun Microsystems Inc.
  */
public interface RegistryImplementor
{
    /**
     * Returns all keys.
     *
     * @return iterator to keys.
     */
    Iterator getAllKeys();

    /**
     * Returns all values.
     *
     * @return collection of values.
     */
    Collection getAllValues();

    /**
     * Returns the value of object.
     * 
     * @param registryKey reg key.
     *
     * @return value.
     */
    Object getValue(String registryKey);

    /**
     * Clears the registry.
     */
    void clearRegistry();

    /**
     * Checks if registry contains the key.
     *
     * @param registryKey reg key.
     * @param direction inbound/outbound.
     *
     * @return true if key is present.
     */
    boolean containsKey(String registryKey);

    /**
     *  Initialises registry.
     *
     * @param props 
     *
     * @throws JBIException jbi exception.
     */
    void init(Properties props)
        throws JBIException;

    /**
     * Adds an entry.
     * 
     * @param key key. 
     * @param value object to be registered.
     *
     * @return key.
     */
    String registerKey(
        String key,
        Object value);

    /**
     * Removes the entry.
     *
     * @param registryKey key.
     */
    void removeKey(String registryKey);

    /**
     * Serializes the registry values.
     *
     * @throws JBIException serialization exception.
     */
    void serialize()
        throws JBIException;
}
