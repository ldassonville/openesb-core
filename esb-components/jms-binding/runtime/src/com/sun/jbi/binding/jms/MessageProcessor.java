/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.deploy.EndpointRegistry;

import com.sun.jbi.binding.jms.handler.InboundMessageHandler;
import com.sun.jbi.binding.jms.handler.MessageHandler;
import com.sun.jbi.binding.jms.handler.OutboundMessageHandler;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.net.URI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;

import javax.jbi.messaging.MessageExchange;

import javax.jbi.messaging.MessageExchange.Role;
import javax.xml.namespace.QName;
/**
 * Processes all the messages from the NMR.
 *
 * @author Sun Microsystems Inc.
 */
public class MessageProcessor
    implements JMSBindingResources
{
    /**
     * String representing property name in exchange.
     */
    private static final String FILENAME_PROPERTY = "FILENAME";

    /**
     * String representing property name in exchange.
     */
    private static final String FILEEXTENSION_PROPERTY = "FILEEXTENSION";

    /**
     * Channel for this binding.
     */
    private DeliveryChannel mChannel;

    /**
     * Endpoint Bean for the exhange being processed.
     */
    private EndpointBean mEndpointBean;

    /**
     * Registry for storing active deployments.
     */
    private EndpointRegistry mRegistry;

    /**
     * Logger object.
     */
    private Logger mLogger;

    /**
     * Message exchange object.
     */
    private MessageExchange mExchange;
    /**
     *    
     */
    private String mError = "";
    /**
     * Helper for i18n.
     */
    private StringTranslator mStringTranslator;
    /**
     * Creates a new MessageProcessor object.
     *
     * @param chnl binding channel.
     */
    public MessageProcessor(DeliveryChannel chnl)
    {
        mChannel = chnl;
        mRegistry = JMSBindingContext.getInstance().getRegistry();
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Gets the date.
     *
     * @return date string.
     */
    public String getDate()
    {
        Calendar cal = new GregorianCalendar();
        Date dt = cal.getTime();
        DateFormat format = new SimpleDateFormat("ddHHmmssSS");
        String sdate = format.format(dt);

        return sdate;
    }

    /**
     * Sets the exchange.
     *
     * @param exch message exchange.
     */
    public void setExchange(MessageExchange exch)
    {
        mExchange = exch;
    }

    /**
     * Command pattern for executing in a thread pool.
     *
     * @return message handler.
     */
    public MessageHandler process()
    {
        mLogger.info("Processing message " + mExchange.getExchangeId());
        if (mExchange == null)
        {
            mLogger.severe("Set message exchange  first");

            return null;
        }

        if (!valid())
        {
            sendError();

            return null;
        }
        mLogger.info("Message is Valid ");
        MessageHandler handler = null;
        URI pattern = mExchange.getPattern();
        String pat = pattern.toString().trim();

        if (pat.trim().equals(ConfigConstants.IN_OUT.trim())
                || pat.trim().equals(ConfigConstants.IN_ONLY.trim())
                || pat.trim().equals(ConfigConstants.ROBUST_IN_ONLY.trim()))
        {
            mLogger.info(mStringTranslator.getString(JMS_RECEIVED_INBOUND, pat,
                    mExchange.getExchangeId()));

            if (mExchange.getRole().equals(MessageExchange.Role.PROVIDER))
            {
                handler = processOutbound();
            }
            else
            {
                handler = processInbound();
            }
        }
        else if (pat.trim().equals(ConfigConstants.IN_OPTIONAL_OUT.trim()))
        {
            mLogger.info(mStringTranslator.getString(
                    JMS_RECEIVED_INOPTIONALOUT, mExchange.getExchangeId()));
        }
        else
        {
            mLogger.info(mStringTranslator.getString(
                    JMS_RECEIVED_UNSUPPORTED_MEP, mExchange.getExchangeId()));
        }

        return handler;
    }

    /**
     * Sets the error.
     *
     * @param err error string.
     */
    private void setError(String err)
    {
        mError = err;
    }

    /**
     * Processes inbound.
     *
     * @return message handler.
     */
    private MessageHandler processInbound()
    {
        MessageHandler hndl = new InboundMessageHandler();

        if (hndl != null)
        {
            hndl.setNMSMessage(mExchange);
            hndl.setBean(mEndpointBean);
        }

        return hndl;
    }

    /**
     * Processes outbound.
     *
     * @return message handler.
     */
    private MessageHandler processOutbound()
    {
        MessageHandler hndl = new OutboundMessageHandler();

        if (hndl != null)
        {
            hndl.setNMSMessage(mExchange);
            hndl.setBean(mEndpointBean);
        }

        return hndl;
    }

    /**
     * Sends the error.
     */
    private void sendError()
    {
        /**
         * Sends an error if servicename or operationb is not supported. or if
         * the binding is not in a position to accept new requests.
         */
        Exception exp = new Exception(mError);

        try
        {
            mExchange.setError(exp);
            mChannel.send(mExchange);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Validity.
     *
     * @return true or false.
     */
    private boolean valid()
    {
        Exception exc;
        String sname = mExchange.getEndpoint().getServiceName().toString();
        String epname = mExchange.getEndpoint().getEndpointName();
        QName intername = mExchange.getInterfaceName();
        mLogger.severe("RECVD MSG : " + sname + " " + epname);
        if ((sname == null) || (epname == null))
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_START_DEPLOYMENT_FAILED_BEANNULL,
                    mExchange.getExchangeId()));
            setError(mStringTranslator.getString(
                    JMS_START_DEPLOYMENT_FAILED_BEANNULL,
                    mExchange.getExchangeId()));

            return false;
        }
        try
        {
            if (mExchange.getRole().equals(MessageExchange.Role.PROVIDER))
            {
                mEndpointBean = mRegistry.getEndpoint(sname + epname);
            }
            else
            {            
                mEndpointBean = mRegistry.getEndpoint(sname);
            
                /**
                 * There is a possiblity that the message was routed initially
                 * with the interface name and not the service name
                 * So we have to try an endpoint with interface name now
                 */

                if (mEndpointBean == null)
                {
                    if (intername != null)
                    {
                        mEndpointBean  = 
                                mRegistry.findEndpointByInterface(intername.toString().
                                            trim());   
                    }
                }
            }
        }
        catch (Exception e)
        {
            mEndpointBean = null;
        }

        if (mEndpointBean == null)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_START_DEPLOYMENT_FAILED_BEANNULL,
                    mExchange.getExchangeId()));
            setError(mStringTranslator.getString(
                    JMS_START_DEPLOYMENT_FAILED_BEANNULL,
                    mExchange.getExchangeId()));

            return false;
        }
        
        if (!mEndpointBean.getStatus().equals(EndpointStatus.STARTED))
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_ENDPOINT_NOT_STARTED,
                    mExchange.getExchangeId()));
            setError(mStringTranslator.getString(
                    JMS_ENDPOINT_NOT_STARTED,
                    mExchange.getExchangeId()));

            return false;
        }

        /**
         *Look at only the operation name, namespace is not necessary
         * WSDL 2.0 spec says that any two operations with same name
         * should be same
         */
        String operation = mExchange.getOperation().getLocalPart();

        List l = mEndpointBean.getAllOperations();
        Iterator it = l.iterator();
        boolean found = false;

        while (it.hasNext())
        {
            OperationBean op = (OperationBean) it.next();

            if (op.getName().trim().equals(operation.trim()))
            {
                found = true;

                break;
            }
        }

        if (!found)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_UNSUPPORED_OPERATION, operation,
                    mExchange.getExchangeId()));
            setError(mStringTranslator.getString(JMS_UNSUPPORED_OPERATION,
                    operation, mExchange.getExchangeId()));

            return false;
        }

        return true;
    }

}
