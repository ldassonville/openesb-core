/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.connection;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * Helper for ConnectionManagers
 * @author Sun Microsystems, Inc
 */
public interface ConnectionHelper
{
    /**
     * Prepare a thread for this connection.
     *
     */
    public void prepare()
        throws ConnectionException;
    
    /**
     * Return a JMS ConnectionFactory.
     * @return ConnectionFactory
     */
    public ConnectionFactory getConnectionFactory()
        throws ConnectionException;

    /**
     * Return a JMS Queue.
     * @return Queue.
     */
    public Queue getQueue(String name)
        throws ConnectionException;
	
    /**
     * Return a JMS Topic.
     * @return Topic.
     */
    public Topic getTopic(String name)
        throws ConnectionException;
}
