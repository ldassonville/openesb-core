<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page frame="true">
            <webuijsf:html>
                <webuijsf:head title="#{ChartApplicationBeansContainer.title}" id="head1"/>
                <webuijsf:frameSet id="topFrame"  rows="85,85%,*" >
                    <webuijsf:frame name="bannerFrame" toolTip="topFrame" url="faces/layout/BannerPage.jsp" frameBorder="false" noResize="true" scrolling="false" />
                    <webuijsf:frameSet id="bottomFrame" cols="20%,*" >
                        <webuijsf:frame  name="treeFrame"   url="faces/layout/Tree.jsp" frameBorder="true" noResize="false"/>
                        <webuijsf:frame name="workspaceFrame" url="http://www.sun.com" frameBorder="true" noResize="false"/>
                    </webuijsf:frameSet>
                    <webuijsf:frame id="_info"  name="infoFrame" url="" frameBorder="true" scrolling="true"/>
                </webuijsf:frameSet>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
