/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SubComponentCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui;
import org.openesb.tools.extpropertysheet.IExtProperty;
import org.openesb.tools.extpropertysheet.IExtPropertyGroup;
import org.openesb.tools.extpropertysheet.IExtPropertyGroupsBean;
import org.openesb.tools.extpropertysheet.IExtPropertyOption;
import org.openesb.tools.extpropertysheet.IExtPropertyOptions;
import com.sun.web.ui.component.Button;
import com.sun.web.ui.component.DropDown;
import com.sun.web.ui.component.HiddenField;
import com.sun.web.ui.component.Label;
import com.sun.web.ui.component.PropertySheet;
import com.sun.web.ui.component.PropertySheetSection;
import com.sun.web.ui.component.RadioButtonGroup;
import com.sun.web.ui.component.Tab;
import com.sun.web.ui.component.TabSet;
import com.sun.web.ui.component.TextField;
import com.sun.web.ui.model.Option;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;

/**
 *
 * @author rdwivedi
 */
public class SubComponentCreator {
    
    private static Logger mLogger = Logger.getLogger(SubComponentCreator.class.getName());
    /** Creates a new instance of SubComponentCreator */
    
    private ResourceBundle mRBundle = null;
    public SubComponentCreator() {
    }
    public SubComponentCreator(String resourceBundleURL) {
        mRBundle = PropertiesResourceBundleHelper.getResourceBundle(resourceBundleURL);
         
    }
    
    private Tab createTab(IExtPropertyGroup grp) {
        mLogger.finer("chart grp " + grp.getGroupName());
        Collection col = grp.getAllProperties();
        String tabLabel = grp.getGroupName();
        Tab tab = new Tab();
        tab.setId("tab_" + tabLabel);
        String tabDisp = tabLabel;
        if(mRBundle != null) {
            tabDisp = (String)mRBundle.getObject(tabLabel);
        }
           /* try{
                tabDisp = PropertiesResourceBundleHelper.getResourceBundle().getString(props.getGroupName());
            } catch(Exception e) {
                mLogger.info("Resource not found for key =" +tabLabel + " :: EXception is " + e.getMessage() );
            }
            */
        tab.setText(tabDisp);
        PropertySheet ps = createPropertySheet(grp);
        if(ps != null) {
            
            
            tab.getChildren().add(ps);
            Button b = new Button();
            b.setId("_b1");
            b.setText("Update");
            
            //MethodBinding binding = getFacesContext().getCurrentInstance().getApplication().createMethodBinding("#{ChartPropertySheetBean.update}",null);
            
            //b.setAction(binding);
            // b.setOnMouseUp("parent.frames['chartPreviewFrame'].document.location=\"\\ChartPanel.jsp\"");
            tab.getChildren().add(b);
            
        }
        return tab;
        
        
    }
    
    public TabSet createTabSet(IExtPropertyGroupsBean grps , FacesContext context ) {
        TabSet mTS = new TabSet();
        mTS.setId("_propTS");
        //mLogger.info("prop group is " + grps);
        if(grps != null) {
            
            Collection col = grps.getAllGroups();
             //mLogger.info("collection group is " + col);
            if(col != null) {
                Iterator iter = col.iterator();
                while(iter.hasNext()) {
                    
                    IExtPropertyGroup group = (IExtPropertyGroup ) iter.next();
                    //mLogger.info(" group is " + group);
                    Tab tab = createTab(group);
                    mTS.getChildren().add(tab);
                }
            }
        }
        return mTS;
        
    }
    private PropertySheet createPropertySheet(IExtPropertyGroup group) {
        String label = group.getGroupName();
        Collection col = group.getAllProperties();
        
        Iterator iter =  col.iterator();
        IExtProperty prop = null;
        PropertySheet ps = new PropertySheet();
        ps.setId("_ps"+label);
        
        PropertySheetSection sec = new PropertySheetSection();
        sec.setId("_pss"+label);
        while(iter.hasNext()) {
            prop = (IExtProperty ) iter.next();
            String propName = prop.getName();
            String propLabel = propName;
            if(mRBundle != null) {
            propLabel = (String)mRBundle.getObject(propName);
            }
            Object propValue = prop.getValue();
            
            IExtPropertyOptions options = prop.getOptions();
            short propType = prop.getType();
            
            String val = null;
            val = propValue.toString();
            /*if(actValue != null) {
                val   = prop.getValueConvertor().getStringValue(actValue);
            }
             */
            addPropertyIntoPropertySheet(sec,propName,propLabel,val,propType,options);
        }
        ps.getChildren().add(sec);
        return ps;
    }
    
    private void addPropertyIntoPropertySheet(PropertySheetSection sec, String pName, String pLabel, String pValue,short type,IExtPropertyOptions options ) {
        com.sun.web.ui.component.Property p = new com.sun.web.ui.component.Property();
        
        p.setId("_p"+pName);
        p.setLabel(pLabel);
        //mLogger.info("Adding a property " + pName + pLabel);
        //TextField field = new TextField();
        //field.setId(pName);
        // field.setLabel(pLabel);
        //field.setValue(pValue);
        UIComponent field = createComponent(type,pName,pValue,options);
        if(type != IExtProperty.HIDDEN_TEXT){
            p.getChildren().add(field);
            sec.getChildren().add(p);
        }
    }
    
    private UIComponent createComponent(short type,String pName, String pValue, IExtPropertyOptions options) {
        UIComponent comp = null;
        //mLogger.info("The component type is  "  + type);
        switch (type) {
        case IExtProperty.INPUT_TEXT:
        case IExtProperty.INPUT_INT_TYPE:
        case IExtProperty.INPUT_FLOAT_TYPE:    
            comp = new TextField();
            ((TextField)comp).setValue(pValue);
            break;
        case IExtProperty.OUTPUT_TEXT:
            comp = new Label();
            ((Label)comp).setValue(pValue);
            break;
        case IExtProperty.HIDDEN_TEXT:
            comp = new HiddenField();
            ((HiddenField)comp).setValue(pValue);
            break;
        case IExtProperty.SELECTONE_MENU_COMPONENT:
            comp = new DropDown();
            ((DropDown)comp).setValue(pValue);
            ((DropDown)comp).setMultiple(false);
            ArrayList pl = new ArrayList();
            Collection col = options.getAllOptions();
            //Option[] listOptions = new Option[list.size()];
            Iterator iter = col.iterator();
            
            while(iter.hasNext()) {
                IExtPropertyOption pOption = (IExtPropertyOption)iter.next();
                Option option = new Option();
                option.setLabel(pOption.getOptionLabel());
                option.setValue(pOption.getOptionValue());
                pl.add(option);
            }
            ((DropDown)comp).setItems(pl);
            break;
        case IExtProperty.COLOR_SELECTOR:
            //comp = new RbCbSelector();
            comp = new Label();
            ((Label)comp).setValue(pValue);
            break;
        case IExtProperty.BOOLEAN_RADIO_BUTTON:
            comp = new RadioButtonGroup();
            
            Option b = new Option();
            b.setLabel("Yes");
            b.setValue("true");
            
            Option d = new Option();
            d.setLabel("No");
            d.setValue("false");
            
            ArrayList l = new ArrayList();
            l.add(b) ;
            l.add(d) ;
            ((RadioButtonGroup)comp).setValue(pValue);
            ((RadioButtonGroup)comp).setColumns(2);
            ((RadioButtonGroup)comp).setItems(l);
            break;
        default:
            comp = new Label();
            ((Label)comp).setValue(pValue);
        }
        comp.setId(pName);
        
        return comp;
    }
    
}
