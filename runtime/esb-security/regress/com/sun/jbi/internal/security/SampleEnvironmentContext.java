/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SampleEnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SampleEnvironmentContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 19, 2004, 12:48 PM
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.JBIProvider;
/**
 *
 * @author Sun Microsystems, Inc.
 */
public class SampleEnvironmentContext
    implements com.sun.jbi.EnvironmentContext
{
    /** App Server Install Root. */
    private String mAsInstRoot;
    private String mJbiInstallRoot;
    
    /** Creates a new instance of SampleEnvironmentContext */
    public SampleEnvironmentContext()
    {
    }
    
    public JBIProvider getProvider()
    {
        return JBIProvider.SUNASPE;
    }

    public long getJbiInitTime()
    {
    	return System.currentTimeMillis();
    }
    
    public String getAppServerInstanceRoot()
    {
        return mAsInstRoot;
    }
    
    public void setAppServerInstanceRoot(String root)
    {
        mAsInstRoot = root;
    }
    
    public com.sun.jbi.ComponentManager getComponentManager()
    {
        return null;
    }
    
    public String getComponentId ()
    {
        return "";
    }
    
    public String getComponentName ()
    {
        return "notNeeded";
    }
    
    public com.sun.jbi.ComponentQuery getComponentQuery ()
    {
        return null;
    }
    
    public com.sun.jbi.ComponentQuery getComponentQuery (String targetName)
    {
        return null;
    }
    
    public String getComponentRoot ()
    {
        return null;
    }
    
    public com.sun.jbi.messaging.ConnectionManager getConnectionManager()
    {
        return null;
    }

    public java.util.Properties getInitialProperties ()
    {
        return null;
    }
    
    public String getJbiInstallRoot ()
    {
        return mJbiInstallRoot;
    }
    
    public void setJbiInstallRoot(String jbiRoot)
    {
        mJbiInstallRoot = jbiRoot;
    }
    
    public com.sun.jbi.management.MBeanHelper getMBeanHelper ()
    {
        return null;
    }
    
    public com.sun.jbi.management.MBeanNames getMBeanNames ()
    {
        return null;
    }
    
    public javax.management.MBeanServer getMBeanServer ()
    {
        return null;
    }
    
    public Object getManagementClass (String aServiceName)
    {
        return null;
    }
    
    public com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory ()
    {
        return null;
    }
    
    public javax.naming.InitialContext getNamingContext ()
    {
        return null;
    }
    
    public com.sun.jbi.ServiceUnitRegistration getServiceUnitRegistration()
    {
        return null;
    }
    
    public com.sun.jbi.StringTranslator getStringTranslator (String packageName)
    {
        return Util.getStringTranslator(packageName);
    }
    
    public com.sun.jbi.StringTranslator getStringTranslatorFor (Object object)
    {
        return Util.getStringTranslator(object.getClass().getPackage().getName());
    }
    
    /**
     * Get the VersionInfo for this runtime.
     * @return The VersionInfo instance.
     */
    public com.sun.jbi.VersionInfo getVersionInfo()
    {
        return (com.sun.jbi.VersionInfo) this;
    }

    /**
     * Get a TransactionManager from the AppServer.
     * @return A TransactionManager instance.
     */
    public javax.transaction.TransactionManager getTransactionManager()
    {
        return null;
    }

    /**
     * Get a Wsdl Factory from the run time.
     * @return A Wsdl factory instance.
     */
    public com.sun.jbi.wsdl2.WsdlFactory getWsdlFactory ()      
    throws com.sun.jbi.wsdl2.WsdlException
    {
        return null;
    }
    
}
