/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityConfigImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityConfigImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 26, 2004, 6:40 PM
 */

package com.sun.jbi.internal.security.https.jregress;

import com.sun.jbi.internal.security.config.SecurityConfiguration;
import com.sun.jbi.internal.security.ContextImpl;
import com.sun.jbi.internal.security.Constants;

import java.util.HashMap;
import java.util.Properties;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityConfigImpl      
    implements SecurityConfiguration
{
    
    /** The Default Security Configuration */
    private String mDefaultConfigName;
    
    /** The Security Context Map */
    private Properties mSecurityContext;
    
    /** The Default User Domain */
    private String mDefaultUserDomain;
    
    /** The User Domain Map */
    private HashMap mUserDomains;
    
    /** The Default KeyStore Manager */
    private String mDefaultKeyStoreManager;
    
    /** The Key Store Manager Map */
    private HashMap mKeyStoreMgrCtxs;
    
    /** My Instance.*/
    private static SecurityConfigImpl mInstance = null;
    
    /** Hide Constructor. */
    private SecurityConfigImpl()
    {
        // -- This constructor is never called.
    };
    
    /**
     * Constructor.
     */
    private SecurityConfigImpl(String userFileName, String userFileName2, 
        String keystorebase, com.sun.jbi.StringTranslator translator)
    {
        mKeyStoreMgrCtxs = new HashMap();
        mUserDomains = new HashMap();
        
        mDefaultKeyStoreManager = "server";
        mDefaultConfigName = "default";
        
        initUserDomains(userFileName, userFileName2);
        initSecurityContexts(keystorebase);
        initKeyStoreManagers(keystorebase);
    }
    
    /**
     * A single shared user domain
     */
    private void initUserDomains(String userFile, String userFile2)
    {
        Properties[] udProps = new Properties[2];
        
        udProps[0] = new Properties();
        
        udProps[0].setProperty(Constants.DOMAIN,
            "JAAS");
        udProps[0].setProperty(Constants.PARAM_FILE_NAME, userFile);
        mUserDomains.put("file", udProps[0]);
        
         udProps[1] = new Properties();
        udProps[1].setProperty(Constants.DOMAIN,
            "JAAS");
        udProps[1].setProperty(Constants.PARAM_FILE_NAME, userFile2);
        mUserDomains.put("file2", udProps[1]);
        
    }
    
    /**
     * A Single Security Context will be created.
     */
    private void initSecurityContexts(String keystorebase)
    {
        
        mSecurityContext = new Properties();
        
        
    }
    /**
     * Two KeyStoreManagers are created "server" and "client" for testing purposes.
     */
    private void initKeyStoreManagers(String ksbase)
    {
        
        SecurityContextInfo info = new SecurityContextInfo(ksbase);
        
        Properties serverKSMgr = new Properties(); 
        serverKSMgr.setProperty(Constants.MANAGER, 
            "JavaStandard");
        
        serverKSMgr.setProperty(Constants.PARAM_KEYSTORE_LOCATION, 
            info.getServerKeyStoreURL());
        serverKSMgr.setProperty(Constants.PARAM_KEYSTORE_TYPE, 
            info.getServerKeyStoreType());
        serverKSMgr.setProperty(Constants.PARAM_KEYSTORE_PASS, 
            info.getClientKeyStorePassword());
        
        serverKSMgr.setProperty(Constants.PARAM_TRUSTSTORE_LOCATION, 
            info.getServerTrustStoreURL());
        serverKSMgr.setProperty(Constants.PARAM_TRUSTSTORE_TYPE, 
            info.getServerTrustStoreType());
        serverKSMgr.setProperty(Constants.PARAM_TRUSTSTORE_PASS, 
            info.getServerTrustStorePassword());
        mKeyStoreMgrCtxs.put("server", serverKSMgr);
        
        Properties clientKSMgr = new Properties();
        clientKSMgr.setProperty(Constants.MANAGER, 
            "JavaStandard");
        clientKSMgr.setProperty(Constants.PARAM_KEYSTORE_LOCATION, 
            info.getClientKeyStoreURL());
        clientKSMgr.setProperty(Constants.PARAM_KEYSTORE_TYPE, 
            info.getClientKeyStoreType());
        clientKSMgr.setProperty(Constants.PARAM_KEYSTORE_PASS, 
            info.getClientKeyStorePassword());
        
        clientKSMgr.setProperty(Constants.PARAM_TRUSTSTORE_LOCATION, 
            info.getClientTrustStoreURL());
        clientKSMgr.setProperty(Constants.PARAM_TRUSTSTORE_TYPE, 
            info.getClientTrustStoreType());
        clientKSMgr.setProperty(Constants.PARAM_TRUSTSTORE_PASS, 
            info.getClientTrustStorePassword());
        mKeyStoreMgrCtxs.put("client", clientKSMgr);   
    }
    
    /**
     *  Get a Map Security Contexts, keyed by their name
     *  [ Key = Name (String) : Value = SecurityContext (Context) ]
     *
     * @return the HashMap which has the mappings
     */
     public Properties getTransportSecurityContext()
     {
         return mSecurityContext;
     }
     
    /**
     * Get the Name of the default Configuration name
     *
     * @param name is the Name of the default configuration
     */
    public void setDefaultConfigName(String name)
    {
        mDefaultConfigName = name;
    }
     
     
     /**
      * Get the Name of the default User Domain
      *
      * @return the name of the Default User Domain
      */
     public String getDefaultUserDomainName()
     {
         return mDefaultUserDomain;
     }
     
     /**
      * Get a Map of User Domain Contexts by their name
      * [ Key = Name (string) : Value = UserDomain Contexts (Properties) ]
      *
      * @return a map of the User Domain Contexts
      */
     public HashMap getUserDomainContexts()
     {
         return mUserDomains;
     }
     
     /**
      * Get the Name of the default KeyStore Service
      *
      * @return the name of the default KeyStore.
      */
     public String getDefaultKeyStoreManagerName()
     {
         return mDefaultKeyStoreManager;
     }
     
     /**
      * Get a Map of KeyStore Services keyed by their name
      * [ Key = Name (string) : Value = KeyStoreContexts (Properties) ]
      *
      * @return a map of the Key Store Contexts
      */
     public HashMap getKeyStoreContexts()
     {
         return mKeyStoreMgrCtxs;
     }
     
     /**
      * Set the Name of the default User Domain
      *
      * @param name is the name of the Default User Domain
      */
     public void setDefaultUserDomainName(String name)
     {
         mDefaultUserDomain = name ;
     }
     
     /**
      * Set the Map of User Domain Contexts by their name.
      * [ Key = Name (string) : Value = UserDomain Contexts (Properties) ]
      *
      * @param map is the UserDomain map
      */
     public void  setUserDomainContexts(HashMap map)
     {
         mUserDomains = map;
     }
     
     /**
      * Set the Map of the default KeyStore Service.
      *
      * @param name is the name of the default KeyStore.
      */
     public void setDefaultKeyStoreManagerName(String name)
     {
         mDefaultKeyStoreManager = name;
     }
     
     /**
      * Set the Map of KeyStore Services keyed by their name
      * [ Key = Name (string) : Value = KeyStoreContexts (Properties) ]
      *
      * @param map the KS Manager Context
      */
     public void setKeyStoreContexts(HashMap map)
     {
         mKeyStoreMgrCtxs = map;
     }
     
     /**
      * Create a test installation security configuration
      */
     public static SecurityConfiguration getTestSecurityConfiguration(
        String userFileName, String userFileName2, String keystorebase,
        com.sun.jbi.StringTranslator translator)
     {
        if ( mInstance == null )
        {
            mInstance = new SecurityConfigImpl(userFileName, userFileName2, 
                keystorebase, translator);
        }
        return mInstance;
     }
     
     /**
      *
      * Not used by the Tests.
      * @return null, method not used.
      */
     public org.w3c.dom.Document generateDocument()
     {
         return null;
     }
}
