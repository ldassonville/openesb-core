/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ContextImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.test.binding1.rt;

import com.sun.jbi.binding.security.Context; 
import java.util.HashMap;

/**
 * Implementation of the Context.
 *
 * @author Sun Microsystems, Inc.
 */
public class ContextImpl
    implements Context
{
    /**
     * The Context.
     */
    private HashMap mContext;
    
    /** 
     * Creates a new instance of ContextImpl.
     */
    public ContextImpl()
    {
        mContext = new HashMap();
    }
    
    /* ------------------------------------------------------------------------------- *\
     *                    Context Impl.                                                *
    \* ------------------------------------------------------------------------------- */ 
    
    /**
     * @param key is the key to look for in the context.
     * @return true if the Context contains a particular Key
     */
    public boolean containsKey(String key)
    {
        return mContext.containsKey(key);
    };
    
    /**
     * @param key whose associated value is to be returned.
     * @return the Object value associated with a particular Key
     */  
    public Object getValue(String key)
    {
        return mContext.get(key);
    }
    
    /**
     * @param key is the Key whose value is to be set.
     * @param value is the value for the Key.
     */   
    public void setValue(String key, Object value)
    {
        mContext.put(key, value);
    };
    
    /**
     * Enumerate all the Keys in the Context.
     *
     * @return the List of Keys (Strings) in the Context.
     */
    public java.util.Set enumerateKeys()
    {
        return mContext.keySet();
    };
    
    /**
     * Remove a entry.
     *
     * @param key is the Key identifying the entry to be deleted.
     */
    public void removeValue(String key)
    
    {
        mContext.remove(key);
    }
    
    /**
     * Print the Contents of the Context to the Logger.
     *
     * @param logger is the java.util.Logger to use for printing out the contents.
     * @param level is the logging level
     */
    public void print(java.util.logging.Logger logger, java.util.logging.Level level)
    {
        StringBuffer buffer = new StringBuffer();
        
        buffer.append("Context : \n");
        
        java.util.Iterator itr = enumerateKeys().iterator();
        
        while ( itr.hasNext() )
        {
            String param = (String) itr.next();
            
            buffer.append("\tParameter : " + param + "\n");
            buffer.append("\tValue     :" + (String) getValue(param) + "\n");
        }
        
        logger.log(level, buffer.toString());
    }
}
