/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)KeyStoreManagerFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  KeyStoreManagerFactory.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 16, 2005, 5:16 PM
 */

package com.sun.jbi.internal.security.keymgt;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class KeyStoreManagerFactory
{
    
    
    /**
     * Create a KeyStoreManager instance.
     *
     * @param props - the name,value pairs which contain KeyStore data.
     * @throws IllegalArgumentException if the properties contain a invalid value
     * for example if Manager is defined to be seomething other than JavaStandard.
     * @return a KeyStoreManager instance.
     */
    public static com.sun.jbi.internal.security.KeyStoreManager 
    createKeyStoreManager(java.util.Properties props)
        throws IllegalArgumentException 
    {
        String type = (String) props.get(com.sun.jbi.internal.security.Constants.MANAGER);
        if ( type == null )
        {
            type = com.sun.jbi.internal.security.KeyStoreManager.JAVA_STD;
        }
        com.sun.jbi.internal.security.KeyStoreManager mgr =  null;
        
        KeyStoreManagerType mgrType = KeyStoreManagerType.valueOf(type);
        if ( mgrType.equals(KeyStoreManagerType.JAVA_STD) )
        {
            mgr = new JavaStdKeyStoreManager();
            
        }
        else if ( mgrType.equals(KeyStoreManagerType.SJSAS) )
        {
            mgr = new AppSrvKeyStoreManager();
        }
        else
        {
            // -- In Shasta 1.0 this will never be called, since the type 
            // -- is always defined.
            throw new IllegalArgumentException();
        }
        mgr.setType(type);
        mgr.initialize(props);
        return mgr;
    }
    
}
