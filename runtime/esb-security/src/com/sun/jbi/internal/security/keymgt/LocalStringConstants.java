/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  PluginConstants.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 30, 2004, 4:57 PM
 */

package com.sun.jbi.internal.security.keymgt;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringConstants
{
    /*-------------------------------------------------------------------------------*\
     *                          Error Messages                                       *
    \*-------------------------------------------------------------------------------*/
    
    /** Err: User Domain Initialization failed. */ 
    String BC_ERR_UD_INIT_FAILED = "BC_ERR_UD_INIT_FAILED";
    
    /** Err: User not found. */
    String BC_ERR_NO_SUCH_USER = "BC_ERR_NO_SUCH_USER";
    
    /** Err: Failed to load User Data. */
    String BC_ERR_LOAD_USERS_FAILED = "BC_ERR_LOAD_USERS_FAILED";
    
    /** Err: KeyStore missing parameter. */
    String BC_ERR_KS_MISSING_PARAM = "BC_ERR_KS_MISSING_PARAM";
    
    /** Err: TrustStore missing parameter. */
    String BC_ERR_TS_MISSING_PARAM = "BC_ERR_TS_MISSING_PARAM";
    
    /** Err: KeyStore init failed. */
    String BC_ERR_KS_INIT_FAILED = "BC_ERR_KS_INIT_FAILED";
    
     /** Err: TrustStore init failed. */
    String BC_ERR_TS_INIT_FAILED = "BC_ERR_TS_INIT_FAILED";
    
    /** Err: Username invalid. */
    String BC_ERR_LOAD_USER_FAILED = "BC_ERR_LOAD_USER_FAILED";
    
    /** Err: CertStore init failed. */
    String BC_ERR_CS_INIT_FAILED = "BC_ERR_CS_INIT_FAILED";
}
