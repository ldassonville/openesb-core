/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TlsContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  TlsContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 11, 2004, 8:46 PM
 */

package com.sun.jbi.internal.security.https;

import com.sun.jbi.internal.security.KeyStoreManager;

import javax.net.ssl.SSLContext;


/**
 *
 * @author Sun Microsystems, Inc.
 */
public class TlsContext
{
    /** The SSL Context. */
    private SSLContext mSSLctx;
    
    /** Client Auth value. */
    private boolean mClientAuthReq;
    
    /** KeyStore Manager. */
    private KeyStoreManager mKSMgr;
    
    /** 
     * Creates a new instance of TlsContext.
     *
     * @param sslCtx is an initialized SSLContext
     * @param isClientAuthReq is a boolean indicating whether Client Auth is required.
     * @param ksMgr is the KeyStoreManager
     */
    public TlsContext (SSLContext sslCtx, boolean isClientAuthReq, 
        KeyStoreManager ksMgr) 
    {
        mSSLctx = sslCtx;
        mClientAuthReq = isClientAuthReq;
        mKSMgr = ksMgr;
    }
    
    /**
     * @return true if ClientAuth required
     */
    public boolean isClientAuthRequired()
    {
        return mClientAuthReq;
    }
    
    /**
     * @return the SSLContext
     */
    public SSLContext getSSLContext()
    {
        return mSSLctx;
    }
    
    /**
     * Get the KeyStoreManager associated with this Tls Context.
     *
     * @return the KeyStoreManager instance.
     */
    public KeyStoreManager getKeyStoreManager()
    {
        return mKSMgr;
    }
    
    
    
}
