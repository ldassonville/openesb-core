/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityServiceConfigMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityServiceConfigMBeanImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 16, 2004, 1:45 PM
 */

package com.sun.jbi.internal.security.mbeans;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.common.GenericConfigurationMBean;

import com.sun.jbi.internal.security.Constants;
import com.sun.jbi.internal.security.config.SecurityInstallConfig;

/**
 * This MBean interface extends the Configuration MBean interface for the
 * Security Service installation configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityServiceConfigMBeanImpl
    implements SecurityServiceConfigMBean
{

    /**
     * The Environment Context.
     */
    private com.sun.jbi.EnvironmentContext mEnvCtx;
    
    /**
     * The Installation time security configuration. 
     */
    private SecurityInstallConfig mSecInstCfg;
    
    /**
     * The String Translator.
     */
    private StringTranslator mTranslator;
    
    /**
     * Why is the Logger in ModelSystemService Private ?
     */
    //private Logger mLogger;
    
    /** 
     * Basic Configuration MBean to delegate the basic tasks to.
     */
    private GenericConfigurationMBean mConfigMBean;
    
    /**
     *
     * @param aContext is the Environment Context.
     * @param configMBean is an instance of the GenericConfigurationMBean to 
     * delegate the basic tasks to.
     * @throws javax.jbi.JBIException one errors
     */
    public SecurityServiceConfigMBeanImpl(GenericConfigurationMBean configMBean, 
        com.sun.jbi.EnvironmentContext aContext)
        throws javax.jbi.JBIException
    {
        mEnvCtx = aContext;
        mTranslator = mEnvCtx.getStringTranslator(Constants.PACKAGE);
        mConfigMBean = configMBean;
        mSecInstCfg = SecurityInstallConfig.createSecurityInstallConfig(mConfigMBean,
            mTranslator);
    }

    /*-------------------------------------------------------------------------------*\
     *                           User Domain Operations                              *
    \*-------------------------------------------------------------------------------*/

    /**
     * @param defUserDomain is the default User Domain.
     */
    public void setDefaultUserDomainName(String defUserDomain)
    {
        mSecInstCfg.setDefaultUserDomainName(defUserDomain);
    }

    /**
     * @return the default User Domain name.
     */
    public String getDefaultUserDomainName()
    {
        return mSecInstCfg.getDefaultUserDomainName();
    }

     /**
     * Add a new User Domain. If a User Domain by the specified name exists, it is
     * overwritten with the new one.
     *
     * @param name of the domain.
     * @param domain is the implementation class that implements the UserDomain interface.
     */
    public void addUserDomain(String name, String domain)
    {
        mSecInstCfg.addUserDomain(name, domain);
    }
    
    /**
     * Remove a User Domain. 
     *
     * @param name of the domain.
     */
    public void removeUserDomain(String name)
    {
        mSecInstCfg.removeUserDomain(name);
    }
    
    /**
     * Add a parameter to a User Domain, if the domain does not exist, the parameter is
     * not added.
     *
     * @param domain name of the UserDomain.
     * @param name is the name of the parameter.
     * @param value is the value for the parameter.
     */
    public void addParameterToUserDomain(String domain, String name, String value)
    {
        mSecInstCfg.addParameterToUserDomain(domain, name, value);
    }
    
        /**
     * Remove a parameter to a User Domain.
     *
     * @param domain name of the UserDomain.
     * @param name is the name of the parameter.
     */
    public void removeParameterFromUserDomain(String domain, String name)
    {
        mSecInstCfg.removeParameterFromUserDomain(domain, name);
    }

   /*-------------------------------------------------------------------------------*\
    *                           KeyStore Manager Operations                         *
   \*-------------------------------------------------------------------------------*/
        
        
    /**
     * @param defKSMgr is the default KeyStore Manager.
     */
    public void setDefaultKeyStoreManagerName(String defKSMgr)
    {
        mSecInstCfg.setDefaultKeyStoreManagerName(defKSMgr);
    }
    
    /**
     * @return the default KeyStore Manager.
     */
    public String getDefaultKeyStoreManagerName()
    {
        return mSecInstCfg.getDefaultKeyStoreManagerName();
    }
    
    /**
     * Add a new KeyStore Manager. If a KeyStoreManager by the specified name exists,
     * it is overwritten with the new one.
     *
     * @param name of the manager.
     * @param manager is the implementation class that implements the KeyStoreManager
     * interface.
     */
    public void addKeyStoreManager (String name, String manager)
    {
        mSecInstCfg.addKeyStoreManager(name, manager);
    }
    
    
    /**
     * Remove a KeyStore Manager.
     *
     * @param name of the manager.
     */
    public void removeKeyStoreManager (String name)
    {
        mSecInstCfg.removeKeyStoreManager(name);
    }

    
    /**
     * Add a parameter to a KeyStoreManager, if it does not exist, the parameter is
     * not added.
     *
     * @param manager is the name of the KeyStoreManager.
     * @param name is the name of the parameter.
     * @param value is the value for the parameter.
     */
    public void addParameterToKeyStoreManager(String manager, String name, String value)
    {
        mSecInstCfg.addParameterToKeyStoreManager(manager, name, value);
    }
    
    /**
     * Remove a parameter from the KeyStoreManager.
     *
     * @param manager is the name of the KeyStoreManager.
     * @param name is the name of the parameter.
     */
    public void removeParameterFromKeyStoreManager(String manager, String name)
    {
        mSecInstCfg.removeParameterFromKeyStoreManager(manager, name);
    }
   
    
   /*-------------------------------------------------------------------------------*\
    *                           Security Context Operations                         *
   \*-------------------------------------------------------------------------------*/

    /**
     * Set the Client Alias from the TransportSecurity Context.
     * @param alias is the SSL Client alias.
     */
    public void setSSLClientAlias(String alias)
    {
        mSecInstCfg.setSSLClientAlias(alias);
    }
    
    /**
     * Get the Client Alias from the TransportSecurity Context.
     * @return the SSL Client alias.
     */
    public String getSSLClientAlias()
    {
        return mSecInstCfg.getSSLClientAlias();
    }
  
    /**
     * Set Client Alias from the TransportSecurity Context.
     *
     * @param protocol is the SSL Client protocol, allowed values are SSLv3, TLS and TLSv1
     */
    public void setSSLClientProtocol (String protocol)
    {
        mSecInstCfg.setSSLClientProtocol(protocol);
    }
    
    /**
     * Get the Client SSL Protocol  from the TransportSecurity Context.
     *
     * @return the SSL Client Protocol.
     */
    public String getSSLClientProtocol()
    {
        return mSecInstCfg.getSSLClientProtocol();
    }
    
    /**
     * Set the SSL Client Use Default parameter.
     *
     * @param flag - true/false inducates whether to use the
     * default Application Server SSL context or not.
     */
    public void setSSLClientUseDefault(boolean flag)
    {
        mSecInstCfg.setSSLClientUseDefault(flag);
    }
    
    /**
     * Get the SSL Client Use Default parameter.
     *
     * @return true/false inducates whether the
     * default Application Server SSL context is being used or not.
     */
    public boolean getSSLClientUseDefault()
    {
        return mSecInstCfg.getSSLClientUseDefault();
    }    
    
    /**
     * Set the SSL Server Req. Client Auth flag.
     *
     * @param flag - true/false inducates whether Client Auth 
     * is required by default.
     */
    public void setSSLServerRequireClientAuth(boolean flag)
    {
        mSecInstCfg.setSSLServerRequireClientAuth(flag);
    }
    
    /**
     * Get the SSL Server Req. Client Auth flag.
     *
     * @return true/false inducates whether Client Auth 
     * is required by default.
     */
    public boolean getSSLServerRequireClientAuth()
    {
        return mSecInstCfg.getSSLServerRequireClientAuth();
    } 
    
    /**
     *                      GenericConfigurationMBean Methods
     */
    
    /**
     * Restore the configuration from persistent storage.
     * @return 0 if successful.
     */
    public int restore()
    {
        try
        {
            mConfigMBean.restore();
            mSecInstCfg = SecurityInstallConfig.createSecurityInstallConfig(mConfigMBean,
                mTranslator);
        }
        catch (Exception ex)
        {
            // Log the Exception
            return 1;
        }
        return 0;
    }
    
    /**
     * Save the configuration to persistent storage.
     * @return 0 if successful.
     */
    public int save()
    {
        try
        {
            mSecInstCfg.updateConfigMBean(mConfigMBean);
            mConfigMBean.save();
        }
        catch (Exception ex)
        {
            // Log the Exception
            //ex.printStackTrace();
            return 1;
        }
        return 0;
    }
    
    /**
     * Apply the current configuration.
     * Components that need to restart when the configurartion is
     * applied will need to override this method.
     *
     * @return 0 if successful.
     */
    public int apply()
    {
        return 0;
    }
    
    /**
     * Set a configuration property for this component.
     * @param aKey the property key
     * @param aValue the property value
     * @return 0 if successful.
     */
    public int setProperty(String aKey, String aValue)
    {
        return 0;
    }

    /**
     * Get a configuration property for this component.
     * @param aKey the property key to retrieve.
     * @return the value of the key or null
     */
    public String getProperty(String aKey)
    {
        mSecInstCfg.updateConfigMBean(mConfigMBean);
        return mConfigMBean.getProperty(aKey);
    }

    /**
     * Get all configuration properties for this component.
     * @return an array containing the property keys.
     */
    public String[] getPropertyKeys()
    {
        mSecInstCfg.updateConfigMBean(mConfigMBean);
        return mConfigMBean.getPropertyKeys();
    }

    /**
     * Delete a configuration property for this component.
     * @param aKey the property key to delete
     * @return zero if successful, otherwise non-zero.
     */
    public int clearProperty(String aKey)
    {
        return 0;
    }

    /**
     * Delete all configuration properties for this component.
     * @return zero if successful, otherwise non-zero.
     */
    public int clearPropertyKeys()
    {
        return 0;
    }
    
   /*-------------------------------------------------------------------------------*\
    *         Operations to display Configuration information                       *
   \*-------------------------------------------------------------------------------*/

    /**
     * Get an XML String which has the meta-data for a particular user domain
     * User Domain.
     *
     * @param name is the logical name of the UserDomain. If there is no such UserDomain
     * or the name is null then a empty string "" is returned.
     * @return an XML String with information for a particular UserDomain.
     */
    public String getUserDomain(String name)
    {
        return mSecInstCfg.getUserDomainCtx(name);
    }
    
    /**
     * Get an XML String listing all the UserDomains and their meta-data.
     *
     * @return an XML String with information for all the UserDomains.
     */
    public String getUserDomains()
    {
        return mSecInstCfg.getUserDomainCtxs();
    }
    
    /**
     * Get an XML String which has the meta-data for a particular key store manager.
     *
     * @param name is the logical name of the KeyStoreManager. If there is no such 
     * KeyStoreManager or the name is null then a empty string "" is returned.
     * @return an XML String with information for a particular KeyStoreManager.
     */
    public String getKeyStoreManager(String name)
    {
        return mSecInstCfg.getKeyStoreCtx(name);
    }
    
    /**
     * Get an XML String listing all the KeyStoreManagers and their meta-data.
     *
     * @return an XML String with information for all the KeyStoreManagers.
     */
    public String getKeyStoreManagers()
    {
        return mSecInstCfg.getKeyStoreCtxs();
    }
    
    
    /**
     * Get an XML String which has the meta-data for the Transport Security Context.
     *
     * @return an XML String with information for the Transport Security Context.
     */
    public String getTransportSecurityConfig()
    {
        return mSecInstCfg.getTransportSecurityConfig();
    }
}
