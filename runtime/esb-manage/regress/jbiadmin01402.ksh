#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01402.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT


echo jbiadmin01402 - Test Runtime Configuration MBeans

. ./regress_defs.ksh

DEFAULT_CONFIG_FILE=$JV_SVC_REGRESS/testdata/jbi-default-config-params.properties

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01402.xml test.descriptor

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01402.xml

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="instance1" -lib "$REGRESS_CLASSPATH" -f jbiadmin01402.xml

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="domain" -lib "$REGRESS_CLASSPATH" -f jbiadmin01402.xml


$JBI_ANT -Djbi.target="server"    -Djbi.config.params.file=$DEFAULT_CONFIG_FILE set-runtime-configuration
$JBI_ANT -Djbi.target="instance1" -Djbi.config.params.file=$DEFAULT_CONFIG_FILE set-runtime-configuration 
$JBI_ANT -Djbi.target="domain"    -Djbi.config.params.file=$DEFAULT_CONFIG_FILE set-runtime-configuration  
