#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00136.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00141: Tests fix for Issue 292 : list service assemblies installed to a component for target = domain"

####################################################################################################
# Tests fix for Issue 292 : list service assemblies installed to a component for target = domain   #
####################################################################################################

. ./regress_defs.ksh

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar
COMPONENT_NAME_1=manage-binding-1

COMPONENT_ARCHIVE_2=$JV_SVC_TEST_CLASSES/dist/component-binding2.jar
COMPONENT_NAME_2=manage-binding-2

SA_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar
SA_NAME_1=esbadmin00089-sa

SA_ARCHIVE_2=$JV_SVC_TEST_CLASSES/dist/deploy-sa-renamed.jar
SA_NAME_2=TestServiceAssembly

#
# Setup :
#
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml pkg.service.assembly.jars
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml pkg.component.jars

# Setup : Install and start components on server instance
$JBI_ANT  -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component
installComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1   start-component
startComponentDelay

$JBI_ANT  -Djbi.install.file=$COMPONENT_ARCHIVE_2   install-component
installComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2    start-component
startComponentDelay

# Setup : Install and start components on instance1

$JBI_ANT  -Djbi.install.file=$COMPONENT_ARCHIVE_2  -Djbi.target=instance1 install-component
installComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2   -Djbi.target=instance1 start-component
startComponentDelay

$JBI_ANT  -Djbi.install.file=$COMPONENT_ARCHIVE_1  -Djbi.target=instance1 install-component
installComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1   -Djbi.target=instance1 start-component
startComponentDelay

# Setup : Deploy one service assembly to server
$JBI_ANT  -Djbi.deploy.file=$SA_ARCHIVE_1   deploy-service-assembly
deploySaDelay
$JBI_ANT   list-service-assemblies

# Setup : Deploy second service assembly to instance1
$JBI_ANT  -Djbi.deploy.file=$SA_ARCHIVE_2   -Djbi.target=instance1 deploy-service-assembly
deploySaDelay
$JBI_ANT   list-service-assemblies -Djbi.target=instance1

# Test : List service assemblies deployed to component for target=domain
#asadmin list-jbi-service-assemblies --target domain --componentname=manage-binding-1 --port $ASADMIN_PORT $ASADMIN_PW_OPTS
$JBI_ANT   list-service-assemblies -Djbi.target=domain -Djbi.component.name=$COMPONENT_NAME_1
# Cleanup : undeploy the service assembly
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME_1  -Djbi.keep.archive=false undeploy-service-assembly
undeploySaDelay
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME_2  -Djbi.target=instance1 -Djbi.keep.archive=false undeploy-service-assembly
undeploySaDelay

# Cleanup : stop/shutdown/uninstall both the components
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1  stop-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1  shut-down-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1  -Djbi.keep.archive=false uninstall-component
uninstallComponentDelay

$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2  stop-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2  shut-down-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2  -Djbi.keep.archive=false uninstall-component
uninstallComponentDelay

$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1  -Djbi.target=instance1 stop-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1  -Djbi.target=instance1 shut-down-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_1  -Djbi.target=instance1 -Djbi.keep.archive=false uninstall-component
uninstallComponentDelay

$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2  -Djbi.target=instance1 stop-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2  -Djbi.target=instance1 shut-down-component
stopComponentDelay
$JBI_ANT  -Djbi.component.name=$COMPONENT_NAME_2  -Djbi.target=instance1 -Djbi.keep.archive=false uninstall-component
uninstallComponentDelay

