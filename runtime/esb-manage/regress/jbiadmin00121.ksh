#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00121.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin00121 - Component configuration tests

echo testname is jbiadmin00121
. ./regress_defs.ksh

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00121.xml

COMPONENT_PATH=$JV_SVC_TEST_CLASSES/dist/config-binding.jar
COMPONENT_NAME=config-binding
COMPONENT_WORKSPACE=$JV_AS8BASE/nodeagents/agent1/instance1/jbi/components/$COMPONENT_NAME/install_root/workspace

# Clean up from prior runs
rm -f $COMPONENT_WORKSPACE/jbiadmin00121

$JBI_ANT -Djbi.install.file=$COMPONENT_PATH -Djbi.install.params.file=$JV_SVC_TEST_CLASSES/deploytest/config/params.properties -Djbi.target=instance1 install-component
$JBI_ANT -Djbi.target=instance1 list-binding-components
# verify that the config file was created
if [ -f $COMPONENT_WORKSPACE/jbiadmin00121 ]
then
   echo Component was configured successfully.
fi 
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=instance1 uninstall-component

# Test off-line installation of a component with configuration properties
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS --nodeagent agent1 configtest >&1
$JBI_ANT -Djbi.install.file=$COMPONENT_PATH -Djbi.install.params.file=$JV_SVC_TEST_CLASSES/deploytest/config/params.properties -Djbi.target=configtest install-component
$JBI_ANT -Djbi.target=configtest list-binding-components
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=configtest uninstall-component

asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS configtest >&1
