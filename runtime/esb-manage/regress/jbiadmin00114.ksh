#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00114.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00114 : Test fixes for 6505606 and 6504358"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml 

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar
COMPONENT1_NAME=manage-binding-1

BOGUS_SA_NAME=bogus-sa

# CR 6505606 : Test shutting down a non-existent service assembly
$JBI_ANT -Djbi.service.assembly.name=$BOGUS_SA_NAME start-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$BOGUS_SA_NAME stop-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$BOGUS_SA_NAME shut-down-service-assembly

# CR 6504358 : Test the ComponentLifeCycle operations for target=domain, should get a error message

# Install the component to the domain target
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE_1  -Djbi.target="domain" install-component

# Test :
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME -Djbi.target="domain" start-component
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME -Djbi.target="domain" stop-component
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME -Djbi.target="domain" shut-down-component

# Cleanup
$JBI_ANT -Djbi.component.name=$COMPONENT1_NAME -Djbi.target="domain" uninstall-component
