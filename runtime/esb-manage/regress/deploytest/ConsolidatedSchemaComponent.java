/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

import javax.management.StandardMBean;
import javax.management.ObjectName;

/**
 * Dummy binding component used to test deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConsolidatedSchemaComponent 
        extends BindingComponent
        implements Component, ComponentLifeCycle, ServiceUnitManager, Bootstrap
        
{  
    /**
     * Register custom MBeans
     */
    protected void registerCustomMBeans()
        throws javax.jbi.JBIException
    {
        try
        {
           
            StandardMBean compMBean;

            if ( mConfig.containsKey(REGISTER_NEW_CONFIG_MBEAN) )
            {

                 CustomConfigurationMBean mbean = new CustomConfiguration(
                        mComponentName + " custom config MBean",
                        mContext );
                compMBean = new StandardMBean(mbean, 
                    CustomConfigurationMBean.class);
            }
            else
            {
                 CustomConfigMBean mbean = new CustomConfig(
                        mComponentName + " custom config MBean",
                        mContext );
                compMBean = new StandardMBean(mbean, 
                    CustomConfigMBean.class);
            }

            ObjectName compMBeanName = 
                mContext.getMBeanNames().createCustomComponentMBeanName("Configuration");

            mContext.getMBeanServer().registerMBean(compMBean, compMBeanName);
        }
        catch ( Exception exp )
        {
            throw new javax.jbi.JBIException(exp.getMessage());
        }
    }
}
