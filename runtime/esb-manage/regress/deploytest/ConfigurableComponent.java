/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurableComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.management.ObjectName;
import java.io.File;

/**
 * ConfigurableComponent is used to test configuration of a component installer.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigurableComponent 
        extends BindingComponent
        implements ConfigurableComponentMBean
{
    private ComponentContext mContext;
    private ObjectName  mConfigMBean;
    private File        mConfigFile;
    
    /** If specified, file is created in this directory. */
    private String mWorkDirectory;
    /** Name of the file to be created. */
    private String mFileName;
    
    /** ################ ConfigurationMBean methods ################ **/

    /** Sets the work directory for the config MBean.
     *  @path absolute path to the work directory
     */
    public void setWorkDirectory(String path)
    {
        mWorkDirectory = path;
    }
    
    /** Returns the absolute path to the installer work directory.
     *  @return absolute path to the work directory, or null if it has not been set.
     */
    public String getWorkDirectory()
    {
        return mWorkDirectory;
    }
    
    /** Specifies the name of the file to create on install.
     *  @fileName file name
     */
    public void setFileName(String fileName)
    {
        mFileName = fileName;
    }
    
    /** Returns the name of the file to create on install.
     *  @return file name
     */
    public String getFileName()
    {
        return mFileName;
    }
    
    /** ###################### 208 SPI methods ###################### **/
    
    /** Install the component.  Creates a new file with the appropriate 
     */
    public void onInstall() throws javax.jbi.JBIException
    {
        File dir = new File(mWorkDirectory != null ? mWorkDirectory : mContext.getWorkspaceRoot());        
        mConfigFile = new File(dir, mFileName);
        
        try
        {
            mConfigFile.createNewFile();
            
            if (mContext.getMBeanServer().isRegistered(mConfigMBean))
            {
                mContext.getMBeanServer().unregisterMBean(mConfigMBean);
            }
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex.toString());
        }
    }
    
    /** Uninstalls the component.  Removes the config file, if present.
     */
    public void onUninstall() 
        throws javax.jbi.JBIException
    {
        if (mConfigFile != null)
        {
            mConfigFile.delete();
        }
    }
    
    /** Initialize the component installer.
     */
    public void init(javax.jbi.component.InstallationContext installationContext)
        throws javax.jbi.JBIException
    {
        mContext = installationContext.getContext();
        mConfigMBean = mContext.getMBeanNames().createCustomComponentMBeanName("myconfig");
        
        try
        {
            // Clean up just in case a prior run did not complete
            if (mContext.getMBeanServer().isRegistered(mConfigMBean))
            {
                mContext.getMBeanServer().unregisterMBean(mConfigMBean);
            }
            
            mContext.getMBeanServer().registerMBean(this, mConfigMBean);
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex.toString());
        }
    }
    
    /**
     * Get the JMX ObjectName for any additional MBean for this BC. This
     * implementation always returns null.
     * @return javax.management.ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return mConfigMBean;
    }
}
