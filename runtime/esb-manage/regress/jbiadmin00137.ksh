#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00134.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00137 : Test deploy and life cycle operations for a service assembly with zero service units"

. ./regress_defs.ksh

SA_ARCHIVE=${MANAGE_EMPTY_SA}
SA_NAME=MyBinding_TestSA

$JBI_ANT  -Djbi.deploy.file=$SA_ARCHIVE   deploy-service-assembly
$JBI_ANT   list-service-assemblies
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME  start-service-assembly
$JBI_ANT   list-service-assemblies
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME  start-service-assembly
$JBI_ANT   list-service-assemblies
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME  stop-service-assembly
$JBI_ANT   list-service-assemblies
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME  shut-down-service-assembly
$JBI_ANT   list-service-assemblies
$JBI_ANT  -Djbi.service.assembly.name=$SA_NAME  undeploy-service-assembly
$JBI_ANT   list-service-assemblies
