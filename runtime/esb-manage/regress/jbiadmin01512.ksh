#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01512.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# This is a -ve test for component upgrade in cluster
# A component is installed in server and a standalone instance
# and two SAs are deployed on that component.
# This component is upgraded with a newer version(that implements the upgrade SPI).
# This newer version throws an exception during upgrade and hence upgrade fails. 
# This test verifies that the SU roots are restored from the backup
# This test depends on jbiadmin01511.ksh to create a cluster with 2 instances and to start them.
####
echo "jbiadmin01511 : -ve test for component upgrade. component throws during upgrade."


. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01505.xml 1>&2

echo install the component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/upgrade-original.jar

echo deploy the SAs in server and instance1
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/upgrade-sa1.jar
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/upgrade-sa2.jar

echo shut-down the component in server and instance1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component

echo upgrade the component
$JBI_ANT -Djbi.component.name=Upgrade-Component -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/upgrade-modified.jar upgrade-component 2>&1

##
# upgrade would have failed. But the bad component would have deleted the jbi.xml from the SU roots. 
# However management layer would have restored the SU roots. verify if the deleted files have been restored from backup.
##

if [ -f "$JBI_DOMAIN_ROOT/../../nodeagents/agent1/CAS-cluster1-inst1/jbi/service-assemblies/Upgrade-SA1/Upgrade-SA1-SU1/Upgrade-Component/META-INF/jbi.xml" ]; then
echo SU1 root has been restored in CAS-cluster1-inst1
fi
if [ -f "$JBI_DOMAIN_ROOT/../../nodeagents/agent1/CAS-cluster1-inst1/jbi/service-assemblies/Upgrade-SA2/Upgrade-SA1-SU2/Upgrade-Component/META-INF/jbi.xml" ]; then
echo SU1 root has been restored in CAS-cluster1-inst1
fi
if [ -f "$JBI_DOMAIN_ROOT/../../nodeagents/agent1/inst2/jbi/service-assemblies/Upgrade-SA1/Upgrade-SA1-SU1/Upgrade-Component/META-INF/jbi.xml" ]; then
echo SU1 root has been restored in inst2
fi
if [ -f "$JBI_DOMAIN_ROOT/../../nodeagents/agent1/inst2/jbi/service-assemblies/Upgrade-SA2/Upgrade-SA1-SU2/Upgrade-Component/META-INF/jbi.xml" ]; then
echo SU1 root has been restored in inst2
fi

echo verify that component install root has been replaced 
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component

echo undeploy the SAs
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 Upgrade-SA1
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 Upgrade-SA2

echo shutdown the component
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component

echo uninstall the component
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component

asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS CAS-cluster1-inst1 >&1
asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS inst2 >&1
asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS inst2 >&1
rm -rf $AS8BASE/nodeagents/agent1/inst2*

