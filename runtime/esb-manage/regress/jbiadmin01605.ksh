#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01401.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT


echo jbiadmin01605 - Test operations for application configuration / variables support

. ./regress_defs.ksh


COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-without-config-mbean.jar
COMPONENT_NAME=manage-binding-1

COMPONENT_ARCHIVE1=$JV_SVC_TEST_CLASSES/dist/component-with-bad-config-mbean.jar

ant -q -emacs -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01605.xml pkg.test.component

# Test support for a component that does not register a component configuration MBean
asadmin install-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_ARCHIVE 

ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01605.xml test.app.config.support
asadmin start-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_NAME 

ant -q -emacs -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01605.xml check.app.vars.support
ant -q -emacs -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01605.xml check.app.config.support
 
asadmin shut-down-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_NAME 
asadmin uninstall-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_NAME

# Test support for a component that registers a component configuration MBean with missing configuration operations
asadmin install-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_ARCHIVE1
asadmin start-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_NAME 

ant -q -emacs -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01605.xml check.app.vars.support
ant -q -emacs -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01605.xml check.app.config.support
 
asadmin shut-down-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_NAME 
asadmin uninstall-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $COMPONENT_NAME
