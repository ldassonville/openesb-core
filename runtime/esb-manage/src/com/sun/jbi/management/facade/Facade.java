/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Facade.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.management.JMException;
import javax.management.JMRuntimeException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.platform.PlatformContext;

import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.util.FacadeMbeanHelper;
import com.sun.jbi.management.util.FileHelper;
import com.sun.jbi.management.util.FileTransfer;
import com.sun.jbi.management.util.Validator;
import com.sun.jbi.management.util.ValidatorFactory;
import com.sun.jbi.management.util.ValidatorType;

import java.io.File;
import java.net.URL;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.management.MBeanServer;

/**
 * Base class with common funtionality for all facade mbean implementations.
 *
 * @author Sun Microsystems, Inc.
 */
public class Facade
{
    protected String                    mTarget;
    protected EnvironmentContext        mEnvCtx;
    protected MBeanServer               mMBeanSvr;
    protected MBeanNames                mMBeanNames;
    protected MessageBuilder            mMsgBuilder;
    protected PlatformContext           mPlatform;
    protected StringTranslator          mTranslator;    
    protected ManagementContext         mMgtCtx; 
    protected Logger                    mLog;
    protected static String             mComponentConfigSchema; 
      
    protected static       HashMap sTypeMap;
    
    protected static final String DOMAIN = "domain";
    protected static final String SERVER = "server";
        
    protected static final String WORKSPACE_ROOT     = "workspace";
    protected static final String MASKED_FIELD_VALUE = "*****";
    protected static final String PASSWORD           = "PASSWORD";
    protected static final String CONFIG_SCHEMA_PATH = "/schemas/componentConfiguration.xsd";

    
    private   Registry                  mRegistry;
    
    public Facade(EnvironmentContext ctx, String target)
        throws ManagementException
    {
        
        mPlatform          = ctx.getPlatformContext();
        mTarget            = target;
        mEnvCtx            = ctx;
        mTranslator        = mEnvCtx.getStringTranslator("com.sun.jbi.management.facade");
        mLog               = Logger.getLogger("com.sun.jbi.management");
        mMsgBuilder        = new MessageBuilder(mTranslator);
        mMBeanNames        = mEnvCtx.getMBeanNames();
        mMBeanSvr          = mEnvCtx.getMBeanServer();
        // -- Management Context dependency will be removed after we remove JbiName from
        // -- instance MBean Names
        mMgtCtx = new ManagementContext(ctx);
    }
    
    /**
     *  Get a reference to ComponentQuery.
     */
    protected ComponentQuery getComponentQuery()
        throws ManagementException
    {
        return getComponentQuery(mTarget);
    }
    
    /**
     *  Get a reference to ComponentQuery.
     */
    protected ComponentQuery getComponentQuery(String target)
        throws ManagementException
    {
        try
        {
            return getRegistry().getComponentQuery(target);
        }   
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("getComponentQuery", rex);
            throw new ManagementException(errMsg);
        }  
    }
    
    /**
     *  Get a reference to GenericQuery.
     */
    protected GenericQuery getGenericQuery()
        throws ManagementException
    {
        try
        {
            return getRegistry().getGenericQuery();
        }   
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("getGenericQuery", rex);
            throw new ManagementException(errMsg);
        }  
    }
    
    /**
     *  Get a reference to registry Updater.
     */
    protected Updater getUpdater()
        throws ManagementException
    {
        try
        {
            return getRegistry().getUpdater();
        }   
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("getUpdater", rex);
            throw new ManagementException(errMsg);
        }  
    }
    
    /**
     *  Get a reference to registry Updater.
     */
    protected ServiceAssemblyQuery getServiceAssemblyQuery()
        throws ManagementException
    {
        try
        {
            return getRegistry().getServiceAssemblyQuery(mTarget);
        }   
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("ServiceAssemblyQuery", rex);
            throw new ManagementException(errMsg);
        }  
    }
    
    /**
     *  Get a reference to the JBI registry.
     */
    protected Registry getRegistry()
    {
        return (com.sun.jbi.management.registry.Registry)mEnvCtx.getRegistry();
    }
    
    /**
     * Get an MBeanServer connection for the target
     */
     protected MBeanServerConnection getMBeanServerConnection(String serverName)
        throws ManagementException
     {
        try
        {
            return mPlatform.getMBeanServerConnection(serverName);
        }
        catch ( Exception ex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage("getMBeanServerConnection", ex);
            throw new ManagementException(msg);
        }
     }
     
     /**
      * Invoke a operation on the remote object w/o a return value. 
      */
     protected void invokeRemoteVoidOperation( ObjectName mbeanName, 
        String op, Object[] params, String[] sign, String serverName)
        throws ManagementException
     {
        mbeanRegistrationCheck(op, mbeanName, serverName);
        
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
                    
            svrConxn.invoke(mbeanName, op, params, sign);
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( java.io.IOException ioex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage(op, ioex);
            throw new ManagementException(msg);
        }
     }
     
     /**
      * Invoke a operation on the remote object with a return value.
      */
     protected Object invokeRemoteOperation( ObjectName mbeanName, 
        String op, Object[] params, String[] sign, String serverName)
        throws ManagementException
     {
        mbeanRegistrationCheck(op, mbeanName, serverName);
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
            
            
            Object rslt;
            
            if (op.startsWith("get") && params != null && params.length == 0)
            {
                // attribute name = operation name - "get"
                String attr = op.substring("get".length());
                rslt = svrConxn.getAttribute(mbeanName, attr);
            }
            else
            {
                rslt = svrConxn.invoke(mbeanName, op, params, sign);
            }
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
                    
            return rslt;
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg = mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( java.io.IOException ioex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage(op, ioex);
            throw new ManagementException(msg);
        }
     }
     
     /**
      * Set an attribute on the remote object with a return value.
      */
     protected void  setRemoteAttribute( ObjectName mbeanName, 
        javax.management.Attribute attribute, String serverName)
        throws ManagementException
     {
        String op = "setAttribute";
        mbeanRegistrationCheck(op, mbeanName, serverName);
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
            
            
            svrConxn.setAttribute(mbeanName, attribute);
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg = mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( java.io.IOException ioex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage(op, ioex);
            throw new ManagementException(msg);
        }
     }

     /**
      * Set an attribute on the remote object with a return value.
      */
     protected Object setRemoteAttributes( ObjectName mbeanName, 
        javax.management.AttributeList attributes, String serverName)
        throws ManagementException
     {
        String op = "setAttributes";
        mbeanRegistrationCheck(op, mbeanName, serverName);
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
                        
            Object rslt = svrConxn.setAttributes(mbeanName, attributes);
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
                    
            return rslt;
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg = mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( java.io.IOException ioex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage(op, ioex);
            throw new ManagementException(msg);
        }
     }

     
     /**
      * Get attributes on the remote object with a return value.
      */
     protected Object getRemoteAttributes( ObjectName mbeanName, 
        String[] attributes, String serverName)
        throws ManagementException
     {
        String op = "getAttributes";
        mbeanRegistrationCheck(op, mbeanName, serverName);
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
            
            
            Object rslt = svrConxn.getAttributes(mbeanName, attributes);
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
                    
            return rslt;
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg = mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( java.io.IOException ioex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage(op, ioex);
            throw new ManagementException(msg);
        }
     }
     
     /**
      * Get attribute on the remote object with a return value.
      */
     protected Object getRemoteAttribute( ObjectName mbeanName, 
        String attribute, String serverName)
        throws ManagementException
     {
        String op = "getAttribute";
        mbeanRegistrationCheck(op, mbeanName, serverName);
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
            
            
            Object rslt = svrConxn.getAttribute(mbeanName, attribute);
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION, 
                    new String[]{op, mbeanName.toString(), serverName}));
                    
            return rslt;
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg = mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( java.io.IOException ioex ) 
        {
            String 
                msg = mMsgBuilder.buildExceptionMessage(op, ioex);
            throw new ManagementException(msg);
        }
     }
     
     /**
      * Invoke a operation on a local MBean
      */
     protected Object invokeLocalOperation( ObjectName mbeanName, 
        String op, Object[] params, String[] sign)
        throws ManagementException
     {
        mbeanRegistrationCheck(op, mbeanName);
        try
        {
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INVOKING_LOCAL_OPERATION, 
                    new String[]{op, mbeanName.toString()}));
            
            Object rslt = mMBeanSvr.invoke(mbeanName, op, params, sign);
            
            mLog.finest(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_DONE_INVOKING_LOCAL_OPERATION, 
                    new String[]{op, mbeanName.toString()}));
                    
            return rslt;
        }
        catch ( JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg = mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  mMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
     }     

    /**
     * @return the ComponentType
     */
    protected ComponentType getComponentType(String componentName)
       throws ManagementException
    {
        ComponentType compType = null;
        
        try
        {
            compType = getGenericQuery().getComponentType(componentName);
        }
        catch(RegistryException rex)
        {
            mLog.fine(MessageHelper.getMsgString(rex));
            throw new ManagementException(mMsgBuilder.buildExceptionMessage("getComponentType", rex));
        }
        return compType;
    }
    
    
    /**
     * This operation invokes the getCurentState() operation on the ComponentLifeCycle
     * MBean for a component on the instance, if the instance is running, if the instance
     * is down the state of the Component is SHUTDOWN.
     *
     * @return the state of the component on the instance. 
     */
    protected String getComponentStateOnInstance(String compName, String instanceName)
        throws ManagementException
    {
        if ( isInstanceRunning(instanceName) )
        {
            // Invoke op on the remote ComponentLifeCycleMBean
            String[] sign   = new String[0];
            Object[] params = new Object[0];
            
            ObjectName     compLCName = getComponentLifeCycleMBeanName(compName, 
                getComponentType(compName), instanceName);
            
            if ( compLCName == null )
            {
                // Component not installed on instance return unknown
                return ComponentState.UNKNOWN.toString();
            }
            
            String state = (String) invokeRemoteOperation(
                compLCName, "getCurrentState", 
                params, sign, instanceName);
            
            mLog.finer(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CURRENT_STATE_ON_INSTANCE, 
                compName, instanceName, state));
            
            return state;
    
        }
        else
        {
            // -- If instance is down, the state has to be shutdown
            return javax.jbi.management.LifeCycleMBean.SHUTDOWN;
        }
    }
    
    /**
     * @return the ComponentState on the instance.
     */
    protected ComponentState getComponentState(String compName, String instanceName)
        throws ManagementException
    {
        String state    = getComponentStateOnInstance(compName, instanceName);
        return ComponentState.valueOfLifeCycleState(state);
    }
    
    /**
     * @return true if the component is installed on the target.
     */
    protected boolean isComponentInstalled(String compName)
        throws ManagementException
    {
        boolean     isInstalled = false;
      
        ComponentInfo compInfo = getComponentQuery().getComponentInfo(compName);
        
        if ( compInfo == null )
        {
            mLog.fine("Component " + compName + " is not installed on target " + mTarget);
            isInstalled = false;
        } 
        else
        {
            isInstalled = true;
        }
        return isInstalled;
    }
    
    /**
     * Get the Facade Installer MBean object name for a component.
     */
    protected ObjectName getFacadeInstallerMBeanName(String componentName)
        throws ManagementException
    {
        ComponentType compType = getComponentType(componentName);
        return getFacadeInstallerMBeanName(componentName, compType);
    }
    
    /**
     * Get the Facade Installer MBean object name for a component.
     */
    protected ObjectName getFacadeInstallerMBeanName(String componentName, 
        ComponentType compType)
        throws ManagementException
    {
        ObjectName instName = null;

        if ( ComponentType.BINDING.equals(compType) )
        {
            instName = mMBeanNames.getBindingMBeanName(componentName, 
                    MBeanNames.ComponentServiceType.Installer, mTarget);
        }
        else if ( ComponentType.ENGINE.equals(compType) )
        {
            instName = mMBeanNames.getEngineMBeanName(componentName, 
                    MBeanNames.ComponentServiceType.Installer, mTarget);
        }
        else
        {
            String[] params = new String[]{componentName};
            mMsgBuilder.throwManagementException("getFacadeInstallerMBeanName", 
                LocalStringKeys.JBI_ADMIN_UNKNOWN_COMPONENT, params); 
        }
        return instName;
    }
    /**
     *  Parses an installation zip url, strips file protocol details 
     *  (if necessary) and returns a File reference.  This method is used
     *  to protect clients which do not understand file URIs (e.g. repository).
     */
    protected File fromFileURL(String urlStr)
    {
        try
        {
            if (urlStr.startsWith("file:"))
            {
                urlStr = new URL(urlStr).getPath();
            }
        }
        catch (java.net.MalformedURLException badURLEx)
        {
            mLog.info(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_BAD_INSTALL_URL,
 	            new String[] {urlStr, badURLEx.getMessage()}));
        }
        
        return new File(urlStr);
    }
    
    /**
     *  Always returns a 208-compliant file URL based on the supplied urlStr.
     *  If urlStr is already a valid file URL, this method is a NOP.
     */
    protected String toFileURL(String urlStr)
    {
        try
        {
            if (!urlStr.startsWith("file:"))
            {
                urlStr = new File(urlStr).toURL().toString();
            }
        }
        catch (java.net.MalformedURLException badURLEx)
        {
            mLog.info(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_BAD_INSTALL_URL,
 	            new String[] {urlStr, badURLEx.getMessage()}));
        }
        
        return urlStr;
    }
    
    /**
     * Validate the Archive
     *
     * @throws ManagementException if the archive is not valid or does not exist.
     * @return the validated Archive
     */
    protected Archive validateArchive(String archiveZipUrl, 
            ArchiveType expectedType, String taskId)
        throws ManagementException
    {
        Archive archive = null;
        
        try
        {
            ValidatorFactory.setDescriptorValidation(true);
            Validator validator = 
                ValidatorFactory.createValidator(mEnvCtx, getValidatorType(expectedType));
            File archiveFile = fromFileURL(archiveZipUrl);
            
            if ( validator != null )
            {
                validator.validate(archiveFile);
                archive = new Archive(archiveFile, false);
            }
            else
            {
                archive = new Archive(archiveFile, true);
            }
            
        }
        catch ( Exception ex)
        {
            throw new ManagementException(mMsgBuilder.buildExceptionMessage(taskId, ex));
        }

        if ( !expectedType.equals(archive.getType()) )
        {
            String[] params = new String[]{archiveZipUrl, expectedType.toString()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_INVALID_ARCHIVE_TYPE, params);
        }
        
        return archive;
    }    
    
    /**
     * MBean registration check. 
     */
    protected void mbeanRegistrationCheck(String op, ObjectName mbeanName, String serverName)
        throws ManagementException
    {
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        boolean isRegistered = isMBeanRegisteredOnInstance(mbeanName, serverName);
        
        if ( !isRegistered )
        {
            String[] params = new String[]{mbeanName.toString(), serverName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_MBEAN_NOT_REGISTERED, params);
            
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(op,
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), params, 
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new ManagementException(jbiTaskMsg);
        }
    }
    
    /**
     * @return true if the MBean is registered on the instance, false otherwise
     */
    protected boolean isMBeanRegisteredOnInstance(ObjectName mbeanName, String serverName)
        throws ManagementException
    {
        MBeanServerConnection svrConxn = getMBeanServerConnection(serverName);
        boolean isRegistered = false;
        try
        {
            isRegistered = svrConxn.isRegistered(mbeanName);
        }
        catch ( java.io.IOException ioex)
        {
            throw new ManagementException(
                mMsgBuilder.buildExceptionMessage("isMBeanRegisteredOnInstance", ioex));
        }
        return isRegistered;
    }
    
    /**
     * Checks to see if the Target (server, cluster) is up or down.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return true if Target is up, false if not
     */
    public boolean isTargetUp(String targetName) 
    {
        boolean isTargetUp = false;
        Set<String> clusters = this.mPlatform.getClusterNames();
        Set<String> servers  = this.mPlatform.getStandaloneServerNames();

        if ( clusters.contains(targetName) )
        {
            Set<String> serversInCluster =  
                this.mPlatform.getServersInCluster(targetName);

            for ( String instanceName : serversInCluster )
            {
                if ( this.mPlatform.isInstanceUp(instanceName) )
                {
                     isTargetUp = true;
                     break;
                }
            }
        }
        else if ( servers.contains(targetName) )
        {
           isTargetUp = this.mPlatform.isInstanceUp(targetName);
        }
        
        return isTargetUp;
    }
    
    /**
     * @param clusterName - cluster name
     * @return true if all the instances in the cluster are up and running.
     */
    public boolean areAllInstancesUp(String clusterName)
    {
        boolean isClusterUp = false;
        Set<String> clusters = this.mPlatform.getClusterNames();

        if ( clusters.contains(clusterName) )
        {
            Set<String> serversInCluster =  
                this.mPlatform.getServersInCluster(clusterName);

            for ( String instanceName : serversInCluster )
            {
                isClusterUp = true;
                if ( !this.mPlatform.isInstanceUp(instanceName) )
                {
                     isClusterUp = false;
                     break;
                }
            }
        }
        
        return isClusterUp;
    }
    
    /** Create a comma-separated string containing items in list. */
    static String listToString(java.util.List<String> list)
    {
        java.util.Iterator itr = list.iterator();
        StringBuffer buff = new StringBuffer();

        buff.append(itr.next());

        while ( itr.hasNext() )
        {
            buff.append( ", " );
            buff.append(itr.next());
        }
        
        return buff.toString();
    }
    
    /**
     * Local MBean registration check. 
     */
    private void mbeanRegistrationCheck(String op, ObjectName mbeanName)
        throws ManagementException
    {
        boolean isRegistered = mMBeanSvr.isRegistered(mbeanName);
        if ( !isRegistered )
        {
            String[] params = new String[]{mbeanName.toString()};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_LOCAL_MBEAN_NOT_REGISTERED, params);
            
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(op,
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), params, 
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new ManagementException(jbiTaskMsg);
        }
    }
    
    /**
     * @return true if the instance is running
     */
    protected boolean isInstanceRunning(String instanceName)
        throws ManagementException
    {
        boolean isInstanceRunning = false;
        try
        {
            isInstanceRunning = mPlatform.isInstanceUp(instanceName);
        }
        catch ( Exception ex)
        {
            ex.printStackTrace();
            String msg = mMsgBuilder.buildExceptionMessage(
                "isInstanceRunning", ex);
            throw new ManagementException(msg);
        }
        if ( !isInstanceRunning )
        {
            mLog.fine("Instance " + instanceName + " is not up");
        }
        return isInstanceRunning;
    }
    
    /**
     * @throws javax.jbi.JBIException if the instance is not running.
     */
    protected void instanceRunningCheck(String instanceName)
        throws javax.jbi.JBIException
    {
        if ( !isInstanceRunning(instanceName) )
        {
            String[] params = new String[]{instanceName};
            String responseMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INSTANCE_DOWN, params);
            String jbiMgtMsg = mMsgBuilder.buildFrameworkMessage(
                "instanceRunningCheck",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(responseMsg),
                params, mMsgBuilder.getMessageToken(responseMsg));
           throw new javax.jbi.JBIException(jbiMgtMsg);
        }
    }
    
    /**
     * Upload a file to the specified target and return the absolute path to the
     * file on the remote target
     */
    protected String uploadFile(String serverName, String localFile)
        throws ManagementException
    {
        String uploadedPath = null;
        
        try
        {
            FileTransfer ft = new FileTransfer(serverName, mMBeanNames,
                mPlatform.getMBeanServerConnection(serverName));

            uploadedPath = ft.uploadFile(localFile);
        }
        catch(Exception ex)
        {
            String msg = mMsgBuilder.buildExceptionMessage(
                "uploadFile", ex);
            throw new ManagementException(msg);
        }
        return uploadedPath;
    }
    
    /**
     * @return the instance root key : ${ + instance root property key + } 
     *
    protected String getInstanceRoot()
    {
        StringBuffer strBuf = new StringBuffer("${");
        strBuf.append(AppServerInfo.getInstanceRootKey());
        strBuf.append( "}");
        return strBuf.toString();
    }
     */
    
    /**
     * Check if a SharedLibrary is installed on a target.
     *
     * @boolean throwExWhenInstalled - if true, a ManagementExcetion is thrown
     * if the shared library is installed on the target. if false, the exception is
     * thrown when not installed.
     */
    protected void sharedLibraryInstallationCheck(String slName, 
        boolean throwExWhenInstalled)
        throws ManagementException
    {
        boolean isInstalled = isSharedLibraryInstalled(slName);
        if (  isInstalled && throwExWhenInstalled )
        {
            String[] params = new String[]{slName, mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_SHARED_LIBRARY_INSTALLED_ON_TARGET,
                params);
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                "sharedLibraryInstallationCheck", 
                MessageBuilder.TaskResult.FAILED, 
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), 
                params,
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new ManagementException(jbiTaskMsg);
        }
        
        if (  !isInstalled && !throwExWhenInstalled )
        {
            String[] params = new String[]{slName, mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_SHARED_LIBRARY_NOT_INSTALLED_ON_TARGET,
                params);
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                "sharedLibraryInstallationCheck", 
                MessageBuilder.TaskResult.FAILED, 
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), 
                params,
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new ManagementException(jbiTaskMsg);
        }
        
    }
    
    /**
     * @return true if the shared library is installed on the target
     */
    protected boolean isSharedLibraryInstalled(String slName)
        throws ManagementException
    {
        java.util.List<String> targets = null;
        try
        {
            if ( DOMAIN.equals(mTarget) )
            {
                return getGenericQuery().isSharedLibraryInstalled(slName);
            }
            else 
            {
                if ( mPlatform.isStandaloneServer(mTarget) )
                {
                    targets = getGenericQuery().getServersInstallingSharedLibrary(slName);
                }
                else
                {
                    targets = getGenericQuery().getClustersInstallingSharedLibrary(slName);
                }
                return targets.contains(mTarget);
            }
        }
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage(
                "sharedLibraryInstallationCheck", rex);
            throw new ManagementException(errMsg);
        }
        
        
    }
    
    /**
     * Check if a Component is installed on a target.
     *
     * @boolean throwExWhenInstalled - if true, a ManagementExcetion is thrown
     * if the component is installed on the target. if false, the exception is
     * thrown when not installed.
     */
    protected void componentInstallationCheck(String componentName, 
        boolean throwExWhenInstalled)
        throws ManagementException
    {
        
        boolean isInstalled = isComponentInstalled(componentName);
        if (  isInstalled && throwExWhenInstalled )
        {
            String[] params = new String[]{componentName, mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_COMPONENT_INSTALLED_ON_TARGET,
                params);
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                "componentInstallationCheck", 
                MessageBuilder.TaskResult.FAILED, 
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), 
                params,
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new ManagementException(jbiTaskMsg);
        }
        
        if (  !isInstalled && !throwExWhenInstalled )
        {
            String[] params = new String[]{componentName, mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_COMPONENT_NOT_INSTALLED_ON_TARGET,
                params);
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                "componentInstallationCheck", 
                MessageBuilder.TaskResult.FAILED, 
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), 
                params,
                mMsgBuilder.getMessageToken(errMsg));
            
            throw new ManagementException(jbiTaskMsg);
        }
        
    }

    /**
     * Handle Cluster installation failures.
     *
     * The exception map contains exceptions thrown by the instance, keyed by instance
     * name. 
     *
     * If there are as many exceptions as there are instances, it is a complete failure,
     * build a composite exception and throw a exception with the composed management
     * message.
     *
     * If there are fewer exceptions than instances, it is a partial success, log the
     * exceptions as warnings.
     *
     */
    protected void handleClusteredInstanceFailures(String taskId, Map<String, Throwable> exceptionMap, 
        int numInstances, String locStrKey)
            throws ManagementException
    {
        if ( exceptionMap.size() == numInstances )
            {
                // Complete failure
                String msg =
                    mMsgBuilder.buildCompositeExceptionMessage(taskId, exceptionMap);
                throw new ManagementException(msg);
            }
            else
            {
                // Partial success
                Set instanceSet = exceptionMap.keySet();
                Iterator itr = instanceSet.iterator();
                while ( itr.hasNext() )
                {
                    String instance = (String) itr.next();
                    mLog.fine(
                        mTranslator.getString(
                            locStrKey,
                            instance, ((Exception) exceptionMap.get(instance)).getMessage()));
                }
            }
    }
    
    /**
     * @return the object name of the Deployment Service
     */
    protected ObjectName getRemoteAdminServiceObjectName(String instanceName)
    {
        MBeanNames mbnNames = mMgtCtx.getMBeanNames(instanceName);
        return mbnNames.getSystemServiceMBeanName(
            MBeanNames.SERVICE_NAME_ADMIN_SERVICE,
            MBeanNames.CONTROL_TYPE_ADMIN_SERVICE);
    }    
    /**
     * Get the Component Lifecycle MBean name on an instance.
     *
     * @throws javax.jbi.JBIException when 
     *        the component is not installed on the target
     *        the instance is not up
     *        AdminService MBean is not registered on the remote instance.
     */
    protected ObjectName getComponentLifeCycleMBeanName(String componentName, 
        ComponentType compType, String instanceName) 
        throws ManagementException
    {
        try
        {
            instanceRunningCheck(instanceName);
        }
        catch( javax.jbi.JBIException jex )
        {
            throw new ManagementException(jex.getMessage());
        }
        componentInstallationCheck(componentName, false);
        ObjectName compLCMBean = null;
         
        ObjectName instanceAdminSvc = getRemoteAdminServiceObjectName(instanceName);
        
        String[] sign   = new String[]{"java.lang.String"};
        Object[] params = new Object[]{componentName};

        compLCMBean = (ObjectName) invokeRemoteOperation(
            instanceAdminSvc, "getComponentByName", 
            params, sign, instanceName);      
        
         return compLCMBean;
    }
    
    /**
     * Unregister the ComponentLifeCycle MBean for the component.
     *
     * @param target - the target value of the facade MBean
     */
    protected void unregisterComponentLifeCycleMBean(String target, String compName)
        throws JBIException    
    {
        ComponentType compType = getComponentType(compName);
        unregisterComponentLifeCycleMBean(target, compName, compType);
    }
    
    /**
     * Unregister the ComponentLifeCycle MBean for the component.
     *
     * @param target - the target value of the facade MBean
     */
    protected void unregisterComponentLifeCycleMBean(String target, String compName,
        ComponentType compType)
        throws JBIException
    {   
        ObjectName 
                compLCName = FacadeMbeanHelper.getComponentLifeCycleFacadeMBeanName(
                    compName, compType, target, mEnvCtx.getMBeanNames());
        
        try
        {
            if ( mMBeanSvr.isRegistered(compLCName) )
            {
                mMBeanSvr.unregisterMBean(compLCName);
            }
        }
        catch(Exception jex)
        {
            String msg = mMsgBuilder.buildExceptionMessage(
                "unregisterComponentLifeCycleMBean", jex);
            throw new ManagementException(msg);
        }
    }
       
    /**
     * Unregister the ComponentExtension MBean for the component.
     *
     * @param target - the target value of the facade MBean
     */
    protected void unregisterComponentExtensionMBean(String target, String compName)
        throws JBIException    
    {
        ComponentType compType = getComponentType(compName);
        unregisterComponentExtensionMBean(target, compName, compType);
    }
    
     /**
     * Unregister the ComponentLifeCycle MBean for the component.
     *
     * @param target - the target value of the facade MBean
     */
    protected void unregisterComponentExtensionMBean(String target, String compName,
        ComponentType compType)
        throws JBIException
    {   
        ObjectName facadeCompInfoMBean = null;
        if ( compType == ComponentType.BINDING )
        {
            facadeCompInfoMBean = mMBeanNames.getBindingMBeanName(
                compName, 
                MBeanNames.ComponentServiceType.Extension, 
                target);
        }
        else
        {
            facadeCompInfoMBean = mMBeanNames.getEngineMBeanName(
                compName, 
                MBeanNames.ComponentServiceType.Extension, 
                target);
        }
        
        try
        {
            if ( mMBeanSvr.isRegistered(facadeCompInfoMBean) )
            {
                mMBeanSvr.unregisterMBean(facadeCompInfoMBean);
            }
        }
        catch(Exception jex)
        {
            String msg = mMsgBuilder.buildExceptionMessage(
                "unregisterComponentExtensionMBean", jex);
            throw new ManagementException(msg);
        }
    }
    
    /**
     * Unregister the Component Configuration MBean for the component.
     *
     * @param target - the target value of the facade MBean
     */
    protected void unregisterComponentConfigurationMBean(String target, String compName)
        throws JBIException    
    {
        ComponentType compType = getComponentType(compName);
        unregisterComponentConfigurationMBean(target, compName, compType);
    }
    
    /**
     * Unregister the Component Configuration MBean for the component.
     *
     * @param target - the target value of the facade MBean
     */
    protected void unregisterComponentConfigurationMBean(String target, String compName,
        ComponentType compType)
        throws JBIException
    {   
        ObjectName facadeCompCfgMBean = null;
        if ( compType == ComponentType.BINDING )
        {
            facadeCompCfgMBean = mMBeanNames.getBindingMBeanName(
                compName, 
                MBeanNames.ComponentServiceType.Configuration, 
                target);
        }
        else
        {
            facadeCompCfgMBean = mMBeanNames.getEngineMBeanName(
                compName, 
                MBeanNames.ComponentServiceType.Configuration, 
                target);
        }
        
        try
        {
            if ( mMBeanSvr.isRegistered(facadeCompCfgMBean) )
            {
                mMBeanSvr.unregisterMBean(facadeCompCfgMBean);
            }
        }
        catch(Exception jex)
        {
            String msg = mMsgBuilder.buildExceptionMessage(
                "unregisterComponentConfigurationMBean", jex);
            throw new ManagementException(msg);
        }
        
        // -- If target is a clustered instance unregister each
        //    cluster instance config MBean
        Set<String> clusterInstances = new java.util.HashSet();
        if ( mPlatform.isClusteredServer(target))
        {
            clusterInstances = mPlatform.getServersInCluster(target);

        }        
        for ( String clusterInstance : clusterInstances )
        {
            facadeCompCfgMBean = null;
            if ( compType == ComponentType.BINDING )
            {
                facadeCompCfgMBean = mMBeanNames.getBindingMBeanName(
                    compName, 
                    MBeanNames.ComponentServiceType.Configuration, 
                    clusterInstance);
            }
            else
            {
                facadeCompCfgMBean = mMBeanNames.getEngineMBeanName(
                    compName, 
                    MBeanNames.ComponentServiceType.Configuration, 
                    clusterInstance);
            }

            try
            {
                if ( mMBeanSvr.isRegistered(facadeCompCfgMBean) )
                {
                    mMBeanSvr.unregisterMBean(facadeCompCfgMBean);
                }
            }
            catch(Exception jex)
            {
                String msg = mMsgBuilder.buildExceptionMessage(
                    "unregisterComponentConfigurationMBean", jex);
                throw new ManagementException(msg);
            }
        }
    }
    
    /**
     * @throw ManagementMessage with a unsupported operation message
     */
    protected void throwNotSupportedManagementException(String taskId)
        throws ManagementException
    {
        String [] params = new String[]{taskId, mTarget};
        mMsgBuilder.throwManagementException(taskId, 
            LocalStringKeys.JBI_ADMIN_UNSUPPORTED_OPERATION, params);
    }
    
    /**
     * Initialize the data structure which maps primitive types to their wrapper classes
     */
    protected static void initPrimitiveTypeMap() 
    {
       sTypeMap = new HashMap();
       sTypeMap.put("int"    ,"java.lang.Integer");
       sTypeMap.put("float"  ,"java.lang.Float");
       sTypeMap.put("long"   ,"java.lang.Long");
       sTypeMap.put("double" ,"java.lang.Double");
       sTypeMap.put("byte"   ,"java.lang.Byte");
       sTypeMap.put("char"   ,"java.lang.Character");
       sTypeMap.put("boolean","java.lang.Boolean");
       sTypeMap.put("short"  ,"java.lang.Short");
    } 
    
    /**
     * Convert the objects in the collection to string.
     *
     * @param set a non-empty set
     */
    protected String convertToString(java.util.Collection colxn)
    {
        StringBuffer strBuf = new StringBuffer("[ ");
        if ( !colxn.isEmpty() )
        {
            java.util.Iterator itr = colxn.iterator();
            while( itr.hasNext() )
            {
                strBuf.append(itr.next().toString());
                if ( itr.hasNext() )
                {
                    strBuf.append(",  ");
                }
                else
                {
                    strBuf.append("  ");
                }
            }
        }
        strBuf.append(" ]");
        return strBuf.toString();
    }    
    
    /**
     * @return a list of all the targets in the domain
     */
    protected java.util.List<String> getAllTargets()
        throws ManagementException
    {
        java.util.List<String> targets = new java.util.ArrayList<String>();
        try
        {
            targets.addAll(getGenericQuery().getServers());
            targets.addAll(getGenericQuery().getClusters());
        }
        catch ( RegistryException ex )
        {
            throw new ManagementException(mMsgBuilder.buildExceptionMessage(
                "getAllTargets", ex));
        }
        
        return targets;
    }
    
    /**
     * @return the ValidatorType based on the archive type
     */
    private ValidatorType getValidatorType(ArchiveType arType)
    {
        ValidatorType type = null;
        
        if( arType.equals(ArchiveType.COMPONENT) )
        {
           type = ValidatorType.COMPONENT;
        }
        else if (arType.equals(ArchiveType.SHARED_LIBRARY) )
        {
            type = ValidatorType.SHARED_LIBRARY;
        }
        else if (arType.equals(ArchiveType.SERVICE_ASSEMBLY) )
        {
            type = ValidatorType.SERVICE_ASSEMBLY;
        }
        else if (arType.equals(ArchiveType.SERVICE_UNIT) )
        {
            type = ValidatorType.SERVICE_UNIT;
        }
        
        return type;
    }
    
    /**
     * Check if two files are identical.If the archives are not identical a 
     * Management exception is thrown indicating a different archive exists in the
     * domain.
     * 
     * @param f1 - file to compare
     * @param f2 - file to compare
     * @param type - type of archives
     * @param entityName - component / shared library / service assembly name
     * @throws ManagementException if the two archives are not identical.
     *
     */
    protected void archiveEqualityCheck(File f1, File f2, ArchiveType type, String entityName)
        throws ManagementException
    {
        boolean areArchivesIdentical = false;
        String token = "UNKNOWN";
        if ( ArchiveType.COMPONENT.equals(type) )
        {
            token = LocalStringKeys.JBI_ADMIN_DIFFERENT_COMPONENT_ARCHIVE_EXISTS;
        }
        else if ( ArchiveType.SHARED_LIBRARY.equals(type) )
        {
            token = LocalStringKeys.JBI_ADMIN_DIFFERENT_SHARED_LIBRARY_ARCHIVE_EXISTS;
        }
        else if ( ArchiveType.SERVICE_ASSEMBLY.equals(type) )
        {
            token = LocalStringKeys.JBI_ADMIN_DIFFERENT_SERVICE_ASSEMBLY_ARCHIVE_EXISTS;
        }
        
        try
        {
            areArchivesIdentical = FileHelper.areFilesIdentical(f1, f2);
        }
        catch ( java.io.IOException ioex)           
        {
            mLog.fine(MessageHelper.getMsgString(ioex));
        }
        if ( !areArchivesIdentical )
        {
            String[] params = new String[]{entityName};
            String errMsg = mTranslator.getString(
                token,
                params);
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                "archiveEqualityCheck", 
                MessageBuilder.TaskResult.FAILED, 
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params,
                mMsgBuilder.getMessageToken(errMsg));
            throw new ManagementException(jbiTaskMsg);
        }
    }
    
    /**
     * Get the namespace of the "Configuration" element in the jbi.xml, if one 
     * exists. Return an empty string if a "Configuration" emenet is not 
     * defined.
     *
     * @param compName - component name
     * @return the configuration element namespace
     */
    protected String getComponentConfigurationNS(String compName)
    {
        String ns = "";
        
        Archive compArchive = getRegistry().getRepository().
                getArchive(ArchiveType.COMPONENT, compName);
        if ( compArchive != null )
        {
            Jbi jbi = compArchive.getJbiXml(true);
            ComponentDescriptor descr = new ComponentDescriptor(jbi);
            ns = descr.getComponentConfigurationNS();
        }
        
        return ns;
    }
    
    /**
     * Get the the "Configuration" element in the jbi.xml. If the Configuration 
     * element is missing in the jbi.xml a null value is returned
     *
     * @param compName - component name
     * @return the configuration xml string
     */
    protected String getComponentConfigurationData(String compName)
    {
        String xmlStr = null;
        
        Archive compArchive = getRegistry().getRepository().
                getArchive(ArchiveType.COMPONENT, compName);
        if ( compArchive != null )
        {
            Jbi jbi = compArchive.getJbiXml(true);
            ComponentDescriptor descr = new ComponentDescriptor(jbi);
            xmlStr = descr.getComponentConfigurationXml();
        }
        
        return xmlStr;
    }
    
    /**
     * Get the the "Configuration" element in the jbi.xml. If the Configuration 
     * element is missing in the jbi.xml a null value is returned
     *
     * @param compName - component name
     * @return the configuration schema string
     * @htrows ManagementException if the schema file cannot be located.
     */
    protected String getComponentConfigurationSchema()
        throws ManagementException
    {
        synchronized(this)
        {
            if ( mComponentConfigSchema == null )
            {
                mComponentConfigSchema = readConfigSchema();
            }
        }
        
        return mComponentConfigSchema;
        
    }
    
    /**
     * Read the configuration schema packaged with the runtime. The schema is 
     * installed in ${JBI_INSTALL_ROOT}/schemas/
     *
     * @return the component configuration schema read into a String value.
     * @htrows ManagementException if the schema file cannot be located.
     */
    private String readConfigSchema()
        throws ManagementException
    {
        String schema = "";
        String installRoot = mEnvCtx.getJbiInstallRoot();
        
        File componentConfigSchema = new File(installRoot, CONFIG_SCHEMA_PATH);
        
        if ( componentConfigSchema.exists() )
        {
            try
            {
                schema = FileHelper.readFile(componentConfigSchema);
            }
            catch(Exception ioex)
            {
                String msg = mMsgBuilder.buildExceptionMessage(
                    "readConfigSchema", ioex);
                throw new ManagementException(msg);
            }
        }
        else
        {
            String[] params = new String[]{
                    componentConfigSchema.getAbsolutePath()};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_MISSING_CONFIG_XSD_FILE,
                params);
            String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                "readConfigSchema", 
                MessageBuilder.TaskResult.FAILED, 
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params,
                mMsgBuilder.getMessageToken(errMsg));
            throw new ManagementException(jbiTaskMsg);
            
        }
        return schema;
    }
    

}
