/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Installer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.management.MBeanNames;


import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.management.util.FacadeMbeanHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.jbi.JBIException;
import javax.management.Attribute;
import javax.management.MBeanInfo;
import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.StandardMBean;

/**
 * The InstallerMBean defines standard installation and uninstallation controls 
 * for components. InstallerMBeans are created by the 
 * {@link InstallationServiceMBean}. The InstallerMBean offers controls to
 * allow an administrative tool to:
 * <ul>
 *   <li>Install the component from the installation package.</li>
 *   <li>Uninstall the component.</li>
 *   <li>Check the installation status of the component.</li>
 *   <li>Get the file path to the component's installation root directory.</li>
 * </ul>
 *
 * @author Sun Microsystems, Inc.
 */
public class Installer
    extends Facade
    implements com.sun.jbi.management.InstallerMBean
{
    private String                          mComponentName;
    private ComponentType                   mComponentType;
    private Map<String, ObjectName>         mInstanceInstallers;
    private InstallerConfiguration          mInstallerConfig;
    
    /**
     * @param instanceInstallers - a map of instance names and their Installer ObjectNames
     */
     public Installer(EnvironmentContext ctx, String target, 
        String compName, Map instanceInstallers)
        throws ManagementException
    {
        super(ctx, target);
        mComponentName         = compName;
        mInstanceInstallers    = instanceInstallers;
        mComponentType         = getComponentType();

    }

    public Installer(EnvironmentContext ctx, String target, String compName)
        throws ManagementException
    {
        this(ctx, target, compName, null);
    }
    
    /**
     * Get the installer configuration MBean name for this component.
     * Initialization of the facade configuration MBean is performed on-demand,
     * so this method is synchronized to prevent against duplicate init in the
     * face of concurrent access.
     * 
     * @return the MBean object name of the Installer Configuration MBean; 
     *         <code>null</code> if none is provided by this component
     * @exception javax.jbi.JBIException if the component is not in the 
     *            appropriate state (after install() but before life cycle
     *            initialization), or if any error occurs during processing
     */
    public synchronized ObjectName getInstallerConfigurationMBean() 
        throws javax.jbi.JBIException
    {
        // Check to see if we already have a config MBean ready
        if (mInstallerConfig != null && 
            mMBeanSvr.isRegistered(mInstallerConfig.getObjectName()))
        {
            return mInstallerConfig.getObjectName();
        }
        
        // Looks like we need to create a new one
        try
        {
            ObjectName configMBean = null;
            if ( DOMAIN.equals(mTarget))
            {
                throwNotSupportedManagementException("getInstallerConfigurationMBean");
            }
            
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                // In this case target name and instance name are the same
                configMBean = createInstanceConfigurationMBean();
            }
            else
            {
                configMBean = createClusterConfigurationMBean();
            }
            
            return configMBean;
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }

    /**
     * Get the installation root directory path for this component.
     * 
     * @return the full installation path of this component; this must be in
     *         absolute path name form, in platform-specific format; must be
     *         non-null and non-empty
     */
    public String getInstallRoot()
    {
        
        String installRoot = "";
        try
        {
            if ( DOMAIN.equals(mTarget))
            {
                throwNotSupportedManagementException("getInstallRoot");
            }
            else
            {
                ComponentInfo compInfo = getComponentQuery().getComponentInfo(mComponentName);
                if ( compInfo != null )
                {
                    installRoot = compInfo.getInstallRoot();
                }
            }
        }
        catch(ManagementException mex)
        {
            throw new RuntimeException(mex.getMessage());
        }
        return installRoot;
    }

    /**
     * Install a component.
     * <p>
     * Note that the implementation must leave the component in its
     * installed, shutdown state. Automatic starting of components during
     * installation by implementations is not allowed.
     * 
     * </br>
     * For the domain target this will only install the component to the domain, 
     * the return value would be null, since there is no LifeCycle MBean for
     * the installed component.
     * 
     * @return JMX ObjectName representing the LifeCycleMBean for the installed
     *         component, or <code>null</code> if the installation did not 
     *         complete
     * @exception javax.jbi.JBIException if the installation fails
     */
    public ObjectName install() throws javax.jbi.JBIException
    {
        ObjectName ComponentLCObjName = null;
        try
        {
            if ( mTarget.equals(DOMAIN))
            {
                ComponentLCObjName = registerComponentLifeCycleMBean(mTarget, "install");
                registerComponentExtensionMBean(DOMAIN);
            }
            else
            {
                ComponentLCObjName = installComponentToTarget();
                /**
                 * Now that the component is installed to the target, it is implicitly
                 * installed to the domain too, need to register the ComponentLifeCycle
                 * MBean and ComponentExtensionMBean for the domain target
                 */
                registerComponentExtensionMBean(DOMAIN);
                registerComponentLifeCycleMBean(DOMAIN, "install");
            }
        }
        catch(Exception ex)
        {
            String errMsg = ex.getMessage();
            
            if ( !(ex instanceof ManagementException) )
            {
                try
                {
                    errMsg = mMsgBuilder.buildExceptionMessage("install", ex);
                } catch(Exception exp) {};
            }
            throw new javax.jbi.JBIException(errMsg);
        }
        return ComponentLCObjName;
    }

    /**
     * Determine whether or not the component is installed.
     * 
     * @return <code>true</code> if this component is currently installed, 
     *         otherwise <code>false</code>
     */
    public boolean isInstalled()
    {
        if ( DOMAIN.equals(mTarget))
        {
            return false;
        }
        else
        {
            try
            {
                if ( mPlatform.isStandaloneServer(mTarget) )
                {
                    List<String> servers = getGenericQuery().getServersInstallingComponent(mComponentName);

                    return ( servers.contains(mTarget) );
                }
                else
                {
                    List<String> clusters = getGenericQuery().getClustersInstallingComponent(mComponentName);

                    return ( clusters.contains(mTarget) );
                }
            }
            catch(JBIException ex)
            {
                throw new RuntimeException(ex.getMessage());
            }
        }
    }

    /**
     * Uninstall the component. This completely removes the component from the
     * JBI system.
     * 
     * @exception javax.jbi.JBIException if the uninstallation fails
     */
    public void uninstall() throws javax.jbi.JBIException
    {
        uninstall(false);
    }
    
    
    /**
     * Uninstall the component. This completely removes the component from the
     * JBI system. If the <code>force</code> flag is set to <code>true</code>,
     * the uninstall operation succeeds regardless of any exception thrown by
     * the component itself.
     *
     * @param force set to true to ignore any failures and proceed with the
     * uninstall.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    public void uninstall(boolean force) throws javax.jbi.JBIException
    {
        try
        {
            if (DOMAIN.equals(mTarget))
            {
                unregisterComponentExtensionMBean(mTarget, mComponentName, mComponentType);
                unregisterComponentLifeCycleMBean(mTarget, mComponentName, mComponentType);
            }
            else
            {
                uninstallComponentFromTarget(force);
            }
        }
        catch( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                          Private Helpers                                         *
    \*----------------------------------------------------------------------------------*/
    /**
     * Register the Facade ComponentExtensionMBean for the Component.
     *
     * @throws ManagementException on errors
     */
    private ObjectName registerComponentExtensionMBean()
        throws ManagementException
    {
        return registerComponentExtensionMBean(mTarget);
    }
        
    /**
     * Register the Facade ComponentExtensionMBean for the Component.
     *
     * @throws ManagementException on errors
     */
    private ObjectName registerComponentExtensionMBean(String target)
        throws ManagementException
    {
        try
        {
            ObjectName facadeCompExtMBean = null;
            if ( mComponentType == ComponentType.BINDING )
            {
                facadeCompExtMBean = mMBeanNames.getBindingMBeanName(
                    mComponentName, 
                    MBeanNames.ComponentServiceType.Extension, 
                    target);
            }
            else
            {
                facadeCompExtMBean = mMBeanNames.getEngineMBeanName(
                    mComponentName, 
                    MBeanNames.ComponentServiceType.Extension, 
                    target);
            }
            if ( !mMBeanSvr.isRegistered(facadeCompExtMBean) )
            {
                
                com.sun.jbi.management.ComponentExtensionMBean 
                    componentInfo = new ComponentExtension(mEnvCtx, target, 
                        mComponentName, mComponentType);
                StandardMBean mbean = new StandardMBean(componentInfo,
                    com.sun.jbi.management.ComponentExtensionMBean.class); 
                mMBeanSvr.registerMBean(mbean, facadeCompExtMBean);
            }
                
            return facadeCompExtMBean; 
        }
        catch ( Exception ex )
        {
            String [] params = new String[]{mComponentName, ex.getMessage()};
            mMsgBuilder.throwManagementException("registerComponentExtensionMBean", 
                LocalStringKeys.JBI_ADMIN_FAILED_REGISTER_COMPONENT_MBEAN, params);
            
            // never reached
            return null;
        }
    }
    
    /**
     * Register the Facade Component  Configuration MBean for the Component.
     *
     * @throws ManagementException on errors
     */
    private void registerComponentConfigurationMBean()
        throws ManagementException
    {
        registerComponentConfigurationMBean(mTarget);
    }
        
    /**
     * Register the Facade ComponentConfigurationMBean for the Component.
     *
     * @throws ManagementException on errors
     */
    private void registerComponentConfigurationMBean(String target)
        throws ManagementException
    {
        try
        {
            ObjectName facadeCompCfgMBean = null;
            if ( mComponentType == ComponentType.BINDING )
            {
                facadeCompCfgMBean = mMBeanNames.getBindingMBeanName(
                    mComponentName, 
                    MBeanNames.ComponentServiceType.Configuration, 
                    target);
            }
            else
            {
                facadeCompCfgMBean = mMBeanNames.getEngineMBeanName(
                    mComponentName, 
                    MBeanNames.ComponentServiceType.Configuration, 
                    target);
            }


            if ( !mMBeanSvr.isRegistered(facadeCompCfgMBean) )
            {
                ComponentConfiguration componentCfg = 
                    new ComponentConfiguration(mEnvCtx, target, 
                        mComponentName, mComponentType);

                mMBeanSvr.registerMBean(componentCfg, facadeCompCfgMBean);

            }
            
            // If its a cluster target - register cluster instance config MBeans
            Set<String> clusterInstances = new java.util.HashSet<String>();
            if ( mPlatform.isCluster(target))
            {
                clusterInstances = mPlatform.getServersInCluster(target);
                
            }
           
            for ( String clusterInstance : clusterInstances )
            {
                facadeCompCfgMBean = null;
                if ( mComponentType == ComponentType.BINDING )
                {
                    facadeCompCfgMBean = mMBeanNames.getBindingMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.Configuration, 
                        clusterInstance);
                }
                else
                {
                    facadeCompCfgMBean = mMBeanNames.getEngineMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.Configuration, 
                        clusterInstance);
                }


                if ( !mMBeanSvr.isRegistered(facadeCompCfgMBean) )
                {
                    ClusterInstanceComponentConfiguration componentCfg = 
                        new ClusterInstanceComponentConfiguration(mEnvCtx, clusterInstance, 
                            target, mComponentName, mComponentType);

                    mMBeanSvr.registerMBean(componentCfg, facadeCompCfgMBean);

                }
            }
        }
        catch ( Exception ex )
        {
            String [] params = new String[]{mComponentName, ex.getMessage()};
            mMsgBuilder.throwManagementException("registerComponentConfigurationMBean", 
                LocalStringKeys.JBI_ADMIN_FAILED_REGISTER_COMPONENT_MBEAN, params);
        }
    }
    
    /**
     * Register the domain Facade ComponentLifeCycleMBean for the Component.
     *
     * @param target - the target property for the MBean
     * @param taskId - the name of the operation being invoked.
     */
    private ObjectName registerComponentLifeCycleMBean(String target, String taskId)
        throws JBIException
    {
        return registerComponentLifeCycleMBean(target, taskId, null);
    }
    
    /**
     * Register the Facade ComponentLifeCycleMBean for the Component.
     *
     * @param instanceComponentLCMBean - Object Name of the ComponentLifeCycle for the 
     * component, which this InstallerMBean is a proxy to.
     */
    private ObjectName registerComponentLifeCycleMBean(String target,
        String taskId, Map instanceComponentLCMBeans)
        throws ManagementException
    {
        try
        {
            ObjectName 
                compLCName = FacadeMbeanHelper.getComponentLifeCycleFacadeMBeanName(mComponentName, 
                    mComponentType, target, mEnvCtx.getMBeanNames());
            
            if ( !mMBeanSvr.isRegistered(compLCName) )
            {
                
                com.sun.jbi.management.ComponentLifeCycleMBean
                    componentLC = new ComponentLifeCycle(mEnvCtx, target, 
                        mComponentName, mComponentType, instanceComponentLCMBeans);
                StandardMBean mbean = new StandardMBean(componentLC,
                    com.sun.jbi.management.ComponentLifeCycleMBean.class); 
                mMBeanSvr.registerMBean(mbean, compLCName);
            }
                
            return compLCName; 
        }
        catch ( Exception ex )
        {
            String [] params = new String[]{mComponentName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, 
                LocalStringKeys.JBI_ADMIN_FAILED_REGISTER_COMPONENT_MBEAN, params);
            
            // never reached
            return null;
        }
    }
    
    /**
     * If the target is a standalone server instance, first the instances
     * install() operation is invoked, the object name of the returned ComponentLifeCycle
     * MBean is passed to the proxy ComponentLifeCycleMBean which is loaded for the instance.
     * The proxy facade ComponentLifeCycle is then registered in the DAS.
     *
     * If the target is a cluster, for each instance in the cluster the install() 
     * operation is invoked, the object names of the returned ComponentLifeCycle
     * MBeans is passed to the proxy ComponentLifeCycle which is loaded for the cluster.
     * The proxy facade ComponentLifeCycle is then registered in the DAS.
     */
    private ObjectName installComponentToTarget()
        throws ManagementException
    {
        componentInstallationCheck(mComponentName, true);
        
        mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_INSTALLING_COMPONENT_TO_TARGET,
            mComponentName, mTarget));
        
        ObjectName remoteComponentLCObjName = null;
        Map<String, ObjectName> instanceComponentLCMBeans;
        
        if (mInstallerConfig != null)
        {
            // We need to prevent any futher updates to the config
            mInstallerConfig.setReadOnly();
        }
        
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            remoteComponentLCObjName =  installComponentToInstance(mTarget);
            instanceComponentLCMBeans = new HashMap<String, ObjectName>();
            instanceComponentLCMBeans.put(mTarget, remoteComponentLCObjName);
        }
        else
        {
            instanceComponentLCMBeans = installComponentToCluster(mTarget);
        }
        addComponentToTarget();
        registerComponentExtensionMBean();
        registerComponentConfigurationMBean();
        return registerComponentLifeCycleMBean(mTarget, "loadNewInstaller", 
            instanceComponentLCMBeans);
    }
    
    /**
     * Invoke install() on the remote instance's Installer MBean for the component
     * and return the ComponentLifeCycleMBean Object Name returned by the remote call.
     */
    private ObjectName installComponentToInstance(String serverName) 
        throws ManagementException
    {
        ObjectName remoteComponentLCObjName = null;
        
        if ( isInstanceRunning(serverName) )
        {
            // See if we need to configure the installer
            if (mInstallerConfig != null && mInstallerConfig.getAttributeCount() > 0)
            {
                configureInstanceInstaller(serverName);
            }
            
            String[] sign   = new String[0];
            Object[] params = new Object[0];
        
            remoteComponentLCObjName = (ObjectName) invokeRemoteOperation(
                mInstanceInstallers.get(serverName),  
                "install", params, sign, serverName); 
        }
        return remoteComponentLCObjName;
    }
    
    /**
     * Install the component to each and every instance in the cluster, return a
     * Hashmap of the instance names and the ObjectName of the ComponentLifeCycleMBean
     * on the instance.
     * </br>
     * Partial Success : If installation to any instance in the cluster fails, a warning
     * is logged for it, and the installation continues for the remaining instances. 
     * A value of null is added to the HashMap for the object name.
     * </br>
     * Complete failure : All the runtime execptions thrown by the remote instance
     * are composed into a management task message and a management exception is 
     * thrown with the composite message.
     */
    private Map<String, ObjectName> installComponentToCluster(String clusterName)
        throws ManagementException
    {
        HashMap<String, ObjectName> componentLCMBeans = new HashMap<String, ObjectName>();
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String,Throwable>();
            for ( String instance : instances )
            {
               
                try
                {
                    ObjectName componentLCMBean = null;
                    if ( canPerformOperationOnInstanceCheck(instance, "install") )
                    {
                        componentLCMBean = installComponentToInstance(instance);
                    }
                    componentLCMBeans.put(instance, componentLCMBean);
                    
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    componentLCMBeans.put(instance, null);
                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("install", 
                    exceptionMap,     
                    instances.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_INSTALL_COMPONENT_TO_INSTANCE);
            }
        }
        return componentLCMBeans;
    }
    
    
    /**
     * @return the type of this Component
     */
    private ComponentType getComponentType()
        throws ManagementException
    {
        ComponentType compType;
        try
        {
            compType = getGenericQuery().getComponentType(mComponentName);
        }
        catch(Exception rex)
        {
            throw new ManagementException(
                mMsgBuilder.buildExceptionMessage("getComponentType", rex));
        }
        return compType;
    }

    
    /**
     * Uninstall component from the target. The component has to be installed on the
     * target. Moreover the component should not have any deployed service units 
     * for the operation to succeed.
     * 
     * @param force - if this flag is set to true the component is forcefully uninstalled
     * from the target. Any exceptions thrown by the component are to be ignored.
     *
     * @throws JBIException if installation fails.
     */
    private void uninstallComponentFromTarget(boolean force)
        throws JBIException
    {
        componentInstallationCheck(mComponentName, false);
        
        dependentServiceAssemblyCheck();
   
        mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_UNINSTALLING_COMPONENT_FROM_TARGET,
            mComponentName, mTarget));
        
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            uninstallComponentFromInstance(mTarget, force);
        }
        else
        {
            uninstallComponentFromCluster(mTarget, force);
        }

        removeComponentFromTarget();
        unregisterComponentExtensionMBean(mTarget, mComponentName, mComponentType);
        unregisterComponentLifeCycleMBean(mTarget, mComponentName, mComponentType);
        unregisterComponentConfigurationMBean(mTarget, mComponentName, mComponentType);
    }
    
    
    /**
     * Uninstall the component from an instance. 
     *
     * @param serverName - instance name
     * @param force - set to true indicates that the component should be forcefully 
     * uninstalled. If set to true the uninstall force operation is invoked on the
     * ComponentLifeCycleMBean registered on the instance.
     */
    private void uninstallComponentFromInstance(String serverName, boolean force)
        throws ManagementException
    {
        // Check: Target component should not be started
        componentStartedCheck(mComponentName, serverName);
        
        if ( isInstanceRunning(serverName) )
        {
            // Invoke the operation on the real Installer
            String[] sign;
            Object[] params;
            if ( force )
            {
                sign   = new String[]{"boolean"};
                params = new Object[]{new Boolean(force)};
            }
            else
            {
                sign   = new String[0];
                params = new Object[0];
            }

            invokeRemoteVoidOperation(
                mInstanceInstallers.get(serverName),  
                "uninstall", params, sign, serverName);
        }
    }
    
    /**
     * Uninstall the component from a cluster. The component is uninstalled from 
     * each and every instance in the cluster.
     *
     * @param clusterName - cluster id
     * @param force - flag if set to true indicates that the component should be 
     * forcefully uninstalled from the cluster
     */
    private void uninstallComponentFromCluster(String clusterName, boolean force)
        throws ManagementException
    {
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    if ( canPerformOperationOnInstanceCheck(instance, "uninstall") )
                    {
                        uninstallComponentFromInstance(instance, force);
                    }
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    continue;
                }      
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("uninstallComponent", 
                    exceptionMap,   
                    /** This is the going down case. Uninstall is a success if the
                     * component is uninstalled from all instances in the cluster.
                     * Even a single exception = failure
                     */
                    exceptionMap.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_UNINSTALL_COMPONENT_FROM_INSTANCE);
            }
        }
    }
    
    /**
     * Create a facade configuration MBean for this target, if the component
     * provides one.
     */
    private ObjectName createClusterConfigurationMBean()
        throws ManagementException
    {
        String     instanceName = null;
        ObjectName facadeMBean = null;
        ObjectName configMBean = null;
        
        // Need to ask a running instance in the cluster for the component
        // configuration MBean.
        for (String instance : mPlatform.getServersInCluster(mTarget))
        {
            if (isInstanceRunning(instance))
            {
                instanceName = instance;
                break;
            }
        }
        
        // If we have a running cluster instance, use it to create the config MBean
        if (instanceName != null)
        {
            // Get the config MBean object name
            configMBean = getConfigurationMBeanForInstance(instanceName);

            // Register a facade config MBean only if the component provides one
            if (configMBean != null)
            {
                facadeMBean = createInstallerConfigurationMBean(instanceName, configMBean);
            }
        }
        else
        {            
            // No instances in the cluster are up, so we have to create a dumb 
            // config MBean that accepts any configuration attributes.
            facadeMBean = createInstallerConfigurationMBean(mTarget, null);
        }
                
        return facadeMBean;
    }
    
    /**
     * Create a facade configuration MBean for this target, if the component
     * provides one.
     */
    private ObjectName createInstanceConfigurationMBean()
        throws ManagementException
    {
        ObjectName facadeMBean = null;
        ObjectName configMBean = null;

        if ( isInstanceRunning(mTarget) )
        {
            configMBean = getConfigurationMBeanForInstance(mTarget);
            
            // Register a facade config MBean only if the component provides one
            if (configMBean != null)
            {
                facadeMBean = createInstallerConfigurationMBean(mTarget, configMBean);
            }
        }
        else
        {
            // The target isn't up, so we have to create a dumb config MBean
            // that accepts any configuration attributes.
            facadeMBean = createInstallerConfigurationMBean(mTarget, null);
        }

        return facadeMBean;
    }
    
    /**
     * Get the Component Extension MBean for an instance.
     *  @param instanceName name of the instance to query for an installer config
     *   MBean.
     */
    private ObjectName getConfigurationMBeanForInstance(String instanceName)
        throws ManagementException
    {
        ObjectName configMBean = null;
        String op = "getInstallerConfigurationMBean";
        if ( isInstanceRunning(instanceName) )
        {
            // Invoke the operation on the real Installer
            String[] sign   = new String[0];
            Object[] params = new Object[0];

            configMBean  = (ObjectName) invokeRemoteOperation(
                mInstanceInstallers.get(instanceName),  
                op, params, sign, instanceName);
            
            if ( configMBean != null )
            {
                // Double check if the configuration MBean is actually registered
                

                if ( !isMBeanRegisteredOnInstance(configMBean, instanceName) )
                {
                    String[] msgParams = new String[]{configMBean.toString(), instanceName};
                    String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_COMPONENT_INSTALLER_CONFIG_MBEAN_NOT_REGISTERED, 
                        msgParams);

                    String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(op,
                        MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                        mMsgBuilder.getMessageString(errMsg), msgParams, 
                        mMsgBuilder.getMessageToken(errMsg));

                    throw new ManagementException(jbiTaskMsg);
                }
               
            }
        }

        return configMBean;
    }
    
    /**
     * Update the DAS registry : add component info to the target entry.
     *
     * The DAS jbi-registry is updated when a action is partial/full success or when 
     * the instance is not up.
     */
    private void addComponentToTarget()
        throws ManagementException
    {
        try
        {
            if ( !mTarget.equals(mPlatform.getAdminServerName()) )
            {
                ComponentQuery compQuery = getRegistry().getComponentQuery("domain");
                ComponentInfoImpl compInfo = (ComponentInfoImpl) 
                    compQuery.getComponentInfo(mComponentName);

                compInfo.setStatus(com.sun.jbi.ComponentState.SHUTDOWN);        
                compInfo.setInstallRoot(getComponentInstallRoot());
                compInfo.setWorkspaceRoot(getComponentWorkspaceRoot());
                
                // set config properties, if available
                if (mInstallerConfig != null)
                {
                    for (Object obj : mInstallerConfig.getAttributeList())
                    {
                        Attribute attr = (Attribute)obj;
                        String attrValue = null;
                        if ( attr.getValue() != null )
                        {
                            attrValue = attr.getValue().toString();
                        }
                        compInfo.setProperty(attr.getName(), attrValue);
                    }
                }
                
                getUpdater().addComponent(mTarget, compInfo);
            }
        }
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("addComponentToTarget", rex);
            throw new ManagementException(errMsg);
        }  
    }
    
    /**
     * Update the DAS registry : remove a component from the target
     *
     * The DAS jbi-registry is updated when a action is partial/full success or when 
     * the instance is not up.
     */
    private void removeComponentFromTarget()
        throws ManagementException
    {
        try
        {
            if ( !mTarget.equals(mPlatform.getAdminServerName()) )
            {
                getUpdater().removeComponent(mTarget, mComponentName);
            }
        }
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage(
                "removeComponentFromTarget", rex);
            throw new ManagementException(errMsg);
        }
    }
    
    /**
     * @return the ComponentInstallRoot for a non-system component
     */
    private String getComponentInstallRoot()
    throws RegistryException, ManagementException
    {
        Archive archive =             
            getRegistry().getRepository().getArchive(
                ArchiveType.COMPONENT, 
                mComponentName);
        
        String archivePath = archive.getPath();
        
        return new File(archivePath).getParent();
    }
    
    /**
     * @return the ComponentWorkspaceRoot for a component
     */
    private String getComponentWorkspaceRoot()
    throws RegistryException, ManagementException
    {
        StringBuffer strBuf = new StringBuffer(getComponentInstallRoot());
        strBuf.append(java.io.File.separator);
        strBuf.append(WORKSPACE_ROOT);
        return strBuf.toString();
    }
    
    /**
     * The Installer MBean ObjectName can be null for an instance, this occurs when 
     * an instance is down when load*Installer is invoked on the cluster taregt. 
     * In this case the operation cannot be performed on the instance.
     */
    private boolean canPerformOperationOnInstanceCheck(String instanceName, String taskId)
    {
        boolean canDo = true;
        ObjectName installer = mInstanceInstallers.get(instanceName);
        
        if ( installer == null )
        {
            String warning = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CANNOT_PERFORM_COMPONENT_INSTALLATION_OP,
                taskId, mComponentName, instanceName);
            
            mLog.fine(warning);
            canDo = false;
        }
        
        return canDo;
    }
    
    /**
     * Check if there are any service units deployed to the component. If there are then
     * an exception is thrown with detailed information on the deployed service units.
     */
    private void dependentServiceAssemblyCheck()
        throws ManagementException
    {
        ComponentInfo compInfo = getComponentQuery().getComponentInfo(mComponentName);
        
        List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();
        
        List<String> sus = new ArrayList();
        if (!suList.isEmpty())
        {       
            for ( ServiceUnitInfo suInfo : suList )
            {
                sus.add(suInfo.getName());
            }   

            
            String[] params = new String[]{mComponentName, listToString(sus)};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CANNOT_UNINSTALL_COMPONENT_WITH_DEPLOYED_SUS, 
                params);

            String response = mMsgBuilder.buildFrameworkMessage(
                "dependentServiceAssemblyCheck",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), params, 
                mMsgBuilder.getMessageToken(errMsg));

            throw new ManagementException(response);
        }
    }
    
    /** Creates and registers an installer configuration MBean for a component
     *  on the DAS.  Only one config MBean is registered per target, so a 
     *  cluster with three instances will only have one config MBean.
     *  @param targetName name of the the target that the facade config MBean
     *   will be registered under
     *  @param instanceName name of the instance that contains an instance of
     *   the config MBean.  The instance is contacted to get a copy of the 
     *   MBeanInfo for the config MBean. 
     *  @param configMBean the name of the configuration MBean on the instance.
     *  @return name of the configuration MBean on the DAS
     */
    private ObjectName createInstallerConfigurationMBean(
            String instanceName, ObjectName configMBean)
            throws ManagementException
    {
        ObjectName mbName;
        MBeanInfo  mbInfo = null;
        
        try
        {
            // Use the MBeanInfo from the actual config MBean in the instance, 
            // if it's available.
            if (configMBean != null)
            {
                mbInfo = getMBeanServerConnection(
                    instanceName).getMBeanInfo(configMBean);
            }

            // Create a name for the facade config MBean on the DAS
            mbName = mMBeanNames.getComponentMBeanName(mComponentName, 
                    mComponentType, MBeanNames.ComponentServiceType.InstallerConfiguration, mTarget);

            // Create and register the facade MBean
            mInstallerConfig = new InstallerConfiguration(mbInfo, mbName);
            mMBeanSvr.registerMBean(mInstallerConfig, mbName);
        }
        catch (javax.management.JMException jmEx)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(jmEx);
            throw new ManagementException(
                    "getInstallerConfigurationMBean", actualEx);
        }
        catch (java.io.IOException ioEx)
        {
            throw new ManagementException(mMsgBuilder.buildExceptionMessage(
                    "getInstallerConfigurationMBean", ioEx));
        }
        
        return mbName;
    }
    
    /** Applies any attributes set on the facade configuration MBean to the
     *  installer configuration MBean on the instance.
     *  @param instanceName name of the instance where config should be applied
     */
    private void configureInstanceInstaller(String instanceName)
        throws ManagementException
    {        
        try
        {
            // Looks like we have some config to apply
            getMBeanServerConnection(instanceName).setAttributes(
                    getConfigurationMBeanForInstance(instanceName),
                    mInstallerConfig.getAttributeList());
        }                
        catch (javax.management.JMException jmEx)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(jmEx);
            throw new ManagementException("install", actualEx);
        }
        catch (java.io.IOException ioEx)
        {
            throw new ManagementException(mMsgBuilder.buildExceptionMessage(
                    "install", ioEx));
        }
    }
    
    /**
     * Checks if the component is started, this is a required check for uninstall.
     * @param compName - name of the component on the instance
     * @param instanceName - instance on which the component state is to be checked.
     * @throws a ManagementException if Component is started
     */
    private void  componentStartedCheck(String compName, String instanceName)
        throws ManagementException
    {
        ComponentState compState = getComponentState(compName, instanceName);

        if ( ComponentState.STARTED == compState )
        {
            String[] params = new String[]{compName, instanceName};
            
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_COMPONENT_STARTED, params);

            String jbiMsg = mMsgBuilder.buildFrameworkMessage("componentStoppedCheck",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), params, 
                mMsgBuilder.getMessageToken(errMsg) );
            
            throw new ManagementException(jbiMsg);
        }
    }    
}
