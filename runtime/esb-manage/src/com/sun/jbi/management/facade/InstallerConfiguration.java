/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallerConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.ObjectName;
import javax.management.ReflectionException;

/**
 *  InstallerConfiguration represents a domain facade for a component's 
 *  installer configuration MBean.  The facade MBean is responsible for 
 *  storing attributes set on the configuration MBean, which are later used
 *  by the Installer object to (a) configure the component installer in a 
 *  stand-alone or clustered instance and (b) serialize configuration properties
 *  for use by offline administration/synchronization code.
 *
 * @author Sun Microsystems, Inc.
 */
public class InstallerConfiguration 
    implements javax.management.DynamicMBean
{
    /** When true, updates to the installer config are not allowed. */
    private boolean     mReadOnly;
    /** Stores configuration attributes. */
    private ConcurrentHashMap<String, Object> mAttributes;
    /** MBeanInfo from the actual component configuration MBean. */
    private MBeanInfo   mMBeanInfo;
    /** The ObjectName of the facade configuration MBean. */
    private ObjectName  mMBeanName;
        
    /** Creates a new InstallerConfiguration MBean.
     *  @param mbeanInfo metadata returned by the 'real' MBean in the 
     *   target instance.
     *  @param mbeanName name of this MBean registered in the DAS.
     */
    public InstallerConfiguration(MBeanInfo mbeanInfo, ObjectName mbeanName)
    {
        if (mbeanInfo != null)
        {
            mMBeanInfo = mbeanInfo;
        }
        else
        {            
            // MBeanInfo was not provided, so we have to go with something generic
            mMBeanInfo = new MBeanInfo("javax.management.DynamicMBean",
                "", null, null, null, null);
        }
        
        mMBeanName  = mbeanName;
        mAttributes = new ConcurrentHashMap<String, Object>();
    }
    
    /** Returns the ObjectName of the facade InstallerConfiguration MBean
     *  registered in the DAS.
     *  @return ObjectName of this MBean on the DAS
     */
    public ObjectName getObjectName()
    {
        return mMBeanName;
    }
    
    /** Return a list of all attributes that have been set on the config MBean.
     *  @return complete list of attributes
     */
    public AttributeList getAttributeList()
    {
        AttributeList list = new AttributeList();
        
        for (Map.Entry<String, Object> attr : mAttributes.entrySet())
        {
            list.add(new Attribute(attr.getKey(), attr.getValue()));
        }        
        
        return list;
    }
    
    /** Returns the total number of attributes that have been set on the 
     *  configuration MBean.
     */
    public int getAttributeCount()
    {
        return mAttributes.size();
    }
    
    /** Indicates whether the configuration MBean is in read-only mode.
     */
    public boolean isReadOnly()
    {
        return mReadOnly;
    }
    
    /** Marks the configuration MBean as read only. 
     */
    public void setReadOnly()
    {
        mReadOnly = true;
    }
    
    
    /* ########## javax.management.DynamicMBean Methods ############ */

    /**
     * Obtain the value of a specific attribute of the Dynamic MBean.
     *
     * @param attribute The name of the attribute to be retrieved
     * @return The value of the attribute retrieved.
     * @exception AttributeNotFoundException
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's getter.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE>
     *            thrown while trying to invoke the getter.
     * @see #setAttribute
     */
    public Object getAttribute(String attribute) 
        throws AttributeNotFoundException, MBeanException, ReflectionException 
    {
        return mAttributes.get(attribute);
    }

    /**
     * Set the value of a specific attribute of the Dynamic MBean.
     * 
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     * @see #getAttribute
     */
    public void setAttribute(Attribute attribute) 
        throws AttributeNotFoundException, InvalidAttributeValueException, 
            MBeanException, ReflectionException 
    {
        if (!isReadOnly())
        {
            mAttributes.put(attribute.getName(), attribute.getValue());
        }
    }

    /**
     * Allows an action to be invoked on the Dynamic MBean.
     * 
     * @param actionName The name of the action to be invoked.
     * @param params An array containing the parameters to be set when the 
     *        action is invoked.
     * @param signature An array containing the signature of the action. The 
     *        class objects will be loaded through the same class loader as the
     *        one used for loading the MBean on which the action is invoked.
     * @return The object returned by the action, which represents the result of
     *         invoking the action on the MBean specified.
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> thrown
     *            by the MBean's invoked method.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE> 
     *            thrown while trying to invoke the method
     */
    public Object invoke(String actionName, Object[] params, String[] signature) 
        throws MBeanException, ReflectionException 
    {
        throw new UnsupportedOperationException("invoke");
    }

    /**
     * Sets the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of attributes: The identification of the
     *        attributes to be set and  the values they are to be set to.
     * @return The list of attributes that were set, with their new values.
     * @see #getAttributes
     */
    public AttributeList setAttributes(AttributeList attributes) 
    {
        if (!isReadOnly())
        {
            for (int i = 0; i < attributes.size(); i++)
            {
                Attribute attr = (Attribute)attributes.get(i);
                mAttributes.put(attr.getName(), attr.getValue());
            }
        }
        
        return attributes;
    }

    /**
     * Provides the exposed attributes and actions of the Dynamic MBean using 
     * an MBeanInfo object.
     * 
     * @return An instance of <CODE>MBeanInfo</CODE> allowing all attributes 
     *         and actions exposed by this Dynamic MBean to be retrieved.
     */
    public MBeanInfo getMBeanInfo() 
    {
        return mMBeanInfo;
    }

    /**
     * Get the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of the attributes to be retrieved.
     * @return The list of attributes retrieved.
     * @see #setAttributes
     */
    public AttributeList getAttributes(String[] attributes) 
    {
        AttributeList list = new AttributeList();
        
        for (String name : attributes)
        {
            if (mAttributes.containsKey(name))
            {
                list.add(new Attribute(name, mAttributes.get(name)));
            }
        }
        
        return list;
    }
}
