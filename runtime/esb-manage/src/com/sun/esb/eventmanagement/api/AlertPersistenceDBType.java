package com.sun.esb.eventmanagement.api;


public enum AlertPersistenceDBType {
    DB2("DB2"),ORACLE("ORACLE"),POINTBASE("POINTBASE"),
    SYBASE("SYBASE"),DERBY("DERBY");
    
    private String databaseType;

    /** @param database type string */
    private AlertPersistenceDBType(String dbType) {
        this.databaseType = dbType;
    }

    
    /** @return the database type description */
    public String getDescription() {
        switch (this) {
            case DB2:
                return "DB2 Database";
            case ORACLE:
                return "Oracle Database";
            case POINTBASE:
                return "pointBase Database";
            case SYBASE:
                return "Sybase Database";
            case DERBY:
                return "Apache Derby Database";
        }
        return "";
    }

    /** @return the database type */
    public String getDatabasetype() {
        return databaseType;
    }

    
}
