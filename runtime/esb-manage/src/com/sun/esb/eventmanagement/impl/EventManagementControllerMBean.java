/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventManagementControllerMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanRegistration;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationFilter;
import javax.management.NotificationFilterSupport;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;
import javax.management.MBeanServerConnection;
import javax.management.openmbean.CompositeData;
import javax.management.timer.Timer;

import com.sun.esb.eventmanagement.api.AlertPersistenceDBType;
import com.sun.esb.eventmanagement.api.InstanceIdentifier;
import com.sun.esb.eventmanagement.api.NotificationEvent;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.platform.PlatformContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * this class is a mbean that all the invocation from the management client
 * and distribute them to the target/targets based on the first paramater
 * passed
 */
public final class EventManagementControllerMBean  extends NotificationBroadcasterSupport
    implements DynamicMBean, MBeanRegistration, Serializable,
    NotificationListener,  // remove after test
    EventManagementConstants {

    private static final Logger mLogger;
    private static boolean isDebugEnabled;
    static {
        mLogger = Logger.getLogger(EventManagementControllerMBean.class.getName());
         isDebugEnabled = mLogger.isLoggable(Level.FINEST);
     
     }
    
    ExecutorService mExecutorService;

    /**
     * MBean attributes
     */
    protected ArrayList<MBeanAttributeInfo> mAttributesInfo = 
            new ArrayList<MBeanAttributeInfo>();

    /**
     * MBean constructors
     */
    protected ArrayList<MBeanConstructorInfo> mConstructorsInfo = 
            new ArrayList<MBeanConstructorInfo>();

    /**
     * MBean operations
     */
    protected ArrayList<MBeanOperationInfo> mOperationsInfo = 
            new ArrayList<MBeanOperationInfo>();

    /**
     * MBean notifications
     */
    protected ArrayList<MBeanNotificationInfo> mNotificationsInfo = 
             new ArrayList<MBeanNotificationInfo>();
    /**
     * MBean info
     */
    protected MBeanInfo mMBeanInfo;
   


    private PlatformContext mPlatformContext;
    
    
    long mNotificationSeqNum; 
    private MBeanServerConnection mMBeanServer;
    private Boolean mJournalEnabled = new Boolean(false);
    private Boolean mPersistenceEnabled = new Boolean(false);
    private List<String> mTargetInstanceList;
    private ObjectName mEventForwardObjcetName;
    private ObjectName mChannelSupportObjectName;
    private ObjectName mMBeanServerNotificationObjectName;
    private String mJBIinstanceRoot;
    private EventForwardingHelper mEventForwardingHelper;
    private AlertPersistenceConfiguration mAlertPersistenceConfiguration;
    private Properties mPersistedProps;
    transient private DBEventStore mEventStore;
    private String mHostName;
    private String mDasUniquePort;
    private Timer mTargetCheckTimer;
    private ObjectName mTargetCheckTimerObjectName;   
    private Integer mTimerNotificationId;
    private Long mTargetCheckInterval = DEFAULT_TARGET_CHECK_INTERVAL;
    private NotificationFilterSupport mTargetCheckTimerFilter;
    private Map<String,EventManagementTargetStateInfo> mCurrentTargetStateMap;
    private EnvironmentContext mContext;
    /**
     * 
     */
    public EventManagementControllerMBean(EnvironmentContext context) throws Exception{
        buildDynamicMBeanInfo();
        mContext = context;
        mCurrentTargetStateMap = new HashMap<String,EventManagementTargetStateInfo>();
        mTargetCheckTimerFilter = new NotificationFilterSupport();
        // create pool for forwarding events to the client
        mJBIinstanceRoot = context.getJbiInstanceRoot()+EVENT_CONFIG_DIRECTORY;
        mExecutorService = Executors.newCachedThreadPool();
        mPlatformContext =  context.getPlatformContext();
        mMBeanServer = mPlatformContext.getMBeanServer();
        mTargetInstanceList = new ArrayList<String>();
        mTargetInstanceList.addAll(mPlatformContext.getStandaloneServerNames());
        mTargetInstanceList.addAll(mPlatformContext.getClusteredServerNames());
        mEventForwardObjcetName = new ObjectName(MBeanNames.EVENT_MANAGEMENT_MBEAN_NAME);
        mChannelSupportObjectName = new ObjectName(EVENTMANAGEMENT_CHANNEL_MBEAN_NAME);
        mMBeanServerNotificationObjectName = new ObjectName(MBEAN_SERVER_DELEGATE_OBJECT_NAME);
        mEventForwardingHelper =  new EventForwardingHelper(mJBIinstanceRoot); 
        mAlertPersistenceConfiguration = AlertPersistenceConfiguration.getInstance(mMBeanServer);
        mAlertPersistenceConfiguration.setAdminServer(mPlatformContext.isAdminServer());
        mPersistedProps = mEventForwardingHelper.GetProperties();
        mHostName=getPhysicalHostName();
        initializeSetting();
        mEventStore = DBEventStore.getInstance(mAlertPersistenceConfiguration,
                mMBeanServer,mPlatformContext);
        mAlertPersistenceConfiguration.setDBEventStore(mEventStore);
        if(mAlertPersistenceConfiguration.getPersistenceEnabled().booleanValue()) {
            try {
                mEventStore.initializeDataSource();
            } catch (ManagementRemoteException e) {
                mLogger.log(Level.WARNING, AlertUtil.getPackageBundle().
                        getMessage("caps.management.server.alert.persistence.setup.error.log.msg"), e);
            }
        }
        // this mbean exist only on the DAS
        mAlertPersistenceConfiguration.setAdminServer(true);
        // register for mbean registeration notification
        mMBeanServer.addNotificationListener(mMBeanServerNotificationObjectName, this, null, new Object());
        // add instance check timer
        registerTargetCheckTimer();
    }

   /**
     * Build the protected MBeanInfo field,
     * which represents the management interface exposed by the MBean;
     * i.e., the set of attributes, constructors, operations and notifications
     * which are available for management.
     *
     * A reference to the MBeanInfo object is returned by the getMBeanInfo()
     * method of the DynamicMBean interface. Note that, once constructed,
     * an MBeanInfo object is immutable.
     */
    protected void buildDynamicMBeanInfo() {

       mConstructorsInfo.add(
          new MBeanConstructorInfo(
             "EventManagementControllerMBean Constructor",
             getClass().getConstructors()[0]));

        addOperationsInfo();
        addAttributesInfo();

        
         mMBeanInfo =  new MBeanInfo(
             getClass().getName(),
             "Event Management DynamicMBean",
             (MBeanAttributeInfo[]) mAttributesInfo.toArray(
                new MBeanAttributeInfo[mAttributesInfo.size()]),
             (MBeanConstructorInfo[]) mConstructorsInfo.toArray(
                new MBeanConstructorInfo[mConstructorsInfo.size()]),
             (MBeanOperationInfo[]) mOperationsInfo.toArray(
                new MBeanOperationInfo[mOperationsInfo.size()]),
             (MBeanNotificationInfo[]) mNotificationsInfo.toArray(
                new MBeanNotificationInfo[mNotificationsInfo.size()]));

    }
    
    /**
     * Invokes an operation on the Dynamic MBean.
     * @param operationName The name of the action to be invoked
     * @param params An array containing the parameters to be set when the
     * action is invoked
     * @param signature An array containing the aSignature of the action.
     * @throws MBeanException MBeanException
     * @throws ReflectionException ReflectionException
     */
    public Object invoke(
       String operationName,
       Object params[],
       String signature[]) throws MBeanException, ReflectionException {
       Object result = "OK"; 
       // Check operationName is not null to avoid NullPointerException later on
       if(operationName.equalsIgnoreCase(GET_HTTP_SERVER_INFO)) {
           result = getHttpServerInfo();
       }else if ( operationName.equals(GET_DAS_ADMINSERVER_INFO) ) {
           result = getDasAdminServerInfo();
       }else if ( operationName.equals(UPDATE_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME) ) {
           updateAlertsRemovalPolicy(params);
       }else if ( operationName.equals(GET_LAST_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME) ) {
           result = getcurrentRemovlPolicy();
       }else if ( operationName.equals(ENABLE_PERSISTENCE_OPERATION_NAME) ) {
           result = executePersistenceOpeartion(true,params);
       }else if ( operationName.equals(DISABLE_PERSISTENCE_OPERATION_NAME) ) {
           result = executePersistenceOpeartion(false,params);
       } else if (operationName.equalsIgnoreCase(ACKNOWLEDGE_ALERT_RECEPTION)) {
           String masteruuid = (String) params[0];
           String eventId = (String)params[1];
           AlertReceptionAcknowledged(eventId);
       }
        return result;
    }
    public void postDeregister() {
    }

    public void postRegister(Boolean registrationDone) {
   }

    public void preDeregister() {
        unRegisterTimerService();
    }

    
   
    public ObjectName preRegister(MBeanServer server, ObjectName name) {
       return name;
    }
    
    private void addOperationsInfo() {
        mOperationsInfo.add(
                new MBeanOperationInfo(
                    UPDATE_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME,
                    "update the policy used to remove persisted alerts",
                    new MBeanParameterInfo[] {
                            new MBeanParameterInfo("params","[Ljava.lang.Object","array of String representing the new Policy")},
                    "void",MBeanOperationInfo.ACTION));    
        mOperationsInfo.add(
                new MBeanOperationInfo(
                    GET_LAST_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME,
                    "get the last policy setting used to remove persisted alerts",null,
                    "[Ljava.lang.String",MBeanOperationInfo.ACTION));    
        mOperationsInfo.add(
                new MBeanOperationInfo(
                        GET_HTTP_SERVER_INFO,
                    "get this physical host name and http port number",null,
                    "String",MBeanOperationInfo.ACTION));    
        mOperationsInfo.add(
                new MBeanOperationInfo(
                        GET_DAS_ADMINSERVER_INFO,
                    "get this physical host name and http admin port number",null,
                    "String",MBeanOperationInfo.ACTION));    
        mOperationsInfo.add(
                new MBeanOperationInfo(
                        ENABLE_PERSISTENCE_OPERATION_NAME,
                    "enable events peristence with the option not removing them",
                    new MBeanParameterInfo[] {
                            new MBeanParameterInfo("enableJournal","java.lang.Boolean","Flag to indicate alerts are journaled or not"),},
                    "void",MBeanOperationInfo.ACTION));    
        mOperationsInfo.add(
                new MBeanOperationInfo(
                        DISABLE_PERSISTENCE_OPERATION_NAME,
                    "disable events peristence ",null,"void",MBeanOperationInfo.ACTION));    
        mOperationsInfo.add(
                new MBeanOperationInfo(ACKNOWLEDGE_ALERT_RECEPTION,
                    "delete the alert that was acknowledge by the client",
                    new MBeanParameterInfo[] {
                        new MBeanParameterInfo("UUID","java.lang.String",
                       "Unique ID for the delivery channel provide by the connected client"),
                        new MBeanParameterInfo("dbAlertID","java.lang.String",
                        "Unique ID that identify the alert to be removed from persistence")},
                        "void",MBeanOperationInfo.ACTION));    
    }
    
    private void addAttributesInfo() {
        mAttributesInfo.add(new MBeanAttributeInfo(
                ENABLE_PERSISTENCE_ATTRIBUTE_NAME, "java.lang.Boolean",
                "flag to indicate if the db persistence should be enabled or disabled. " ,
                true, true, false));

        mAttributesInfo.add(new MBeanAttributeInfo(
                ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME, "java.lang.Boolean",
                "flag to indicate if the persisted alert removal polciy should be enforced or not. " ,
                true, true, false));

        mAttributesInfo.add(new MBeanAttributeInfo(
                PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME, "java.lang.Long",
                "indicate the interval (in milliseconds) of alert in the db  will be be subject to removal" ,
                true, true, false));

        mAttributesInfo.add(new MBeanAttributeInfo(
                PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME, "java.lang.Long",
                "indicate the max age (in milliseconds) of alert in the db that will be be subject to removal" ,
                true, true, false));

        mAttributesInfo.add(new MBeanAttributeInfo(
                PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME, "java.lang.Long",
                "indicate the max count of alerts alllowed to persisted in the db" ,
                true, true, false));

        mAttributesInfo.add(new MBeanAttributeInfo(
                PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME, "com.stc.eventmanagement.imp.Alert",
                "indicate the max level of alerts the are subject to removel from the db" ,
                true, true, false));
        

       mAttributesInfo.add(new MBeanAttributeInfo(
               CURRENT_PERSISTED_EVENT_COUNT_ATTRIBUTE_NAME ,"java.lang.Integer",
                "The current number of persisted alert/events waiting to be forwarded to " +
                "Enterprise monitor",
                true, false, false));
       
       mAttributesInfo.add(new MBeanAttributeInfo(
               DATABASE_JNDI_NAME__ATTRIBUTE_NAME ,"java.lang.String",
               "The JNDI name for the data source used when alert are persisted",
               true, true, false));
       
       mAttributesInfo.add(new MBeanAttributeInfo(
                EVENT_MANAGEMENT_UNIQUE_TABLE_NAME_ATTRIBUTE_NAME ,"java.lang.Integer",
                "Each domain in the enterprise has unique name from the alert it generated and persisted" +
                "Enterprise monitor",
                true, false, false));
       
       mAttributesInfo.add(new MBeanAttributeInfo(
               TARGET_CHECK_INTERVAL_ATTRIBUTE_NAME ,"java.lang.Long",
               "The interval used to query the current targets list in order to register the control mbean for notification",
               true, true, false));
       mAttributesInfo.add(new MBeanAttributeInfo(
               IS_SECURE_ADMIN_PORT ,"java.lang.Long",
               "return true if secure http admin port is used",
               true, false, false));
     }

    /**
     */
    public Object getAttribute(String aName)
       throws AttributeNotFoundException, MBeanException, ReflectionException {
        
        
        if(aName.equals(ENABLE_PERSISTENCE_ATTRIBUTE_NAME) == true || 
           aName.equals(PERSISTENCE_ENABLED_ATTRIBUTE_NAME)) {
            synchronized (mAlertPersistenceConfiguration) {
                return mAlertPersistenceConfiguration.getPersistenceEnabled();
            }
        } else 
            if(aName.equals(JOURNAL_ENABLED_ATTRIBUTE_NAME) == true) {
                synchronized (mAlertPersistenceConfiguration) {
                    return mAlertPersistenceConfiguration.isJournalEnabled();
                }
        } else 
        if(aName.equals(ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME) == true) {
            synchronized (mAlertPersistenceConfiguration) {
                return mAlertPersistenceConfiguration.getPersistedAlertRemovalPolicyExecEnabled();
            }
        } else
        if(aName.equals(PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME) == true) {
                synchronized (mAlertPersistenceConfiguration) {
                    return mAlertPersistenceConfiguration.getPersistenceAlertAgePolicyValue();
                }
        } else
        if(aName.equals(PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME) == true) {
                synchronized (mAlertPersistenceConfiguration) {
                    return mAlertPersistenceConfiguration.getPersistenceAlertCountPolicyValue();
                }
        } else
        if(aName.equals(PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME) == true) {
                 synchronized (mAlertPersistenceConfiguration) {
                     return mAlertPersistenceConfiguration.getPersistenceAlertLevelPolicyValue().getAlertLevel();
                 }
        }  else if(aName.equals(PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME) == true) {
            synchronized (mAlertPersistenceConfiguration) {
                return mAlertPersistenceConfiguration.getPersistenceAlertPolicyExecInterval();
            }
        } else if(aName.equals(CURRENT_PERSISTED_EVENT_COUNT_ATTRIBUTE_NAME) == true) {
                return mEventStore.getPersistedEventCount();
        } else if(aName.equals(DATABASE_JNDI_NAME__ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getDataSourceJndiName();
        } else if(aName.equals(this.DATABASE_TYPE_ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getAlertPersistenceDBType().getDatabasetype();
        } else if(aName.equals(JBI_INSTALL_ROOT_ATTRIBUTE_NAME) == true) {
                return mJBIinstanceRoot;
        } else if(aName.equals(EVENT_MANAGEMENT_UNIQUE_TABLE_NAME_ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getAlertTableName();
        } else if(aName.equals(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME) == true) {
                return mAlertPersistenceConfiguration.getAlertTableName();
        } else if(aName.equals(TARGET_CHECK_INTERVAL_ATTRIBUTE_NAME) == true) {
                return mTargetCheckInterval;
        } else if(aName.equals(IS_SECURE_ADMIN_PORT) == true) {
                return isSecureAdminPort();
        }else {
            throw new AttributeNotFoundException();
        }
        
    }
    
    public AttributeList getAttributes(String[] aNames) {
        AttributeList attributes = new AttributeList(aNames.length);
        for (int i = 0; i < aNames.length; i++) {
             try {
                 attributes.add(new Attribute(aNames[i], 
                         getAttribute(aNames[i])));
             } catch (Exception e) {
                 if(mLogger.isLoggable(Level.FINE)) {
                     mLogger.fine(">>>>EventForwarderMBean :: Unable to add attribute " 
                         + aNames[i] + " "+ e);
                 }
                 continue;
             }
         }

         return attributes;
    }
    
    public MBeanInfo getMBeanInfo() {
        return mMBeanInfo;
     }
    
    public void setAttribute(Attribute aAttribute)
    throws
       AttributeNotFoundException,
       InvalidAttributeValueException,
       MBeanException,
       ReflectionException {
     
     
//     if(aAttribute.getName().equals(ENABLE_PERSISTENCE_ATTRIBUTE_NAME)
//             == true) {
//         synchronized (mAlertPersistenceConfiguration) {
//             mAlertPersistenceConfiguration.setPersistenceEnabled((Boolean) aAttribute.getValue());
//         }
//     } else 
     if(aAttribute.getName().equals(ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME) 
             == true) {
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setPersistedAlertRemovalPolicyExecEnabled((Boolean) aAttribute.getValue());
         }
     } else if(aAttribute.getName().equals(PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME) 
             == true) {
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setPersistenceAlertAgePolicyValue((Long) aAttribute.getValue());
         }
     } else if(aAttribute.getName().equals(PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME) 
             == true) {
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setPersistenceAlertCountPolicyValue((Long) aAttribute.getValue());
         }
     } else if(aAttribute.getName().equals(PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME) 
             == true) {
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setPersistenceAlertLevelPolicyValue( AlertLevelType.valueOf((String) aAttribute.getValue()));
         }        
     } else if(aAttribute.getName().equals(PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME) 
             == true) {
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setPersistenceAlertPolicyExecInterval((Long) aAttribute.getValue());
         }        
     } else if(aAttribute.getName().equals(DATABASE_JNDI_NAME__ATTRIBUTE_NAME) == true) {
         String newJndiName = (String) aAttribute.getValue();
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setDataSourceJndiName(newJndiName);
         }
         propogateAttributeValueToAllTargets(DATABASE_JNDI_NAME__ATTRIBUTE_NAME,
                 newJndiName);
         propogateAttributeValueToChannelSuportBean(DATABASE_JNDI_NAME__ATTRIBUTE_NAME,newJndiName);
     } else if(aAttribute.getName().equals(DATABASE_TYPE_ATTRIBUTE_NAME) == true) {
         String dbType = (String) aAttribute.getValue();
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setAlertPersistenceDBType(AlertPersistenceDBType.valueOf(dbType));
         }
         propogateAttributeValueToAllTargets(DATABASE_TYPE_ATTRIBUTE_NAME,dbType);
         propogateAttributeValueToChannelSuportBean(DATABASE_TYPE_ATTRIBUTE_NAME,dbType);
     } else if(aAttribute.getName().equals(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME) == true) {
         String tableName = (String) aAttribute.getValue();
         synchronized (mAlertPersistenceConfiguration) {
             mAlertPersistenceConfiguration.setAlertTableName(tableName);
         }
         propogateAttributeValueToAllTargets(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME,tableName);
         propogateAttributeValueToChannelSuportBean(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME,tableName);
     } else if(aAttribute.getName().equals(TARGET_CHECK_INTERVAL_ATTRIBUTE_NAME) == true) {
         mTargetCheckInterval = (Long)aAttribute.getValue();
         mTargetCheckTimer.stop();
         unHookFromTimerService();
         hookUpToTimerService(mTargetCheckInterval);
         mTargetCheckTimer.start();
     }
     
     
     updatePersistedProperties();
     
 }
    
    public void handleNotification(Notification notification, Object handback) {
        String notificationType = notification.getType();
        if(notificationType.equals(JCAPS_EVENT_TYPE)) {
           if(mPersistenceEnabled.booleanValue() || mJournalEnabled.booleanValue()) {
               return; // once persistence is enabled event on the jmx channel are
                       // discarded 
           } else {
               String instanceName = (String)notification.getUserData();
               synchronized(this) {
                   forwardJMXNotificationToDeliveryChannel(instanceName);
               }
           }
        } else if(notificationType.equals(MBeanServerNotification.REGISTRATION_NOTIFICATION)) {
            MBeanServerNotification nbsn = (MBeanServerNotification)notification;
            ObjectName regMBean= nbsn.getMBeanName();
            if(regMBean.getKeyPropertyList().contains("instance")) {
                updateNewInstance(regMBean.getKeyProperty("instance"));
            }else if(regMBean.equals(mEventForwardObjcetName) ) {
                // the forward mbean on the DAS is registered
                // add listener for its notification
                try {
                    mMBeanServer.addNotificationListener(mEventForwardObjcetName, this, null, new Object());
                } catch (Exception e) {
                }
                
            }
        } else  if(notificationType.equals(EVENT_TARGETCHECK_FILTER_TYPE)) {
            this.mTargetCheckTimer.stop();
            updateTargetsIfNeeded();
            this.mTargetCheckTimer.start();
        }
        
    }

    private void updateNewInstance(String newInstanceName) {
        // if new server instance was added
        // pass configuration info (persistence enabled JNDI name & database type to it)
        // as well as  register for notification from that MBean.
        if(mTargetInstanceList.contains(newInstanceName)) {
            return;
        } else {
            mTargetInstanceList.add(newInstanceName);
        }
            
        try {
            MBeanServerConnection mBeanServer = 
                mPlatformContext.getMBeanServerConnection(newInstanceName);
            if(mBeanServer == null) {
                return; // target server not accessible
            }
            // register for notification from the new target 
            RegisterWithTarget(newInstanceName);
            Attribute dbAttribute = new Attribute(DATABASE_JNDI_NAME__ATTRIBUTE_NAME,
                    mAlertPersistenceConfiguration.getDataSourceJndiName()); 
            mBeanServer.setAttribute(mEventForwardObjcetName, dbAttribute);
            dbAttribute = new Attribute(DATABASE_TYPE_ATTRIBUTE_NAME,
                    mAlertPersistenceConfiguration.getAlertPersistenceDBType()); 
            mBeanServer.setAttribute(mEventForwardObjcetName, dbAttribute);
            Attribute enableAttribute = new Attribute(ENABLE_PERSISTENCE_ATTRIBUTE_NAME,
                    mPersistenceEnabled); 
            mBeanServer.setAttribute(mEventForwardObjcetName, enableAttribute);
        } catch (InstanceNotFoundException e) {
            mLogger.log(Level.FINEST, "Instance Not Found Exception ", e);       
        } catch (AttributeNotFoundException e) {
            mLogger.log(Level.FINEST, "Attribute Not Found Exception ", e);       
        } catch (InvalidAttributeValueException e) {
            mLogger.log(Level.FINEST, "Invalid Attribute value ", e);       
        } catch (MBeanException e) {
            mLogger.log(Level.FINEST, "MBean Exception ", e);       
        } catch (ReflectionException e) {
            mLogger.log(Level.FINEST, "Reflection Exception ", e);       
        } catch (IOException e) {
            mLogger.log(Level.FINEST, "IO Exception ", e);       
        } catch (Exception e) {
            mLogger.log(Level.FINEST, "Exception ", e);       
        }
        

        
    }
    
    private void AlertReceptionAcknowledged(String aEventId) throws MBeanException{
        if(mJournalEnabled == false) {
           try {
               mEventStore.deleteEvent(aEventId);
            } catch (Exception e) {
               throw new  MBeanException(e);
            }
        }
    }

    public void forwardJMXNotificationToDeliveryChannel(String aInstanceName) {

         try {
              // get the events from the forwarder queue
             Collection<CompositeData> eventsData = null;
             MBeanServerConnection instancemBeanServer = 
                 mPlatformContext.getMBeanServerConnection(aInstanceName);
             eventsData = (Collection<CompositeData>)instancemBeanServer.invoke(mEventForwardObjcetName, 
                     GET_QUEUED_EVENTS, null, null);
             for (CompositeData data : eventsData) {
                 // if channel support is not registered mbean drop the event
                 if(mMBeanServer.isRegistered(mChannelSupportObjectName)) {
                     // invoke forward event method on the channel support mbean
                     Object[] params = new Object[] {data};
                     String[] signature = new String[] {"javax.management.openmbean.CompositeData"}; 
                     mMBeanServer.invoke(mChannelSupportObjectName, EVENT_FORWARD_OPERATION, params, signature);
                 }
             
             }
        } catch (Exception e) {
            //jmx channel is not the reliable channel 
            // therefore we can drop the event without
            // processing the exception
        }
    
    }
    
    private void RegisterForNotificationFromExistingTargets() {
        Set<String> instnaceNames= mPlatformContext.getStandaloneServerNames();
        instnaceNames.addAll(mPlatformContext.getClusteredServerNames());

      for (String instanceName : instnaceNames) {
          try {
            this.RegisterWithTarget(instanceName);
        } catch (MBeanException e) {
            mLogger.log(Level.FINEST, "MBean Exception ", e);       
        }
      }
      
    }
    
    
    /**
     * Sets the value of specified aAttributes of the Dynamic MBean.
     * @param aAttributes list of attribute names and values.
     * @return AttributeList list of attribute names and values
     */
    public AttributeList setAttributes(AttributeList aAttributes) {
       for (Iterator it = aAttributes.iterator(); it.hasNext();) {
          Attribute attribute = (Attribute) it.next();
          try {
             setAttribute(attribute);
          } catch (Exception e) {
             continue;
          }
       }
       return aAttributes;
    }

    private Object executePersistenceOpeartion(boolean enable,Object[] params) 
                throws MBeanException, ReflectionException  {
        Object result = "OK"; 
        if(enable) {
            updateIdentifierInformation();
            mAlertPersistenceConfiguration.setAlertTableName(createTableName());
            try {
                mAlertPersistenceConfiguration.setPersistenceEnabled(true);
            } catch (ManagementRemoteException e) {
                Exception exp = AlertUtil.createManagementException("caps.management.server.alert.enable.persistence.error",
                        null, e);
                throw new MBeanException(exp);
            }
            mJournalEnabled = (Boolean) params[0];
            mAlertPersistenceConfiguration.setJournalEnabled(mJournalEnabled);
            updatePersistedProperties();
            mPersistenceEnabled = new Boolean(true);
            result = enablePersistanceOnAllTargets();
         } else {
            mJournalEnabled = new Boolean(false);
            mPersistenceEnabled = new Boolean(false);
            // setPersistenceEnabled to false will force journal to false
            try {
                mAlertPersistenceConfiguration.setPersistenceEnabled(false);
            } catch (ManagementRemoteException e) {
                throw new MBeanException(e); 
            }
            updatePersistedProperties();
            result = disablePersistanceOnAllTargets();
        }    
        propogateAttributeValueToChannelSuportBean(JOURNAL_ENABLED_ATTRIBUTE_NAME,
                mJournalEnabled);
        propogateAttributeValueToChannelSuportBean(ENABLE_PERSISTENCE_ATTRIBUTE_NAME,
                mPersistenceEnabled);
        return result;
   }
    
    private Object enablePersistanceOnAllTargets() throws MBeanException {
        
        Set<String> allServers= mPlatformContext.getStandaloneServerNames();
        allServers.addAll(mPlatformContext.getClusteredServerNames());

         for (String server : allServers) {
             if(mPlatformContext.isInstanceUp(server)) {
                 enablePersistanceOnTarget(server);
             }
         }
         Object result =   "OK";
         return result;
    }
    
    private void enablePersistanceOnTarget(String server) throws MBeanException {
        

          try {
                  // get the mbean server for the given instance
                  MBeanServerConnection mBeanServer = 
                      mPlatformContext.getMBeanServerConnection(server);
                  if(mBeanServer == null) {
                      return;
                  }
                  // make sure that the lastest jndi and table names are avialable to the target
                  // before actaully enable persistence
                  propogateAttributeValueToTarget(server,DATABASE_TYPE_ATTRIBUTE_NAME,
                          this.mAlertPersistenceConfiguration.getAlertPersistenceDBType().getDatabasetype());
                  propogateAttributeValueToTarget(server,DATABASE_JNDI_NAME__ATTRIBUTE_NAME,
                          this.mAlertPersistenceConfiguration.getDataSourceJndiName());
                  propogateAttributeValueToTarget(server,DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME,
                          this.mAlertPersistenceConfiguration.getAlertTableName());
                  Attribute lAttribute = new Attribute(ENABLE_PERSISTENCE_ATTRIBUTE_NAME,new Boolean(true));
                  mBeanServer.setAttribute(mEventForwardObjcetName, lAttribute);
              } catch (Exception e) {
                throw new MBeanException(e);
              }
    }
    
    private Object disablePersistanceOnAllTargets() throws MBeanException {
        Set<String> allServers= mPlatformContext.getStandaloneServerNames();
        allServers.addAll(mPlatformContext.getClusteredServerNames());

         for (String server : allServers) {
             if(mPlatformContext.isInstanceUp(server)) {
                 disablePersistanceOnTarget(server);
             }
         }
         Object result =   "OK";
         return result;
    }
  
    
    
    private void disablePersistanceOnTarget(String server) throws MBeanException {
          try {
                  // get the mbean server for the given instance
                  MBeanServerConnection mBeanServer = 
                      mPlatformContext.getMBeanServerConnection(server);
                  if(mBeanServer == null) {
                      return;
                  }

                  Attribute lAttribute = new Attribute(ENABLE_PERSISTENCE_ATTRIBUTE_NAME,new Boolean(false));
                  mBeanServer.setAttribute(mEventForwardObjcetName, lAttribute);
              } catch (Exception e) {
                throw new MBeanException(e);
              }
    }
    
    private void propogateAttributeValueToAllTargets(String attributeName,
            String attributeVale) {
        Set<String> allServers= mPlatformContext.getStandaloneServerNames();
        allServers.addAll(mPlatformContext.getClusteredServerNames());

         for (String server : allServers) {
             propogateAttributeValueToTarget(server,attributeName,attributeVale);
         }
        
    }

    private void propogateAttributeValueToTarget(String server,String attributeName,
            String attributeVale) {

          try {
                  // get the mbean server for the given instance
                  MBeanServerConnection mBeanServer = 
                      mPlatformContext.getMBeanServerConnection(server);
                  if(mBeanServer == null) {
                      return;
                  }

                  Attribute lAttribute = new Attribute(attributeName,attributeVale);
                  mBeanServer.setAttribute(mEventForwardObjcetName, lAttribute);
              } catch (Exception e) {
              }
        
    }
    

    private void propogateAttributeValueToChannelSuportBean(String attributeName,
            Object attributeVale) {

        try {
            if(mMBeanServer.isRegistered(mChannelSupportObjectName)) {
                Attribute lAttribute = new Attribute(attributeName,attributeVale);
                mMBeanServer.setAttribute(mChannelSupportObjectName, lAttribute);
            }
        } catch (Exception e) {
            // we should not get io failure since is not a remote call 
            // both running in the same JVM 
        }
    }
    
   
    private void updateAlertsRemovalPolicy(Object[] params) {
        List<String> policyElementsList = (List)params[0];
        mAlertPersistenceConfiguration.setAlertRemovalPolicyTypeList(policyElementsList);
    }

    private String[] getcurrentRemovlPolicy() {
        List<AlertRemovalPolicyType> list = mAlertPersistenceConfiguration.getAlertRemovalPolicyTypeList();
        String[]  policyElementsArray =  new String[list.size()];
        for (int index = 0; index < list.size(); index++) {
            AlertRemovalPolicyType alertRemovalPolicyType = list.get(index);
            policyElementsArray[index] = alertRemovalPolicyType.getPolicyType(); 
        }
        return policyElementsArray;
    }
    
    private String  getHttpServerInfo() {
        StringBuffer buffer = new StringBuffer(getPhysicalHostName());
        InstanceIdentifier ii = 
            InstanceIdentifierFactory.getInstanceIdentifier(mPlatformContext);
        buffer.append(":");
        buffer.append(ii.getHttpPort());
        return buffer.toString();
    }

    private String  getDasAdminServerInfo() {
        StringBuffer buffer = new StringBuffer(getPhysicalHostName());
         InstanceIdentifier ii = 
            InstanceIdentifierFactory.getInstanceIdentifier(mPlatformContext);
         buffer.append(":");
         buffer.append(ii.getDomainAdminPort());
         return buffer.toString();
    }

    private Boolean  isSecureAdminPort() {
         InstanceIdentifier ii = 
            InstanceIdentifierFactory.getInstanceIdentifier(mPlatformContext);
         return ii.isSecureAdminPort();
    }
   
    private String getPhysicalHostName() {
        String hostName = null;
       try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
        }
        if(hostName == null) {
            try {
                hostName = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e1) {
            }
        }
        if(hostName == null) {
            hostName = "127.0.0.1";
        }
        return hostName;
    }

    
    private void RegisterWithTarget(String instanceName) throws MBeanException{
        Object handback = new Object();
        
        try {
            MBeanServerConnection instanceConnection = 
                mPlatformContext.getMBeanServerConnection(instanceName);
            InstanceNotificationListener inl = new InstanceNotificationListener(this);
            instanceConnection.addNotificationListener(mEventForwardObjcetName, inl, null, handback);
       } catch (Exception e) {
            throw new MBeanException(e);
       }
    }
    private void initializeSetting() {
        
        if(mPersistedProps == null || mPersistedProps.size()==0) {
            // by default persistence is disabled as well as policy enforcement
            try {
                mAlertPersistenceConfiguration.setPersistenceEnabled(new Boolean(false));
            } catch (ManagementRemoteException e) {
                // do nothing the database setup logic does not throw exception when
                // persistence enabled set to false
            }       
            mAlertPersistenceConfiguration.setJournalEnabled(mJournalEnabled);
            mAlertPersistenceConfiguration.setPersistenceAlertPolicyExecInterval(DEFAULT_POLICY_EXEC_INTERVAL);
            mAlertPersistenceConfiguration.setPersistedAlertRemovalPolicyExecEnabled(new Boolean(false));
            mAlertPersistenceConfiguration.setPersistenceAlertLevelPolicyValue(AlertLevelType.NONE);       
            mAlertPersistenceConfiguration.setPersistenceAlertAgePolicyValue(new Long(-1));
            mAlertPersistenceConfiguration.setPersistenceAlertCountPolicyValue(new Long(-1));
            mAlertPersistenceConfiguration.setAlertPersistenceDBType(AlertPersistenceDBType.DERBY);
            mAlertPersistenceConfiguration.setDataSourceJndiName(mAlertPersistenceConfiguration.DEFAULT_PERSISTENCE_JNDI_NAME);
            mAlertPersistenceConfiguration.setAlertTableName(createTableName());

            Properties alertRemovalProps = mAlertPersistenceConfiguration.getPersistenceProperties();
            mPersistedProps.putAll(alertRemovalProps);
            mEventForwardingHelper.saveProperties(mPersistedProps);
        }else {

            String propValue = mPersistedProps.getProperty(EventManagementConstants.JOURNAL_ENABLED_PROPERTY);
            mAlertPersistenceConfiguration.setJournalEnabled(propValue == null ? new Boolean(false) : new Boolean(propValue));
            
            propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_EXEC_ENABLED_PROPERTY);
            mAlertPersistenceConfiguration.setPersistedAlertRemovalPolicyExecEnabled(
                    propValue == null ? new Boolean(false) : new Boolean(propValue));
            
            propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_EXEC_INTERVAL_PROPERTY);
            mAlertPersistenceConfiguration.setPersistenceAlertPolicyExecInterval(
                    propValue == null ? DEFAULT_POLICY_EXEC_INTERVAL : new Long(propValue));
            propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_AGE_PROPERTY);
            mAlertPersistenceConfiguration.setPersistenceAlertAgePolicyValue(
                    propValue == null ? new Long(-1) : new Long(propValue));
            propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_COUNT_PROPERTY);
            mAlertPersistenceConfiguration.setPersistenceAlertCountPolicyValue(
                    propValue == null ? new Long(-1)  : new Long(propValue));
            propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_LEVEL_PROPERTY);
            mAlertPersistenceConfiguration.setPersistenceAlertLevelPolicyValue(
                    propValue == null ? AlertLevelType.NONE  : AlertLevelType.valueOf(propValue)); 
            propValue = mPersistedProps.getProperty(EventManagementConstants.DATABASE_JNDI_NAME_PROPERTY);
            mAlertPersistenceConfiguration.setDataSourceJndiName(propValue == null ? "" :propValue);
            propValue = mPersistedProps.getProperty(EventManagementConstants.DATABASE_TYPE_PROPERTY);
            mAlertPersistenceConfiguration.setAlertPersistenceDBType(propValue == null ? 
                    AlertPersistenceDBType.DERBY  : AlertPersistenceDBType.valueOf(propValue)); 
            propValue = mPersistedProps.getProperty(EventManagementConstants.DATABASE_ALERT_TABLE_NAME_PROPERTY_NAME);
            mAlertPersistenceConfiguration.setAlertTableName((propValue == null ? 
                    createTableName() :  propValue)); 

            propValue = mPersistedProps.getProperty(EventManagementConstants.PERSISTENCE_ENABLED_PROPERTY);
            try {
                mAlertPersistenceConfiguration.setPersistenceEnabled(propValue == null ? new Boolean(false) : new Boolean(propValue));
            } catch (ManagementRemoteException e) {
                // do nothing - reading from configuration persistence can mean
                // 1. persistence not enabled and therefore database is not setup 
                // 2. persistence is  enabled and database setup was done before.
            }
        
        }
    }
    
    private void updateIdentifierInformation() {
        
        InstanceIdentifier instnaceID = InstanceIdentifierFactory.getInstanceIdentifier(mPlatformContext);
        if(instnaceID == null) {
            mDasUniquePort = "";
            
        } else {
            mDasUniquePort = instnaceID.getInstanceUniquePort();
        }
    }
    
    
    private String createTableName() {
        StringBuffer ltableName = new StringBuffer();
        String name =  EVENT_MAMAGENEMT_TABLE_PREFIX  + mHostName + mDasUniquePort ;
        // remove any special char. in order to valid table name
        char[] chr = name.toCharArray();
        
        for(int i=0; i<chr.length; i++)
        {
              if(chr[i] >= '0' && chr[i] <= '9')
              {
                  ltableName.append(chr[i]);
                   continue;  
              }
              if((chr[i] >= 'A' && chr[i] <= 'Z') || (chr[i] >= 'a' && chr[i] <= 'z'))
              {
                  ltableName.append(chr[i]);
                  continue;  
              }
        }
 

        return ltableName.toString();
    }
    
    private void updatePersistedProperties() {
        Properties alertRemovalProps = mAlertPersistenceConfiguration.getPersistenceProperties();
        mPersistedProps.putAll(alertRemovalProps);
        mEventForwardingHelper.saveProperties(mPersistedProps);

    }
    
    private void registerTargetCheckTimer() {
        try {
            
            mTargetCheckTimer = new Timer();
            mTargetCheckTimerObjectName = new ObjectName(EventManagementConstants.EVENT_MANAGEMENT_TARGET_CHECK_TIMER_MBEAN_NAME);
            ((MBeanServer) mMBeanServer).registerMBean(mTargetCheckTimer, mTargetCheckTimerObjectName);
            hookUpToTimerService(mTargetCheckInterval);
            mTargetCheckTimer.start();
        } catch (Exception e) {
             String bundleKey = "caps.management.server.alert.instance.check.timer.setup.error.log.msg";
             String message = AlertUtil.constructLogMessage(bundleKey);
             mLogger.log(Level.WARNING, message, e);
        }
    }

    public void unRegisterTimerService(){
        mTargetCheckTimer.stop();
        unHookFromTimerService();

        try{
            if ( mMBeanServer.isRegistered(mTargetCheckTimerObjectName) ) {
                 mMBeanServer.unregisterMBean(mTargetCheckTimerObjectName);
            }
        }catch(Exception e){
            String bundleKey = "caps.management.server.alert.instance.check.timer.shutdown.error.log.msg";
            String message = AlertUtil.constructLogMessage(bundleKey);
            mLogger.log(Level.WARNING, message, e);
       }
    }
    
    private void hookUpToTimerService(Long aTimeIntervalForTargetCheck){
        try {
            mTargetCheckTimerFilter.enableType(EVENT_TARGETCHECK_FILTER_TYPE);
            mMBeanServer.addNotificationListener(mTargetCheckTimerObjectName, this, mTargetCheckTimerFilter, null);
            // setup the parameters for the timer addNotification method
            Object[] paramsArray = {EVENT_TARGETCHECK_FILTER_TYPE,
                                    "instance check interval",
                                    null, new Date(),
                                    aTimeIntervalForTargetCheck.longValue()};
            String[] paramsSig = {"java.lang.String", "java.lang.String",
                                  "java.lang.Object", "java.util.Date",
                                  "long"};
            mTimerNotificationId =(Integer) mMBeanServer.invoke(mTargetCheckTimerObjectName,
                    "addNotification", paramsArray, paramsSig);
 
        }  catch (Exception ex) {
            String bundleKey = "caps.management.server.alert.instance.check.timer.hook.error.msg";
            String message = AlertUtil.constructLogMessage(bundleKey);
            mLogger.log(Level.WARNING,message, ex);
        }
    }
    
    private void unHookFromTimerService(){
        try{
           if ( mMBeanServer.isRegistered(mTargetCheckTimerObjectName) ) {
               mMBeanServer.removeNotificationListener(mTargetCheckTimerObjectName, this);
               if(mTimerNotificationId != null) {
                   Object[] paramsArray = {mTimerNotificationId};
                   String[] paramsSig = {"java.lang.Integer"};
                   mMBeanServer.invoke(mTargetCheckTimerObjectName,"removeNotification", paramsArray, paramsSig);
               }
           }
       }catch(Exception e){
           String bundleKey = "caps.management.server.alert.instance.check.timer.unhook.error.msg";
           String message = AlertUtil.constructLogMessage(bundleKey);
           mLogger.log(Level.WARNING,message, e.getMessage());
       }
    }    
    
    private void updateTargetsIfNeeded() {
        List<String> currentRegisteredTargets = new ArrayList<String>(mCurrentTargetStateMap.keySet());
        List<String> removeTargetsCandidates = new ArrayList<String>();
        for (String currentTarget : currentRegisteredTargets) {
            removeTargetsCandidates.add(currentTarget);
        }
        Set<String> allTargets= mPlatformContext.getStandaloneServerNames();
        allTargets.addAll(mPlatformContext.getClusteredServerNames());
        for (String target : allTargets) {
            boolean registered = false;
            boolean isUp = mPlatformContext.isInstanceUp(target);
            if(mCurrentTargetStateMap.containsKey(target)== false) {
                EventManagementTargetStateInfo tsi =  
                    new EventManagementTargetStateInfo(target,isUp,false);
                mCurrentTargetStateMap.put(target, tsi);
                if(isUp) {
                  registered=  updateTarget(target);
                  tsi.setHasNotificationRegisteration(registered);
                }
            } else {
                // the current target still exist
                removeTargetsCandidates.remove(target);
                EventManagementTargetStateInfo tsi = mCurrentTargetStateMap.get(target);
                tsi.setIsUp(isUp);
                if(!isUp) {
                    continue; // the target is down not update or reg. will be done
                }else if(tsi.isLastStateUp() == false && tsi.isUp() == true) {
                    registered=  updateTarget(target);
                    tsi.setHasNotificationRegisteration(registered);
                } else {
                    if(!tsi.hasNotificationRegisteration()) {
                        // try to reg. now
                        registered=  updateTarget(target);
                        tsi.setHasNotificationRegisteration(registered);
                    }
                }
            }
        }
        // if any target are left in the list these target
        // were removed
        for (String target : removeTargetsCandidates) {
            mCurrentTargetStateMap.remove(target);
        }
    }
    
    private boolean updateTarget(String target) {
        boolean registered = false;
        if(mPersistenceEnabled) {
            try {
                this.enablePersistanceOnTarget(target);
            } catch (MBeanException e) {
            }
        }
        
        try {
            RegisterWithTarget(target);
            registered = true;
            String bundleKey = "caps.management.server.alert.addlistener";
            String message = AlertUtil.constructMessage(bundleKey,new String[] {target});
            mLogger.log(Level.FINE, message);
            
        } catch (MBeanException e) {
            registered = false;
        }
        return registered;

    }
    
    
}
