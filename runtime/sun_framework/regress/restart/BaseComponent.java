/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BaseComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package restart;

import com.sun.jbi.framework.AbstractComponent;

import java.util.logging.Logger;
import javax.jbi.component.Bootstrap;
import javax.jbi.component.ComponentContext;

/**
 * Base component implementation shared by restart test components.
 *
 * @author Sun Microsystems, Inc.
 */
public class BaseComponent extends AbstractComponent implements Bootstrap
{
    
    /* ######### Component Life Cycle SPI Methods ########## */
    
    /**
     * Initialize the Binding Component.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        mContext = context;
    }
    
    /* #################  Bootstrap SPI Methods ################# */
    
    public void onUninstall() 
        throws javax.jbi.JBIException
    {
    }

    public void onInstall() throws javax.jbi.JBIException
    {
        
    }

    public void init(javax.jbi.component.InstallationContext installationContext)
        throws javax.jbi.JBIException
    {
        
    }

    public void cleanUp() throws javax.jbi.JBIException
    {
        
    }
    
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }
    
    /* ############# ServiceUnitManager SPI Methods ############# */
    
    /**
     * Deploy a Service Unit.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return a deployment status message.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        return createDeployResult("deploy", true);
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return an undeployment status message.
     * @throws javax.jbi.management.DeploymentException if the undeployment
     * operation is unsuccessful.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        return createDeployResult("undeploy", true);
    }
    
    
    /* #####################  Utility Methods #################### */
    
    /** Creates a (un)deployment result string.
     *  @param task 'deploy' or 'undeploy'
     */
    protected String createDeployResult(String task, boolean isSuccess)
    {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
            + "<component-name>" + mComponentName + "</component-name>"
            + "<component-task-result-details>"
            + "<task-result-details>"
            + "<task-id>" + task + "</task-id>"
            + "<task-result>" + (isSuccess ? "SUCCESS" : "FAILED") + "</task-result>"
            + "</task-result-details>"
            + "</component-task-result-details>"
            + "</component-task-result>";
    }
    
    /** Creates a component failure response with a configurable message.
     *  @param task 'deploy' or 'undeploy'
     */
    protected String createErrorResult(String task, String msg)
    {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
            + "<component-name>" + mComponentName + "</component-name>"
            + "<component-task-result-details>"
            + "<task-result-details>"
            + "<task-id>" + task + "</task-id>"
            + "<task-result>" + "FAILED" + "</task-result>"
            + "<message-type>ERROR</message-type>"
            + "<task-status-msg>"
            + "<msg-loc-info>"
            + "<loc-token>JBIWHOOPS</loc-token>"
            + "<loc-message>" + msg + "</loc-message>"
            + "</msg-loc-info>"
            + "</task-status-msg>"
            + "</task-result-details>"
            + "</component-task-result-details>"
            + "</component-task-result>";
    }
}
