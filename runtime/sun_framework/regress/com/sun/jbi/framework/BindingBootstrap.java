/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.component.InstallationContext;

import javax.management.ObjectName;
import javax.management.StandardMBean;

/**
 * This is an implementation of the bootstrap class for a Binding Component
 * that is purely for unit testing. It does nothing but log messages when
 * its methods are called.
 *
 * @author Sun Microsystems, Inc.
 */
public class BindingBootstrap implements javax.jbi.component.Bootstrap
{
    /**
     * InstallationContext instance.
     */
    InstallationContext mCtx;

    /**
     * ComponentContext instance.
     */
    ComponentContext mCompCtx;

    /**
     * Logger instance.
     */
    Logger mLog =
        Logger.getLogger("com.sun.jbi.framework.test.BindingBootstrap");


    /**
     * Cleans up any resources allocated by the bootstrap implementation,
     * including deregistration of the extension MBean, if applicable.
     * This method will be called after the onInstall() or onUninstall() method
     * is called, whether it succeeds or fails.
     * @throws javax.jbi.JBIException when cleanup processing fails to complete
     * successfully.
     */
    public void cleanUp()
        throws javax.jbi.JBIException
    {
        String name = mCtx.getComponentName();
        mLog.info(name + ": cleanUp() called");
        if ( mCtx.isInstall() )
        {
            if ( name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_INSTALL_CLEANUP)
                || name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP)
                || name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_INIT_CLEANUP) )
            {
                throw new javax.jbi.JBIException("bootstrap cleanup failed");
            }
        }
        else
        {
            if ( name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP)
                || name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP) 
                || name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_INIT_CLEANUP) )
            {
                throw new javax.jbi.JBIException("bootstrap cleanup failed");
            }
        }
    }

    /**
     * Initialize for installation.
     * @param installContext is the InstallationContext
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void init(InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        mCtx = installContext;
        mCompCtx = installContext.getContext();
        mLog.info(mCtx.getComponentName() + ": init() called");

        // Create and register extension MBean.

        javax.jbi.management.MBeanNames mbn = mCompCtx.getMBeanNames();
        ObjectName mbName = mbn.createCustomComponentMBeanName(
            "InstallerConfigurationMBean");
        registerMBean(new BindingBootstrapExtension(),
                      BindingBootstrapExtensionMBean.class,
                      mbName);
        String name = mCtx.getComponentName();
        if ( name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_INIT) 
            || name.equals(Constants.BC_NAME_BAD_BOOTSTRAP_INIT_CLEANUP) )
        {
            throw new javax.jbi.JBIException("bootstrap init failed");
        }
        return;
    }

    /**
     * Return the optional installation configuration MBean ObjectName.
     * @return The MBean ObjectName for this MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        mLog.info(mCtx.getComponentName() + ": getExtensionMBeanName() called");
        javax.jbi.management.MBeanNames mbn = mCompCtx.getMBeanNames();
        ObjectName on = mbn.createCustomComponentMBeanName(
            "InstallerConfigurationMBean");
        return on;
    }

    /**
     * Install the Binding Component into the JBI framework.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void onInstall()
        throws javax.jbi.JBIException
    {
        mLog.info(mCtx.getComponentName() + ": onInstall() called");
        String compId = mCtx.getComponentName();
        if ( compId.equals(Constants.BC_NAME_BAD_BOOTSTRAP_ONINSTALL)
            || compId.equals(Constants.BC_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP) )
        {
            throw new javax.jbi.JBIException("unable to install binding");
        }
        else if ( compId.equals(Constants.BC_NAME_BOOTSTRAP_MODIFY_CLASS_PATH) )
        {
            mLog.info("Life cycle class path was " + mCtx.getClassPathElements());
            List cp = new ArrayList();
            cp.add("framework" + File.separator + "bld" + File.separator +
                   "regress" + File.separator + "Binding");
            mCtx.setClassPathElements(cp);
            mLog.info("Life cycle class path now " + mCtx.getClassPathElements());
            mLog.info("Binding component " + compId + " now installed");
        }
        else
        {
            mLog.info("Binding component " + compId + " now installed");
        }
        return;
    }

    /**
     * Uninstall a Binding Component from the JBI framework.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void onUninstall()
        throws javax.jbi.JBIException
    {
        mLog.info(mCtx.getComponentName() + ": onUninstall() called");
        String compId = mCtx.getComponentName();
        if ( compId.equals(Constants.BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL)
            || compId.equals(Constants.BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP) )
        {
            throw new javax.jbi.JBIException("unable to uninstall binding");
        }
        else
        {
            mLog.info("Binding component " + compId + " now uninstalled");
        }
        return;
    }

    /**
     * Register an MBean with the main MBean Server if it is available.
     * @param instance - the MBean implementation instance.
     * @param interfaceClass - the MBean interface implemented by the instance.
     * @param mbeanName - the JMX ObjectName of the MBean.
     * @throws javax.jbi.JBIException If the MBean registration fails.
     */
    private void registerMBean(
        Object instance,
        Class interfaceClass,
        ObjectName mbeanName)
        throws javax.jbi.JBIException
    {
        if ( null == mCompCtx.getMBeanServer() )
        {
            return;
        }
        // Create a StandardMBean.

        StandardMBean mbean = null;
        try
        {
            mbean = new StandardMBean(instance, interfaceClass);
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            throw new javax.jbi.JBIException(
                "Failed to create MBean due to " +
                ncEx.getClass().getName() +
                " exception. Exception message is: " +
                ncEx.getMessage(),
                ncEx);
        }

        // Register the MBean. If it is already registered, unregister it
        // first, as it is an old registration with a stale instance reference.

        try
        {
            if ( mCompCtx.getMBeanServer().isRegistered(mbeanName) )
            {
                try
                {
                    mCompCtx.getMBeanServer().unregisterMBean(mbeanName);
                }
                catch ( javax.management.InstanceNotFoundException infEx )
                {
                    ; // Just ignore this error
                }
            }
            mCompCtx.getMBeanServer().registerMBean(mbean, mbeanName);
        }
        catch ( javax.management.InstanceAlreadyExistsException iaeEx )
        {
            throw new javax.jbi.JBIException(
                "Failed to register MBean due to " +
                iaeEx.getClass().getName() +
                " exception. Exception message is: " +
                iaeEx.getMessage(),
                iaeEx);
        }
        catch ( javax.management.MBeanRegistrationException mbrEx )
        {
            throw new javax.jbi.JBIException(
                "Failed to register MBean due to " +
                mbrEx.getClass().getName() +
                " exception. Exception message is: " +
                mbrEx.getMessage(),
                mbrEx);
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            throw new javax.jbi.JBIException(
                "Failed to register MBean due to " +
                ncEx.getClass().getName() +
                " exception. Exception message is: " +
                ncEx.getMessage(),
                ncEx);
        }
    }

}
