/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSharedLibrary.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.framework.ScaffoldPlatformContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.sun.jbi.JBIProvider;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ComponentState;
/**
 * Tests the SharedLibrary class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSharedLibrary
    extends junit.framework.TestCase
{
    /**
     * Local instance of the EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * Local instance of the SharedLibrary class
     */
    private SharedLibrary mSl1;

    /**
     * Classpath String for Shared Library 1
     */
    private String mSl1ClassPath;

    /**
     * Classpath element list for Shared Library 1
     */
    private ArrayList mSl1ClassPathElements;

    /**
     * Local instance of the SharedLibrary class
     */
    private SharedLibrary mSl2;

    /**
     * Classpath String for Shared Library 2
     */
    private String mSl2ClassPath;

    /**
     * Classpath element list for Shared Library 2
     */
    private ArrayList mSl2ClassPathElements;

    /**
     * Local instance of the SharedLibrary class
     */
    private SharedLibrary mSl3;

    /**
     * Classpath String for Shared Library 3
     */
    private String mSl3ClassPath;

    /**
     * Classpath element list for Shared Library 3
     */
    private ArrayList mSl3ClassPathElements;

    /**
     * Constant for Shared Library 1 unique ID
     */
    static final String SL_1 = "SL01";

    /**
     * Constant for Shared Library 1 description
     */
    static final String SL_1_DESC = "Spacely Sprockets library namespace";

    /**
     * Constant for Shared Library 1 root directory
     */
    static final String SL_1_ROOT = "/SL01";

    /**
     * Constant for Shared Library 1 classpath element
     */
    static final String SL_1_CP_1 = "/com/spacely/sprockets/jetson.jar/";

    /**
     * Constant for Shared Library 2 unique ID
     */
    static final String SL_2 = "SL02";

    /**
     * Constant for Shared Library 2 description
     */
    static final String SL_2_DESC = "Acme Rocket Company library namespace";

    /**
     * Constant for Shared Library 2 root directory
     */
    static final String SL_2_ROOT = "/SL02";

    /**
     * Constant for Shared Library 2 classpath element
     */
    static final String SL_2_CP_1 = "/com/acme/rocket/classes/";

    /**
     * Constant for Shared Library 2 classpath element
     */
    static final String SL_2_CP_2 = "/com/acme/rocket/lib/fuel.jar";

    /**
     * Constant for Shared Library 3 unique ID
     */
    static final String SL_3 = "SL03";

    /**
     * Constant for Shared Library 3 description
     */
    static final String SL_3_DESC = "Flinstones family library";

    /**
     * Constant for Shared Library 3 root directory
     */
    static final String SL_3_ROOT = "/SL03";

    /**
     * Constant for Shared Library 3 classpath element
     */
    static final String SL_3_CP_1 = "/com/flint/stone/lib/fred.jar";

    /**
     * Constant for Shared Library 3 classpath element
     */
    static final String SL_3_CP_2 = "/com/flint/stone/lib/wilma.jar";

    /**
     * Constant for Shared Library 3 classpath element
     */
    static final String SL_3_CP_3 = "/com/flint/stone/lib/pebbles.jar";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestSharedLibrary(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the SharedLibrary instances
     * and other objects needed for the tests. It also serves to test
     * the constructor for the class.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mContext = new EnvironmentContext(new ScaffoldPlatformContext(), 
                new JBIFramework(), new Properties());

        String sep = System.getProperty("path.separator");

        // Create a SharedLibrary with a single-element class path

        mSl1ClassPath = new String(SL_1_CP_1);
        mSl1ClassPathElements = new ArrayList();
        mSl1ClassPathElements.add(SL_1_CP_1);
        mSl1 = new SharedLibrary(SL_1, SL_1_DESC, SL_1_ROOT, true,
                                 mSl1ClassPathElements);

        // Create a SharedLibrary with a two-element class path

        mSl2ClassPath = new String(SL_2_CP_1 + sep +
                                    SL_2_CP_2);
        mSl2ClassPathElements = new ArrayList();
        mSl2ClassPathElements.add(SL_2_CP_1);
        mSl2ClassPathElements.add(SL_2_CP_2);
        mSl2 = new SharedLibrary(SL_2, SL_2_DESC, SL_2_ROOT, false,
                                 mSl2ClassPathElements);

        // Create a SharedLibrary with a three-element class path

        mSl3ClassPath = new String(SL_3_CP_1 + sep +
                                    SL_3_CP_2 + sep +
                                    SL_3_CP_3);
        mSl3ClassPathElements = new ArrayList();
        mSl3ClassPathElements.add(SL_3_CP_1);
        mSl3ClassPathElements.add(SL_3_CP_2);
        mSl3ClassPathElements.add(SL_3_CP_3);
        mSl3 = new SharedLibrary(SL_3, SL_3_DESC, SL_3_ROOT,
                                 mSl3ClassPathElements);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Tests getClassPath with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetClassPathAsString()
        throws Exception
    {
        String cp1 = mSl1.getClassPathAsString();
        String cp2 = mSl2.getClassPathAsString();
        String cp3 = mSl3.getClassPathAsString();

        assertEquals("Got wrong classpath: ",
                     mSl1ClassPath, cp1);
        assertEquals("Got wrong classpath: ",
                     mSl2ClassPath, cp2);
        assertEquals("Got wrong classpath: ",
                     mSl3ClassPath, cp3);
    }

    /**
     * Tests getClassPathElements with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetClassPathElements()
        throws Exception
    {
        List cpList1 = mSl1.getClassPathElements();
        List cpList2 = mSl2.getClassPathElements();
        List cpList3 = mSl3.getClassPathElements();

        assertEquals("Got wrong classpath elements list: ", 
                     mSl1ClassPathElements, cpList1);
        assertEquals("Got wrong classpath elements list: ", 
                     mSl2ClassPathElements, cpList2);
        assertEquals("Got wrong classpath elements list: ", 
                     mSl3ClassPathElements, cpList3);
    }

    /**
     * Tests getComponentClassName with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentClassName()
        throws Exception
    {
        String name = mSl1.getComponentClassName();
                   
        assertNull("Got wrong ComponentClassName, expected null, got " + name,
                   name);
    }

    /**
     * Tests getComponentType with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentType()
        throws Exception
    {
        ComponentType type = mSl1.getComponentType();

        assertEquals("Got incorrect component type: ",
                     ComponentType.SHARED_LIBRARY, type);
    }

    /**
     * Tests getDescription with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDescription()
        throws Exception
    {
        String desc = mSl1.getDescription();

        assertEquals("Got incorrect description: ",
                     SL_1_DESC, desc);
    }

    /**
     * Tests getInstallRoot with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInstallRoot()
        throws Exception
    {
        String root = mSl1.getInstallRoot();

        assertEquals("Got incorrect install root: ",
                     SL_1_ROOT, root);
    }

    /**
     * Tests getName with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetName()
        throws Exception
    {
        String name = mSl1.getName();

        assertEquals("Got incorrect name: ",
                     SL_1, name);
    }

    /**
     * Tests getSharedLibraryNames with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSharedLibraryNames()
        throws Exception
    {
        List list = mSl1.getSharedLibraryNames();
                   
        assertNull("Got wrong SharedLibraryNames, expected null, got " +
                   list, list);
    }

    /**
     * Tests getStatus with good results.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStatus()
        throws Exception
    {
        ComponentState status = mSl1.getStatus();

        assertEquals("Got incorrect status: ", 
                     ComponentState.SHUTDOWN, status);
    }

    /**
     * Test the isClassLoaderSelfFirst method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsClassLoaderSelfFirst()
        throws Exception
    {
        assertTrue("Failure in isClassLoaderSelfFirst: " +
                   "expected true, got false",
                   mSl1.isClassLoaderSelfFirst());
        assertFalse("Failure in isClassLoaderSelfFirst: " +
                    "expected false, got true",
                    mSl2.isClassLoaderSelfFirst());
    }

    /**
     * Test the getBootstrapClassName method. It should always throw an
     * exception.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBootstrapClassName()
        throws Exception
    {
        try
        {
            String cn = mSl1.getBootstrapClassName();
            fail("Expected exception not received");
        }
        catch( UnsupportedOperationException ex )
        {
        }
        catch( Exception e )
        {
            fail("Incorrect exception received");
        }
    }

    /**
     * Test the getBootstrapClassPathElements method. It should always throw an
     * exception.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetBootstrapClassPathElements()
        throws Exception
    {
        try
        {
            java.util.List cp = mSl1.getBootstrapClassPathElements();
            fail("Expected exception not received");
        }
        catch( UnsupportedOperationException ex )
        {
        }
        catch( Exception e )
        {
            fail("Incorrect exception received");
        }
    }

    /**
     * Test the getProperties method. It should always throw an exception.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetProperties()
        throws Exception
    {
        try
        {
            java.util.Map p = mSl1.getProperties();
            fail("Expected exception not received");
        }
        catch( UnsupportedOperationException ex )
        {
        }
        catch( Exception e )
        {
            fail("Incorrect exception received");
        }
    }

    /**
     * Test the getWorkspaceRoot method. It should always throw an exception.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetWorkspaceRoot()
        throws Exception
    {
        try
        {
            String wr = mSl1.getWorkspaceRoot();
            fail("Expected exception not received");
        }
        catch( UnsupportedOperationException ex )
        {
        }
        catch( Exception e )
        {
            fail("Incorrect exception received");
        }
    }

    /**
     * Test the isBootstrapClassLoaderSelfFirst method. It should always throw 
     * an exception.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsBootstrapClassLoaderSelfFirst()
        throws Exception
    {
        try
        {
            boolean sf = mSl1.isBootstrapClassLoaderSelfFirst();
            fail("Expected exception not received");
        }
        catch( UnsupportedOperationException ex )
        {
        }
        catch( Exception e )
        {
            fail("Incorrect exception received");
        }
    }

}
