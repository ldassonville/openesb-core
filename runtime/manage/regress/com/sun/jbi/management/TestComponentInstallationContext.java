/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentInstallationContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Tests for the ComponentInstallationContext class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentInstallationContext
    extends junit.framework.TestCase
{
    /**
     * Component Installation Context.
     */
    private ComponentInstallationContext mContext;

    /**
     * Component class name.
     */
    private String mClassName;

    /**
     * Component class path elements.
     */
    private List mClassPathElements;

    /**
     * Component Context.
     */
    private javax.jbi.component.ComponentContext mComponentContext;

    /**
     * Component name.
     */
    private String mComponentName;

    /**
     * Component type.
     */
    private int mComponentType;

    /**
     * Component description.
     */
    private String mDescription;

    /**
     * Installation descriptor extension data.
     */
    private org.w3c.dom.DocumentFragment mExtensionData;

    /**
     * Component install root directory.
     */
    private String mInstallRoot;

    /**
     * Component work root directory
     */
    private String mWorkspaceRoot;

    /**
     * JBI root directory
     */
    private String mJbiRoot;

    /**
     * Current test name
     */
    private String mTestName;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentInstallationContext(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentInstallationContext
     * instance and other objects needed for the tests. It serves to test the
     * ComponentInstallationContext constructor.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);

        // Create a ComponentInstallationContext instance

        mComponentName = "AcmeSMTPBinding";
        mComponentType = com.sun.jbi.component.InstallationContext.BINDING;
        mClassName = "com.acme.smtp.BindingRuntime";
        mClassPathElements = new ArrayList();
        mClassPathElements.add("lib" + File.separator + "binding.jar");
        mClassPathElements.add("classes");
        mClassPathElements.add("lib" + File.separator + "acmeSMTP.jar");
        mExtensionData = buildExtensionData();

        mContext = new ComponentInstallationContext(
            mComponentName,
            mComponentType,
            mClassName,
            mClassPathElements,
            mExtensionData);

        // Set other fields used by tests

        mComponentContext = null;
        mDescription = "Acme SMTP JBI Binding Component";

        mJbiRoot = File.separator + "sun" + File.separator + "as8" +
            File.separator + "domains" + File.separator + "domain1" +
            File.separator + "jbi" + File.separator + "bindings";
        mInstallRoot = mJbiRoot + File.separator + mComponentName;
        mWorkspaceRoot = mInstallRoot + File.separator + "workspace";
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Test the method for getting the class path elements list.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetClassPathElements()
    {
        System.err.println("mClassPathElements = " + mClassPathElements);
        assertEquals("Failure on getClassPathElements(): ",
                     mClassPathElements,
                     mContext.getClassPathElements());
    }

    /**
     * Test the method for setting the class path elements list.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetClassPathElements()
    {
        List classPathElements = convertPathList(mContext.getClassPathElements());
        classPathElements.add("lib" + File.separator + "extra.jar");
        classPathElements.add("lib" + File.separator + "acmeExtra.jar");
        System.err.println("classPathElements = " + classPathElements);
        mContext.setClassPathElements(classPathElements);
        assertEquals("Failure on setClassPathElements(): ",
                     classPathElements,
                     convertPathList(mContext.getClassPathElements()));
    }

    /**
     * Test the method for setting the class path elements list with a null
     * argument. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetClassPathElementsBadNull()
    {
        List classPathElements = null;
        System.err.println("classPathElements = " + classPathElements);
        try
        {
            mContext.setClassPathElements(classPathElements);
            fail("Expected exception not received");
        }
        catch ( IllegalArgumentException ex )
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIMA0701")));
        }
    }

    /**
     * Test the method for setting the class path elements list with an empty
     * argument. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetClassPathElementsBadEmpty()
    {
        List classPathElements = new ArrayList();
        System.err.println("classPathElements = " + classPathElements);
        try
        {
            mContext.setClassPathElements(classPathElements);
            fail("Expected exception not received");
        }
        catch ( IllegalArgumentException ex )
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIMA0702")));
        }
    }

    /**
     * Test the method for setting the class path elements list with an absolute
     * path in the argument. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetClassPathElementsBadAbsolute()
    {

        List classPathElements = new ArrayList();
        String prefix;
        if ( File.separator.equals("\\") )
        {
            prefix = "C:" + File.separator;
        }
        else
        {
            prefix = File.separator;
        }
        classPathElements.add(prefix + "lib" + File.separator + "extra.jar");
        System.err.println("classPathElements = " + classPathElements);
        try
        {
            mContext.setClassPathElements(classPathElements);
            fail("Expected exception not received");
        }
        catch ( IllegalArgumentException ex )
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIMA0703")));
        }
    }

    /**
     * Test the method for setting the class path elements list with invalid
     * separators in the argument. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetClassPathElementsBadSeparators()
    {

        List classPathElements = new ArrayList();
        String sep = File.separator.equals("/") ? "\\" : "/";
        classPathElements.add("lib" + sep + "extra.jar");
        System.err.println("classPathElements = " + classPathElements);
        try
        {
            mContext.setClassPathElements(classPathElements);
            fail("Expected exception not received");
        }
        catch ( IllegalArgumentException ex )
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIMA0704")));
        }
    }

    /**
     * Test the methods for setting and getting the component class name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetComponentClassName()
        throws Exception
    {
        System.err.println("mClassName = " + mClassName);
        mContext.setComponentClassName(mClassName);
        assertEquals("Failure on getComponentClassName(): ",
                     mClassName, mContext.getComponentClassName());
    }

    /**
     * Test the method for setting and getting the component name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetComponentName()
        throws Exception
    {
        System.err.println("mComponentName = " + mComponentName);
        mContext.setComponentName(mComponentName);
        assertEquals("Failure on getComponentName(): ",
                     mComponentName, mContext.getComponentName());
    }

    /**
     * Test the methods for setting and getting the component context.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetContext()
        throws Exception
    {
        System.err.println("mComponentContext = " + mComponentContext);
        mContext.setContext(mComponentContext);
        assertSame("Failure on setContext()/getContext(): ",
                   mComponentContext, mContext.getContext());
    }

    /**
     * Test the methods for setting and getting the component description.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetDescription()
        throws Exception
    {
        System.err.println("mDescription = " + mDescription);
        mContext.setDescription(mDescription);
        assertEquals("Failure on set/getDescription(): ",
                     mDescription, mContext.getDescription());
    }

    /**
     * Test the method for getting the installation descriptor extension data.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInstallationDescriptorExtension()
        throws Exception
    {
        System.err.println("mExtensionData = ");
        printExtensionData(mExtensionData);
        System.err.println("");
        assertSame("Failure on getInstallationDescriptorExtension(): ",
                   mExtensionData, mContext.getInstallationDescriptorExtension());
    }

    /**
     * Test the method for setting the installation descriptor extension data.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetInstallationDescriptorExtension()
        throws Exception
    {
        System.err.println("mExtensionData = ");
        printExtensionData(mExtensionData);
        System.err.println("");
        mContext.setInstallationDescriptorExtension(mExtensionData);
        assertSame("Failure on setInstallationDescriptorExtension(): ",
                   mExtensionData, mContext.getInstallationDescriptorExtension());
    }

    /**
     * Test the methods for setting/getting the component install root.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetInstallRoot()
        throws Exception
    {
        System.err.println("mInstallRoot = " + mInstallRoot);
        mContext.setInstallRoot(mInstallRoot);
        assertEquals("Failure on setInstallRoot()/getInstallRoot(): ",
                     mInstallRoot, mContext.getInstallRoot());
    }

    /**
     * Test the methods for setting and getting the component work root
     * directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetGetWorkspaceRoot()
        throws Exception
    {
        System.err.println("mWorkspaceRoot = " + mWorkspaceRoot);
        mContext.setWorkspaceRoot(mWorkspaceRoot);
        assertEquals("Failure on getWorkspaceRoot() after setWorkspaceRoot(): ",
                     mWorkspaceRoot, mContext.getWorkspaceRoot());
    }

    /**
     * Test the method for testing for a binding component type.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsBinding()
        throws Exception
    {
        assertTrue("Failure on isBinding(): ",
                   mContext.isBinding());
    }

    /**
     * Test the methods for setting/testing the self-first flag for the
     * bootstrap class loader.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetIsBootstrapClassLoaderSelfFirst()
        throws Exception
    {
        assertFalse("Failure on isBootstrapClassLoaderSelfFirst() " +
                    "after constructor: ",
                    mContext.isBootstrapClassLoaderSelfFirst());

        mContext.setBootstrapClassLoaderSelfFirst();
        assertTrue("Failure on isBootstrapClassLoaderSelfFirst() " +
                   "after setBootstrapClassLoaderSelfFirst(): ",
                   mContext.isBootstrapClassLoaderSelfFirst());

    }

    /**
     * Test the methods for setting/testing the self-first flag for the
     * component class loader.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetIsComponentClassLoaderSelfFirst()
        throws Exception
    {
        assertFalse("Failure on isComponentClassLoaderSelfFirst() " +
                    "after constructor: ",
                    mContext.isComponentClassLoaderSelfFirst());

        mContext.setComponentClassLoaderSelfFirst();
        assertTrue("Failure on isComponentClassLoaderSelfFirst() " +
                   "after setComponentClassLoaderSelfFirst(): ",
                   mContext.isComponentClassLoaderSelfFirst());

    }

    /**
     * Test the method for testing for an engine component type.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsEngine()
        throws Exception
    {
        assertFalse("Failure on isEngine(): ",
                    mContext.isEngine());
    }

    /**
     * Test the method for determining installation/uninstallation.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsInstall()
        throws Exception
    {
        mContext.setIsInstall(true);
        assertTrue("Failure on isInstall() after setIsInstall(true): ",
                   mContext.isInstall());

        mContext.setIsInstall(false);
        assertFalse("Failure on isInstall() after setIsInstall(false): ",
                    mContext.isInstall());
    }

    /**
     * Private method to construct DocumentFragment for extension data.
     * @throws Exception if an unexpected error occurs.
     */
    private org.w3c.dom.DocumentFragment buildExtensionData()
        throws Exception
    {
        javax.xml.parsers.DocumentBuilderFactory docBuilderFactory =
            javax.xml.parsers.DocumentBuilderFactory.newInstance();
        javax.xml.parsers.DocumentBuilder docBuilder =
            docBuilderFactory.newDocumentBuilder();

        org.w3c.dom.Document doc = docBuilder.newDocument();
        org.w3c.dom.DocumentFragment frag = doc.createDocumentFragment();
        org.w3c.dom.Text text1 = doc.createTextNode("409");
        org.w3c.dom.Element element1 = doc.createElement("acmePort");
        element1.appendChild(text1);
        org.w3c.dom.Text text2 = doc.createTextNode("localhost");
        org.w3c.dom.Element element2 = doc.createElement("acmeHost");
        element2.appendChild(text2);
        frag.appendChild(element1);
        frag.appendChild(element2);

        return frag;
    }

    /**
     * Private method to print a DocumentFragment to System.out.
     * @throws Exception if an unexpected error occurs.
     */
    private void printExtensionData(org.w3c.dom.DocumentFragment fragment)
        throws Exception
    {
        javax.xml.transform.TransformerFactory tFactory =
            javax.xml.transform.TransformerFactory.newInstance();
        javax.xml.transform.Transformer transformer =
            tFactory.newTransformer();
   
        javax.xml.transform.dom.DOMSource source =
            new javax.xml.transform.dom.DOMSource(fragment);
        javax.xml.transform.stream.StreamResult result =
            new javax.xml.transform.stream.StreamResult(System.err);
        transformer.transform(source, result);
    }

    /**
     * Convert a file path specification to the platform-specific form by using
     * the value of File.separator. Internally all paths are stored in Unix
     * form using the forward slash ('/') as the separator.
     * @param path the file path specification to be converted.
     * @return the converted file path.
     */
    private String convertPath(String path)
    {
        String retPath;
        if ( File.separator.equals("\\") )
        {
            System.err.println("converting '" + path + "' to Windows form");
            retPath = path.replace('/', File.separatorChar);
        }
        else
        {
            retPath = path;
        }
        return retPath;
    }

    /**
     * Convert a list of file path specifications to the platform-specific form
     * by using the value of File.separator. Internally all paths are stored in
     * Unix form using the forward slash ('/') as the separator.
     * @param pathList the list of file path specifications to be converted.
     * @return a list of String objects containing converted file paths.
     */
    private List convertPathList(List pathList)
    {
        List retList;
        if ( File.separator.equals("\\") )
        {
            retList = new ArrayList();
            Iterator i = pathList.iterator();
            while ( i.hasNext() )
            {
                String e = ((String) i.next()).replace('/', File.separatorChar);
                System.err.println("new value is '" + e + "'");
                retList.add(e);
            }
        }
        else
        {
            retList = pathList;
        }
        return retList;
    }
}
