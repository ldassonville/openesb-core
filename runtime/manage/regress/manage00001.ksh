#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#create a JBI test installation and start it up

. ./regress_defs.ksh

rm -f $JBI_DOMAIN_ROOT/logs/server.log

#reset to a known state by removing previous installations and deployments:
rm -rf $JBI_DOMAIN_ROOT/jbi/components
rm -rf $JBI_DOMAIN_ROOT/jbi/shared-libraries
rm -rf $JBI_DOMAIN_ROOT/jbi/system
rm -rf $JBI_DOMAIN_ROOT/jbi/tmp
mkdir $JBI_DOMAIN_ROOT/jbi/components
mkdir $JBI_DOMAIN_ROOT/jbi/shared-libraries
mkdir $JBI_DOMAIN_ROOT/jbi/system
mkdir $JBI_DOMAIN_ROOT/jbi/tmp

#remove existing JBI registry:
rm -rf $JBI_DOMAIN_ROOT/jbi/config/jbi-registry*

#use common routine to start domain (see antbld/regress/common_defs.ksh):
start_domain
exit $?
