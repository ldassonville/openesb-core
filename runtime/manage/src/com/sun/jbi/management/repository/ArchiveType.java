/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ArchiveType.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

/** 
 *  Typesafe enumeration containing the valid types for an archive.
 */
public class ArchiveType 
{
    private static final String BINDING = "binding-component";
    private static final String ENGINE  = "service-engine";
    
    public static final ArchiveType COMPONENT         = new ArchiveType("COMPONENT");
    public static final ArchiveType SHARED_LIBRARY    = new ArchiveType("SHARED-LIBRARY");
    public static final ArchiveType SERVICE_ASSEMBLY  = new ArchiveType("SERVICE-ASSEMBLY");
    public static final ArchiveType SERVICE_UNIT      = new ArchiveType("SERVICE-UNIT");
    
    private String mType;
    
    private ArchiveType(String type)
    {
        mType = type;
    }
    
    public String toString()
    {
        return mType;
    }
    
    public boolean equals(Object obj)
    {
        boolean isEqual = false;
        
        if (obj instanceof ArchiveType &&
            ((ArchiveType)obj).mType.equals(mType))
        {
            isEqual = true;
        }
        
        return isEqual;
    }
    
    public int hashCode()
    {
        return mType.hashCode();
    }
    
    public static ArchiveType valueOf(String jbiType)
    {
        ArchiveType aType;
        
        if (jbiType.equalsIgnoreCase(BINDING))
        {
            aType = COMPONENT;
        }
        else if (jbiType.equalsIgnoreCase(ENGINE))
        {
            aType = COMPONENT;
        }
        else if (jbiType.equalsIgnoreCase(SHARED_LIBRARY.mType))
        {
            aType = SHARED_LIBRARY;
        }
        else if (jbiType.equalsIgnoreCase(SERVICE_ASSEMBLY.mType))
        {
            aType = SERVICE_ASSEMBLY;
        }
        else if (jbiType.equalsIgnoreCase(SERVICE_UNIT.mType))
        {
            aType = SERVICE_UNIT;
        }
        else
        {
            throw new IllegalArgumentException(jbiType);
        }
        
        return aType;
    }
}
