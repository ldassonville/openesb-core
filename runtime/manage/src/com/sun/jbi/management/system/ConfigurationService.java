/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.config.RuntimeConfiguration;
import com.sun.jbi.management.config.ConfigurationBuilder;
import com.sun.jbi.management.config.ConfigurationFactory;
import com.sun.jbi.management.support.JbiNameInfo;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.management.MBeanServerConnection;
import javax.management.modelmbean.ModelMBeanInfo;
import javax.management.modelmbean.RequiredModelMBean;

/**
 * This is the JBI Framework Configuration Service, which registers the 
 * copnfiguration MBeans required to configure some of the sysytem services.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigurationService extends ModelSystemService
    implements ConfigurationServiceMBean, com.sun.jbi.ServiceLifecycle
{
    /** our immutable name: */
    private final JbiNameInfo mJbiNameInfo =
            new JbiNameInfo("ConfigurationService");

    /**
     * Management context
     */
    private EnvironmentContext mEnv = null;
    
    
    /** The default configuration file name */
    private static final String DEFAULT_CONFIG_FILE = "runtime-config.defaults";

    /** Registered Configuration MBean service types */
    private java.util.Set<MBeanNames.ServiceType> mConfigSvcTypes;

    /** local model init - called by constructor - create custom mbeans. */
    protected void initModelSystemService(com.sun.jbi.EnvironmentContext anEnv)
        throws javax.jbi.JBIException
    {
        String loggerName = 
            com.sun.jbi.management.config.LoggerConfigurationFactory.CONFIGURATION_LOGGER;
       
        Logger logger = Logger.getLogger(loggerName);
        
        //initialize the super.
        super.initModelSystemService(anEnv, logger, mJbiNameInfo);

        //add ConfigurationService MBean to START/STOP mbean set:
        mStartMBeans.add(mConfigServiceMBeanName, com.sun.jbi.management.system.ConfigurationServiceMBean.class, this);
    }
    
    /*-----------------------------------------------------------------------------------*\
     *                  com.sun.jbi.ServiceLifeCycle operations                          *
    \*-----------------------------------------------------------------------------------*/
    
    /**
     * Initialize a service. This performs any initialization tasks
     * required by the service but does not make the service ready
     * to process requests.
     * @param aContext the JBI environment context created
     * by the JBI framework
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void initService(EnvironmentContext aContext)
        throws javax.jbi.JBIException
    {
        mEnv = aContext;
        /*
         * Local initialization of this service.
         * Local routine is responsible for calling super.initModelSystemService(..).
         */
        initModelSystemService(mEnv);
        
        // Register the instance configuration MBeans these are registered here
        // and not in the LifeCycle MBean start() since these MBeans are 
        // required before any of the other system services initialize in
        // initService()
        try
        {
            registerGlobalRuntimeConfigMBeans();
            registerRuntimeConfigMBeans();
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex);
        }
        
        // Register the life cycle MBean for this servie
        bootstrap();
        

    }

    /**
     * Start a service. This makes the service ready to process requests.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void startService()
        throws javax.jbi.JBIException
    {
        // Nothing to be done here
    }

    /**
     * Stop a service. This makes the service stop processing requests.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void stopService()
        throws javax.jbi.JBIException
    {
        try
        {
            // Unregister the config MBeans now
            unregisterRuntimeConfigMBeans();
            unregisterGlobalRuntimeConfigMBeans();
        }
        catch (Exception ex)
        {
            throw new javax.jbi.JBIException(ex);
        }
    }

    /*-----------------------------------------------------------------------------------*\
     *                  ConfigurationServiceMBEan operations                             *
    \*-----------------------------------------------------------------------------------*/
    
    /**
     * Lookup a system Configuration MBean by system service type. For each service type
     * there are two instance MBeans the target specific instance configuration MBean
     * and the global configuration instance MBean.
     *
     * @param aSvcName is the name of the system service
     * @return the JMX object name array of the service Configuration MBean names.
     */
    public ObjectName[] getSystemConfigMBean(String aSvcType)
    {
        MBeanNames mbn = mEnv.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.SERVICE_NAME_KEY + "=" + mbn.SERVICE_NAME_CONFIG_SERVICE;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + aSvcType;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        tmp += ",*";


        ObjectName[] names = null;
        try
        {
            ObjectName configMBeanFilter = new ObjectName(tmp);
            java.util.Set<ObjectName> nameSet = mMBeanServer.queryNames(configMBeanFilter, null);
            names = new ObjectName[nameSet.size()];
            nameSet.toArray(names);
        }
        catch(javax.management.MalformedObjectNameException mbex)
        {
            mLogger.warning(mbex.toString());
        }

        return names;
    }

    /**
     * Looks up all the instance ConfigurationMBeans registered for the JBI system services.
     *
     * This returns all the instance configuration MBeans : instance configuration 
     * and global instance configuration MBeans
     * </br>
     *   ObjectName pattern is : 
     * </br>
     *       com.sun.jbi:ComponentType=System, ServiceName=ConfigurationService
     *
     *
     * @return array of object names for all system service ConfigurationMBeans.
     * @return zero-length array if no services registered.
     */
    public ObjectName[] getSystemConfigMBeans()
    {
        MBeanNames mbn = mEnv.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        //   ObjectName pattern is : com.sun.jbi:ComponentType=System, ServiceName=ConfigurationService
        tmp += ":" + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        tmp += "," + mbn.SERVICE_NAME_KEY + "=" + mbn.SERVICE_NAME_CONFIG_SERVICE;
        //wildcard goes at the end:
        tmp += ",*";

        ObjectName[] names = null;
        try
        {
            ObjectName configMBeanFilter = new ObjectName(tmp);
            java.util.Set<ObjectName> nameSet = mMBeanServer.queryNames(configMBeanFilter, null);
            names = new ObjectName[nameSet.size()];
            nameSet.toArray(names);
        }
        catch(javax.management.MalformedObjectNameException mbex)
        {
            mLogger.warning(mbex.toString());
        }

        //if no configuration mbeans found for any system service...
        if (names.length <= 1)
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.CS_GETSYSTEMCONFIGMBEANS_NO_SERVICES);
            mLogger.severe(statusMsg);
        }

        return names;
    }


/////////
//methods private to ConfigurationService
/////////
    
    /**
     * Register the runtime configuration dynamic MBeans for this instance.
     */
    private void registerRuntimeConfigMBeans()
        throws Exception
    {
        Map<MBeanNames.ServiceType, ConfigurationFactory> 
            configs = ConfigurationBuilder.createConfigurations(readDefaultProperties());
        mConfigSvcTypes = configs.keySet();
        
        for ( MBeanNames.ServiceType serviceType : mConfigSvcTypes )
        {
            ObjectName name = mEnv.getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    ConfigurationBuilder.getControlType(serviceType));

            if ( !mMBeanServer.isRegistered(name))
            {
                // Create and register the facade config MBean
                ModelMBeanInfo mbeanInfo = RuntimeConfiguration.createMBeanInfo(
                    configs.get(serviceType).createMBeanAttributeInfo());
                RequiredModelMBean 
                    modelConfigMBean = new com.sun.jbi.management.config.
                        InstanceConfiguration(mbeanInfo, 
                            ConfigurationCategory.valueOf(serviceType.toString()));
                modelConfigMBean.setManagedResource(modelConfigMBean, "ObjectReference");
                mMBeanServer.registerMBean(modelConfigMBean, name );
            }
        }
    }
    
    
    /**
     * Unregister the runtime configuration dynamic MBeans.
     *
     * @param target - target instance / cluster name
     */
    private void unregisterRuntimeConfigMBeans()
     throws Exception
    {        
        for ( MBeanNames.ServiceType serviceType : mConfigSvcTypes )
        {
            ObjectName name = mEnv.getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    ConfigurationBuilder.getControlType(serviceType));

            // Unregister the config MBean
            if ( mMBeanServer.isRegistered(name) )
            {
                mMBeanServer.unregisterMBean(name);
            }
        }
    }
    
    /**
     * Register the runtime configuration dynamic MBeans for this instance.
     */
    private void registerGlobalRuntimeConfigMBeans()
        throws Exception
    {
        Map<MBeanNames.ServiceType, ConfigurationFactory> 
            configs = ConfigurationBuilder.createConfigurations(readDefaultProperties());
        mConfigSvcTypes = configs.keySet();
        
        ManagementContext mgtCtx = new ManagementContext(mEnv);
        for ( MBeanNames.ServiceType serviceType : mConfigSvcTypes )
        {
            ObjectName name = mgtCtx.getMBeanNames("domain").
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE, 
                    ConfigurationBuilder.getControlType(serviceType));

            if ( !mMBeanServer.isRegistered(name))
            {
                // Create and register the facade config MBean
                ModelMBeanInfo mbeanInfo = RuntimeConfiguration.createMBeanInfo(
                    configs.get(serviceType).createMBeanAttributeInfo());
                RequiredModelMBean 
                    modelConfigMBean = new com.sun.jbi.management.config.
                        GlobalConfiguration(mbeanInfo, 
                            ConfigurationCategory.valueOf(serviceType.toString()));
                modelConfigMBean.setManagedResource(modelConfigMBean, "ObjectReference");
                mMBeanServer.registerMBean(modelConfigMBean, name );
            }
        }
    }
    
    
    /**
     * Unregister the runtime configuration dynamic MBeans.
     *
     * @param target - target instance / cluster name
     */
    private void unregisterGlobalRuntimeConfigMBeans()
     throws Exception
    {   
        ManagementContext mgtCtx = new ManagementContext(mEnv);
        for ( MBeanNames.ServiceType serviceType : mConfigSvcTypes )
        {
            ObjectName name = mgtCtx.getMBeanNames("domain").
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE, 
                    ConfigurationBuilder.getControlType(serviceType));

            // Unregister the config MBean
            if ( mMBeanServer.isRegistered(name) )
            {
                mMBeanServer.unregisterMBean(name);
            }
        }
    }
    
    /** 
     * Read the default configuration file from $JBI_INSTALL_ROOT/lib/templates
     *
     * @return the loaded properties
     * @exception java.io.IOException on file read errors
     */
    private Properties readDefaultProperties()
        throws java.io.IOException
    {
        Properties defaults = new Properties();
        StringBuffer pathToConfig = new StringBuffer(
            mEnv.getJbiInstallRoot());
        pathToConfig.append(java.io.File.separatorChar);
        pathToConfig.append("lib");
        pathToConfig.append(java.io.File.separatorChar);
        pathToConfig.append("install");
        pathToConfig.append(java.io.File.separatorChar);
        pathToConfig.append("templates");
        pathToConfig.append(java.io.File.separatorChar);

        java.io.File defConfig = new java.io.File( pathToConfig.toString() , DEFAULT_CONFIG_FILE);
        if ( defConfig.exists() && defConfig.length() > 0 )
        {
            java.io.InputStream ipStr = new java.io.FileInputStream(defConfig);
            if ( ipStr != null )
            {
                defaults.load(ipStr);
                ipStr.close();
            }
        }
            
        return defaults;
    }
}
