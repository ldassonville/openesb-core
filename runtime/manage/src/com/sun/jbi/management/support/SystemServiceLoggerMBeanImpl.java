/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SystemServiceLoggerMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

import com.sun.jbi.management.common.LoggerMBean;
import com.sun.jbi.management.support.MBeanHelper;
import com.sun.jbi.management.LocalStringKeys;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.AttributeChangeNotification;
import javax.management.MBeanServer;
import javax.management.MBeanRegistration;
import javax.management.ObjectName;

/**
 * LoggerMBean defines standard controls for setting the properties of a
 * single JBI Framework service or JBI Installable Component logger.
 *
 * @author Sun Microsystems, Inc.
 */
public class SystemServiceLoggerMBeanImpl 
    extends DefaultLoggerMBeanImpl
    implements LoggerMBean, NotificationListener, MBeanRegistration,
               com.sun.jbi.util.Constants
{
    /**
     * Flag to determine if this logger Mbean is listening to log level
     * changes.
     */
    private boolean isListening;
    
    /**
     *
     */
    com.sun.jbi.StringTranslator mTranslator;
    
    
    /** Constructs a <CODE>SystemServiceLoggerMBeanImpl</CODE>.  */
    public SystemServiceLoggerMBeanImpl(Logger mLogger) 
        throws Exception
    {
        //allocate a logger:
       this(mLogger, mLogger.getName());
       
       mTranslator = com.sun.jbi.util.EnvironmentAccess.
               getContext().getStringTranslator("com.sun.jbi.management");
       isListening = false;
    }

    /** Constructs a <CODE>SystemServiceLoggerMBeanImpl</CODE>.  */
    public SystemServiceLoggerMBeanImpl(Logger mLogger, String aDisplayName) 
        throws Exception
    {
        //allocate a logger:
        //mLogger = mLogger;
        //mDisplayName = aDisplayName;
        super(mLogger, aDisplayName);
        
        // If there is a persisted log level for this logger
        // set the log level based on that.
        Level level = MBeanHelper.getLogLevel(mLogger.getName(), 
            com.sun.jbi.util.EnvironmentAccess.getContext());
        
        if ( level != null )
        {
            mLogger.setLevel(level);
        }  
        
        mTranslator = com.sun.jbi.util.EnvironmentAccess.
               getContext().getStringTranslator("com.sun.jbi.management");
        
        isListening = false;
    }
   
    /*---------------------------------------------------------------------------------*\
     *                           NotificationListener Operations                       *
    \*---------------------------------------------------------------------------------*/
    
    /**
     *  handle a notification from the Logger Configuration MBean
     *
     * @param notification - the notification 
     * @param the callback passed in 
     */
    public void handleNotification(Notification notification, Object handback)
    {
        if ( notification instanceof AttributeChangeNotification )
        {
            AttributeChangeNotification notif = (AttributeChangeNotification) notification;
            
            if ( notif.getAttributeName().equals(mLogger.getName()) )
            {
                mLogger.fine("run time log level change notifictaion for " 
                        + mLogger.getName() + " from " 
                        + notif.getOldValue() + " to " + notif.getNewValue());
            }
            
            String level = (String) notif.getNewValue();
            try
            {
                if ( level == null || level == "null" || LOG_LEVEL_DEFAULT.equals(level) )
                {
                    super.setDefault();
                }
                else
                {
                    mLogger.setLevel(Level.parse(level));
                }
            }
            catch ( IllegalArgumentException iex )
            {
                mLogger.fine("Failed to set log level for logger "
                            + mLogger.getName() + " to " + level + " : "
                            + iex.getMessage());
            }
        }
    }
    
    /*---------------------------------------------------------------------------------*\
     *                           MBean Registration Operations                         *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Preregsitration event. 
     */
    public ObjectName preRegister(MBeanServer server,
                       ObjectName name) throws Exception
    {
        return name;
    }
    
    /**
     * Post registration event. Start listening to log level changes
     */
    public void postRegister(Boolean registrationDone)
    {
        if (registrationDone)
        {
            startListeningToLogLevelChanges();
            isListening = true;
            mLogger.fine( "Logger " + mLogger.getName() 
             + " is now listening for changes in the logger configuration");
        }
    }
    
    /**
     * Prederegistration event. 
     */
    public void preDeregister()
        throws Exception
    {
        // nop
    }
    
        
    /**
     * Post deregistration event. Stop listening to log level changes
     */
    public void postDeregister()
    {
        if (isListening)
        {
            stopListeningToLogLevelChanges();
            isListening = false;
            mLogger.fine( "Logger " + mLogger.getName() 
             + " has stopped listening for changes in the logger configuration");
        }
    }
   
    /*---------------------------------------------------------------------------------*\
     *                           Private Helpers                                       *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Register as a listener for attribute change events
     */
    private void startListeningToLogLevelChanges()
    {
        javax.management.MBeanServer mbeanServer =
                com.sun.jbi.util.EnvironmentAccess.getContext().getMBeanServer();
         
        /**
         * Create a AttributeChangeNotificationFilter 
         */
        javax.management.AttributeChangeNotificationFilter filter =
                new javax.management.AttributeChangeNotificationFilter();
        filter.disableAllAttributes();
        filter.enableAttribute(mLogger.getName());
        
        try
        {
            mbeanServer.addNotificationListener(MBeanHelper.getLoggerConfigMBeanName(), 
                this, filter, null);
        }
        catch( Exception ex )
        {
            String[] params = new String[]{
                                mDisplayName, 
                                mLogger.getName(), 
                                MBeanHelper.getLoggerConfigMBeanName().toString(),
                                ex.getMessage()};
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_FAILED_ADD_LOGGER_LISTENER,
                    params);
            mLogger.info(errMsg);
        }
    }
    
        
    /**
     * Stop listening to attribute change events
     */
    private void stopListeningToLogLevelChanges()
    {
        javax.management.MBeanServer mbeanServer =
                com.sun.jbi.util.EnvironmentAccess.getContext().getMBeanServer();
        
        
        try
        {
            if (mbeanServer.isRegistered(MBeanHelper.getLoggerConfigMBeanName()))
            {
                mbeanServer.removeNotificationListener(MBeanHelper.getLoggerConfigMBeanName(), 
                    this);
            }
        }
        catch( Exception ex )
        {
            String[] params = new String[]{
                                mDisplayName, 
                                mLogger.getName(), 
                                MBeanHelper.getLoggerConfigMBeanName().toString(),
                                ex.getMessage()};
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_FAILED_RM_LOGGER_LISTENER,
                    params);
            mLogger.warning(errMsg);
        }
    }

    

}
