/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)GenericQuery.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  GenericQuery.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry;

import java.math.BigInteger;
import java.util.List;
import com.sun.jbi.ComponentType;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.descriptor.Jbi;

/**
 * This interface encapsulates queries which are common to 
 * all targets ( domain / server / cluster ).
 *
 * @author Sun Microsystems, Inc.
 */
public interface GenericQuery
{
 
    /** 
     * @param componentName - Component name
     * @return a List<String> of all servers installing a component.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    List<String> getServersInstallingComponent(String componentName)
        throws RegistryException;
    
    /** 
     * @param  sharedLibraryName - Shared Library name
     * @return a List<String> of all servers installing a shared library.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    List<String> getServersInstallingSharedLibrary(String sharedLibraryName)
        throws RegistryException;
    
    /** 
     * @param  serviceAssemblyName - Service Assembly name
     * @return a List<String> of all servers deploying a service assembly.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    List<String> getServersDeployingServiceAssembly(String serviceAssemblyName)
        throws RegistryException;  
    
    
    /** 
     * @param componentName - Component name
     * @return a List<String> of all clusters installing a component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */    
      List<String> getClustersInstallingComponent(String componentName)
        throws RegistryException;
    
    /** 
     * @param  sharedLibraryName - Shared Library name
     * @return a List<String> of all clusters installing a shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    List<String> getClustersInstallingSharedLibrary(String sharedLibraryName)
        throws RegistryException;
    
    /** 
     * @param  serviceAssemblyName - Service Assembly name
     * @return a List<String> of all clusters deploying a service assembly.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    List<String> getClustersDeployingServiceAssembly(String serviceAssemblyName)
        throws RegistryException;   
    
    
    /**
     * @param componentName - component name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    String getComponentInstallationDescriptor(String componentName)
        throws RegistryException;

    /**
     * @param componentName - component name
     * @return wrapper for the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    ComponentDescriptor getComponentDescriptor(String componentName)
        throws RegistryException;

    /**
     * @param sharedLibraryName - shared library name
     * @return the Installation descriptor for the shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    String getSharedLibraryInstallationDescriptor(String sharedLibraryName)
        throws RegistryException;
    
    /**
     * @param serviceAssemblyName - service assembly name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    String getServiceAssemblyDeploymentDescriptor(String serviceAssemblyName)
        throws RegistryException;  
    
    /**
     * @param serviceAssemblyName - service assembly name
     * @param serviceUnitName - service unit name
     * @return the Installation descriptor for the service unit.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    String getServiceUnitDeploymentDescriptor(String serviceAssemblyName,
						String serviceUnitName)
        throws RegistryException;  

    /**
     * @param serviceAssemblyName - service assembly name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    String getServiceAssemblyArchive(String serviceAssemblyName)
        throws RegistryException;
    
    /**
     * @param componentName - component name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    String getComponentArchive(String componentName)
        throws RegistryException;
    
     /**
     * @param sharedLibraryName - sharedLibrary name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    String getSharedLibraryArchive(String sharedLibraryName)
        throws RegistryException;
    
   /**
     * @return true if a component is installed on one or more servers / clusters
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    boolean isComponentInstalled(String componentName)
        throws RegistryException;
    
    /**
     * @return true if a shared library is installed on one or more servers / clusters
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    boolean isSharedLibraryInstalled(String sharedLibraryName)
        throws RegistryException;
    
    /**
     * @return true if a service assembly is deployed on one or more servers / clusters
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    boolean isServiceAssemblyDeployed(String serviceAssemblyName)
        throws RegistryException;
    
    ComponentType getComponentType(String componentName)
        throws RegistryException;
    
    /**
     * @return a list of Servers from the registry
     */
    List<String> getServers()
        throws RegistryException;
    
    /**
     * @return  a list of Clusters from the registry
     */
    List<String> getClusters()
        throws RegistryException;
    
            
    /**
     * @return true if the Shared Library is registered in the domain.
     */
    boolean isSharedLibraryRegistered(String sharedLibraryName)
        throws RegistryException;
    
    
    /**
     * @return true if the Service Assembly is registered in the domain.
     */
    boolean isServiceAssemblyRegistered(String serviceAssemblyName)
        throws RegistryException;
    
    /**
     * @return true if the component is registered in the domain.
     */
    boolean isComponentRegistered(String componentName)
        throws RegistryException;
    
    /**
     * @return a List of Service Assemblies registered in the domain.
     */
    List<String> getRegisteredServiceAssemblies()
        throws RegistryException;
    
    /**
     * @return a List of Shared Libraries registered in the domain.
     */
    List<String> getRegisteredSharedLibraries()
        throws RegistryException;
    
    
    /**
     * @return a List of Components registered in the domain.
     */
    List<String> getRegisteredComponents()
        throws RegistryException;
    
 
    /**
     * This method is used to find out if this is a system
     * component.
     * This method returns the value of system-install attribute
     * for this component from the registry
     * A component that has system-install set to true will have
     * its install root under AS_INSTALL/jbi and it should not be
     * deleted on unload of the component's uninstaller.
     * @param componentName the name of the component
     * @return boolean true if system-install is true, false otherwise
     *
     */
    boolean isSystemComponent(String componentName)
        throws RegistryException;
    
    /**
     * This method is used to find out if this is a system
     * shared library 
     * This method returns the value of system-install attribute
     * for this shared library from the registry
     * A shared library that has system-install set to true will have
     * its install root under AS_INSTALL/jbi and it should not be
     * deleted on uninstall
     * @param sharedLibraryName the name of the shared library
     * @return boolean true if system-install is true, false otherwise
     *
     */
    boolean isSystemSharedLibrary(String sharedLibraryName)
        throws RegistryException;   
    
    /**
     * Get the file name for the domain component.
     *
     * @param componentName - component name
     */
    String getComponentFileName(String componentName)
        throws RegistryException;
    
    /**
     * Get the file name for the domain shared library.
     *
     * @param slName - shared library  name
     */
    String getSharedLibraryFileName(String slName)
        throws RegistryException;
     
    /**
     * Get the file name for the domain service assembly.
     *
     * @param saName - service assembly name
     */
    String getServiceAssemblyFileName(String saName)
        throws RegistryException;    
  
    /**
     * Get the timestamp for the domain component.
     *
     * @param componentName - component name
     */
    long getComponentTimestamp(String componentName)
        throws RegistryException;
    
    /**
     * Get the timestamp for the domain shared library.
     *
     * @param slName - shared library  name
     */
    long getSharedLibraryTimestamp(String slName)
        throws RegistryException;
     
    /**
     * Get the timestamp for the domain service assembly.
     *
     * @param saName - service assembly name
     */
    long getServiceAssemblyTimestamp(String saName)
        throws RegistryException;  
    
    /**
     * This method is used to get the value of the attribute upgrade-number from the 
     * domain level entry for the component in the registry
     * @param componentName the componentName
     * @return BigInteger the upgrade number
     * @throws RegistryException if the upgrade number could not be retrieved
     */
    public BigInteger getComponentUpgradeNumber(String componentName)
        throws RegistryException;    
    
    /*--------------------------------------------------------------------------------*\
     *                       Get configuration attribute values                       *
    \*--------------------------------------------------------------------------------*/
     
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     * @return the String representation of the attributes value
     */
    String getAttribute(ConfigurationCategory type, String name)
        throws RegistryException;
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the specified target. 
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     * @return the String representation of the attributes value
     */
    String getAttribute(String targetName, ConfigurationCategory type, String name)
        throws RegistryException;
    
    /** 
     * Determine if a configuration attribute is overriden by a target.
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @return true if the attribute is overriden by a target
     */
    public boolean isAttributeOverriden(String targetName, ConfigurationCategory type, 
        String name);
    
    /**
     * Determine if the domain configuration is present in the registry
     *
     * @return true if the domain-config is defined in the registry
     */
    boolean isGlobalConfigurationDefined();
}
