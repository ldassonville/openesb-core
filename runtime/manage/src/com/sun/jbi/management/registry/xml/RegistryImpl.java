/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  RegistryImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 
 *  Created on August 22, 2005, 4:27 PM
 */

package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentQuery;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistrySpec;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.Repository;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Writer;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;
import java.text.DateFormat;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Validator;

import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.TimeUnit;
import com.sun.jbi.management.util.LockManager;
import com.sun.jbi.management.util.FileHelper;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.system.ManagementException;

import org.xml.sax.SAXException;
/**
 * This is an implementation of the Registry which persists the information as XML.
 *
 * @author Sun Microsystems, Inc.
 */
public class RegistryImpl
    implements Registry
{   
    /**
     * The Registry folder and files.
     */
    //public static final String REGISTRY_FOLDER_PROPERTY = "jbi.registry.folder";
    public static final String REGISTRY_FILE = "jbi-registry.xml";
    public static final String REGISTRY_SYNC_FILE = "jbi-registry-sync.xml";
    public static final String REGISTRY_BKUP_FILE = "jbi-registry-backup.xml";
    public static final String REGISTRY_ERROR_FOLDER = "error";

    /**
     * registry schema file
     */
    public static final String JBI_REGISTRY_SCHEMA = "jbi-registry.xsd";
    
    /**
     * registry schema subdir in JBI_HOME
     */
    public static final String JBI_REGISTRY_SCHEMA_DIR = "schemas";
    
    /**
     * The Validation Property : value / { true / false }. Indicates 
     * whether the contents of the registry should be validated before
     * serialization.
     */
    public static final String VALIDATE = "validate";
    
    
    /**
     * The Registry XML Package.
     */
    public static final String REGISTRY_XML_PACKAGE = 
        "com.sun.jbi.management.registry.xml";
    
    /**
     * The Properties for the Registry.
     */
    private Properties mProps = null;
    
    /**
     * The Root of the EsbConfiguration.
     */
    private Jbi mJbiRegistry;

    /**
     * Platform details.
     */
    private PlatformContext mPlatform;
   
    /**
     * The Marshaller for persisting the EsbConfiguration.
     */
    private Marshaller mMarshaller = null;
    
   
    /**
     * The LockManager which is used to synchronize access to the 
     * in-memory registry.
     */
    private LockManager mRegistryObjRWLockManager;
    
    /**
     * The LockManager which is used to synchronize access to the 
     * serialized registry.
     */ 
    private LockManager mSerializationRWLockManager;
       
     /**
      * The Ctx
      */
    private ManagementContext mMgtCtx;
    
    private RegistrySpec mSpec;
    
    /**
     *
     */
    static final String STRING_TRANSLATOR_NAME = "com.sun.jbi.management";
    
    /**
     *
     */
    private StringTranslator mTranslator;
    
    /**
     *
     */
    private Logger mLogger;
    
    /**
     * The JAXB Context.
     */
    private static JAXBContext sJC;
    
    /**
     * registry path strings
     */
    private String mRegistryFolderPath;
    private String mErrorFolderPath;
    private String mRegistryFilePath;
    private String mRegistryFile;
    private String mRegistrySyncFilePath;
    private String mRegistrySyncFile;
    private String mRegistryBackupFilePath;
    
    /**
     * The Registry utility.
     */
    private RegistryUtil mRegUtil;
    
    /**
     * registry schema file
     */
    private File mRegSchema;
   
    
    /**
     * @throws RegistryExcepion if the JAXB Context cannot be initialized
     */
    private static void initJaxbContext()
        throws Exception
    {
        if ( sJC == null )
        {
            ClassLoader cl = 
                    Class.forName(
                        "com.sun.jbi.management.registry.xml.Jbi").
                        getClassLoader();
            sJC = JAXBContext.newInstance( REGISTRY_XML_PACKAGE, cl);
        }
    }
        
    /** 
     * Creates a new instance of RegistryImpl.
     *
     * @param spec - XML Registry specification.
     * @throws RegistryException if the JAXBContext cannot be initialized.
     */
    public RegistryImpl(RegistrySpec spec)
        throws RegistryException
    {
        String      regFile;
        String      syncFile;
        
        mSpec = spec;
        mProps = spec.getProperties(); 
        mRegistryObjRWLockManager = null;
        mSerializationRWLockManager = null;
        
        mLogger = Logger.getLogger("com.sun.jbi.management.registry");
        
        mMgtCtx = spec.getManagementContext();
        mPlatform = mMgtCtx.getEnvironmentContext().getPlatformContext();
        mTranslator = mMgtCtx.getEnvironmentContext().getStringTranslator(STRING_TRANSLATOR_NAME);
        mRegistryFolderPath = mProps.getProperty(REGISTRY_FOLDER_PROPERTY);
        mErrorFolderPath  = mRegistryFolderPath + File.separator + REGISTRY_ERROR_FOLDER;
        
        //
        //  Allow opening different named file.
        //
        if ((regFile = mProps.getProperty(REGISTRY_FILE_PROPERTY)) == null)
        {
            regFile = REGISTRY_FILE;
        }

        mRegistryFile = regFile;
        mRegistryFilePath = mRegistryFolderPath + File.separator + regFile;
        mRegistryBackupFilePath = mRegistryFolderPath + File.separator + REGISTRY_BKUP_FILE;
        
        try
        {
            File schemaDir = new File(mMgtCtx.getEnvironmentContext().getJbiInstallRoot(), JBI_REGISTRY_SCHEMA_DIR);
            mRegSchema = new File(schemaDir, JBI_REGISTRY_SCHEMA);            

            initJaxbContext();
        }
        catch (Exception ex ) 
        {
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_FAILED_JAXB_INIT,
                ex.toString());
            mLogger.severe(errMsg);
            
            throw new RegistryException(errMsg, ex);
        }
        mJbiRegistry = initJbiRegistry();
        
        try
        {
            // -- Make sure registry and repository are in sync
            mRegUtil = new RegistryUtil(mMgtCtx, this);
            mRegUtil.syncWithRepository();
        }
        catch (Exception ex)
        {
            throw new RegistryException(ex);
        }
    }

    /**
     * Destroy the Registry
     */
    public void destroy()
    {
        // -- Delete the esb registry files and derefernce memory object
        File regFile = new File( getRegistryFilePath());
        File bkupFile = new File( getRegistryBkupFilePath());
        
        if ( regFile.exists() )
        {
            regFile.delete();
        }

        if ( bkupFile.exists() )
        {
            bkupFile.delete();
        }        
    
        mJbiRegistry = null;
    }
    

    /**
     * Initialize the registry
     */
    public void reinitialize()
        throws RegistryException
    {
        mJbiRegistry = null;
        initJbiRegistry();
    }    
    
    /**
     * Get the Esb Configuration Root. If the esb configuration is not loaded, it is 
     * loaded anew.
     *
     * @return the EsbConfiguration root.
     * @throws RegistryException if the in-memory Registry cannot be created
     */
    private synchronized Jbi initJbiRegistry()
        throws RegistryException
    {
        if ( mJbiRegistry == null )
        {
            try
            {
                String lockWaitInterval = 
                    mProps.getProperty(Registry.REGISTRY_LOCK_INTERVAL_PROPERTY);
                mRegistryObjRWLockManager = new LockManager( 
                    new ReentrantReadWriteLock(), "Registry Object Lock", mMgtCtx,
                        lockWaitInterval);
                mSerializationRWLockManager = new LockManager( 
                    new ReentrantReadWriteLock(), "Registry Serialization Lock", mMgtCtx,
                        lockWaitInterval);
                
                mMarshaller = sJC.createMarshaller();
                mMarshaller.setProperty("jaxb.formatted.output", new Boolean(true));

                mMarshaller.setSchema(
                    javax.xml.validation.SchemaFactory.newInstance(
                        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(mRegSchema));
                
                File regFile = new File(getRegistryFilePath());
                File bkupFile = new File(getRegistryBkupFilePath());
                
                mLogger.finer("Registry file is " + getRegistryFilePath());
                /**
                if ( regFile.exists() && regFile.length() == 0 )
                {
                    mLogger.info("Deleting ZERO length " + getRegistryFilePath());
                    regFile.delete();
                }*/
                
                if ( !regFile.exists() )
                    
                {
                    if ( !bkupFile.exists() )
                    {
                        createPrimaryRegistry();
                    }
                    else
                    {
                        loadBackupRegistry();
                    }
                }
                else
                {
                    try
                    {
                        loadPrimaryRegistry();
                    }
                    catch ( Exception ex )
                    {     
                        mLogger.warning(ex.toString ());
                        if ( bkupFile.exists() )
                        {
                            loadBackupRegistry();
                        }
                        else
                        {
                           throw new RegistryException(mTranslator.getString(
                                LocalStringKeys.JBI_ADMIN_REGISTRY_CORRUPT,
                                getRegistryFilePath(),
                                ex.getMessage(),
                                getRegistryBkupFilePath())); 
                        }
                    }
                }
            }
            catch (SAXException se) {
                se.printStackTrace();
                
                String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_REGISTRY_CREATION_FAILED, 
                        se.toString());
                throw new RegistryException(errMsg, se);
                               
            }
            catch ( JAXBException ex ) 
            {
                ex.printStackTrace();
                
                String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_REGISTRY_CREATION_FAILED, 
                        ex.toString());
                throw new RegistryException(errMsg, ex);
               
            }
            catch ( java.io.IOException ex ) 
            {
                ex.printStackTrace();
                String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_REGISTRY_CREATION_FAILED, 
                        ex.toString());
                throw new RegistryException(errMsg, ex);
            }
        }
        
        return mJbiRegistry;
    }
    
    /**
     * Load the Registry information and validate it.
     *
     * @throws java.io.IOException
     * @throws JAXBException
     */
    private synchronized void loadPrimaryRegistry()
        throws java.io.FileNotFoundException, JAXBException
    {
        File registryFile = new File( getRegistryFilePath() );
        try
        {
            Unmarshaller u         = sJC.createUnmarshaller();
            
            u.setSchema(
                    javax.xml.validation.SchemaFactory.newInstance(
                        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(mRegSchema));            
 
            FileInputStream fis = new FileInputStream(registryFile);
            mJbiRegistry = (Jbi) u.unmarshal( fis );
            try
            {
                fis.close();
            }
            catch ( java.io.IOException ioex)
            { 
                mLogger.warning(ioex.toString());
            }
            
        }
        catch(SAXException se)
        {
            se.printStackTrace();
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_INVALID_REGISTRY_FORMAT, 
                    se.toString());
            throw new JAXBException(errMsg, se);
        }               
        catch ( JAXBException ex )
        {
            File errFile = new File(mErrorFolderPath + File.separator + "registry-" + 
                System.currentTimeMillis () + ".xml");
            movetoErrFile(registryFile, errFile);
            throw ex;
        }
    }
    
    /**
     * Take a back-up of a bad registry file in a err file
     */
    private void movetoErrFile(File origFile, File errFile)
    {

        File errFolder = new File(mErrorFolderPath);
        if ( !errFolder.exists() )
        {
            FileHelper.createFolder(errFolder);
        }
        
        if (!origFile.renameTo(errFile))
        {
            mLogger.warning(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_REGISTRY_FILE_RENAME_FAILED, 
                    origFile.getAbsolutePath(), 
                    errFile.getAbsolutePath()));
        }
    }
    
    /**
     * @throws JAXBException if backup file is corrupted / invalid
     * @throws java.io.IOException
     */
    private synchronized void loadBackupRegistry()
        throws java.io.IOException, JAXBException
    {
        File bkupFile     = new File( mRegistryBackupFilePath ); 
        File regFile      = new File( getRegistryFilePath() );
        Date lastModified = new Date(bkupFile.lastModified());
        
        mLogger.warning(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_REGISTRY_USING_BKUP, 
            mRegistryBackupFilePath, 
            DateFormat.getDateTimeInstance().format(lastModified)));
       
        
        // -- Move the back-up data to the main file
        if (!bkupFile.renameTo(regFile))
        {
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_REGISTRY_FILE_RENAME_FAILED,
                mRegistryBackupFilePath, mRegistryFilePath);

            throw new java.io.IOException(errMsg);
        }
        
        try{
            

            Unmarshaller u      = sJC.createUnmarshaller();
            u.setSchema(
                    javax.xml.validation.SchemaFactory.newInstance(
                        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(mRegSchema));            
 
            FileInputStream fis = new FileInputStream(regFile);
            mJbiRegistry = (Jbi) u.unmarshal( fis ); 
            try
            {
                fis.close();
            }
            catch ( java.io.IOException ioex)
            {
                mLogger.warning(ioex.toString());
            }

        }
        catch (SAXException se)
        {
                se.printStackTrace();
                String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_INVALID_REGISTRY_FORMAT, 
                        se.toString());
                throw new JAXBException(errMsg, se);
                                           
        }
    }
    
    /**
     * Fresh start-up. Create the Primary registry file.
     * @throws RegistryException
     */
    private synchronized void createPrimaryRegistry()
        throws RegistryException
    {

        try
        {
            File registryFile = new File( mRegistryFilePath ); 
       
            
            mLogger.fine(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_REGISTRY_CREATE,
                mRegistryFilePath));
            
            
            FileHelper.createFile(registryFile);
            // -- Create the Esb Configuration from scratch
            ObjectFactory factory = new ObjectFactory();
            mJbiRegistry = 
                (Jbi) new ObjectFactory().createJbi();

            // -- Add empty Instances, Components, Shared Libraries
            // -- and Service Assemblies
            mJbiRegistry.setClusters(factory.createClusterListType());
            mJbiRegistry.setServers(factory.createServerListType());
            mJbiRegistry.setSharedLibraries(factory.createSharedLibraries());
            mJbiRegistry.setServiceAssemblies(factory.createServiceAssemblies());
            mJbiRegistry.setConfigs(factory.createConfigs());

            // -- Save the Changes
            Writer wr = getRegistryWriter();
            String validate = mProps.getProperty (VALIDATE, "false").toLowerCase();
            if ( Boolean.getBoolean(validate) )
            {            
                mMarshaller.setSchema(
                    javax.xml.validation.SchemaFactory.newInstance(
                        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(mRegSchema));
            }
                
            mMarshaller.marshal(mJbiRegistry, wr);
            wr.close();                    
        }
        catch (SAXException se)
        {
            se.printStackTrace(); 
            String errMsg = 
                mTranslator.getString(LocalStringKeys.JBI_ADMIN_INVALID_REGISTRY_OBJECT);
             mLogger.severe(errMsg);
             throw new RegistryException(errMsg);
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
            throw new RegistryException(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_REGISTRY_CREATE_FAILED, 
                mRegistryFilePath, ex.getMessage()));
           
        }       
    }
    
    /**
     * @return the Marshaller.
     */
    Marshaller getMarshaller()
    {
        return mMarshaller;
    }
    
    /**
     * Get the Updater instance to be used for updating the Registry.
     *
     * @return an instance of the Updater to be used to manipulate 
     * ( create / delete / modify ) contents of the registry.
     * @throws RegistryException if problems are encountered in creating
     * a Updater.
     */
    public Updater getUpdater() throws RegistryException
    {
        String validate = mProps.getProperty (VALIDATE, "false").toLowerCase();
        initJbiRegistry();
        return new UpdaterImpl(mJbiRegistry, mMgtCtx, Boolean.getBoolean(validate), this );
    }
    
    /**
     * Get a Generic Query Instance
     * @return an instance of GenericQuery.
     * @throws RegistryException on errors.
     */
    public GenericQuery getGenericQuery() throws RegistryException
    {

        String validate = mProps.getProperty (VALIDATE, "false").toLowerCase();
        initJbiRegistry();
        return new GenericQueryImpl(mJbiRegistry, mMgtCtx, Boolean.getBoolean(validate), 
            this);
    }
    
    /**
     * Get a ComponentQuery instance for a given target
     *
     * @param targetName - name identifying the target.
     * @return an instance of ComponentQuery.
     * @throws RegistryException on errors.
     */
    public ComponentQuery getComponentQuery(String targetName) throws RegistryException
    {
        String validate = mProps.getProperty (VALIDATE, "false").toLowerCase();
        initJbiRegistry();
        return new ComponentQueryImpl(mJbiRegistry, mMgtCtx, Boolean.getBoolean(validate),
            targetName, this);
    }
    
    /**
     * Get the ComponentQuery specific to the target of the instance.
     *
     * @return an instance of ComponentQuery.
     * @throws RegistryException on errors.
     */
    public ComponentQuery getComponentQuery() throws RegistryException
    {
        return getComponentQuery(mPlatform.getTargetName());
    }
    
    /**
     * Get a ServiceAssemblyQuery instance for a given target
     *
     * @param targetName - name identifying the target.
     * @return an instance of ServiceAssemblyQuery.
     * @throws RegistryException on errors.
     */
    public ServiceAssemblyQuery getServiceAssemblyQuery(String targetName) throws RegistryException
    {        
        String validate = mProps.getProperty (VALIDATE, "false").toLowerCase();
        initJbiRegistry();
        return new ServiceAssemblyQueryImpl(mJbiRegistry, mMgtCtx, Boolean.getBoolean(validate),
            targetName, this);
    }
    
    
    /**
     * Get the ServiceAssemblyQuery specific to the target of the instance.
     *
     * @return an instance of ServiceAssemblyQuery.
     * @throws RegistryException on errors.
     */
    public ServiceAssemblyQuery getServiceAssemblyQuery()
        throws RegistryException
    {
        return getServiceAssemblyQuery(mPlatform.getTargetName());
    }
    
    /**
     * @return a Writer to the XML file.
     */
    Writer getRegistryWriter()
    {
        OutputStreamWriter osw = null;
        try
        {
            FileOutputStream fos =  new FileOutputStream( getRegistryFile() ); 
            osw = new OutputStreamWriter(fos, "UTF8");
        }
        catch ( IOException ioe )
        {
            ioe.printStackTrace();
        }
        return osw;
    }
    
    /**
     * @return a the Registry File reference.
     */
    public File getRegistryFile()
    {
        File regFile = new File(mRegistryFilePath);

        if ( !(regFile.exists()) )
        {
            FileHelper.createFile(regFile);
        }
        return regFile;
    }
    
    /**
     * @return the path to the Registry Folder
     */
    public String getRegistryFolderPath()
    {
        return mRegistryFolderPath;
    }
    
    /**
     * @return the path to the Registry File
     */
    public String getRegistryFilePath()
    {
        return mRegistryFilePath;
    }
    
    /**
     * @return the path to the Registry Backup File
     */
    public String getRegistryBkupFilePath()
    {
        return mRegistryBackupFilePath;
    }
    
    public ManagementContext getManagementContext()
    {
        return mMgtCtx;
    }
    
    public Repository getRepository()
    {
        return mMgtCtx.getRepository();
    }
    
    
    /**
     * Get the Registry Specification.
     */
    public RegistrySpec getRegistrySpec()
    {
        return mSpec;
    }
    
    /**
     * Get a Registry Property.
     */
    public String getProperty(String propName)
    {
        if (propName.equals(Registry.REGISTRY_FILE_PROPERTY))
        {
            return (mRegistryFile);
        }
        else if (propName.equals(Registry.REGISTRY_SYNC_FILE_PROPERTY))
        {
            return (REGISTRY_SYNC_FILE);
        }
        else if (propName.equals(Registry.REGISTRY_FOLDER_PROPERTY))
        {
            return (mRegistryFolderPath);
        }
        else if (propName.equals(Registry.REGISTRY_READONLY_PROPERTY))
        {
            return (mProps.getProperty(Registry.REGISTRY_READONLY_PROPERTY));
        }
        return (null);
    }
    
    /**
     * Commit the changes to the registry.
     *
     * @param validate - if true indicates that the Registry Content is
     * to be validated.
     */
    public void commit()
        throws RegistryException
    {  
        boolean regFileBackedUp = false; 
        
                    
        File backupFile = new File(this.getRegistryBkupFilePath());
        File regFile = new File(this.getRegistryFilePath());
            
        try
        {
            mSerializationRWLockManager.acquireWriteLock();
             
            if ( backupFile.exists() )
            {
                if ( !backupFile.delete() )
                {
                    mLogger.warning(mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_REGISTRY_FILE_DELETE_FAILED, 
                        this.getRegistryBkupFilePath()));
                }
            }
            
            if (!regFile.renameTo(backupFile))
            {
                mLogger.warning(mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_REGISTRY_FILE_RENAME_FAILED, 
                        this.getRegistryBkupFilePath(), 
                        this.getRegistryFilePath()));
            }
            else
            {
                regFileBackedUp = true;
            }
            
            Writer wr = this.getRegistryWriter();
            mMarshaller.marshal(mJbiRegistry, wr);
            wr.close();
        }
        catch ( Exception ex)
        {
            
            // -- Restore the registry file if backed up
            if ( regFileBackedUp )
            {
                backupFile.renameTo(regFile);
            }
                                
            // -- Revert to the last good state
            this.reinitialize();
            
            mSerializationRWLockManager.releaseWriteLock();
            String errMsg = 
                mTranslator.getString(LocalStringKeys.JBI_ADMIN_FAILED_SERIALIZE_REGISTRY,
                    ex.toString());
            ex.printStackTrace();
            mLogger.severe(errMsg);
            throw new RegistryException(errMsg);           
        }
        finally
        {
            mSerializationRWLockManager.releaseWriteLock();
        }
    }  
    
   /**
     * Take a consistent snapshot of the registry.
     *
     */
    public ByteArrayInputStream snapshot()
        throws RegistryException
    {  
        try
        {
            mSerializationRWLockManager.acquireWriteLock();
            ByteArrayOutputStream   baos = new ByteArrayOutputStream();
            
            mMarshaller.marshal(mJbiRegistry, baos);
            baos.flush();
            baos.close();
            return (new ByteArrayInputStream(baos.toByteArray()));
        }
        catch ( Exception ex)
        {
            String errMsg = 
                mTranslator.getString(LocalStringKeys.JBI_ADMIN_FAILED_SERIALIZE_REGISTRY,
                    ex.toString());
            ex.printStackTrace();
            mLogger.severe(errMsg);
            throw new RegistryException(errMsg);           
        }
        finally
        {
            mSerializationRWLockManager.releaseWriteLock();
        }
    }  
  
    /**
     * @return the Read / Write Lock Manager used to synchronize access to the in-memory
     * registry.
     */
    LockManager getRegistryObjectLockManager()
    {
        return mRegistryObjRWLockManager;
    }
    
    /**
     * @return the Read / Write Lock Manager used to synchronize access to the serialized
     * registry.
     */
    LockManager getSerializationLockManager()
    {
        return mSerializationRWLockManager;
    }

}
