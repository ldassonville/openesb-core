/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentDescriptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.management.descriptor;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.management.descriptor.Component;
import com.sun.jbi.management.internal.support.DOMUtil;
import com.sun.jbi.management.util.StringHelper;

import org.w3c.dom.Element;

import java.util.List;

/**
 *
 * This class encapsulates the JAXB model for the Component Descriptor
 *
 * @author Sun Microsystems, Inc
 */
public class ComponentDescriptor
{
    /**
     * The type of the descriptor.
     */
    private Component mCompType;

    /**
     * Local logger for messages.
     */
    private Logger mLog;
    
    /**
     * Element for configuration section of descriptor.
     */
    private static final String CONFIGURATION = "Configuration";
    
    /**
     * Element for logging section of descriptor.
     */
    private static final String LOGGING = "Logging";
    
    /**
     * Element for observer section of descriptor.
     */
    private static final String OBSERVER = "Observer";
    
    /**
     * Constructs a ComponentDescriptor.
     *
     * @param jbi the JAXB model for the descriptor.
     * @throws an IllegalArgumentException if the Jbi passed in not that
     * for a component
     */
    public ComponentDescriptor(Jbi jbi)
        throws IllegalArgumentException
    {
        mCompType = jbi.getComponent();
        mLog = Logger.getLogger("com.sun.jbi.management");
        
        
        if ( mCompType == null )
        {
            throw new IllegalArgumentException();
        }
    }
    
    /**
     * No-argument constructor for junit testing only.
     */
    public ComponentDescriptor()
    {
    }
    
    /**
     * Get the name of the component.
     * @return the component name or an empty string if none found.
     */
    public String getName()
    {
        if ( mCompType.getIdentification() != null )
        {
            return StringHelper.trim(mCompType.getIdentification().getName());
        }
        else
        {
            return "";
        }
    }
    
    /**
     * Get the description of the component.
     * @return the component description or an empty string if none found.
     */
    public String getDescription()
    {
        if ( mCompType.getIdentification() != null )
        {
            return StringHelper.trim(mCompType.getIdentification().getDescription());
        }
        else
        {
            return "";
        }
    }
    
    /**
     * Get the component type (BINDING or ENGINE).
     * @return the component type or null if none found.
     */
    public com.sun.jbi.ComponentType getComponentType()
    {
        com.sun.jbi.ComponentType compType = null;
    
        String compTypeStr = StringHelper.trim(mCompType.getType());
        if ( compTypeStr.equals("binding-component") )
        {
            compType = com.sun.jbi.ComponentType.BINDING;
        } 
        else if ( compTypeStr.equals("service-engine") )
        {
            compType = com.sun.jbi.ComponentType.ENGINE;
        }
        return compType;
    }
    
    /**
     * Get the component runtime self-first classloader setting.
     * @return true if the ComponentClassLoaderDelegation is selfFirst, false
     * if not.
     */
    public boolean isComponentClassLoaderSelfFirst()
    {    
        return ( InstallationContext.SELF_FIRST.equals(
        StringHelper.trim(mCompType.getComponentClassLoaderDelegation())) );
    }
    
    
    /**
     * Get the component installer self-first classloader setting.
     * @return true if the BootstrapClassLoaderDelegation is selfFirst, false
     * if not.
     */
    public boolean isBootstrapClassLoaderSelfFirst()
    {   
        return ( InstallationContext.SELF_FIRST.equals(
            StringHelper.trim(mCompType.getBootstrapClassLoaderDelegation())) );
    }
    
    /**
     * Get the component class path elements.
     * @return the component class path elements, with white space removed.
     */
    public List<String> getComponentClassPathElements()
    {   
        if ( mCompType.getComponentClassPath()!= null )
        {
            return StringHelper.trim(mCompType.getComponentClassPath().getPathElement());
        }
        else
        {
            return new java.util.ArrayList<String>();
        }
    }
    
    
    /**
     * Get the bootstrap class path elements.
     * @return the bootstrap class path elements, with white space removed.
     */
    public List<String> getBootstrapClassPathElements()
    {
        if ( mCompType.getBootstrapClassPath() != null )
        {
            return StringHelper.trim(mCompType.getBootstrapClassPath().getPathElement());
        }
        else
        {
            return new java.util.ArrayList<String>();
        }
    }
    
    
    /**
     * Get the bootstrap class name for the component.
     * @return the bootstrap class name.
     */
    public String getBootstrapClassName()
    {   
        String bootClassName = StringHelper.trim(mCompType.getBootstrapClassName());
       
        return bootClassName;
    }
    
    /**
     * Get the runtime class name for the component.
     * @return the runtime class name.
     */
    public String getComponentClassName()
    {   
        String compClassName = "";
        if ( mCompType.getComponentClassName() != null )
        {
            compClassName = 
                StringHelper.trim(mCompType.getComponentClassName().getContent());
        }
        return compClassName;
    }
    
    /**
     * Get the list of shared library names for the component.
     * @return a List of Shared Library names.
     */
    public List<String> getSharedLibraryIds()
    {
        List<String> sls = new java.util.ArrayList();
        
        List<com.sun.jbi.management.descriptor.Component.SharedLibrary>
            slList = mCompType.getSharedLibraryList();
        
        for( com.sun.jbi.management.descriptor.Component.SharedLibrary sl : slList )
        {
            String slName = StringHelper.trim(((String) sl.getContent()));
            if ( !"".equals(slName) )
            {
                sls.add(slName);
            }
        }
        return sls;
    }
    
    /**
     * Get the namespace of the "Configuration" element in the jbi.xml, if one 
     * exists. Return an empty string if a "Configuration" element is not 
     * defined.
     *
     * @return the configuration element namespace
     */
    public String getComponentConfigurationNS()
    {
        String ns = "";
        
        List<Element> extensions = mCompType.getAnyOrAny();

        for ( Element element : extensions )
        {
            if ( element.getLocalName().equals(CONFIGURATION) )
            {
                ns = element.getNamespaceURI();
            }
        }
        
        return ns;
    }
    
    /**
     * Get the the "Configuration" element in the jbi.xml. If the Configuration 
     * element is missing in the jbi.xml a null value is returned
     *
     * @return the configuration xml string
     */
    public String getComponentConfigurationXml()
    {
        String xmlStr = null;
        
        List<Element> extensions = mCompType.getAnyOrAny();

        for ( Element element : extensions )
        {
            if ( element.getLocalName().equals(CONFIGURATION) )
            {
                DOMUtil domUtil = new DOMUtil();
                try
                {
                    xmlStr = domUtil.elementToString(element);
                }
                catch (Throwable ex)
                {
                    mLog.log(Level.FINE, 
                             "Failed to convert configuration element to String", 
                             ex);
                }
            }
        }
        
        return xmlStr;
    }

    /**
     * Get the the "Logging" element in the jbi.xml. If the Logging 
     * element is missing in the jbi.xml a null value is returned.
     *
     * @return the logging element.
     */
    public Element getComponentLoggingXml()
    {
        Element xml = null;
        
        List<Element> extensions = mCompType.getAnyOrAny();

        for ( Element element : extensions )
        {
            if ( element.getLocalName().equals(LOGGING) )
            {
                xml = element;
            }
        }
        
        return xml;
    }

    /**
     * Get the the "Observer" element in the jbi.xml. If the Observer 
     * element is missing in the jbi.xml a null value is returned.
     *
     * @return the observer element.
     */
    public Element getComponentObserverXml()
    {
        Element xml = null;
        
        List<Element> extensions = mCompType.getAnyOrAny();

        for ( Element element : extensions )
        {
            if ( element.getLocalName().equals(OBSERVER) )
            {
                xml = element;
            }
        }
        
        return xml;
    }

    
}
