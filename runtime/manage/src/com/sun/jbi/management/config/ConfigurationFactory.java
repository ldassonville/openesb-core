/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicRuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.message.MessageBuilder;

import java.util.Properties;
import java.util.logging.Logger;
import javax.management.modelmbean.ModelMBeanAttributeInfo;

/**
 * ConfigurationFactory is the producer of configuration data set for a 
 * particular category.
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class ConfigurationFactory 
{
    /** The Logger */
    private Logger mLog;
    
    /** The string translator */
    private StringTranslator mTranslator;
    
    /** The category */
    protected ConfigurationCategory mCategory;
    
    /** The default proiperties */
    protected Properties mDefaults;
    
    /**
     * Contructor that gets the default properties and category
     *
     * @param defaults - defualt attribute values
     * @category - the configuration category
     */
    public ConfigurationFactory(Properties defaults, ConfigurationCategory category)
    {
        mDefaults = defaults;
        mCategory = category;
    }
    
    /**
     * @return an array of attribute meta data for a configuration category.
     */
    public abstract ModelMBeanAttributeInfo[] createMBeanAttributeInfo();
    
         
    /**
      * Get the I18N String from the resource bundle.
      *
      * @return the 118N string after removing the token prefix
      */
     protected String getString( String key )
     {
         String str = getTranslator().getString(key);
         return MessageBuilder.getMessageString(str);
     }
     
     /**
      * Get the I18N String token from the resource bundle.
      *
      * @return the 118N token from the localized string.
      */
     protected String getToken(String key )
     {
         String str = getTranslator().getString(key);
         return MessageBuilder.getMessageToken(str);
     }
     
     /**
      */
     protected StringTranslator getTranslator()
     {
        if ( mTranslator == null )
        {
            com.sun.jbi.EnvironmentContext envCtx = com.sun.jbi.util.EnvironmentAccess.getContext();
            mTranslator = envCtx.getStringTranslator("com.sun.jbi.management");
        }
        return mTranslator;
     }
     
     /**
      *
      */
     protected Logger getLogger()
     {
         if ( mLog  == null )
         {
             mLog =Logger.getLogger("com.sun.jbi.management");
         }
         return mLog;
     }
     
         
    /**
     * @return the attrbute name prefixed with the category
     * @param key - identification for the attribute
     */
    protected String getQualifiedKey(String key)
    {
        StringBuffer strBuf = new StringBuffer(mCategory.toString().toLowerCase());
        strBuf.append(".");
        strBuf.append(key);
        return strBuf.toString();
    }
    
    /**
     * @return the default log level for the JBI Runtime
     */
    protected String getDefaultLogLevel()
    {
        java.util.logging.Level level = Logger.getLogger("com.sun.jbi").getLevel();
        if ( level != null )
        {
            return  level.toString();
        }
        else
        {
            return "INFO";
        }
    }
     

}
