/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LoggerConfigurationFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;

import java.util.Properties;

import javax.management.Descriptor;
import javax.management.modelmbean.ModelMBeanAttributeInfo;

import java.util.logging.Logger;

/**
 * LoggerConfigurationFactory exposes the log levels for configuration.
 * Addition of new runtime loggers would require updates to this class.
 * 
 * @author Sun Microsystems, Inc.
 */
public class LoggerConfigurationFactory 
    extends ConfigurationFactory
        implements com.sun.jbi.util.Constants
{
    /** Runtime Logger Names */
    public static final String FRAMEWORK_LOGGER          = "com.sun.jbi.framework";
    public static final String MESSAGING_LOGGER          = "com.sun.jbi.messaging";
    public static final String MANAGEMENT_LOGGER         = "com.sun.jbi.management";
    public static final String ADMIN_LOGGER              = "com.sun.jbi.management.AdminService";
    public static final String INSTALLATION_LOGGER       = "com.sun.jbi.management.InstallationService";
    public static final String DEPLOYMENT_LOGGER         = "com.sun.jbi.management.DeploymentService";
    public static final String CONFIGURATION_LOGGER      = "com.sun.jbi.management.ConfigurationService";
    public static final String LOGGING_LOGGER            = "com.sun.jbi.management.LoggingService";
    
    private static final int sNumAttributes = 9;
    private static final String LOGGER_VALUES = 
        "{ALL, CONFIG, FINE, FINER, FINEST, INFO, OFF, SEVERE, WARNING, DEFAULT}";
    
    /**
     * Constructor 
     *
     * @param defProps - default properties
     */
    public LoggerConfigurationFactory(Properties defProps)
    {
        super(defProps, ConfigurationCategory.Logger);
    }
    
    /**
     *
     */
    public ModelMBeanAttributeInfo[] createMBeanAttributeInfo()
    {
         ModelMBeanAttributeInfo[] attributeInfos = new ModelMBeanAttributeInfo[sNumAttributes];
         DescriptorSupport descr;
         String attrName;
         String attrDescr;

         // -- JBI Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.JBI_LOGGER_DESCR);
         descr.setAttributeName(com.sun.jbi.platform.PlatformContext.JBI_LOGGER_NAME);
         descr.setDisplayName(getString(LocalStringKeys.JBI_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.JBI_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.JBI_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.JBI_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.JBI_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[0] = new ModelMBeanAttributeInfo(
            // name                 
            com.sun.jbi.platform.PlatformContext.JBI_LOGGER_NAME, 
                 //type                descr      R      W    isIs  Descriptor
                 "java.lang.String", attrDescr, true, true, false, descr);
         
         // -- Framework Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.FRAMEWORK_LOGGER_DESCR);
         descr.setAttributeName(FRAMEWORK_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.FRAMEWORK_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.FRAMEWORK_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.FRAMEWORK_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.FRAMEWORK_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.FRAMEWORK_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[1] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            FRAMEWORK_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         
         // -- Messaging Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.MESSAGING_LOGGER_DESCR);
         descr.setAttributeName(MESSAGING_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.MESSAGING_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.MESSAGING_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.MESSAGING_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.MESSAGING_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.MESSAGING_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[2] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            MESSAGING_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         
         // -- Management Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.MANAGEMENT_LOGGER_DESCR);
         descr.setAttributeName(MANAGEMENT_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.MANAGEMENT_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.MANAGEMENT_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.MANAGEMENT_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.MANAGEMENT_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.MANAGEMENT_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[3] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            MANAGEMENT_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         

         // -- AdminService Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ADMIN_LOGGER_DESCR);
         descr.setAttributeName(ADMIN_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.ADMIN_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ADMIN_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ADMIN_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ADMIN_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ADMIN_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[4] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            ADMIN_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         
         // -- InstallationService Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.INSTALLATION_LOGGER_DESCR);
         descr.setAttributeName(INSTALLATION_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.INSTALLATION_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.INSTALLATION_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.INSTALLATION_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.INSTALLATION_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.INSTALLATION_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[5] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            INSTALLATION_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         
         // -- DeploymentService Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.DEPLOYMENT_LOGGER_DESCR);
         descr.setAttributeName(DEPLOYMENT_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.DEPLOYMENT_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.DEPLOYMENT_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.DEPLOYMENT_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.DEPLOYMENT_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.DEPLOYMENT_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[6] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            DEPLOYMENT_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         
         // -- ConfigurationService Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.CONFIGURATION_LOGGER_DESCR);
         descr.setAttributeName(CONFIGURATION_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.CONFIGURATION_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.CONFIGURATION_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.CONFIGURATION_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.CONFIGURATION_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.CONFIGURATION_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[7] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            CONFIGURATION_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);

          // -- LoggingService Logger
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.LOGGING_LOGGER_DESCR);
         descr.setAttributeName(LOGGING_LOGGER);
         descr.setDisplayName(getString(LocalStringKeys.LOGGING_LOGGER_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.LOGGING_LOGGER_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.LOGGING_LOGGER_DESCR));
         descr.setToolTip(getString(LocalStringKeys.LOGGING_LOGGER_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.LOGGING_LOGGER_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         descr.setDefault(LOG_LEVEL_DEFAULT);
         descr.setEnumValue(LOGGER_VALUES);
         
         attributeInfos[8] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            LOGGING_LOGGER, "java.lang.String", attrDescr, true, true, false, descr);
         
         return attributeInfos;
    }
    
    /*--------------------------------------------------------------------------------*\
     *                         private helpers                                        *
    \*--------------------------------------------------------------------------------*/
    

}
