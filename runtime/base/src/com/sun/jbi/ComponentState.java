/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentState.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * ComponentState Enumeration
 *
 * @author Sun Microsystems, Inc.
 */
public enum ComponentState
{  
    /**
     * Loaded status. Note: This never appears in the permanent registry,
     * only in the runtime registry. It is a transient state used only during
     * installation of a component.
     */
    LOADED("Loaded"),
    SHUTDOWN("Shutdown"), 
    STOPPED("Stopped"), 
    STARTED("Started"),
    UNKNOWN("Unknown");

    /** The String value */
    private String mString;

    ComponentState(String strValue)
    {
	mString = strValue;
    }

    /**
     * Compute the effective state of the Component from a list of Component States
     *
     * If one or more state is a STARTED state the state is STARTED
     * If one or more state is STOPPED state and there is no STARTED state
     * the state is STOPPED
     * If there are no STOPPED or STARTED states the state is SHUTDOWN
     *
     * @param states - a List of  states.
     * @return the effective component state
     */
    public static ComponentState computeEffectiveState(java.util.List<ComponentState> states)
    {
        if ( states.contains(ComponentState.STARTED))
        {
            return ComponentState.STARTED;
        }
        else if ( states.contains(ComponentState.STOPPED))
        {
            return ComponentState.STOPPED;
        }
        else
        {
            return ComponentState.SHUTDOWN;
        }   
    }
    
    /**
     * Given the LifeCycleMBean state string convert it to a ComponentState.
     *
     * This method will be deleted after 
     * (a) state strings are fixed "Shutdown" and not "SHUTDOWN"
     * (b) LifeCycleMBean.RUNNING is changed to LifeCycleMBean.STARTED
     */
    public static ComponentState valueOfLifeCycleState(String state)
    {
        if ( javax.jbi.management.LifeCycleMBean.STARTED.equals(state))
        {
            return ComponentState.STARTED;
        }
        
        if ( javax.jbi.management.LifeCycleMBean.STOPPED.equals(state))
        {
            return ComponentState.STOPPED;
        }
        
        if ( javax.jbi.management.LifeCycleMBean.SHUTDOWN.equals(state))
        {
            return ComponentState.SHUTDOWN;
        }
        
        return ComponentState.UNKNOWN;
    }
    
    /**
     * Given the ComponentState convert it to a  LifeCycleMBean state string.
     *
     * This method will be deleted after 
     * (a) state strings are fixed "Shutdown" and not "SHUTDOWN"
     * (b) LifeCycleMBean.RUNNING is changed to LifeCycleMBean.STARTED
     */
    public static String getLifeCycleState(ComponentState state)
    {
         if ( ComponentState.STARTED == state )
        {
            return javax.jbi.management.LifeCycleMBean.STARTED;
        }
        
        if ( ComponentState.STOPPED == state )
        {
            return javax.jbi.management.LifeCycleMBean.STOPPED;
        }
        
        if ( ComponentState.SHUTDOWN == state )
        {
            return javax.jbi.management.LifeCycleMBean.SHUTDOWN;
        }
        
        return javax.jbi.management.LifeCycleMBean.UNKNOWN;
    }

    /**
     * @return the String value for the ComponentType
     */
    public String toString()
    {
        return mString;
    }
    
    /**
     * @return a ComponentState based on the String value. 
     * @param valStr - the string whose equivalent ComponentState
     *                 instance is required.  This operation ignores
     *                 the case, i.e. Stopped or STOPPED or sToPped
     *                 would return ComponentState.STOPPED
     */
    public static ComponentState valueOfString(String valStr)
    {
        return ComponentState.valueOf(valStr.toUpperCase());
    }
    
    public static void main(String[] args)
    {
        System.out.println(ComponentState.STOPPED.toString());
        System.out.println(ComponentState.valueOfString("Stopped").toString());
        System.out.println(ComponentState.valueOfString("StoPPed").toString());
        System.out.println(ComponentState.valueOfString("sTaRted").toString());
    }
};
