/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyState.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import java.util.List;
import javax.jbi.management.DeploymentServiceMBean;

/**
 * This interface provides information on Service Assemblys.
 *
 * @author Sun Microsystems, Inc.
 */
public enum ServiceAssemblyState
{
    SHUTDOWN("Shutdown"),
    STOPPED("Stopped"),
    STARTED("Started"),
    UNKNOWN("Unknown");

    /** The String value */
    private String mString;
    
    ServiceAssemblyState(String strValue)
    {
        mString = strValue;
    }
    
    /**
     * Compute the effective state of the Service Assembly.
     *
     * If one or more SUs is in a STARTED state the state is STARTED
     * If one or more SUs is in the STOPPED state and no SU is in the STARTED state
     * the state is STOPPED
     * If no SU is in the STOPPED or STARTED state the state is SHUTDOWN
     *
     * @param suStates - a List of service unit states.
     * @return the effective service assembly state
     */
    public static ServiceAssemblyState computeServiceAssemblyState(
        List<ServiceUnitState> suStates)
    {
        if ( suStates.contains(ServiceUnitState.STARTED))
        {
            return ServiceAssemblyState.STARTED;
        }
        else if ( suStates.contains(ServiceUnitState.STOPPED))
        {
            return ServiceAssemblyState.STOPPED;
        }
        else
        {
            return ServiceAssemblyState.SHUTDOWN;
        }   
    }
    
        
    /**
     * Compute the effective state of a Service Assembly given a list of  
     * Service Assembly States.
     *
     * If one or more SAs is in a STARTED state the state is STARTED
     * If one or more SAs is in the STOPPED state and no SA is in the STARTED state
     * the state is STOPPED
     * If no SA is in the STOPPED or STARTED state and one or more SAs is in the SHUTDOWN
     * state, the state is SHUTDOWN.
     * If none of the aboe are true the state is UNKNOWN
     *
     * @return the effective service assembly state
     */
    public static ServiceAssemblyState getEffectiveServiceAssemblyState(List<ServiceAssemblyState> saStates)
    {
        if ( saStates.contains(ServiceAssemblyState.STARTED))
        {
            return ServiceAssemblyState.STARTED;
        }
        else if ( saStates.contains(ServiceAssemblyState.STOPPED))
        {
            return ServiceAssemblyState.STOPPED;
        }
        else if ( saStates.contains(ServiceAssemblyState.SHUTDOWN))
        {
            return ServiceAssemblyState.SHUTDOWN;
        }   
        else
        {
            return ServiceAssemblyState.UNKNOWN;
        }
    }
    
    /**
     * Converts the service assembly state to the equivalent 
     * DeploymentService state.
     */
    public static String convertState(ServiceAssemblyState state)
    {
        String dsState;
        
        if (state == ServiceAssemblyState.STARTED)
        {
            dsState = DeploymentServiceMBean.STARTED;
        }
        else if (state == ServiceAssemblyState.STOPPED)
        {
            dsState = DeploymentServiceMBean.STOPPED;
        }
        else
        {
            dsState = DeploymentServiceMBean.SHUTDOWN;
        }
        
        return dsState;
    }
    
    /**
     * Get the equivalent ServiceAssemblyState from DeploymentServiceMBean state.
     */
    public static ServiceAssemblyState valueOfDeploymentServiceState(String state)
    {
        ServiceAssemblyState saState;
        
        if (DeploymentServiceMBean.STARTED.equals(state))
        {
            saState = ServiceAssemblyState.STARTED;
        }
        else if (DeploymentServiceMBean.STOPPED.equals(state))
        {
            saState = ServiceAssemblyState.STOPPED;
        }
        else
        {
            saState = ServiceAssemblyState.SHUTDOWN;
        }
        
        return saState;
    }

    /**
     * @return the String value for the ServiceAssemblyType
     */
    public String toString()
    {
        return mString;
    }
    
    /**
     * @return a ServuceAssemblyState based on the String value. 
     * @param valStr - the string whose equivalent ServiceAssemblyState
     *                 instance is required.  This operation
     *                 ignores the case, i.e. Stopped or STOPPED
     *                 or sToPped would return ServiceAssemblyState.STOPPED
     */
    public static ServiceAssemblyState valueOfString(String valStr)
    {
        return ServiceAssemblyState.valueOf(valStr.toUpperCase());
    }
    
    public static void main(String[] args)
    {
        System.out.println(ServiceAssemblyState.STOPPED.toString());
        System.out.println(ServiceAssemblyState.valueOfString("Stopped").toString());
        System.out.println(ServiceAssemblyState.valueOfString("StoPPed").toString());
        System.out.println(ServiceAssemblyState.valueOfString("sTaRted").toString());
    }
}
