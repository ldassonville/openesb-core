/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;

/**
 * This interface defines the shared read-write portion of the MBean for
 * collection of statistics for a single installed component. Only those
 * methods required to maintain statistics that are collected outside the
 * framework are defined here; framework-only methods are not specified here.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentStatisticsBase
{
    /**
     * Get the instance of StatisticsBase.
     * @return the StatisticsBase instance for this object.
     */
    StatisticsBase getStatisticsBase();

    /**
     * Decrement the current number of services or endpoints registered with
     * the NMR.
     */
    void decrementRegisteredServicesOrEndpoints();

    /**
     * Increment the current number of services or endpoints registered with
     * the NMR.
     */
    void incrementRegisteredServicesOrEndpoints();
}
