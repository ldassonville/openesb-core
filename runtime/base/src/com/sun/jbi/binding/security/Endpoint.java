/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Endpoint.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.security;

import java.util.Iterator;
import javax.xml.namespace.QName;


/**
 * Interface that defines an Endpoint.
 *
 * @author Sun Microsystems, Inc.
 */
public interface Endpoint
{
    /**
     * Indicates that the deployment is for inbound endpoints.
     */
    public static final int CONSUMER_ROLE = 0;

    /**
     * Indicates that the deployment is for outbound endpoints.
     */
    public static final int PROVIDER_ROLE = 1;
    
    
    /**
     * Returns the service QName.
     *
     * @return service Qname
     */
    QName getServiceName();

    /**
     * Returns the endpoint name.
     *
     * @return endpoint name.
     */
    String getEndpointName();

    /**
     * Gets list of operations supported by this endpoint.
     *
     * @return list of operations.
     */
    Iterator getOperationNames();
    
    /**
     * Get the component id of the Component the endpoint is deployed to.
     *
     * @return Component Id of the Component this Endpoint id deployed to
     */
    String getComponentId();
    
    /**
     * Get the name of the SecurityConfiguration provided with the Endpoint deployment.
     * @return The name of the security configuration file.
     */
    String getSecurityConfigFileName();
    
    /**
     * Get the Endpoint Role ( Provider / Consumer )
     *
     * @return the Endpoint role
     */
    public javax.jbi.messaging.MessageExchange.Role getRole();
}
