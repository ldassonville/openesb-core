/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;

import java.util.ArrayList;
import java.util.List;

/**
 * Tests the ComponentRegistry class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentRegistry
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * Local handle to the EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * The EnvironmentSetup helper class
     */
    private EnvironmentSetup mSetup;

    /**
     * Local handle to the ComponentRegistry class
     */
    private ComponentRegistry mCompReg;

    /**
     * Local instance of the Component class
     */
    private Component mBinding;

    /**
     * Local instance of the Component class
     */
    private Component mEngine;

    /**
     * Local instance of the SharedLibrary class
     */
    private SharedLibrary mSl1;

    /**
     * Local instance of the SharedLibrary class
     */
    private SharedLibrary mSl2;

    /**
     * Constant for test results
     */
    static final int THREE = 3;

    /**
     * Constant for test results
     */
    static final int FOUR = 4;

    /**
     * Constant for BC component ID
     */
    static final String BC_ID = "BC01";

    /**
     * Constant for BC bootstrap class name
     */
    static final String BC_BOOTSTRAP_CLASS_NAME =
                            "com.sun.jbi.framework.BindingBootstrap";

    /**
     * Constant for BC bootstrap class path
     */
    static final String BC_BOOTSTRAP_CLASS_PATH =
                            "framework/bld/com/sun/jbi/framework/";

    /**
     * Constant for BC life cycle class name
     */
    static final String BC_LIFECYCLE_CLASS_NAME =
                            "com.sun.jbi.framework.Binding";

    /**
     * Constant for BC life cycle class path
     */
    static final String BC_LIFECYCLE_CLASS_PATH =
                            "framework/bld/com/sun/jbi/framework/";
    /**
     * Constant for SE component ID
     */
    static final String SE_ID = "SE01";

    /**
     * Constant for SE bootstrap class name
     */
    static final String SE_BOOTSTRAP_CLASS_NAME =
                            "com.sun.jbi.framework.EngineBootstrap";

    /**
     * Constant for SE bootstrap class path
     */
    static final String SE_BOOTSTRAP_CLASS_PATH =
                            "framework/bld/com/sun/jbi/framework/";

    /**
     * Constant for SE life cycle class name
     */
    static final String SE_LIFECYCLE_CLASS_NAME =
                            "com.sun.jbi.framework.Engine";

    /**
     * Constant for SE life cycle class path
     */
    static final String SE_LIFECYCLE_CLASS_PATH =
                            "framework/bld/com/sun/jbi/framework/";

    /**
     * Constant for Shared Library 1
     */
    static final String SHARED_LIBRARY_1 = "SL01";

    /**
     * Constant for Shared Library 2
     */
    static final String SHARED_LIBRARY_2 = "SL02";

    /**
     * Constant for Service Assembly 1
     */
    static final String SA_NAME_1 = "SA01";

    /**
     * Constant for Service Assembly 2
     */
    static final String SA_NAME_2 = "SA02";

    /**
     * Constant for Service Unit 1
     */
    static final String SU_NAME_1 = "SU01";

    /**
     * Constant for Service Unit 1 root file path
     */
    static final String SU_PATH_1 =
                            "framework/bld/com/sun/jbi/framework/su01.xml";

    /**
     * Constant for Service Unit 2
     */
    static final String SU_NAME_2 = "SU02";

    /**
     * Constant for Service Unit 2 root file path
     */
    static final String SU_PATH_2 =
                            "framework/bld/com/sun/jbi/framework/su02.xml";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentRegistry(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mSrcroot = System.getProperty("junit.srcroot") + "/";

        // Create EnvironmentContext and ComponentRegistry, and initialize
        // and start the ComponentRegistry
        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        mSetup.startup(true, false);
        mCompReg = mContext.getComponentRegistry();

        // Create a Component instance for a binding component

        mBinding = new Component();
        mBinding.setName(BC_ID);
        mBinding.setComponentType(ComponentType.BINDING);
        mBinding.setBootstrapClassName(BC_BOOTSTRAP_CLASS_NAME);
        ArrayList bcBootClassPath = new ArrayList();
        bcBootClassPath.add(mSrcroot + BC_BOOTSTRAP_CLASS_PATH);
        mBinding.setBootstrapClassPathElements(bcBootClassPath);
        mBinding.setComponentClassName(BC_LIFECYCLE_CLASS_NAME);
        ArrayList bcLifeClassPath = new ArrayList();
        bcLifeClassPath.add(mSrcroot + BC_LIFECYCLE_CLASS_PATH);
        mBinding.setComponentClassPathElements(bcLifeClassPath);
        mBinding.setInstallRoot("C:/BC01");
        mBinding.setWorkspaceRoot("C:/BC01");

        // Create a Component instance for a service engine

        mEngine = new Component();
        mEngine.setName(SE_ID);
        mEngine.setComponentType(ComponentType.ENGINE);
        mEngine.setBootstrapClassName(SE_BOOTSTRAP_CLASS_NAME);
        ArrayList bpeBootClassPath = new ArrayList();
        bpeBootClassPath.add(mSrcroot + SE_BOOTSTRAP_CLASS_PATH);
        mEngine.setBootstrapClassPathElements(bpeBootClassPath);
        mEngine.setComponentClassName(SE_LIFECYCLE_CLASS_NAME);
        ArrayList bpeLifeClassPath = new ArrayList();
        bpeLifeClassPath.add(mSrcroot + SE_LIFECYCLE_CLASS_PATH);
        mEngine.setComponentClassPathElements(bpeLifeClassPath);
        mEngine.setInstallRoot("C:/SE01");
        mEngine.setWorkspaceRoot("C:/SE01");

        // Create SharedLibrary instances for two Shared Libraries

        ArrayList cp1 = new ArrayList();
        cp1.add("C:/d/jbi/antbld/lib/sl01.jar");
        cp1.add("C:/d/jbi/framework/bld/com/sun/jbi/framework");
        mSl1 = new SharedLibrary(SHARED_LIBRARY_1,
                                 "Framework library and classes",
                                 "/SHARED_LIBRARY_1",
                                 cp1);

        ArrayList cp2 = new ArrayList();
        cp2.add("C:/d/jbi/antbld/lib/sl02.jar");
        mSl2 = new SharedLibrary(SHARED_LIBRARY_2,
                                 "Shared library jar file",
                                 "/SHARED_LIBRARY_2",
                                 cp2);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        mSetup.shutdown(true, false);
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

// ----------------------------------------------------------------------------
// First, test the registration methods, as the other tests depend upon
// these to set up their test data.
// ----------------------------------------------------------------------------

    /**
     * Tests registerComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterComponentGood()
        throws Exception
    {
        mCompReg.registerComponent(mBinding);
        mCompReg.registerComponent(mEngine);
        assertTrue("Failure registering component",
                   mCompReg.isComponentRegistered(BC_ID));
        assertTrue("Failure registering component",
                   mCompReg.isComponentRegistered(SE_ID));
    }

    /**
     * Tests registerComponent with a null component parameter. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterComponentBadInput()
        throws Exception
    {
        try
        {
            mCompReg.registerComponent(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests registerComponent with a duplicate Component. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterComponentBadDuplicate()
        throws Exception
    {
        try
        {
            // Attempt to register the same component twice
            mCompReg.registerComponent(mBinding);
            mCompReg.registerComponent(mBinding);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("already registered")));
        }
    }

    /**
     * Tests registerSharedLibrary with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterSharedLibraryGood()
        throws Exception
    {
        mCompReg.registerSharedLibrary(mSl1);
        mCompReg.registerSharedLibrary(mSl2);
        assertTrue("Failure registering shared library",
                   mCompReg.isSharedLibraryRegistered(SHARED_LIBRARY_1));
        assertTrue("Failure registering shared library",
                   mCompReg.isSharedLibraryRegistered(SHARED_LIBRARY_2));
    }

    /**
     * Tests registerSharedLibrary with a null SharedLibrary parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterSharedLibraryBadInput()
        throws Exception
    {
        try
        {
            mCompReg.registerSharedLibrary(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests registerSharedLibrary with a duplicate SharedLibrary. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterSharedLibraryBadDuplicate()
        throws Exception
    {
        try
        {
            // Try to register the same SharedLibrary twice
            mCompReg.registerSharedLibrary(mSl1);
            mCompReg.registerSharedLibrary(mSl1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("already registered")));
        }
    }

// ----------------------------------------------------------------------------
// Test methods that are part of the ServiceUnitRegistration interface.
// ----------------------------------------------------------------------------

    /**
     * Tests registerServiceUnit with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterServiceUnitGood()
        throws Exception
    {
        mCompReg.registerComponent(mBinding);
        mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, SU_NAME_1, SU_PATH_1);
        assertTrue("Failure registering Service Unit 1",
                   mCompReg.isServiceUnitRegistered(BC_ID, SU_NAME_1));
        mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, SU_NAME_2, SU_PATH_2);
        assertTrue("Failure registering Service Unit 2",
                   mCompReg.isServiceUnitRegistered(BC_ID, SU_NAME_2));
    }

    /**
     * Tests registerServiceUnit with a null input parameter. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterServiceUnitBadInput()
        throws Exception
    {
        try
        {
            mCompReg.registerServiceUnit(null, SA_NAME_1, SU_NAME_1, SU_PATH_1);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
        try
        {
            mCompReg.registerServiceUnit(BC_ID, null, SU_NAME_1, SU_PATH_1);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
        try
        {
            mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, null, SU_PATH_1);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
        try
        {
            mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, SU_NAME_1, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests registerServiceUnit with a duplicate Service Unit. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterServiceUnitBadDuplicate()
        throws Exception
    {
        mCompReg.registerComponent(mBinding);
        try
        {
            // Attempt to register the same Service Unit twice
            mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, SU_NAME_1, SU_PATH_1);
            mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, SU_NAME_1, SU_PATH_1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2400")));
        }
    }

    /**
     * Tests registerServiceUnit with a nonexistent Component. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisterServiceUnitBadNotFound()
        throws Exception
    {
        try
        {
            mCompReg.registerServiceUnit(BC_ID, SA_NAME_1, SU_NAME_1, SU_PATH_1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("No component")));
        }
    }

    /**
     * Tests unregisterServiceUnit with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterServiceUnitGood()
        throws Exception
    {
        mCompReg.registerComponent(mEngine);
        mCompReg.registerServiceUnit(SE_ID, SA_NAME_1, SU_NAME_1, SU_PATH_1);
        mCompReg.registerComponent(mBinding);
        mCompReg.registerServiceUnit(BC_ID, SA_NAME_2, SU_NAME_2, SU_PATH_2);
        mCompReg.unregisterServiceUnit(BC_ID, SU_NAME_2);
        assertFalse("Failure unregistering Service Unit 2",
                    mCompReg.isServiceUnitRegistered(BC_ID, SU_NAME_2));
        mCompReg.unregisterServiceUnit(SE_ID, SU_NAME_1);
        assertFalse("Failure unregistering Service Unit 1",
                    mCompReg.isServiceUnitRegistered(SE_ID, SU_NAME_1));
    }

    /**
     * Tests unregisterServiceUnit with null parameters. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterServiceUnitBadInput()
        throws Exception
    {
        try
        {
            mCompReg.unregisterServiceUnit(null, SU_NAME_1);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
        try
        {
            mCompReg.unregisterServiceUnit(BC_ID, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests unregisterServiceUnit with a Service Unit name that is not
     * registered.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterServiceUnitBadNotRegistered()
        throws Exception
    {
        mCompReg.registerComponent(mBinding);
        try
        {
            mCompReg.unregisterServiceUnit(BC_ID, SU_NAME_1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("not found for")));
        }
    }

    /**
     * Tests unregisterServiceUnit with a Component name that does not exist.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterServiceUnitBadNotFound()
        throws Exception
    {
        try
        {
            mCompReg.unregisterServiceUnit(BC_ID, SU_NAME_1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("No component")));
        }
    }

// ----------------------------------------------------------------------------
// Test methods that are not accessible outside the framework.
// ----------------------------------------------------------------------------

    /**
     * Tests getComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentGood()
        throws Exception
    {
        Component comp = null;

        // Test that a null is returned when the component does not exist
        comp = mCompReg.getComponent(BC_ID);
        assertNull("Failure getting component: " +
                   "expected null return value , got non-null",
                   comp);

        // Test that the correct component is returned
        mCompReg.registerComponent(mBinding);
        mCompReg.registerComponent(mEngine);
        comp = mCompReg.getComponent(BC_ID);
        assertNotNull("Failure getting component: " +
                      "expected non-null return value, got null",
                      comp);
        assertEquals("Failure getting component, returned object incorrect: ",
                     mBinding, comp);
    }

    /**
     * Tests getComponent with a null component ID parameter. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentBadInput()
        throws Exception
    {
        try
        {
            Component comp = null;
            comp = mCompReg.getComponent(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests getSharedLibrary with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSharedLibrary()
        throws Exception
    {
        SharedLibrary sl1 = null;
        SharedLibrary sl2 = null;
        sl1 = mCompReg.getSharedLibrary(SHARED_LIBRARY_1);
        assertNull("Failure getting shared library: " +
                   "expected null return value , got non-null",
                   sl1);
        mCompReg.registerSharedLibrary(mSl1);
        mCompReg.registerSharedLibrary(mSl2);
        sl2 = mCompReg.getSharedLibrary(SHARED_LIBRARY_2);
        sl1 = mCompReg.getSharedLibrary(SHARED_LIBRARY_1);
        assertNotNull("Failure getting shared library: " +
                      "expected non-null return value, got null",
                      sl1);
        assertEquals("Failure getting shared library, returned object incorrect: ",
                     mSl1, sl1);
        assertNotNull("Failure getting shared library: " +
                      "expected non-null return value, got null",
                      sl2);
        assertEquals("Failure getting shared library, returned object incorrect: ",
                     mSl2, sl2);
    }

    /**
     * Tests getSharedLibrary with a null componentId parameter. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSharedLibraryBadInput()
        throws Exception
    {
        try
        {
            SharedLibrary sl = null;
            sl = mCompReg.getSharedLibrary(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests getStatus with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testgetStatusGood()
        throws Exception
    {
        ComponentState status;

        // Register component for test.
        mCompReg.registerComponent(mBinding);

        mBinding.setStatus(ComponentState.LOADED);
        status = mCompReg.getStatus(BC_ID);
        assertEquals("Incorrect status received for " + BC_ID + ": " +
                   "expected " + ComponentState.LOADED.toString() +
                   ", got " + status.toString(),
                   status, ComponentState.LOADED);

        mBinding.setStatus(ComponentState.SHUTDOWN);
        status = mCompReg.getStatus(BC_ID);
        assertEquals("Incorrect status received for " + BC_ID + ": " +
                   "expected " + ComponentState.SHUTDOWN.toString() +
                   ", got " + status.toString(),
                   status, ComponentState.SHUTDOWN);

        mBinding.setStatus(ComponentState.STOPPED);
        status = mCompReg.getStatus(BC_ID);
        assertEquals("Incorrect status received for " + BC_ID + ": " +
                   "expected " + ComponentState.STOPPED.toString() +
                   ", got " + status.toString(),
                   status, ComponentState.STOPPED);

        mBinding.setStatus(ComponentState.STARTED);
        status = mCompReg.getStatus(BC_ID);
        assertEquals("Incorrect status received for " + BC_ID + ": " +
                   "expected " + ComponentState.STARTED.toString() +
                   ", got " + status.toString(),
                   status, ComponentState.STARTED);
    }

    /**
     * Tests getStatus with a null argument. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testgetStatusBadNull()
        throws Exception
    {
        ComponentState status;
        try
        {
            status = mCompReg.getStatus(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests getStatus with a component ID that is not registered. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testgetStatusBadNotFound()
        throws Exception
    {
        ComponentState status;
        try
        {
            status = mCompReg.getStatus(BC_ID);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("No component")) &&
                       (-1 < ex.getMessage().indexOf("is registered")));
        }
    }

    /**
     * Tests isComponentRegistered with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsComponentRegisteredGood()
        throws Exception
    {
        assertFalse("Failure checking component status: " +
                    "expected false return value , got true",
                    mCompReg.isComponentRegistered(BC_ID));
        mCompReg.registerComponent(mBinding);
        assertTrue("Failure checking component status: " +
                   "expected true return value, got false",
                   mCompReg.isComponentRegistered(BC_ID));
    }

    /**
     * Tests isComponentRegistered with a null component ID parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsComponentRegisteredBadInput()
        throws Exception
    {
        try
        {
            mCompReg.isComponentRegistered(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests isSharedLibraryRegistered with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsSharedLibraryRegisteredGood()
        throws Exception
    {
        assertFalse("Failure checking shared library status: " +
                    "expected false return value , got true",
                    mCompReg.isSharedLibraryRegistered(SHARED_LIBRARY_1));
        mCompReg.registerSharedLibrary(mSl1);
        assertTrue("Failure checking shared library status: " +
                   "expected true return value, got false",
                   mCompReg.isSharedLibraryRegistered(SHARED_LIBRARY_1));
    }

    /**
     * Tests isSharedLibraryRegistered with a null shared library name
     * parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsSharedLibraryRegisteredBadInput()
        throws Exception
    {
        try
        {
            mCompReg.isSharedLibraryRegistered(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests unregisterComponent with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterComponentGood()
        throws Exception
    {
        mCompReg.registerComponent(mEngine);
        mCompReg.registerComponent(mBinding);
        mCompReg.unregisterComponent(BC_ID);
        assertFalse("Failure unregistering binding",
                    mCompReg.isComponentRegistered(BC_ID));
        mCompReg.unregisterComponent(SE_ID);
        assertFalse("Failure unregistering engine",
                    mCompReg.isComponentRegistered(SE_ID));
    }

    /**
     * Tests unregisterComponent with a null component ID parameter. An
     * exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterComponentBadInput()
        throws Exception
    {
        try
        {
            mCompReg.unregisterComponent(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests unregisterComponent with a component ID that is not registered.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterComponentBadNotRegistered()
        throws Exception
    {
        try
        {
            mCompReg.unregisterComponent(BC_ID);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("No component")));
        }
    }

    /**
     * Tests unregisterSharedLibrary with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterSharedLibraryGood()
        throws Exception
    {
        mCompReg.registerSharedLibrary(mSl1);
        mCompReg.unregisterSharedLibrary(SHARED_LIBRARY_1);
        assertFalse("Failure unregistering shared library",
                    mCompReg.isSharedLibraryRegistered(SHARED_LIBRARY_1));
    }

    /**
     * Tests unregisterSharedLibrary with a null shared library name parameter.
     * An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterSharedLibraryBadInput()
        throws Exception
    {
        try
        {
            mCompReg.unregisterSharedLibrary(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests unregisterSharedLibrary with a shared library name that is not
     * registered. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testUnregisterSharedLibraryBadNotRegistered()
        throws Exception
    {
        try
        {
            mCompReg.unregisterSharedLibrary(SHARED_LIBRARY_1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("No Shared Library")));
        }
    }

}
