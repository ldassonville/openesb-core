/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SunASJBIFramework.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.sun;

import com.sun.appserv.server.LifecycleEvent;
import com.sun.appserv.server.LifecycleEventContext;
import com.sun.appserv.server.LifecycleListener;
import com.sun.appserv.server.ServerLifecycleException;

import com.sun.enterprise.resource.RecoveryResourceListener;
import com.sun.enterprise.resource.RecoveryResourceRegistry;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.framework.EnvironmentContext;

import java.lang.management.ManagementFactory;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.transaction.TransactionManager;

/**
 * This is the top-level class that provides the lifecycle for the JBI
 * framework. It implements the Sun AppServer <CODE>LifecycleListener</CODE>
 * interface to allow the JBI runtime framework to start automatically when the
 * AppServer starts, and stop automatically when the AppServer stops. The one
 * method required by the <CODE>LifecycleListener</CODE> interface is
 * <CODE>handleEvent()</CODE>, which handles AppServer life cycle events for
 * initialization, startup, ready, shutdown, and termination. Each event is
 * handled by a corresponding method in this class, as follows:
 *
 * <UL>
 *  <LI>INIT_EVENT        ---> <CODE>init()</CODE></LI>
 *  <LI>STARTUP_EVENT     ---> <CODE>startup()</CODE></LI>
 *  <LI>READY_EVENT       ---> <CODE>ready()</CODE></LI>
 *  <LI>SHUTDOWN_EVENT    ---> <CODE>shutdown()</CODE></LI>
 *  <LI>TERMINATION_EVENT ---> <CODE>terminate()</CODE></LI>
 * </UL>
 *
 * This class is loaded by the <CODE>SunASJBIBootstrap</CODE> class, which also
 * implements the <CODE>LifecycleListener</CODE> interface. All events passed
 * to its <handleEvent()</CODE> method are merely passed through to the
 * <CODE>handleEvent()</CODE> method of this class, with one exception. When
 * the INIT_EVENT is received, the required class loader hierarchy for the JBI
 * runtime is established prior to passing the event through. The reason for
 * this is to ensure that the JBI runtime implementation classes are isolated
 * from the class loaders that are created for installed Components and Shared
 * Libraries. For further information, please refer to the Javadoc for the
 * <CODE>SunASJBIBootstrap</CODE> class.
 *
 * @author Sun Microsystems, Inc.
 */
public class SunASJBIFramework extends com.sun.jbi.framework.JBIFramework
    implements LifecycleListener, Runnable, RecoveryResourceListener
{
    private static final String	STRING_TRANSLATOR_NAME = "com.sun.jbi.framework.sun";
    
    /**
     * Glassfish platform details.
     */
    private SunASPlatformContext mPlatform;
    
    /**
     * Sync thread. Will be running if synchronization is required until completed.
     */
    private Thread              mSyncThread;
    private RegistryHelper      mRegistryHelper;
    private SyncProcessor       mSyncProcessor;
    private Logger              mLogger;
    private StringTranslator    mTranslator;
    
    /**
     * Last event received.
     */
    private int mLastEvent;

    private boolean mIsActive = true;
    private long    mStartStamp;
    private long    mStartTime;
    private long    mStartupTime;
    
    /**
     * Tracks the current phase of the Framework. This is used to block activity that
     * depends on being in a particular phase.
     */
    private int mPhase;
    private static final int  PHASE_NONE = 0;
    private static final int  PHASE_INIT  = 1;
    private static final int  PHASE_STARTUP = 2;
    private static final int  PHASE_PREPARE = 3;
    private static final int  PHASE_READY = 4;
    private static final int  PHASE_SHUTDOWN = 5;
    private static final int  PHASE_TERMINATE = 6;
    
    /**
     * Provide the processing for AppServer lifecycle events. For each
     * event, a corresponding method is called to perform the processing
     * for that event.
     * @param anEvent LifecycleEvent from AppServer
     * @throws ServerLifecycleException if any error occurs processing
     * an event.
     */
    public void handleEvent(LifecycleEvent anEvent)
        throws ServerLifecycleException
    {
        // This entire method must be enclosed in a try-catch block to
        // prevent any exception other than ServerLifecycleException from
        // being thrown. Any other exception causes the AppServer startup
        // to fail.

        try
        {
            LifecycleEventContext eventContext =
                anEvent.getLifecycleEventContext();
            
            if ( LifecycleEvent.INIT_EVENT == anEvent.getEventType() )
            {
                mStartStamp = System.currentTimeMillis();
                mPlatform = new SunASPlatformContext();
                mPhase = PHASE_NONE;
                Properties props = (Properties) anEvent.getData();
                RecoveryResourceRegistry.getInstance().addListener(this);
                super.init(mPlatform, props);
                mTranslator = getEnvironment().getStringTranslator(STRING_TRANSLATOR_NAME);
                mLogger = Logger.getLogger("com.sun.jbi.framework");  
                mRegistryHelper = new RegistryHelper(getEnvironment());
                mSyncProcessor = new SyncProcessor(getEnvironment(), mRegistryHelper);
                endPhase(PHASE_INIT);
                mLogger.fine("JBI:Start Time 0: " + (System.currentTimeMillis() - mStartStamp));
            }
            else if ( LifecycleEvent.STARTUP_EVENT == anEvent.getEventType() )
            {
                if ( null != getEnvironment() )
                {
                    mPlatform.setNamingContext(eventContext.getInitialContext());
                    super.startup(mPlatform.getNamingContext(), "");
                    endPhase(PHASE_STARTUP);
                    mLogger.fine("JBI:Start Time 1: " + (System.currentTimeMillis() - mStartStamp));
                }
            }
            else if ( LifecycleEvent.READY_EVENT == anEvent.getEventType() )
            {
                if ( null != getEnvironment() )
                {
                    long startTime = System.currentTimeMillis();
                    // Perform intial processing (e.g. config) for system
                    // components
                    bootstrapSystemComponents();

                    //
                    //  
                    boolean isStartupActive = mRegistryHelper.isActiveStartup();
                    mStartTime += System.currentTimeMillis() - startTime;
                    if (isStartupActive)
                    {
                        //
                        // If we are going from STARTUP to READY, synthesize a
                        // PREPARE so that the underlying code doesn't have to
                        // track the AS-specific behavior,
                        // 
                        if ( mLastEvent == LifecycleEvent.STARTUP_EVENT )
                        {
                            mLogger.fine("JBI:ActiveStartup");
                            super.prepare();
                            endPhase(PHASE_PREPARE);
                        }
                        ready();                        
                        endPhase(PHASE_READY);
                    }
                    else
                    {
                        enterLazyMode();
                    }
                }
                mLogger.fine("JBI:Start Time 2: " + (System.currentTimeMillis() - mStartStamp));
            }
            else if ( LifecycleEvent.SHUTDOWN_EVENT == anEvent.getEventType() )
            {
                stopSync();
                if ( null != getEnvironment() )
                {
                    super.shutdown();
                }
                endPhase(PHASE_SHUTDOWN);
            }
            else if ( LifecycleEvent.TERMINATION_EVENT == anEvent.getEventType() )
            {
                stopSync();
                if ( null != getEnvironment() )
                {
                    super.terminate();
                }
                endPhase(PHASE_TERMINATE);
            }
            else
            {
                String msg = "JBIFW0049: Unknown event type (" +
                    anEvent.getEventType() + ").";
                eventContext.log(msg);
                throw new javax.jbi.JBIException(msg);
            }
            mLastEvent = anEvent.getEventType();
        }
        catch ( Throwable ex )
        {
            throw new ServerLifecycleException(ex);
        }

        return;
    }
    
    /**
     * The GF Lifecycle interface will call this method after START and before READY to
     * allow local XAResources to be declared for recovery.
     */
    public javax.transaction.xa.XAResource[] getXAResources()
    {
        //
        //  Just in case GF calls us while STARTUP is still active, we will wait until
        //  we know the parts of startup that interest us have completed.
        //
        mLogger.fine("JBI:getXAResources: " + (System.currentTimeMillis() - mStartStamp));

        waitForPhase(PHASE_STARTUP);

        //
        //  See if need to start the JBI framework for XA resource recovery resources
        //
        if (mSyncProcessor != null)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                boolean isStartupActive = mRegistryHelper.isActiveStartup();
                mStartTime += System.currentTimeMillis() - startTime;
                setStartupTime();
                if (isStartupActive)
                {
                    mLogger.fine("JBI:XAActiveStartup");
                    super.prepare();
                    endPhase(PHASE_PREPARE);
                    mLastEvent = 0;
                }
            }
            catch (Exception e)
            {
                mLogger.warning(mTranslator.getString(LocalStringKeys.GET_XA_RESOURCES, e));
            }
        }
        
        //
        //  At this point we should have collected all local XAResources declared by JBI Components.
        //
        return (getEnvironment().getNormalizedMessageService().getXAResources());
    }
    
    /**
     *  Performs one-time initialization of JBI system components during 
     *  startup.  Exceptions during component bootstrap are (a) unlikely and 
     *  (b) not considered fatal, so any exceptions are just dumped to the 
     *  server log to assist in debugging.
     */
    private void bootstrapSystemComponents()
    {
        SystemComponentBootstrap bootstrap;
        
        try
        {
            bootstrap = new SystemComponentBootstrap(getEnvironment());
            bootstrap.configureHttpSoapDefaultPorts();
        }
        catch (Exception ex)
        {
            mLogger.log(Level.INFO, mTranslator.getString(
                LocalStringKeys.SYSTEM_COMPONENT_INIT_EXCEPTION), ex);
        }
    }    
    
    /**
     * Get the implementation of the TransactionManager.
     * @return the TransactionManager implementation.
     * @throws Exception if the TransactionManager instance cannot be obtained.
     */
    public TransactionManager getTransactionManager()
        throws Exception
    {
        TransactionManager transactionManager =
            (javax.transaction.TransactionManager)
                 getEnvironment().getNamingContext().lookup(
                     "java:appserver/TransactionManager");
        return transactionManager;
    }
       
//-------------------------ResourceRecoveryListener---------------------------------

    /**
     *  Callback stating the XAResource recovery has started.
     */
    public void recoveryStarted()
    {
        mLogger.fine("JBI:recoveryStarted: " + (System.currentTimeMillis() - mStartStamp));
        
    }
    
    /**
     *  Callback stating that XAResource recovery has completed.
     *  We purge an XA resource registrations held by the Message Service so as to 
     *  not hold on to any memory if the component is shutdown.
     */
    public void recoveryCompleted()
    {
        getEnvironment().getNormalizedMessageService().purgeXAResources();        
        mLogger.fine("JBI:recoveryCompleted: " + (System.currentTimeMillis() - mStartStamp));
    }
    
    /**
     *  Perform the ready() and sync() actions in a separate thread
     */
    private void ready()
    {
        String asyncEnabled = System.getProperty(
                "com.sun.jbi.AsyncReadySync",
                "true");  // Asynch is enabled by default
        if (Boolean.parseBoolean(asyncEnabled))        
        {
            mSyncThread = new Thread(this, "JBI-Ready-Sync");
            mSyncThread.setDaemon(true);
            mSyncThread.start();
        }
        else
        {
            run();
        }
    }
    
    /**
     *  Stop any ready/synchronization action that may be in process.
     */
    private void stopSync()
    {
        if (mSyncThread != null)
        {
            if (mSyncProcessor != null)
            {
                mSyncProcessor.stop();
            }
            try
            {
                mSyncThread.join(60000);
            }
            catch (java.lang.InterruptedException iEx)
            {
                
            }
            finally
            {
                mSyncThread = null;
            }
        }
    }
    
    /**
     * Have Framework bring the environment to ready, and then perform synchronization actions.
     */            
    public void run()
    {
        
        try
        {
            super.ready(true);
            {
                String syncEnabled = System.getProperty(
                        "com.sun.jbi.SyncEnabled",
                        "true");  // synch is enabled by default
                if (Boolean.parseBoolean(syncEnabled) && mSyncProcessor != null)        
                {
                    mSyncProcessor.start();
                }
                else
                {
                    mLogger.warning(mTranslator.getString(LocalStringKeys.JBI_SYNC_DISABLED));
                }
                mSyncProcessor = null;
            }
        }
        catch (Exception ex)
        {
            
        }
        finally
        {
            mSyncThread = null;
        }
    }
    
    /**
     * Signal that particular SunJBIFramework phase has been completed.
     */
    synchronized void endPhase(int phase)
    {
        mLogger.fine("JBI:endPhase-" + phase + ": " + (System.currentTimeMillis() - mStartStamp));
        mPhase = phase;
        this.notifyAll();
    }
    
    /**
     * Wait for a particular SunJBIFramework phase to be completed.
     */
    synchronized void waitForPhase(int phase)
    {
        while (phase > mPhase)
        {
            try
            {
                this.wait();
            }
            catch (InterruptedException iEx)
            {
            }
        }
        mLogger.fine("JBI:waitForPhase-" + phase + ": " + (System.currentTimeMillis() - mStartStamp));
    }
    
    /**
     * This method returns the startup time for the JBI Framework. It may be overridden
     * for specific AS types. Sun AS provider includes the cost of synchronization.
     * @return long - time for startup
     */
    public long getStartupTime()
    {
        return ( mStartTime + super.getStartupTime());
    }

}

