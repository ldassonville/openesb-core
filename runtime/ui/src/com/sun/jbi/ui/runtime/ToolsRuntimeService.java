/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ToolsRuntimeService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime;

import javax.jbi.JBIException;
import javax.management.MBeanServer;
import javax.management.StandardMBean;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ServiceLifecycle;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.runtime.mbeans.JBIAdminCommandsUIMBean;
import com.sun.jbi.ui.runtime.mbeans.UIMBeanFactory;

/**
 * Tools Runtime as a JBI Internal service
 * 
 * @author graj
 */
public class ToolsRuntimeService implements ServiceLifecycle {
    /** context */
    private EnvironmentContext mEnvContext;

    /** Creates a new instance of ToolsRuntimeService */
    public ToolsRuntimeService() {
        this.mEnvContext = null;
    }

    /**
     * inits service
     * 
     * @param aContext
     *            contex
     * @throws JBIException
     *             on error
     */
    public void initService(EnvironmentContext aContext) throws JBIException {
        this.mEnvContext = aContext;
    }

    /**
     * starts service
     * 
     * @throws JBIException
     *             on error
     */
    public void startService() throws JBIException {
        // register the ui mbeans here
        registerJavaCAPSManagementServiceMBeans();
        registerJbiAdminUiMBeans();
        // other initialization here.
    }

    /**
     * stops service
     * 
     * @throws JBIException
     *             on error
     */
    public void stopService() throws JBIException {
        // unregister the ui mbeans here
        unregisterJbiAdminUiMBeans();
        unregisterJavaCAPSManagementServiceMBeans();
        // other release of resources here.
    }

    /**
     * registers the ui mbeans
     * 
     * @throws JBIException
     *             on error
     */
    private void registerJbiAdminUiMBeans() throws JBIException {
        MBeanServer mbeanServer = null;
        JBIAdminCommandsUIMBean jbiAdminCommandsUIMBeanImpl = null;
        StandardMBean mbean = null;
        try {
            mbeanServer = this.mEnvContext.getMBeanServer();
        } catch (Exception ex) {

            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Original ESB MBean --
            // ///////////////////////////////////////////
            jbiAdminCommandsUIMBeanImpl = this
                    .createJBIAdminCommandsUIMBean(this.mEnvContext);

            mbean = new StandardMBean(jbiAdminCommandsUIMBeanImpl,
                    JBIAdminCommandsUIMBean.class);

            mbeanServer.registerMBean(mbean, JBIJMXObjectNames
                    .getJbiAdminUiMBeanObjectName());

        } catch (Exception ex) {

            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Reference Implementation MBean --
            // ///////////////////////////////////////////
            jbiAdminCommandsUIMBeanImpl = this
                    .createReferenceJBIAdminCommandsUIMBean(this.mEnvContext);

            mbean = new StandardMBean(jbiAdminCommandsUIMBeanImpl,
                    JBIAdminCommandsUIMBean.class);

            mbeanServer.registerMBean(mbean, JBIJMXObjectNames
                    .getJbiReferenceAdminUiMBeanObjectName());

        } catch (Exception ex) {

            throw new JBIException(ex);
        }
    }

    /**
     * unregisters the ui mbean
     * 
     * @throws JBIException
     *             on error
     */
    private void unregisterJbiAdminUiMBeans() throws JBIException {
        MBeanServer mbeanServer = null;
        try {
            // ///////////////////////////////////////////
            // -- The Original ESB MBean --
            // ///////////////////////////////////////////
            mbeanServer = this.mEnvContext.getMBeanServer();
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames
                    .getJbiAdminUiMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Reference Implementation MBean --
            // ///////////////////////////////////////////
            mbeanServer.unregisterMBean(JBIJMXObjectNames
                    .getJbiReferenceAdminUiMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

    }

    /**
     * registers the Java CAPS Common Management MBeans
     * 
     * @throws JBIException
     *             on error
     */
    private void registerJavaCAPSManagementServiceMBeans() throws JBIException {
        MBeanServer mbeanServer = null;
        AdministrationService administrationServiceMBeanImpl = null;
        ConfigurationService configurationServiceMBeanImpl = null;
        DeploymentService deploymentServiceMBeanImpl = null;
        InstallationService installationServiceMBeanImpl = null;
        RuntimeManagementService runtimeManagementMBeanImpl = null;
        PerformanceMeasurementService performanceMeasurementMBeanImpl = null;
        
        StandardMBean mbean = null;
        try {
            mbeanServer = this.mEnvContext.getMBeanServer();
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Administration Service MBean --
            // ///////////////////////////////////////////
            administrationServiceMBeanImpl = this.createJavaCAPSAdministrationServiceMBean(this.mEnvContext);
            mbean = new StandardMBean(administrationServiceMBeanImpl, AdministrationService.class);
            mbeanServer.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsAdministrationServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Configuration Service MBean --
            // ///////////////////////////////////////////
            configurationServiceMBeanImpl = this.createJavaCAPSConfigurationServiceMBean(this.mEnvContext);
            mbean = new StandardMBean(configurationServiceMBeanImpl, ConfigurationService.class);
            mbeanServer.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsConfigurationServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Deployment Service MBean --
            // ///////////////////////////////////////////
            deploymentServiceMBeanImpl = this.createJavaCAPSDeploymentServiceMBean(this.mEnvContext);
            mbean = new StandardMBean(deploymentServiceMBeanImpl, DeploymentService.class);
            mbeanServer.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsDeploymentServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ///////////////////////////////////////////
            // -- The Installation Service MBean --
            // ///////////////////////////////////////////
            installationServiceMBeanImpl = this.createJavaCAPSInstallationServiceMBean(this.mEnvContext);
            mbean = new StandardMBean(installationServiceMBeanImpl, InstallationService.class);
            mbeanServer.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsInstallationServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ////////////////////////////////////////////////
            // -- The Runtime Management Service MBean --
            // ////////////////////////////////////////////////
            runtimeManagementMBeanImpl = this.createJavaCAPSRuntimeManagementServiceMBean(this.mEnvContext);
            mbean = new StandardMBean(runtimeManagementMBeanImpl, RuntimeManagementService.class);
            mbeanServer.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsRuntimeManagementServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            // ////////////////////////////////////////////////
            // -- The Performance Measurement Service MBean --
            // ////////////////////////////////////////////////
            performanceMeasurementMBeanImpl = this.createJavaCAPSPerformanceMeasurementServiceMBean(this.mEnvContext);
            mbean = new StandardMBean(performanceMeasurementMBeanImpl, PerformanceMeasurementService.class);
            mbeanServer.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsPerformanceMeasurementServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }
    }

    /**
     * unregisters the Java CAPS Common Management MBeans
     * 
     * @throws JBIException
     *             on error
     */
    private void unregisterJavaCAPSManagementServiceMBeans() throws JBIException {
        MBeanServer mbeanServer = null;
        try {
            mbeanServer = this.mEnvContext.getMBeanServer();
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames.getJavaCapsAdministrationServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames.getJavaCapsConfigurationServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames.getJavaCapsDeploymentServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames.getJavaCapsInstallationServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames.getJavaCapsRuntimeManagementServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }

        try {
            mbeanServer.unregisterMBean(JBIJMXObjectNames.getJavaCapsPerformanceMeasurementServiceMBeanObjectName());
        } catch (Exception ex) {
            throw new JBIException(ex);
        }
    }

    /**
     * Create Java CAPS Common Management Service MBean
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected ConfigurationService createJavaCAPSConfigurationServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSConfigurationServiceMBean(aContext);
    }
    
    /**
     * Create Java CAPS Common Management Service MBean
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected DeploymentService createJavaCAPSDeploymentServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSDeploymentServiceMBean(aContext);
    }
    
    /**
     * Create Java CAPS Common Management Service MBean
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected InstallationService createJavaCAPSInstallationServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSInstallationServiceMBean(aContext);
    }
    
    /**
     * Create Java CAPS Common Management Service MBean
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected RuntimeManagementService createJavaCAPSRuntimeManagementServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSRuntimeManagementServiceMBean(aContext);
    }
    
    /**
     * Create Java CAPS Common Management Service MBean
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected PerformanceMeasurementService createJavaCAPSPerformanceMeasurementServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSPerformanceMeasurementServiceMBean(aContext);
    }
    
    /**
     * Create Java CAPS Common Management Service MBean
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected AdministrationService createJavaCAPSAdministrationServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSAdministrationServiceMBean(aContext);
    }
    
    /**
     * 
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected JBIAdminCommandsUIMBean createJBIAdminCommandsUIMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getInstance().createJBIAdminCommandsUIMBean(
                aContext);
    }

    /**
     * The Reference Implementation MBean
     * 
     * @param aContext
     * @return
     * @throws JBIException
     */
    protected JBIAdminCommandsUIMBean createReferenceJBIAdminCommandsUIMBean(
            EnvironmentContext aContext) throws JBIException {
        return UIMBeanFactory.getReferenceInstance()
                .createJBIAdminCommandsUIMBean(aContext);
    }
}
