/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UIMBeanFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.mbeans;

import javax.jbi.JBIException;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.esb.management.impl.administration.AdministrationServiceMBeanImpl;
import com.sun.esb.management.impl.configuration.ConfigurationServiceMBeanImpl;
import com.sun.esb.management.impl.deployment.DeploymentServiceMBeanImpl;
import com.sun.esb.management.impl.installation.InstallationServiceMBeanImpl;
import com.sun.esb.management.impl.performance.PerformanceMeasurementServiceMBeanImpl;
import com.sun.esb.management.impl.runtime.RuntimeManagementServiceMBeanImpl;
import com.sun.jbi.EnvironmentContext;

/**
 * This class is a default factory implemenation that creates the UIMBean impl
 * for RI runtime
 *
 * @author graj
 */
public class UIMBeanFactory {
    /**
     * property that can be set for creating different UIMBeanFactory
     * implemenation
     */
    public static String UIMBEAN_FACTORY_CLASS_PROP = "com.sun.jbi.tools.uimbean.factory";

    /**
     * Default impl class for the ESB UIMBeanFactory
     */
    public static String DEFAULT_ESB_UIMBEAN_FACTORY_CLASS = "com.sun.jbi.esb.ui.admin.runtime.mbeans.UIMBeanFactoryESBImpl";

    /**
     * Default impl class for the RI UIMBeanFactory
     */
    public static String DEFAULT_RI_UIMBEAN_FACTORY_CLASS = "com.sun.jbi.ui.runtime.mbeans.UIMBeanFactoryReferenceImpl";

    /**
     * factory instance
     */
    private static UIMBeanFactory sUIMBeanFactory = null;

    /**
     * RI factory instance
     */
    private static UIMBeanFactory sUIMBeanFactoryReference = null;

    /** Creates a new instance of UIMBeanFactory */
    public UIMBeanFactory() {
        // check the system properties and intialize if there is a different
        // UIMBeanFactory impl exists
    }

    /**
     * Returns a configured instance of the UIMBeanFactory class.
     *
     * @return The instance of the UIMBeanFactory
     */
    public static UIMBeanFactory getInstance() {
        /*
         * if (null == sUIMBeanFactory) { String factoryImplClass =
         * System.getProperty( UIMBEAN_FACTORY_CLASS_PROP,
         * DEFAULT_ESB_UIMBEAN_FACTORY_CLASS);
         * 
         * ToolsLogManager.getRuntimeLogger().info( "UIMBeanFactory Impl Class
         * Name from System properties " + factoryImplClass);
         * 
         * try { Class cl = Class.forName(factoryImplClass); sUIMBeanFactory =
         * (UIMBeanFactory) cl.newInstance(); } catch (Exception ex) {
         * ToolsLogManager .getRuntimeLogger() .log( Level.FINER, "Exception
         * occured loading UIMBeanFactory Impl Class Name from System properties " +
         * factoryImplClass, ex); ToolsLogManager .getRuntimeLogger() .info(
         * "Creating default UIMBeanFactory which creates RI UIMBean Impl");
         * sUIMBeanFactory = new UIMBeanFactory(); } } return sUIMBeanFactory;
         */
        // Added new
        sUIMBeanFactory = getReferenceInstance();
        return sUIMBeanFactory;
    }

    /**
     * Returns a configured instance of the UIMBeanFactory class.
     *
     * @return The instance of the UIMBeanFactory
     */
    public static UIMBeanFactory getReferenceInstance() {
        if (null == sUIMBeanFactoryReference) {
            sUIMBeanFactoryReference = new UIMBeanFactoryReferenceImpl();
        }
        return sUIMBeanFactoryReference;
    }

    /**
     * creates the UIMBean implemenation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return UIMBean implemenation
     */
    public JBIAdminCommandsUIMBean createJBIAdminCommandsUIMBean(
            EnvironmentContext aContext) throws JBIException {
        return new JBIAdminCommandsUIMBeanImpl(aContext);
    }

    /**
     * Creates the CAPS Management Administration Service implementation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return CAPS Management Service implementation
     */
    public AdministrationService createJavaCAPSAdministrationServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return new AdministrationServiceMBeanImpl(aContext);
    }
    
    /**
     * Creates the CAPS Management Deployment Service implementation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return CAPS Management Service implementation
     */
    public DeploymentService createJavaCAPSDeploymentServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return new DeploymentServiceMBeanImpl(aContext);
    }

    /**
     * Creates the CAPS Management Installation Service implementation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return CAPS Management Service implementation
     */
    public InstallationService createJavaCAPSInstallationServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return new InstallationServiceMBeanImpl(aContext);
    }

    /**
     * Creates the CAPS Management Configuration Service implementation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return CAPS Management Service implementation
     */
    public ConfigurationService createJavaCAPSConfigurationServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return new ConfigurationServiceMBeanImpl(aContext);
    }

    /**
     * Creates the CAPS Runtime Management Service implementation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return CAPS Management Service implementation
     */
    public RuntimeManagementService createJavaCAPSRuntimeManagementServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return new RuntimeManagementServiceMBeanImpl(aContext);
    }

    /**
     * Creates the CAPS Performance Measurement Service implementation.
     *
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return CAPS Management Service implementation
     */
    public PerformanceMeasurementService createJavaCAPSPerformanceMeasurementServiceMBean(
            EnvironmentContext aContext) throws JBIException {
        return new PerformanceMeasurementServiceMBeanImpl(aContext);
    }

    /**
     * creates the JBIStatisticsMBean.
     * @param aContext EnvironmentContext
     * @throws javax.jbi.JBIException on error
     * @return UIMBean implemenation
     */
    public JBIStatisticsMBean createJBIStatisticsMBean(
            EnvironmentContext aContext) 
    throws JBIException {
        return new JBIStatisticsMBeanImpl(aContext);
    }    

}
