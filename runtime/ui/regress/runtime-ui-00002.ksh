#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00002.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# Verifier Test
# A SA is provided to the verifier. Verifier checks if all the application
# variables are set in the given target
# report accordingly.
####

echo "runtime-ui-00002 : Test Verifier."

. ./regress_defs.ksh

# start the framework
start_jbise -Dcom.sun.jbi.registry.readonly=true &
testDelay 5

echo prepare the artifacts
ant -q  -emacs $JBISE_PROPS -lib "$REGRESS_CLASSPATH" -f runtime-ui-00002.xml

echo install the components
$JBISE_ANT -Djbi.install.file=$JV_SRCROOT/runtime/ui/bld/test-classes/dist/test-component.jar install-component
$JBISE_ANT -Djbi.install.file=$JV_SRCROOT/runtime/ui/bld/test-classes/dist/test-component1.jar install-component

echo verify the SA
ant -q  -emacs $JBISE_PROPS -lib "$REGRESS_CLASSPATH" -f runtime-ui-00002.xml verify-sa

echo uninstall the components
$JBISE_ANT -Djbi.component.name=test-component uninstall-component
$JBISE_ANT -Djbi.component.name=test-component1 uninstall-component

# stop the JBI framework
shutdown_jbise
testDelay 1
