/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.Set;
import javax.activation.DataHandler;
import javax.security.auth.Subject;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.dom.DOMSource;

/**
 * Tests for the MessageImpl class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMessageImpl extends junit.framework.TestCase
{
    /** Path to endpoint descriptor. */
    public static final String XML_CONTENT_PATH = 
        System.getProperty("junit.srcroot") + "/nms/regress/payload.xml";
    
    public static final String ATTACH_CONTENT = "my attachment content";
    
    /** Test instance message */
    private MessageImpl mMessage;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestMessageImpl(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mMessage = new MessageImpl();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        
    }

// =============================  test methods ================================

    /**
     * testGetSetContent
     * @throws Exception if an unexpected error occurs
     */
    public void testGetSetContent()
           throws Exception
    {
        /* Test get/set for all content types */
        mMessage.setContent(new StreamSource());
        assertTrue(mMessage.getContent() instanceof StreamSource);
        
        mMessage.setContent(new SAXSource());
        assertTrue(mMessage.getContent() instanceof SAXSource);
        
        mMessage.setContent(new DOMSource());
        assertTrue(mMessage.getContent() instanceof DOMSource);
        
        /* Setting null is allowed right now. This test should start failing
         */
        mMessage.setContent(null);
    }    
    
    /**
     * testGetSetProperty
     * @throws Exception if an unexpected error occurs
     */
    public void testGetSetProperty()
           throws Exception
    {
        final String PROP_KEY_1 = "foo";
        final String PROP_VAL_1 = "fooval";
        final String PROP_VAL_2 = "barval";
        
        mMessage.setProperty(PROP_KEY_1, PROP_VAL_1);
        assertEquals(mMessage.getProperty(PROP_KEY_1), PROP_VAL_1);
        
        mMessage.setProperty(PROP_KEY_1, PROP_VAL_2);
        assertEquals(mMessage.getProperty(PROP_KEY_1), PROP_VAL_2);

        mMessage.setProperty(PROP_KEY_1, null);
        assertEquals(mMessage.getProperty(PROP_KEY_1), null);

        //
        // Make sure no NPE for null property value in debugging code.
        //
	mMessage.toString();
    }
    
    /**
     * testGetSetKnownProperties
     * @throws Exception if an unexpected error occurs
     */
    public void testGetSetKnownProperties()
           throws Exception
    {
        Subject PROP_VAL_1 = new Subject();
        Subject PROP_VAL_2 = new Subject();
        
        mMessage.setProperty("javax.jbi.security.subject", PROP_VAL_1);
        assertEquals(mMessage.getSecuritySubject(), PROP_VAL_1);
        
        mMessage.setSecuritySubject(PROP_VAL_2);
        assertEquals(mMessage.getProperty("javax.jbi.security.subject"), PROP_VAL_2);
    }
    
    /**
     * testAddAtachment
     * @throws Exception if an unexpected error occurs
     */
    public void testAddAtachment()
           throws Exception
    {
        final String ID = "add";
        
        mMessage.addAttachment(ID, createTextAttachment());
        
        // check content equality against original object
        assertEquals(mMessage.getAttachment(ID).getContent(), ATTACH_CONTENT);
    }
    
    /**
     * testRemoveAtachment
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveAtachment()
           throws Exception
    {
        final String ID = "remove";
        
        mMessage.addAttachment(ID, createTextAttachment());
        mMessage.removeAttachment(ID);
        
        // check content equality against original object
        assertTrue(mMessage.getAttachment(ID) == null);
    }
    
    /**
     * testListAttachments
     * @throws Exception if an unexpected error occurs
     */
    public void testListAttachments()
           throws Exception
    {
        final String ID_1 = "list1";
        final String ID_2 = "list2";
        
        int         count = 0;
        Set         list;
        
        mMessage.addAttachment(ID_1, createTextAttachment());        
        mMessage.addAttachment(ID_2, createTextAttachment());
        
        list = mMessage.getAttachmentNames();
                
        assertTrue(list.size() == 2);
    }
    
    /** Creates a simple text attachment. */
    DataHandler createTextAttachment()
    {        
        return new DataHandler(ATTACH_CONTENT, "text/plain");
    }
}
