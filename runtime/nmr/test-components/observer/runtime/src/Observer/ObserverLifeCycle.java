/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ObserverLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package Observer;

import java.io.File;
import java.io.FileWriter;

import java.util.Random;
import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.ObjectName;

import javax.xml.namespace.QName;
        
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
        

/**
 * This class implements ComponentLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class ObserverLifeCycle
    implements ComponentLifeCycle, Component, Runnable
{
    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private ComponentContext mContext = null;

    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private DeliveryChannel mChannel = null;

    /**
     * Refernce to logger.
     */
    private Logger mLogger = null;

    private Thread                      mRunThread = null;
    private boolean                     mStopping = false;
    private Random                      mRandom = new Random(System.nanoTime());
    private MessageExchangeFactory      mFactory;
    private QName                       mOperation = new QName("operation1");
    private QName                       mService = new QName("NMR-Test-Service");
    private ServiceEndpoint             mEndpoint;
    private int				mCount;
   
    /**
     *    
     */
    private boolean mInitSuccess = false;

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }

    public ServiceUnitManager getSUManager()
    {
        return null;
    }

    //javax.jbi.component.ComponentLifeCycle methods

    /**
     * Initialize the observer binding. 
     *
     * @param context the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext context)
        throws javax.jbi.JBIException
    {
        mLogger = Logger.getLogger(this.getClass().getPackage().getName()); 
        
        mContext = context;
        
        mInitSuccess = true;
    }

    /**
     * Shutdown the  observer binding.
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mContext = null;
    }

    /**
     * Start the observer binding.
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         start.
     */
    public void start()
        throws javax.jbi.JBIException
    {

        if (!mInitSuccess)
        {

            return;
        }

        mChannel = mContext.getDeliveryChannel();

        if (mRunThread == null)
        {
            mStopping = false;
            mRunThread = new Thread(this, "Observer-Engine");
            mRunThread.setDaemon(true);
            mRunThread.start();
        }
        
    }

    /**
     * Stop the observer binding.
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        // To add code to stop all services
        try
        {
            if (mRunThread != null)
            {
                mStopping = true;
                mRunThread.interrupt();
                try
                {
                    mRunThread.join();
                }
                catch (InterruptedException ie)
                {

                }
		FileWriter	fw = new FileWriter(mContext.getWorkspaceRoot() + File.separator + "observer.stats");
		fw.write("Observer saw: ");
		fw.write(Integer.valueOf(mCount).toString());
		fw.write(" Exchange actions.\n");
		fw.write(mCount == 0 ? "FAILED\n" : "SUCCESSFUL\n");
		fw.close();
            }

            if (mChannel != null)
            {
                mChannel.close();
            }
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
        }

    }
    
     //-------------------------------Component---------------------------------
   
    /**
     * Returns an object of ObserverLifeCycle.
     *
     * @return ComponentLifeCycle Object implementing
     *         javax.jbi.component.ComponentLifeCycle interface.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Returns an object implementing javax.jbi.component.ServiceUnitManager 
     * interface.
     *
     * @return ServiceUnitManager Object implementing
     *         javax.jbi.component.ServiceUnitManager interface.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return null;
    }
    
    /**
     *
     * @param ref ServiceEndpoint object
     *
     * @return Descriptor Object implementing javax.jbi.servicedesc.Descriptor
     *         interface.
     */
    public Document getServiceDescription(ServiceEndpoint ref)
    {
	org.w3c.dom.Document desc = null;

	try
	{
	}
	catch (Exception e)
	{
    		e.printStackTrace();
	}
	return desc;

    }

    
    /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. The consumer is described by the given 
     *  capabilities, and JBI has already ensured that a fit exists between the 
     *  set of required capabilities of the provider and the available 
     *  capabilities of the consumer, and vice versa. This matching consists of
     *  simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the consumer.
     *  @param endpoint the endpoint to be used by the consumer
     *  @param exchange the proposed message exchange to be performed
     *  @param consumerCapabilities the consumers capabilities and requirements
     *  @return true if this provider component can perform the the given 
     *   exchange with the described consumer
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. Ths provider is described 
     *  by the given capabilities, and JBI has already ensure that a fit exists 
     *  between the set of required capabilities of the consumer and the 
     *  available capabilities of the provider, and vice versa. This matching 
     *  consists of simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the provider.
     *  @param exchange the proposed message exchange to be performed
     *  @param providerCapabilities the providers capabilities and requirements
     *  @return true if this consurer component can interact with the described
     *   provider to perform the given exchange
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
    
    public void run()
    {
        int         count = 0;
       	Object      o;
 
        try
        {
            while (!mStopping)
            {
                try
                {
                    o = mChannel.accept();
	            if (o.getClass().getName().equals("com.sun.jbi.messaging.Observer"))
		    {
			    mCount++;
		    }
                }
                catch (javax.jbi.messaging.MessagingException mex)
                {
                    mex.printStackTrace();                    
                }
           }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        mStopping = true;
    }
    
}
