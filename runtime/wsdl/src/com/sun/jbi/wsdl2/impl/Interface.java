/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Interface.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import javax.xml.namespace.QName;

import org.w3.ns.wsdl.InterfaceType;

/**
 * Abstract implementation of
 * WSDL 2.0 Interface Component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Interface extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.Interface
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final InterfaceType getBean()
    {
        return (InterfaceType) this.mXmlObject;
    }

    /**
     * Construct an abstract Interface implementation base component.
     * 
     * @param bean      The XML bean for this Interface component
     */
    Interface(InterfaceType bean)
    {
        super(bean);
    }

    /**
     * Get the interfaces extended by this interface, including those
     * extended by this interface's super-interfaces.
     *
     * @return Extended interfaces, including those extended by this
     * interface's super-interfaces
     */
    public abstract com.sun.jbi.wsdl2.Interface[] getExtendedInterfaces();

    /**
     * Get the operations defined by this interface, and all its
     * super-interfaces
     *
     * @return Operations defined by this interface, and all super-interfaces
     */
    public abstract com.sun.jbi.wsdl2.InterfaceOperation[] getExtendedOperations();

    /**
     * Create a new operation, appending it to this interface's operations
     * list.
     *
     * @return Newly created operation, appended to the operations list.
     */
    public abstract com.sun.jbi.wsdl2.InterfaceOperation addNewOperation();

    /**
     * Create a new fault, and append it to this interface's faults list.
     *
     * @param name Name of the new fault
     * @return Newly created interface, appended to the faults list.
     */
    public abstract com.sun.jbi.wsdl2.InterfaceFault addNewFault(String name);

}

// End-of-file: Interface.java
