/*
 * XML Type:  InterfaceOperationType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.InterfaceOperationType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML InterfaceOperationType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class InterfaceOperationTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.InterfaceOperationType
{
    
    public InterfaceOperationTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INPUT$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "input");
    private static final javax.xml.namespace.QName OUTPUT$2 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "output");
    private static final javax.xml.namespace.QName INFAULT$4 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "infault");
    private static final javax.xml.namespace.QName OUTFAULT$6 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "outfault");
    private static final javax.xml.namespace.QName NAME$8 = 
        new javax.xml.namespace.QName("", "name");
    private static final javax.xml.namespace.QName PATTERN$10 = 
        new javax.xml.namespace.QName("", "pattern");
    private static final javax.xml.namespace.QName SAFE$12 = 
        new javax.xml.namespace.QName("", "safe");
    private static final javax.xml.namespace.QName STYLE$14 = 
        new javax.xml.namespace.QName("", "style");
    
    
    /**
     * Gets array of all "input" elements
     */
    public org.w3.ns.wsdl.MessageRefType[] getInputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INPUT$0, targetList);
            org.w3.ns.wsdl.MessageRefType[] result = new org.w3.ns.wsdl.MessageRefType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "input" element
     */
    public org.w3.ns.wsdl.MessageRefType getInputArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().find_element_user(INPUT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "input" element
     */
    public int sizeOfInputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INPUT$0);
        }
    }
    
    /**
     * Sets array of all "input" element
     */
    public void setInputArray(org.w3.ns.wsdl.MessageRefType[] inputArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(inputArray, INPUT$0);
        }
    }
    
    /**
     * Sets ith "input" element
     */
    public void setInputArray(int i, org.w3.ns.wsdl.MessageRefType input)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().find_element_user(INPUT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(input);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "input" element
     */
    public org.w3.ns.wsdl.MessageRefType insertNewInput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().insert_element_user(INPUT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "input" element
     */
    public org.w3.ns.wsdl.MessageRefType addNewInput()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().add_element_user(INPUT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "input" element
     */
    public void removeInput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INPUT$0, i);
        }
    }
    
    /**
     * Gets array of all "output" elements
     */
    public org.w3.ns.wsdl.MessageRefType[] getOutputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OUTPUT$2, targetList);
            org.w3.ns.wsdl.MessageRefType[] result = new org.w3.ns.wsdl.MessageRefType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "output" element
     */
    public org.w3.ns.wsdl.MessageRefType getOutputArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().find_element_user(OUTPUT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "output" element
     */
    public int sizeOfOutputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OUTPUT$2);
        }
    }
    
    /**
     * Sets array of all "output" element
     */
    public void setOutputArray(org.w3.ns.wsdl.MessageRefType[] outputArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(outputArray, OUTPUT$2);
        }
    }
    
    /**
     * Sets ith "output" element
     */
    public void setOutputArray(int i, org.w3.ns.wsdl.MessageRefType output)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().find_element_user(OUTPUT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(output);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "output" element
     */
    public org.w3.ns.wsdl.MessageRefType insertNewOutput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().insert_element_user(OUTPUT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "output" element
     */
    public org.w3.ns.wsdl.MessageRefType addNewOutput()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefType target = null;
            target = (org.w3.ns.wsdl.MessageRefType)get_store().add_element_user(OUTPUT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "output" element
     */
    public void removeOutput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OUTPUT$2, i);
        }
    }
    
    /**
     * Gets array of all "infault" elements
     */
    public org.w3.ns.wsdl.MessageRefFaultType[] getInfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INFAULT$4, targetList);
            org.w3.ns.wsdl.MessageRefFaultType[] result = new org.w3.ns.wsdl.MessageRefFaultType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "infault" element
     */
    public org.w3.ns.wsdl.MessageRefFaultType getInfaultArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().find_element_user(INFAULT$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "infault" element
     */
    public int sizeOfInfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INFAULT$4);
        }
    }
    
    /**
     * Sets array of all "infault" element
     */
    public void setInfaultArray(org.w3.ns.wsdl.MessageRefFaultType[] infaultArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(infaultArray, INFAULT$4);
        }
    }
    
    /**
     * Sets ith "infault" element
     */
    public void setInfaultArray(int i, org.w3.ns.wsdl.MessageRefFaultType infault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().find_element_user(INFAULT$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(infault);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "infault" element
     */
    public org.w3.ns.wsdl.MessageRefFaultType insertNewInfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().insert_element_user(INFAULT$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "infault" element
     */
    public org.w3.ns.wsdl.MessageRefFaultType addNewInfault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().add_element_user(INFAULT$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "infault" element
     */
    public void removeInfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INFAULT$4, i);
        }
    }
    
    /**
     * Gets array of all "outfault" elements
     */
    public org.w3.ns.wsdl.MessageRefFaultType[] getOutfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OUTFAULT$6, targetList);
            org.w3.ns.wsdl.MessageRefFaultType[] result = new org.w3.ns.wsdl.MessageRefFaultType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "outfault" element
     */
    public org.w3.ns.wsdl.MessageRefFaultType getOutfaultArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().find_element_user(OUTFAULT$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "outfault" element
     */
    public int sizeOfOutfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OUTFAULT$6);
        }
    }
    
    /**
     * Sets array of all "outfault" element
     */
    public void setOutfaultArray(org.w3.ns.wsdl.MessageRefFaultType[] outfaultArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(outfaultArray, OUTFAULT$6);
        }
    }
    
    /**
     * Sets ith "outfault" element
     */
    public void setOutfaultArray(int i, org.w3.ns.wsdl.MessageRefFaultType outfault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().find_element_user(OUTFAULT$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(outfault);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "outfault" element
     */
    public org.w3.ns.wsdl.MessageRefFaultType insertNewOutfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().insert_element_user(OUTFAULT$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "outfault" element
     */
    public org.w3.ns.wsdl.MessageRefFaultType addNewOutfault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.MessageRefFaultType target = null;
            target = (org.w3.ns.wsdl.MessageRefFaultType)get_store().add_element_user(OUTFAULT$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "outfault" element
     */
    public void removeOutfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OUTFAULT$6, i);
        }
    }
    
    /**
     * Gets the "name" attribute
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$8);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" attribute
     */
    public org.apache.xmlbeans.XmlNCName xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$8);
            return target;
        }
    }
    
    /**
     * Sets the "name" attribute
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$8);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" attribute
     */
    public void xsetName(org.apache.xmlbeans.XmlNCName name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlNCName)get_store().add_attribute_user(NAME$8);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "pattern" attribute
     */
    public java.lang.String getPattern()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PATTERN$10);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "pattern" attribute
     */
    public org.apache.xmlbeans.XmlAnyURI xgetPattern()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(PATTERN$10);
            return target;
        }
    }
    
    /**
     * True if has "pattern" attribute
     */
    public boolean isSetPattern()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(PATTERN$10) != null;
        }
    }
    
    /**
     * Sets the "pattern" attribute
     */
    public void setPattern(java.lang.String pattern)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(PATTERN$10);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(PATTERN$10);
            }
            target.setStringValue(pattern);
        }
    }
    
    /**
     * Sets (as xml) the "pattern" attribute
     */
    public void xsetPattern(org.apache.xmlbeans.XmlAnyURI pattern)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(PATTERN$10);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlAnyURI)get_store().add_attribute_user(PATTERN$10);
            }
            target.set(pattern);
        }
    }
    
    /**
     * Unsets the "pattern" attribute
     */
    public void unsetPattern()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(PATTERN$10);
        }
    }
    
    /**
     * Gets the "safe" attribute
     */
    public boolean getSafe()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SAFE$12);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "safe" attribute
     */
    public org.apache.xmlbeans.XmlBoolean xgetSafe()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_attribute_user(SAFE$12);
            return target;
        }
    }
    
    /**
     * True if has "safe" attribute
     */
    public boolean isSetSafe()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(SAFE$12) != null;
        }
    }
    
    /**
     * Sets the "safe" attribute
     */
    public void setSafe(boolean safe)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SAFE$12);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SAFE$12);
            }
            target.setBooleanValue(safe);
        }
    }
    
    /**
     * Sets (as xml) the "safe" attribute
     */
    public void xsetSafe(org.apache.xmlbeans.XmlBoolean safe)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_attribute_user(SAFE$12);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_attribute_user(SAFE$12);
            }
            target.set(safe);
        }
    }
    
    /**
     * Unsets the "safe" attribute
     */
    public void unsetSafe()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(SAFE$12);
        }
    }
    
    /**
     * Gets the "style" attribute
     */
    public java.lang.String getStyle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(STYLE$14);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "style" attribute
     */
    public org.apache.xmlbeans.XmlAnyURI xgetStyle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(STYLE$14);
            return target;
        }
    }
    
    /**
     * True if has "style" attribute
     */
    public boolean isSetStyle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(STYLE$14) != null;
        }
    }
    
    /**
     * Sets the "style" attribute
     */
    public void setStyle(java.lang.String style)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(STYLE$14);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(STYLE$14);
            }
            target.setStringValue(style);
        }
    }
    
    /**
     * Sets (as xml) the "style" attribute
     */
    public void xsetStyle(org.apache.xmlbeans.XmlAnyURI style)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(STYLE$14);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlAnyURI)get_store().add_attribute_user(STYLE$14);
            }
            target.set(style);
        }
    }
    
    /**
     * Unsets the "style" attribute
     */
    public void unsetStyle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(STYLE$14);
        }
    }
}
