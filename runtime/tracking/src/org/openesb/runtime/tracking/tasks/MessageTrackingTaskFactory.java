/*
 * BEGIN_HEADER - DO NOT EDIT

 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.

 * You can obtain a copy of the license at
 * http://opensource.org/licenses/cddl1.php.
 * See the License for the specific language governing
 * permissions and limitations under the License.

 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * http://opensource.org/licenses/cddl1.php.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.openesb.runtime.tracking.tasks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.openesb.runtime.tracking.MessageTrackingInfo;
import org.openesb.runtime.tracking.tasks.sample.SimpleFileTask;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */
public class MessageTrackingTaskFactory {

    private static MessageTrackingTaskFactory me_;

    public static MessageTrackingTaskFactory get() {
        if (me_ == null) {
            synchronized (MessageTrackingTaskFactory.class) {
                if (me_ == null) {
                    me_ = new MessageTrackingTaskFactory();
                }
            }
        }
        return me_;

    }

    public MessageTrackingTask getTask(MessageTrackingInfo trackingInfo) {
        MessageTrackingTaskDecorator decor = new MessageTrackingTaskDecorator();
        //Muting test output now
        //SimpleFileTask sft = new SimpleFileTask(trackingInfo);
        //decor.addTask(sft);
        return decor;
    }
}
