/*
  * BEGIN_HEADER - DO NOT EDIT

  * The contents of this file are subject to the terms
  * of the Common Development and Distribution License
  * (the "License").  You may not use this file except
  * in compliance with the License.

  * You can obtain a copy of the license at
  * http://opensource.org/licenses/cddl1.php.
  * See the License for the specific language governing
  * permissions and limitations under the License.

  * When distributing Covered Code, include this CDDL
  * HEADER in each file and include the License file at
  * http://opensource.org/licenses/cddl1.php.
  * If applicable add the following below this CDDL HEADER,
  * with the fields enclosed by brackets "[]" replaced with
  * your own identifying information: Portions Copyright
  * [year] [name of copyright owner]
 */

package org.openesb.runtime.tracking.settings;


/**
 *
 * Copyright 2011 Alexander Lomov.
 */

public class MessageTrackingSettingsException extends Exception {

    private String message;
    private Throwable cause;

    public MessageTrackingSettingsException() {
        super();
    }

    public MessageTrackingSettingsException(String message) {
        super(message);
    }

    public MessageTrackingSettingsException(Throwable cause) {
        super(cause);
    }


    public MessageTrackingSettingsException(String message, Throwable cause) {
        super(message, cause);
    }





}
