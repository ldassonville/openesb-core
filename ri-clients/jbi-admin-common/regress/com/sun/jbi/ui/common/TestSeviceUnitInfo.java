/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSeviceUnitInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import com.sun.jbi.ui.common.JBIManagementMessage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import junit.framework.TestCase;
import java.util.*;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestSeviceUnitInfo extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestSeviceUnitInfo(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testServiceUnitInfo() throws Exception
    {
        List list = getTestData1();
        
        for ( Iterator itr = list.iterator(); itr.hasNext();)
        {
            System.out.println(itr.next());
        }
    }
    /** test data
     * @return test list objects
     */
    public static List getTestData1()
    {
        ArrayList list = new ArrayList();
        
        ServiceUnitInfo info1 =
        new ServiceUnitInfo("su21 Name","su21 desc","su target name21",
        ServiceUnitInfo.SHUTDOWN_STATE);
        
        list.add(info1);
        
        return list;
    }
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestSeviceUnitInfo("test").testServiceUnitInfo();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
