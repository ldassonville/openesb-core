#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)admincommon00004.ksh - test that issue 60 remains fixed.
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


echo "admincommon00010: Test the application variable and application configuration management operations for component configuration for target=clustered-instance."

#regress setup
. ./regress_defs.ksh

COMPONENT_ARCHIVE=$UI_REGRESS_DIST_DIR/component-with-custom-mbean.jar
COMPONENT_NAME=admin-common-binding-1

NODEAGENT=admin-agent1
CLUSTER=admin-cluster
CLUSTER_INSTANCE=admin-cluster-instance-1

# cluster and member instance setup
asadmin create-node-agent -t --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $NODEAGENT
createAgentDelay
asadmin create-cluster  --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $CLUSTER
createClusterDelay
asadmin create-instance --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --cluster $CLUSTER --nodeagent $NODEAGENT $CLUSTER_INSTANCE
createInstanceDelay

asadmin start-node-agent  -t --passwordfile $JV_AS8BASE/passwords $NODEAGENT
startAgentDelay
asadmin start-instance  --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $CLUSTER_INSTANCE
startInstanceDelay

# component setup
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f admincommon00007.xml pkg.test.component
$JBI_ANT -Djbi.target=$CLUSTER -Djbi.install.file=$COMPONENT_ARCHIVE  install-component
installComponentDelay
$JBI_ANT -Djbi.target=$CLUSTER -Djbi.component.name=$COMPONENT_NAME start-component
startComponentDelay

# Test application varible ops
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml list.app.vars
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml add.app.vars
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml list.app.vars
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml set.app.vars
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml list.app.vars
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml delete.app.vars
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00007.xml list.app.vars

# Test application configuration ops
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml list.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml add.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml list.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml set.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml list.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml delete.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget=$CLUSTER_INSTANCE -f admincommon00008.xml list.app.config

# component cleanup
$JBI_ANT -Djbi.target=$CLUSTER -Djbi.component.name=$COMPONENT_NAME shut-down-component
stopComponentDelay
$JBI_ANT -Djbi.target=$CLUSTER -Djbi.component.name=$COMPONENT_NAME uninstall-component
uninstallComponentDelay

# cluster and member cleanup
asadmin stop-instance   --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $CLUSTER_INSTANCE
stopInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $CLUSTER_INSTANCE
deleteClusterDelay
asadmin delete-cluster  --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $CLUSTER
deleteClusterDelay
asadmin stop-node-agent   $NODEAGENT
stopAgentDelay
asadmin delete-node-agent $NODEAGENT
deleteAgentDelay
asadmin delete-node-agent-config --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $NODEAGENT

exit 0
