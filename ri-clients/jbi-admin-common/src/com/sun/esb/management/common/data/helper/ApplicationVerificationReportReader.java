/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ApplicationVerificationReportReader.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.common.data.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.ApplicationVerificationReport;
import com.sun.esb.management.common.data.EndpointInformation;
import com.sun.esb.management.common.data.JavaEEVerifierReport;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

/**
 * Parses Application Verification Report from an XML String or file
 * 
 * @author graj
 * 
 */
public class ApplicationVerificationReportReader implements
		ApplicationVerificationReportXMLConstants, Serializable {
	static final long serialVersionUID = -1L;

	// Private members needed to parse the XML document

	// keep track of QName
	private Stack<String> qNameStack = new Stack<String>();

	private EndpointInformation endpointInformation;

	private List<EndpointInformation> endpointInformationList;

	/** java ee verifier report list */
	private List<JavaEEVerifierReport> javaEEVerifierReportList;

	/** java ee verifier report */
	private JavaEEVerifierReport javaEEVerifierReport;

	/** jave ee verifier report table item type */
	private JavaEEVerifierReport.JavaEEReportItem javaEEVerifierTableItem;

	private List<String /* ComponentName */> missingComponentsList;

	private ApplicationVerificationReport report;

	private String applicationVerificationReportVersion;

	private List<String> missingAppVars;

	private List<String> missingAppConfigs;

	private HashMap<String, String> javaEEReportMap;

	/**
	 * Constructor - creates a new instance of
	 * ApplicationVerificationReportReader
	 */
	public ApplicationVerificationReportReader() {
	}

	/**
	 * @return the Application Verification Report
	 */
	public ApplicationVerificationReport getApplicationVerificationReport() {
		return this.report;
	}

	/**
	 * Read the document
	 * 
	 * @param document
	 * @return
	 * @throws IOException
	 */
	public ApplicationVerificationReport read(Document document)
			throws IOException {
		traverseNode(document);
		return report;
	}

	/**
	 * Parse from raw XML String
	 * @param rawXMLData
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 */
	public static ApplicationVerificationReport parseFromXMLData(
			String rawXMLData) throws SAXException, IOException,
			ParserConfigurationException, URISyntaxException {

		// Initialize the XML Document InputStream
		Reader stringReader = new StringReader(rawXMLData);

		// Create an InputSource from the InputStream
		InputSource inputSource = new InputSource(stringReader);
		DOMParser parser = new DOMParser();
		// Get the DOM tree as a Document object
		parser.parse(inputSource);

		Document document = parser.getDocument();

		// Parse the aspectInput XML document stream, using my event handler
		ApplicationVerificationReportReader reader = new ApplicationVerificationReportReader();
		reader.read(document);

		return reader.getApplicationVerificationReport();

	}

	/**
	 * Parse from File String
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static ApplicationVerificationReport parseFromFile(String fileName)
			throws FileNotFoundException, SAXException, IOException,
			ParserConfigurationException, URISyntaxException {
		File file = new File(fileName);
		return parseFromFile(file);
	}

	/**
	 * Parse from File
	 * @param fileName
	 * @return
	 * @throws MalformedURLException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static ApplicationVerificationReport parseFromFile(File file)
			throws FileNotFoundException, SAXException, IOException {

		// Initialize the URI and XML Document InputStream
		ApplicationVerificationReportReader reader;
		InputStream inputStream = new FileInputStream(file);

		// Create an InputSource from the InputStream
		InputSource inputSource = new InputSource(inputStream);
		DOMParser parser = new DOMParser();
		// Get the DOM tree as a Document object
		parser.parse(inputSource);
		Document document = parser.getDocument();

		reader = new ApplicationVerificationReportReader();
		reader.read(document);
		return reader.getApplicationVerificationReport();
	}

	/**
	 * Parse from URI string
	 * @param uriString
	 * @return
	 * @throws SAXException
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static ApplicationVerificationReport parseFromURI(String uriString)
			throws URISyntaxException, IOException, SAXException {
		URI uri = new URI(uriString);
		return parseFromURI(uri);
	}

	/**
	 * Parse from URI
	 * @param uri
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 */
	public static ApplicationVerificationReport parseFromURI(URI uri)
			throws IOException, SAXException {

		// Initialize the URI and XML Document InputStream
		InputStream inputStream = uri.toURL().openStream();

		// Create an InputSource from the InputStream
		InputSource inputSource = new InputSource(inputStream);

		DOMParser parser = new DOMParser();
		// Get the DOM tree as a Document object
		parser.parse(inputSource);
		Document document = parser.getDocument();

		// Parse the aspectInput XML document stream, using my event handler
		ApplicationVerificationReportReader reader = new ApplicationVerificationReportReader();
		reader.read(document);

		return reader.getApplicationVerificationReport();
	}

	/**
	 * Parse the Node
	 * 
	 * @param node
	 * @throws IOException
	 */
	private void traverseNode(Node node) throws IOException {
		// Determine action based on node type
		switch (node.getNodeType()) {
		case Node.DOCUMENT_NODE:
			// recurse on each top-level node
			NodeList nodes = node.getChildNodes();
			if (nodes != null) {
				for (int i = 0; i < nodes.getLength(); i++) {
					traverseNode(nodes.item(i));
				}
			}
			break;
		case Node.ELEMENT_NODE:
			if (node != null) {
				String name = node.getNodeName();
				qNameStack.push(name);
				if (node.getNodeName().equals(
						APPLICATION_VERIFICATION_REPORT_KEY)) {
					if (report != null) {
						break;
					}
					NamedNodeMap attributes = node.getAttributes();
					if (attributes != null) {
						String namespace = attributes.getNamedItem(
								NAMESPACE_KEY).getNodeValue();
						// //////////////////////////////////////////////////////
						// Read performanceDataListVersion attribute and ensure
						// you
						// store the right
						// performanceDataListVersion of the report map list
						// //////////////////////////////////////////////////////
						Node namedItem = null;
						namedItem = attributes.getNamedItem(VERSION_KEY);
						if (namedItem != null) {
							this.applicationVerificationReportVersion = namedItem
									.getNodeValue();
							if ((applicationVerificationReportVersion != null)
									&& (VERSION_VALUE
											.equals(applicationVerificationReportVersion))) {
								this.report = new ApplicationVerificationReport();
							} else {
								// Invalid applicationVerificationReportVersion.
								// Not storing it
							}
						}
					}
				} else if (node.getNodeName().equals(
						MISSING_COMPONENTS_LIST_KEY)) {
					if (this.report != null) {
						this.missingComponentsList = new ArrayList<String /* ComponentName */>();
						this.report
								.setMissingComponentsList(this.missingComponentsList);
					}
				} else if (node.getNodeName().equals(
						ENDPOINT_INFORMATION_LIST_KEY)) {
					if (this.report != null) {
						this.endpointInformationList = new ArrayList<EndpointInformation>();
						this.report
								.setEndpointInformationList(this.endpointInformationList);
					}
				} else if (node.getNodeName().equals(ENDPOINT_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)) {
						this.endpointInformation = new EndpointInformation();
						this.endpointInformationList
								.add(this.endpointInformation);
					}

				} else if (node.getNodeName().equals(MISSING_APPVARS_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)) {
						this.missingAppVars = new ArrayList<String>();
						this.endpointInformation
								.setMissingApplicationVariables((String[]) this.missingAppVars
										.toArray(new String[] {}));
					}

				} else if (node.getNodeName().equals(MISSING_APPCONFIGS_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)) {
						this.missingAppConfigs = new ArrayList<String>();
						this.endpointInformation
								.setMissingApplicationConfigurations((String[]) this.missingAppConfigs
										.toArray(new String[] {}));

					}

				} else if (node.getNodeName().equals(
						JAVAEE_VERIFIER_REPORTS_LIST_KEY)) {
					if (this.report != null) {
						this.javaEEVerifierReportList = new ArrayList<JavaEEVerifierReport>();
						this.report
								.setJavaEEVerifierReports(this.javaEEVerifierReportList);
					}
				} else if (node.getNodeName()
						.equals(JAVAEE_VERIFIER_REPORT_KEY)) {
					if ((this.report != null)
							&& (this.javaEEVerifierReportList != null)) {
						this.javaEEVerifierReport = new JavaEEVerifierReport();
						this.javaEEVerifierReportList.add(javaEEVerifierReport);
					}
				} else if (node.getNodeName().equals(
						JAVAEE_VERIFIER_REPORT_ITEM_KEY)) {
					if ((this.report != null)
							&& (this.javaEEVerifierReport != null)) {
						this.javaEEVerifierTableItem = javaEEVerifierReport.new JavaEEReportItem();
						this.javaEEReportMap = new HashMap<String, String>();
						this.javaEEVerifierTableItem
								.setReportItems(javaEEReportMap);
						this.javaEEVerifierReport
								.addJavaEEVerifierReportItem(this.javaEEVerifierTableItem);
					}

				} else if (node.getNodeName().equals(
						JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_KEY)) {
					if ((this.report != null)
							&& (this.javaEEVerifierReportList != null)
							&& (this.javaEEVerifierReport != null)
							&& (this.javaEEVerifierTableItem != null)
							&& (this.javaEEReportMap != null)) {
						NamedNodeMap attributes = node.getAttributes();
						if (attributes != null) {
							Node item = null;
							String key = null;
							item = attributes
									.getNamedItem(JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_NAME_KEY);
							if (item != null) {
								key = item.getNodeValue();
							}
							item = attributes
									.getNamedItem(JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_VALUE_KEY);
							String value = item.getNodeValue();
							if (key != null) {
								javaEEReportMap.put(key, value);
							}
						}
					}
				} else if (node.getNodeName().equals(SERVICE_ASSEMBLY_NAME_KEY)) {
					if (this.report != null) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.report.setServiceAssemblyName(item
									.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(
						SERVICE_ASSEMBLY_DESCRIPTION_KEY)) {
					if (this.report != null) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.report.setServiceAssemblyDescription(item
									.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(
						NUMBER_OF_SERVICE_UNITS_KEY)) {
					if (this.report != null) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							int numberOfServiceUnits = Integer.valueOf(
									item.getNodeValue()).intValue();
							this.report
									.setNumberOfServiceUnits(numberOfServiceUnits);
						}
					}

				} else if (node.getNodeName().equals(
						ALL_COMPONENTS_INSTALLED_KEY)) {
					if (this.report != null) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							boolean allComponentsInstalled = Boolean.valueOf(
									item.getNodeValue()).booleanValue();
							this.report
									.setAllComponentsInstalled(allComponentsInstalled);
						}
					}
				} else if (node.getNodeName().equals(TEMPLATE_ZIPID_KEY)) {
					if (this.report != null) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.report.setTemplateZipId(item.getNodeValue());
						}
					}
				} else if (node.getNodeName()
						.equals(MISSING_COMPONENT_NAME_KEY)) {
					if ((this.report != null)
							&& (this.missingComponentsList != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.missingComponentsList.add(item.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(ENDPOINT_NAME_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)
							&& (this.endpointInformation != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.endpointInformation.setEndpointName(item
									.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(SERVICE_UNIT_NAME_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)
							&& (this.endpointInformation != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.endpointInformation.setServiceUnitName(item
									.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(COMPONENT_NAME_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)
							&& (this.endpointInformation != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.endpointInformation.setComponentName(item
									.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(STATUS_KEY)) {
					if ((this.report != null)
							&& (this.endpointInformationList != null)
							&& (this.endpointInformation != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.endpointInformation.setStatus(item
									.getNodeValue());
						}
					}

				} else if (node.getNodeName().equals(MISSING_APPVAR_NAME_KEY)) {
					if ((this.report != null) && (this.missingAppVars != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.addMissingApplicationVariable(
									this.endpointInformation, item
											.getNodeValue());
						}
					}
				} else if (node.getNodeName()
						.equals(MISSING_APPCONFIG_NAME_KEY)) {
					if ((this.report != null)
							&& (this.missingAppConfigs != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.addMissingApplicationConfiguration(
									this.endpointInformation, item
											.getNodeValue());
						}
					}
				} else if (node.getNodeName().equals(
						JAVAEE_VERIFIER_SERVICE_UNIT_NAME)) {
					if ((this.report != null)
							&& (this.javaEEVerifierReportList != null)
							&& (this.javaEEVerifierReport != null)) {
						Node item = null;
						item = node.getFirstChild();
						if (item != null) {
							this.javaEEVerifierReport.setServiceUnitName(item
									.getNodeValue());
						}
					}
				}
				// recurse on each child
				NodeList children = node.getChildNodes();
				if (children != null) {
					for (int i = 0; i < children.getLength(); i++) {
						if (children.item(i).getNodeType() == Node.TEXT_NODE) {
							if (children.item(i).getNodeValue() != null) {
								if ((qNameStack != null)
										&& (qNameStack.size() > 0)
										&& (qNameStack.peek().equals(name) == true)) {
									qNameStack.pop();
									continue;
								}
							}
						}
						traverseNode(children.item(i));
					}
				}
			}
			break;
		case Node.TEXT_NODE:
			break;
		case Node.CDATA_SECTION_NODE:
			break;
		case Node.COMMENT_NODE:
			break;
		case Node.PROCESSING_INSTRUCTION_NODE:
			break;
		case Node.ENTITY_REFERENCE_NODE:
			break;
		case Node.DOCUMENT_TYPE_NODE:
			break;
		}
	}

	/**
	 * Add missing variable to EndpointInformation
	 * 
	 * @param information
	 * @param appvar
	 */
	private void addMissingApplicationVariable(EndpointInformation information,
			String appvar) {
		String[] oldVariables = information.getMissingApplicationVariables();
		int size = ((oldVariables != null) ? oldVariables.length : 0);
		String[] variables = new String[size + 1];
		if (oldVariables != null) {
			for (int index = 0; index < size; index++) {
				variables[index] = oldVariables[index];
			}
		}
		variables[size] = appvar;
		information.setMissingApplicationVariables(variables);
	}

	/**
	 * Add missing config to EndpointInformation
	 * 
	 * @param information
	 * @param appConfig
	 */
	private void addMissingApplicationConfiguration(
			EndpointInformation information, String appConfig) {
		String[] oldVariables = information
				.getMissingApplicationConfigurations();
		int size = ((oldVariables != null) ? oldVariables.length : 0);
		String[] variables = new String[size + 1];
		if (oldVariables != null) {
			for (int index = 0; index < size; index++) {
				variables[index] = oldVariables[index];
			}
		}
		variables[size] = appConfig;
		information.setMissingApplicationConfigurations(variables);
	}

	public static void main(String[] args) {

		String uri = "C:/test/schema/verification/ApplicationVerificationReport.xml";

		try {
			ApplicationVerificationReport report = ApplicationVerificationReportReader
					.parseFromFile(uri);
			System.out.println(report.getDisplayString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
