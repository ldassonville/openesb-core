/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRStatisticsDataWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.NMRStatisticsData;

/**
 * @author graj
 *
 */
public class NMRStatisticsDataWriter implements NMRStatisticsDataXMLConstants, Serializable {
    
    static final long                       serialVersionUID = -1L;

    static final String       FILE_NAME_KEY    = "NMRStatisticsData.xml";
    
    /** Constructor - Creates an NMRStatisticsDataWriter */
    public NMRStatisticsDataWriter() {
    }
    
    /**
     * 
     * @param document
     * @param directoryPath
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws Exception
     */
    public static void writeToFile(Document document, String directoryPath)
            throws TransformerConfigurationException, TransformerException,
            Exception {
        File file = new File(directoryPath);
        if ((file.isDirectory() == false) || (file.exists() == false)) {
            throw new Exception("Directory Path: " + directoryPath
                    + " is invalid.");
        }
        String fileLocation = file.getAbsolutePath() + File.separator
                + FILE_NAME_KEY;
        System.out.println("Writing out to file: " + fileLocation);
        File outputFile = new File(fileLocation);
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputFile);
        
        // indent the Output to make it more legible...
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        transformer.transform(source, result);
    }
    
    /**
     * Change the contents of text file in its entirety, overwriting any
     * existing text. This style of implementation throws all exceptions to the
     * caller.
     * 
     * @param aFile
     *            is an existing file which can be written to.
     * @throws IllegalArgumentException
     *             if param does not comply.
     * @throws FileNotFoundException
     *             if the file does not exist.
     * @throws IOException
     *             if problem encountered during write.
     */
    public static void setContents(File aFile, String aContents)
            throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            aFile.createNewFile();
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: "
                    + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: "
                    + aFile);
        }
        
        // declared here only to make visible to finally clause; generic
        // reference
        Writer output = null;
        try {
            // use buffering
            // FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            // flush and close both "aspectOutput" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
    }
    
    /**
     * 
     * @param NMRStatisticsData
     *            data 
     * @return XML string
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static String serialize(Map<String /* instanceName */, NMRStatisticsData> dataMap)
            throws ParserConfigurationException, TransformerException {
        Document document = null;
        NMRStatisticsDataWriter writer = new NMRStatisticsDataWriter();
        if (dataMap != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument(); // Create from whole cloth
            
            // ////////////////////////////////
            // <NMRStatisticsDataList>
            Element root = (Element) document
                    .createElement(NMR_STATISTICS_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/NMRStatisticsDataList"
            root.setAttribute(NAMESPACE_KEY, NAMESPACE_VALUE);
            // version = "1.0"
            root.setAttribute(VERSION_KEY, VERSION_VALUE);
            
            for (String instanceName : dataMap.keySet()) {
                NMRStatisticsData data = dataMap.get(instanceName);
                // ////////////////////////////////
                // <NMRStatisticsData>
                Element nmrStatisticsDataElementChild = writer.createNMRStatisticsDataElement(document, data);
                // </NMRStatisticsData>
                root.appendChild(nmrStatisticsDataElementChild);
                // ////////////////////////////////
            }
            // ////////////////////////////////
            // </NMRStatisticsDataList>
            document.appendChild(root);
            // ////////////////////////////////
            
        }
        return writer.writeToString(document);
    }
    
    /**
     * 
     * @param document
     * @param data
     * @return
     */
    protected Element createNMRStatisticsDataElement(Document document,
            NMRStatisticsData data) {
        Element nmrStatisticsDataElement = null;
        if ((document != null) && (data != null)) {
            
            // <NMRStatisticsData>
            nmrStatisticsDataElement = document
                    .createElement(NMR_STATISTICS_DATA_KEY);
            
            // <InstanceName>
            Element instanceNameElementChild = document
                    .createElement(INSTANCE_NAME_KEY);
            if (instanceNameElementChild != null) {
                instanceNameElementChild.setTextContent(data.getInstanceName());
            }
            // </InstanceName>
            nmrStatisticsDataElement.appendChild(instanceNameElementChild);
            
            // <ActiveChannelsList>
            Element activeChannelsListElementChild = createActiveChannelsList(
                    document, data);
            if (activeChannelsListElementChild != null) {
                nmrStatisticsDataElement.appendChild(activeChannelsListElementChild);
            }
            // </ActiveChannelsList>
            
            // <ActiveEndpointsList>
            Element activeEndpointsListElementChild = createActiveEndpointsList(
                    document, data);
            if (activeEndpointsListElementChild != null) {
                nmrStatisticsDataElement.appendChild(activeEndpointsListElementChild);
            }
            // </ActiveEndpointsList>
        }
        return nmrStatisticsDataElement;
    }
    
    /**
     * Create Active Channels List
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createActiveChannelsList(Document document,
            NMRStatisticsData data) {
        Element activeChannelsListElement = null;
        if ((document != null) && (data != null)) {
            // <ActiveChannelsList>
            activeChannelsListElement = document
                    .createElement(ACTIVE_CHANNELS_LIST_KEY);
            if((data.getActiveChannelsList() != null) && (data.getActiveChannelsList().size() > 0)) {
                for (String channel : data.getActiveChannelsList()) {
                    // <ActiveChannel>
                    Element activeChannelElementChild = document
                            .createElement(ACTIVE_CHANNEL_KEY);
                    if (activeChannelElementChild != null) {
                        activeChannelElementChild
                                .setTextContent(channel);
                    }
                    // </ActiveChannel>
                    activeChannelsListElement
                            .appendChild(activeChannelElementChild);
                }
            }
        }
        return activeChannelsListElement;
    }
    
    /**
     * Create Active Channels List
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createActiveEndpointsList(Document document,
            NMRStatisticsData data) {
        Element activeEndpointsListElement = null;
        if ((document != null) && (data != null)) {
            // <ActiveEndpointsList>
            activeEndpointsListElement = document
                    .createElement(ACTIVE_ENDPOINTS_LIST_KEY);
            if((data.getActiveEndpointsList() != null) && (data.getActiveEndpointsList().size() > 0)) {
                for (String endpoint : data.getActiveEndpointsList()) {
                    // <ActiveEndpoint>
                    Element activeEndpointElementChild = document
                            .createElement(ACTIVE_ENDPOINT_KEY);
                    if (activeEndpointElementChild != null) {
                        activeEndpointElementChild
                                .setTextContent(endpoint);
                    }
                    // </ActiveEndpoint>
                    activeEndpointsListElement
                            .appendChild(activeEndpointElementChild);
                }
            }
        }
        return activeEndpointsListElement;
    }
    
    /**
     * @param document
     * @return
     * @throws TransformerException
     */
    protected String writeToString(Document document)
            throws TransformerException {
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        
        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
        
        return result.getWriter().toString();
    }
    
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/nmrstatistics/NMRStatisticsData.xml";
        try {
            Map<String /* instanceName */, NMRStatisticsData> map = null;
            map = NMRStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
            
            String content = NMRStatisticsDataWriter.serialize(map);
            System.out.println(content);
            NMRStatisticsDataWriter.setContents(new File(uri), content);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    
}
