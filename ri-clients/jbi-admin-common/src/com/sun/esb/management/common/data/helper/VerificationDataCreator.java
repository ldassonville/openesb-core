/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)VerificationDataCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularType;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ApplicationVerificationReport;
import com.sun.esb.management.common.data.JavaEEVerifierReport;
import com.sun.esb.management.common.data.EndpointInformation;

/**
 * Creates Composite Data out of an ApplicationVerificationReport object
 * 
 * @author graj
 */
public class VerificationDataCreator {
    
    /** EndpointInfo CompositeType items */
    protected static String[]   endpointDataItemNames          = {
            "EndpointName", 
            "ServiceUnitName", 
            "ComponentName", 
            "Status", 
            "MissingApplicationVariables",
            "MissingApplicationConfigurations"
            };
    
    /** EndpointInfo CompositeType descriptions */
    protected static String[]   endpointDataItemDescriptions   = {
            "Endpoint Name", 
            "Service Unit Name", 
            "Component Name",
            "Status of the endpoint",
            "Missing Application Variables",
            "Missing Application Configurations"};
    
    
    /** Constructor - Create a new VerificationDataCreator */
    private VerificationDataCreator() {
    }
    
    /**
     * This method is used to compose the verification report
     * @return Composite Data out of an ApplicationVerificationReport object
     * @throws ManagementRemoteException
     */
    public static CompositeData createCompositeData(
            ApplicationVerificationReport report)
            throws ManagementRemoteException 
    {
        try 
        {
            ArrayList<String>   verifierReportItemNames = new ArrayList<String>();
            verifierReportItemNames.add("ServiceAssemblyName");
            verifierReportItemNames.add("ServiceAssemblyDescription");
            verifierReportItemNames.add("NumServiceUnits");
            verifierReportItemNames.add("AllComponentsInstalled");
            verifierReportItemNames.add("MissingComponentsList");
            verifierReportItemNames.add("EndpointInfo");
            verifierReportItemNames.add("TemplateZIPID");
            
            ArrayList<String> verifierReportItemDescriptions = new ArrayList<String>();
            verifierReportItemDescriptions.add("Name of the Service Assembly");
            verifierReportItemDescriptions.add("Description of the Service Assembly");
            verifierReportItemDescriptions.add("Number of Service Units");
            verifierReportItemDescriptions.add("Are all necessary components installed");
            verifierReportItemDescriptions.add("List of missing components");
            verifierReportItemDescriptions.add("Information about the endpoints");
            verifierReportItemDescriptions.add("Id for the zip file with configuration templates");
    
            ArrayList<OpenType> verifierReportItemTypes = new ArrayList<OpenType>();
            verifierReportItemTypes.add(SimpleType.STRING);
            verifierReportItemTypes.add(SimpleType.STRING);
            verifierReportItemTypes.add(SimpleType.INTEGER);
            verifierReportItemTypes.add(SimpleType.BOOLEAN);
            verifierReportItemTypes.add(new ArrayType(1, SimpleType.STRING));
            verifierReportItemTypes.add(new ArrayType(1, getEndpointInfoType()));
            verifierReportItemTypes.add(SimpleType.STRING);
            
            ArrayList verifierReportValues = new ArrayList();
            verifierReportValues.add(report.getServiceAssemblyName());
            verifierReportValues.add(report.getServiceAssemblyDescription());
            verifierReportValues.add(report.getNumberOfServiceUnits());
            verifierReportValues.add(new Boolean(report.getMissingComponentsList().size() == 0));
            verifierReportValues.add(report.getMissingComponentsArray());
            verifierReportValues.add(getEndpointsInformation(report)); 
            verifierReportValues.add(report.getTemplateZipId());


            //do not let any exceptions in getting javaeeverifier affect the overall verifier report
            CompositeType javaEEReportType = report.getJavaEEVerifierReportCompositeType();
            if(report.getJavaEEVerifierReports() != null &&
               report.getJavaEEVerifierReports().size() > 0 &&
               javaEEReportType != null)
            {
                verifierReportItemNames.add("JavaEEVerificationReport");
                verifierReportItemDescriptions.add("Java EE Verification Report");
                verifierReportItemTypes.add(new ArrayType(1, javaEEReportType));

                int i=0;
                CompositeData[] cDataArray = new CompositeData[report.getJavaEEVerifierReports().size()];
                for (JavaEEVerifierReport verifierReport : report.getJavaEEVerifierReports())
                {
                    cDataArray[i++] = verifierReport.getCompositeData();
                }
                verifierReportValues.add(cDataArray);
            }





            CompositeType verifierReportType = new CompositeType(
                    "VerifierReportType", 
                    "Type of the verification report",
                    (String[])verifierReportItemNames.toArray(new String[]{}), 
                    (String[])verifierReportItemDescriptions.toArray(new String[]{}),
                    (OpenType[])verifierReportItemTypes.toArray(new OpenType[]{}));

            return new CompositeDataSupport(
                     verifierReportType,
                    (String[])verifierReportItemNames.toArray(new String[]{}), 
                    (Object[])verifierReportValues.toArray(new Object[]{}));
        } catch (OpenDataException ode) 
        {
            throw new ManagementRemoteException(ode.getMessage());
        } 
        
    }
    
    /**
     * This method is used to get information about the status of 
     * the endpoint
     * @param report
     * @returns the endpointinfo 
     * @throws ManagementRemoteException if the endpoint info type could not be obtained
     */
    protected static CompositeData[] getEndpointsInformation(
            ApplicationVerificationReport report)
            throws ManagementRemoteException {
        Map<String, EndpointInformation> endpointInfoMap = null;
        CompositeData[] endpointArray = null;
        try {
            
        OpenType[] endpointDataItemTypes          = {
            SimpleType.STRING, 
            SimpleType.STRING, 
            SimpleType.STRING,
            SimpleType.STRING, 
            new ArrayType(1, SimpleType.STRING),
            new ArrayType(1, SimpleType.STRING)
            };
                
            CompositeType endpointInfoType = new CompositeType(
                    "EndpointInfoType",
                    "Provides information about an endpoint",
                    endpointDataItemNames, endpointDataItemDescriptions,
                    endpointDataItemTypes);
            
            endpointInfoMap = new HashMap<String, EndpointInformation>();
            List<EndpointInformation> endpointInfoList = report
                    .getEndpointInformationList();
            for (EndpointInformation info : endpointInfoList) {
                if (info != null) {
                    endpointInfoMap.put(info.getEndpointName(), info);
                }
            }
            endpointArray = new CompositeData[endpointInfoMap.keySet().size()];
            int counter = 0;
            for (Object key : endpointInfoMap.keySet()) {
                EndpointInformation info = (EndpointInformation) endpointInfoMap
                        .get(key);
                Object[] endpointValues = new Object[6];
                endpointValues[0] = info.getEndpointName();
                endpointValues[1] = info.getServiceUnitName();
                endpointValues[2] = info.getComponentName();
                endpointValues[3] = info.getStatus();
                endpointValues[4] = info.getMissingApplicationVariables();
                endpointValues[5] = info.getMissingApplicationConfigurations();
                CompositeData endpointData = new CompositeDataSupport(
                        endpointInfoType, endpointDataItemNames, endpointValues);
                endpointArray[counter++] = endpointData;
            }
        } catch (OpenDataException ode) {
            throw new ManagementRemoteException(ode.getMessage());
        }
        return endpointArray;
    }
    
    
    /**
     * This method is used to return the endpoint info CompositeType
     * @returns CompositeType endpointInfoType.
     * @throws ManagementRemoteException if the composite type could not be created.
     */
    protected static CompositeType getEndpointInfoType()
            throws ManagementRemoteException {
        try {

        OpenType[] endpointDataItemTypes          = {
            SimpleType.STRING, 
            SimpleType.STRING, 
            SimpleType.STRING,
            SimpleType.STRING, 
            new ArrayType(1, SimpleType.STRING),
            new ArrayType(1, SimpleType.STRING)
            };
    
            return new CompositeType("EndpointInfoType",
                    "Provides information about an endpoint",
                    endpointDataItemNames, endpointDataItemDescriptions,
                    endpointDataItemTypes);
        } catch (OpenDataException e) {
            throw new ManagementRemoteException(e.getMessage());
        }
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/verification/ApplicationVerificationReport.xml";
        try {
            ApplicationVerificationReport report = ApplicationVerificationReportReader
                    .parseFromFile(uri);
            System.out.println(report.getDisplayString());
            CompositeData data = VerificationDataCreator.createCompositeData(report);
            System.out.println(data);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ManagementRemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
    }
    
    
}
