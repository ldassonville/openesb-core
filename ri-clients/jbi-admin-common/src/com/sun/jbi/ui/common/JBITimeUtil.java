/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBITimeUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;


/**
 * This object provides utility methods to convert a (long)timestamp value into
 * String values.
 *
 * @author Sun Microsystems, Inc.
 */
public class JBITimeUtil
{
    /* Constants */
    public static final long   DAY_BASE  = 24 * 60 * 60 * 1000;
    public static final long   HOUR_BASE = 60 * 60 * 1000;
    public static final long   MIN_BASE  = 60 * 1000;
    public static final double SEC_BASE  = 1000f;

    /* Private members */
    private int mDays    = 0;
    private int mHours   = 0;
    private int mMins    = 0;
    private double mSecs = 0;

    /*
     * public default constructor
     */
    public JBITimeUtil ()
    {
    }

    /*
     * public constructor
     */
    public JBITimeUtil (long milliseconds)
    {
        setTime(milliseconds);
    }

    /**
     * Will set the values for the Days, Hours, Minutes and Seconds, given
     * the timestamp (milliseconds).
     * @param milliseconds the number of milliseconds 
     */

    public void setTime (long milliseconds)
    {
        mDays  = (int) (milliseconds / DAY_BASE);
        mHours = (int) ((milliseconds - mDays * DAY_BASE) / HOUR_BASE);
        mMins  = (int) ((milliseconds - mDays * DAY_BASE - mHours * HOUR_BASE) / MIN_BASE);
        mSecs  = ((milliseconds - mDays * DAY_BASE - mHours * HOUR_BASE - mMins * MIN_BASE) / SEC_BASE);
    }

    /**
     * Get the days portion
     * @return the number of days
     */
    public int getDays()
    {
        return mDays;
    }

    /**
     * Get the hours portion
     * @return the number of hours
     */
    public int getHours()
    {
        return mHours;
    }

    /**
     * Get the minutes portion
     * @return the number of minutes
     */
    public int getMinutes()
    {
        return mMins;
    }

    /**
     * Get the seconds, truncated, no milliseconds
     * @return the seconds only
     */
    public int getSecondsOnly()
    {
        int seconds = (int) (mSecs / 1000);
        return seconds;
    }

    /**
     * Get the seconds with millsecconds portion
     * @return the seconds (including milliseconds)
     */
    public double getSeconds()
    {
        return mSecs;
    }
    
}


