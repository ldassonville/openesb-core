/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMXConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JMXConnectionException;
import javax.management.Attribute;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

/** This interface defines the higher level interface
 * to access jmx apis from client side.
 * @author Sun Microsystems, Inc.
 */

public interface JMXConnection
{    
    /**
     * opens the jmx connection
     * @throws JMXConnectionException on jmx error
     */
    public void openConnection() throws JMXConnectionException;
    /**
     * closes the jmx connection
     * @throws JMXConnectionException on jmx error
     */
    public void closeConnection() throws JMXConnectionException;
    
    /**
     * gives the MBeanSever connection interface. Should be called after open
     * @return mbean server connection interface
     * @throws IllegalStateException on jmx error
     */
    public MBeanServerConnection getMBeanServerConnection()
    throws IllegalStateException ;
    
    /**
     * invokes the operation on mbean
     * @return result object
     * @param name object name
     * @param operationName operation name
     * @param params parameters
     * @param signature signature of the parameters
     * @throws JMXConnectionException on jmx error
     * @throws JBIRemoteException on user error
     */
    public Object invokeMBeanOperation(ObjectName name, String operationName,
    Object[] params, String[] signature)
    throws JMXConnectionException, JBIRemoteException;
    
    /**
     * retrieves the attribute value
     * @return result object
     * @param name object name
     * @param attribute attribute object
     * @throws JMXConnectionException on jmx error
     * @throws JBIRemoteException on user error
     */
    public Object getMBeanAttribute(ObjectName name, String attribute)
    throws JMXConnectionException, JBIRemoteException;
    
    /**
     * sets the attribute value
     * @param name object name
     * @param attribute attribute object
     * @throws JMXConnectionException on jmx error
     * @throws JBIRemoteException on user error
     */
    public void setMBeanAttribute(ObjectName name, Attribute attribute)
    throws JMXConnectionException, JBIRemoteException;
    
}
