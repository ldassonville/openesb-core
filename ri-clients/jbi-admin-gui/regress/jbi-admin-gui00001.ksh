#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

# WARNING - this script is called from jbiBuild to patch glassfish install for distribution.
# DO not alter without testing with "runjbiBuild -repackgf".  RT 9/5/07

echo jbi-admin-gui00001.ksh

#regress setup
. ./regress_defs.ksh

exit 0

# the following is only needed to patch glassfish 9.1 before 9.1_01-b06
#ant -f inc/patch-admin-jar.ant            # remove changed classes
#ant -f inc/patch-admin-jsf-jar.ant        # remove changed views
#ant -f inc/patch-jbi-faces-config-xml.ant # add new bean to managed beans
#ant -f inc/patch-sun-web-xml.ant          # add new jar to classpath
#ant -f inc/promote-jbi-admin-gui-jar.ant  # copy/rename jar with new/changed classes/views

