/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  CompStatsUtils.java
 */

package com.sun.jbi.jsf.util;

import com.sun.jbi.ui.common.JBIAdminCommands;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.openmbean.TabularData;


/**
 *
 * Utilities to get Service Assembly and Service Unit 
 * statistics data
 *
 **/

public final class SaStatsUtils
{

     /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();


    /**
     * Utility class - constructor not used.
     */
    private SaStatsUtils()
    {
    }

    /**
     * Gets the statistics for the specified JBI Service Unit
     * in the specified Service Assembly, for the specified
     * target.
     * @param aServiceAssemblyName the JBI Service Assembly name
     * @param aServiceUnitName the JBI Service Unit name
     * @param aTargetName the name of a standalone server
     * or the name of a cluster.
     * @return a statistics for the specified Service Unit
     */
    public static TabularData getStats(String aTargetName,
                                       String aServiceAssemblyName)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("SaStatsUtils.getStats(" + aTargetName +
                  ", " + aServiceAssemblyName + ")");
    	}
        
        TabularData result = null;
        try
            {
                JBIAdminCommands jac = 
                    BeanUtilities.getClient();

                result =
                    jac.getServiceAssemblyStats(aServiceAssemblyName,
                                                aTargetName);
                
                if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("SaStatsUtils.getStats(...), result=" +
                          result);
                }
                
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.log(Level.FINE,
                         ("SaStatsUtils.getStats(...) caught jrEx=" + 
                          jrEx),
                         jrEx);
        	    }
            }
        return result;
    }

}
