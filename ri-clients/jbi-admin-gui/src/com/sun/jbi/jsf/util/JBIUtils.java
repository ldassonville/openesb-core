/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  JBIUtils.java
 *
 */

package com.sun.jbi.jsf.util;

import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.DOMUtil;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.webui.jsf.model.Option;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.openmbean.TabularData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 *
 * This class is used to provide JBI Component oriented utilitiies 
 *
 **/

public final class JBIUtils
{

    /**
     * gets properties for a named component application configuration
     * @param aCompName the JBI component
     * @param anInstanceName the glassfish instance
     * @param anAppConfigName the application configuration
     * @return properties for the specified application configuration
     */
    public static Properties getCompAppConfigProps(String aCompName,
                                                   String anInstanceName,
                                                   String anAppConfigName)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aCompName,
                        anInstanceName,
                        anAppConfigName,
                    };
                JBILogger.entering(CN, MN_GET_COMP_APP_CONFIG_PROPS, 
                                   args);
            }

        Properties result = null;
    
    
        try
            {
                JBIAdminCommands jac = 
                    BeanUtilities.getClient();

                Map<String,Properties> compAppConfigMaps = 
                    jac.getApplicationConfigurations (aCompName,
                                                      anInstanceName);
        
                if (sLog.isLoggable(Level.FINER)) {
                   sLog.finer("AppConfigUtils.getProperties(...), compAppConfigMaps=" +
                          compAppConfigMaps);
                }
        
                result =
                    compAppConfigMaps.get(anAppConfigName);
        
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	    if (sLog.isLoggable(Level.FINE)) {
                sLog.log(Level.FINE,
                         ("AppConfigUtils.getProperties(...) caught jrEx=" + 
                          jrEx),
                         jrEx);
        	    }
            }

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_COMP_APP_CONFIG_PROPS,
                                  result);
            }

        return result;
    }

    /**
     * gets properties for the specified component runtime configuration
     * @param aCompName the JBI Component name
     * @param aTarget the glassfish target
     * @return propertie for the component configuration
     */
    public static Properties getCompConfigProps(String aCompName,
                                                String aTarget)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aCompName,
                        aTarget,
                    };
                JBILogger.entering(CN, MN_GET_COMP_CONFIG_PROPS, 
                                   args);
            }

        Properties result = null;
        try
            {
                result = 
                    sJac.getComponentConfiguration(aCompName,
                                                   aTarget);
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
    	        if (sLog.isLoggable(Level.FINE)) {
                sLog.log(Level.FINE,
                         (CN + "." +
                          MN_GET_COMP_CONFIG_PROPS +
                          "(), caught jrEx=" +
                          jrEx),
                         jrEx);
    	        }

            }        

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_COMP_CONFIG_PROPS,
                                  result);
            }

        return result;
    }

    /**
     * gets the JBI Metadata <code>jbi.xml</code> for a component
     * @param aCompName a JBI Component name
     * @return the metadata in an XML string
     */
    public static String getCompMetadata(String aCompName)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aCompName,
                    };
                JBILogger.entering(CN, MN_GET_COMP_METADATA, 
                                   args);
            }

        String result = null;
        try
            {
                result = 
                    sJac.getComponentInstallationDescriptor(aCompName);
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
    	        if (sLog.isLoggable(Level.FINE)) {        	
                sLog.log(Level.FINE,
                         (CN + "." +
                          MN_GET_COMP_METADATA +
                          "(), caught jrEx=" +
                          jrEx),
                         jrEx);
    	        }

            }        

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_COMP_METADATA,
                                  result);
            }

        return result;
    }

    /**
     * gets the component state
     * @param aCompName the JBI Component name
     * @param aCompType one of: <code>binding-component</code> or
     * <code>service-enginge</code>
     * @param aTarget the glassfish target
     * @param aStatus properties to error information
     * @return the state of the component on the instance, one of:
     * <code>Started</code>, <code>Stopped</code>, <code>Shutdown</code>,
     * or <code>Unknown<code>
     */
    public static String getCompState(String aCompName,
                                      String aCompType,
                                      String aTarget,
                                      Properties aStatus)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aCompName,
                        aCompType,
                        aTarget,
                    };
                JBILogger.entering(CN, MN_GET_COMP_STATE, 
                                   args);
            }

        String result = 
            SharedConstants.STATE_UNKNOWN;
        
        String queryResult = null;
        
        try
            {
                if (JBIConstants.JBI_BINDING_COMPONENT_TYPE.equals(aCompType))
                    {
                        queryResult =
                            sJac.showBindingComponent(aCompName,
                                                      SharedConstants.NO_STATE_CHECK,
                                                      SharedConstants.NO_LIBRARY_CHECK,
                                                      SharedConstants.NO_DEPLOYMENT_CHECK,
                                                      aTarget);
                    }
                else if (JBIConstants.JBI_SERVICE_ENGINE_TYPE.equals(aCompType))
                    {
                        queryResult =
                            sJac.showServiceEngine(aCompName,
                                                   SharedConstants.NO_STATE_CHECK,
                                                   SharedConstants.NO_LIBRARY_CHECK,
                                                   SharedConstants.NO_DEPLOYMENT_CHECK,
                                                   aTarget);
                        
                    }
                
                List list = 
                    null;
                
                if (null != queryResult)
                    {
                        list =
                            JBIComponentInfo.readFromXmlText(queryResult);
                    }
                
                if ((null != list)
                    &&(1 == list.size()))
                    {
                        JBIComponentInfo compInfo =
                            (JBIComponentInfo) list.get(0);
                        result =
                            compInfo.getState();
                    }
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
    	        if (sLog.isLoggable(Level.FINE)) {        	
                  sLog.log(Level.FINE,
                         (CN + "." +
                          MN_GET_COMP_STATE +
                          "(), caught jrEx=" +
                          jrEx),
                         jrEx);
    	        }

                aStatus.put(SharedConstants.ALERT_SUMMARY_KEY, 
                            I18nUtilities
                            .getResourceString("jbi.comp.config.info.not.available"));

                JBIManagementMessage mgmtMsg =
                    BeanUtilities.extractJBIManagementMessage(jrEx);

                if (null != mgmtMsg)
                    {
                        aStatus.put(SharedConstants.ALERT_DETAIL_KEY, 
                                    mgmtMsg.getMessage());
                    }
                else
                    {
                        String internalErrorMsg =
                            I18nUtilities
                            .getResourceString("jbi.internal.error.invalid.remote.exception");
                        aStatus.put(SharedConstants.ALERT_DETAIL_KEY, 
                                    internalErrorMsg);
                    }
            }


        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_COMP_STATE,
                                  result);
            }

        return result;
    }


    /** Get the extended componentInfo - version and build number
     * [Issue-1526]
     * @param compInfo
     */
    public static void getComponentInfoExt(JBIComponentInfo compInfo) {
        try {

            String xmlText = sJac.getComponentInstallationDescriptor(compInfo.getName());
            // parse it
            Reader xmlReader = new StringReader(xmlText);
            Document xmlDoc = null;

            xmlDoc = DOMUtil.UTIL.buildDOMDocument(xmlReader, true);

            if (xmlDoc != null) {
                // construct the compInfo list
                Element jbiElement = DOMUtil.UTIL.getElement(xmlDoc, "jbi");
                if ( jbiElement != null) {
                    Element verElement = DOMUtil.UTIL.getElement(jbiElement, "identification:VersionInfo");
                    if (verElement != null) {
                        String compVer = verElement.getAttribute("component-version");
                        String buildNumber = verElement.getAttribute("build-number");
                        compInfo.setComponentVersion(compVer);
                        compInfo.setBuildNumber(buildNumber);
                    }
                }
        }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * @param aType one of: <code>consuming</code> or
     * <code>providing</code>
     * @param aCompName a JBI component name
     * @param anInstanceName an instance name
     * @return the endpoints for the component
     */
    public static TabularData getEndpoints(String aType,
                                           String aCompName,
                                           String anInstanceName)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aType,
                        aCompName,
                        anInstanceName,
                    };
                JBILogger.entering(CN, MN_GET_ENDPOINTS, 
                                   args);
            }

        TabularData result = null;
    
        String targetName =
            ClusterUtilities
            .getInstanceDomainCluster(anInstanceName);

        try
            {
                JBIAdminCommands jac = 
                    BeanUtilities.getClient();

                if (SharedConstants
                    .ENDPOINTS_TYPE_CONSUMING
                    .equals(aType))
                    {
                        result =
                            jac.getConsumingEndpointsForComponent(aCompName,
                                                                  targetName);
                    }
                else if (SharedConstants
                    .ENDPOINTS_TYPE_PROVIDING
                    .equals(aType))
                    {
                        result =
                            jac.getProvidingEndpointsForComponent(aCompName,
                                                                  targetName);
                    }
        
                if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("JBIUtils.getEndpoints(), aType=" + aType +
                          ", targetName=" + targetName +
                          ", result=" + result);
                }
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
    	        if (sLog.isLoggable(Level.FINE)) {        	
                   sLog.log(Level.FINE,
                         ("JBIUtils.getEndpoints(), aType=" + aType +
                          ", targetName=" + targetName +
                          ", caught jrEx=" + jrEx),
                         jrEx);
    	        }
            }

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_ENDPOINTS,
                                  RESULT_ALREADY_LOGGED);
                // workaround: skip logging TabularData result here
                // to avoid ClassCastException in TabularDataSupport
                // called indirectly by AMXLoggingHook
                // (clutters the server.log)
                // for some reason log of result= (above) works okay.
            }

        return result;
    }



    /**
     * Gets the instance names for the specified JBI Shared Library,
     * Component, or Service Assembly.
     *
     * @param aJBIName  JBI Library, Component, or Service Assembly name
     * @param aJBIType  JBI type, one of: <code>shared-library</code>,
     * <code>binding-component</code>, <code>service-engine</code>, or
     * <code>service-assembly</code>
     * @return           A sorted list of <code>Option</code>
     * objects suitable for populating a <code>sun:dropDown</code>,
     * each representing an instance for this JBI library, component,
     * or service assembly.
     */
    public static Option[] getInstancesAsOptionArray(String aJBIName,
                                                     String aJBIType)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aJBIName,
                        aJBIType,
                    };
                JBILogger.entering(CN,
                                   MN_GET_INSTANCES_AS_OPTION_ARRAY,
                                   args);
            }

        Option[] result =
            new Option[0];

        List<String> instancesAsStringList =
            getInstancesAsStringList(aJBIName,
                                     aJBIType);

        // if there is an instance list,
        //    get currSel for instance list for comp name/type and prev selection
        if ((null != instancesAsStringList)
            && (0 < instancesAsStringList.size()))
            {
                List<Option> selectableInstancesList =
                    new ArrayList<Option>();


                for (int i = 0; i < instancesAsStringList.size(); ++i)
                    {
                        String instanceName =
                            (String) instancesAsStringList.get(i);
                        Option selectableInstance =
                            new Option(instanceName,
                                       instanceName);
                        selectableInstancesList
                            .add(selectableInstance);

                    }

                result =
                    (Option[])
                    selectableInstancesList
                    .toArray(result);
            }

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_INSTANCES_AS_OPTION_ARRAY,
                                  result);
            }
        return result;
    }


    /**
     * gets the glassfish instances for a JBI name and type.
     * @param aJBIName the JBI library, component, or service assembly name
     * @param aJBIType one of: <code>shared-library</code>, 
     * <code>binding-component</code>, <code>service-engine</code>, or
     * <code>service-assembly</code>
     * @return a possibly empty list of glassfish instances
     */
    public static List<String> getInstancesAsStringList(String aJBIName,
                                                        String aJBIType)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aJBIName,
                        aJBIType,
                    };
                JBILogger.entering(CN,
                                   MN_GET_INSTANCES_AS_STRING_LIST,
                                   args);
            }

        List<String> result = 
            null;

        List<Properties> targetsAsPropertiesList = 
            ClusterUtilities
            .findTargetsForNameByType(aJBIName,
                                      aJBIType);
        
        List<String> targetsAsStringList =
            new ArrayList();
        for (int i = 0; i < targetsAsPropertiesList.size(); ++i)
            {
                Properties targetProp =
                    (Properties) targetsAsPropertiesList.get(i);
                String targetName =
                    (String) targetProp.get(SharedConstants.KEY_NAME);
                
                targetsAsStringList.add(targetName);
            }
        
        result = 
            ClusterUtilities
            .getAllInstances(targetsAsStringList);
        
        Collections.sort(result);

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_GET_INSTANCES_AS_STRING_LIST,
                                  result);
            }
        return result;
    }

    /**
     * Determines if the specified JBI Component of the specified
     * type is started on the specified instanace.
     *
     * @param aCompName       JBI Component Name
     * @param aCompType       JBI Component Type, one of: 
     * <code>binding-component</code> or <code>service-engine</code>
     * @param anInstanceName  An instance name
     * @param aStatus         An empty <code>Properties</code>
     * object to receive a <code>success-result</code> diagnostic for logging,
     * or an <code>alert summary</code> and <code>alert-details</code> for display, 
     * if needed.
     * @return                true if the component is started
     */
    public static boolean isCompStartedOnInstance(String aCompName,
                                                  String aCompType,
                                                  String anInstanceName,
                                                  Properties aStatus)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aCompName,
                        aCompType,
                        anInstanceName,
                        aStatus,
                    };
                JBILogger.entering(CN, MN_IS_COMP_STARTED_ON_INSTANCE, 
                                   args);
            }

        boolean result = false;

        String target =
            ClusterUtilities
            .getInstanceDomainCluster(anInstanceName);

        String state = getCompState(aCompName,
                                    aCompType,
                                    target,
                                    aStatus);

        if (JBIComponentInfo.STARTED_STATE.equals(state) || JBIComponentInfo.STOPPED_STATE.equals(state))
            {
                result = true;
                String diagnostic =
                    "(diagnostic) '" + aCompName + "' started/stopped on '" + target + "'";

                aStatus.put(SharedConstants.SUCCESS_RESULT, 
                            diagnostic);
            }
        else
            {
                String infoNotAvail =
                    I18nUtilities.getResourceString("jbi.comp.config.info.not.available");
                aStatus.put(SharedConstants.ALERT_SUMMARY_KEY, 
                            infoNotAvail);
                String compNotStartedKey;
                if (ClusterUtilities.isClusterProfile())
                    {
                        compNotStartedKey =
                            "jbi.comp.config.comp.not.started";
                    }
                else
                    {
                        compNotStartedKey =
                            "jbi.comp.config.comp.not.started.pe";
                    }
                String compNotStarted;
                        compNotStarted =
                            I18nUtilities
                            .getResourceString(compNotStartedKey);

                aStatus.put(SharedConstants.ALERT_DETAIL_KEY,
                            compNotStarted);
            }

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_IS_COMP_STARTED_ON_INSTANCE,
                                  result);
            }

        return result;
    }

    /**
     * Determines which instance to use, if any, as the currently selected instance.
     * Uses either the previously selected instance if it is in the list,
     * the default <code>server</code> target if it is in the list,
     * the first instance in the list if it is non empty,
     * or <code>null</code> if the list is empty.
     *
     * @param anInstancesList        a possibly <code>null</code> or empty list
     * of instances
     * @param aPrevSelectedInstance  a possibly invalid or valid instance
     * @return                       The first valid instance or <code>null</code>
     * if none.  Precedent: previous instance, <code>server</code>, the first
     * (alphabetically) instance in the list, or <code>null</code>.
     */
    public static String selectCurrentInstance(List<String> anInstancesList,
                                               String aPrevSelectedInstance)
    {
        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        anInstancesList,
                        aPrevSelectedInstance,
                    };
                JBILogger.entering(CN,
                                   MN_SELECT_CURRENT_INSTANCE,
                                   args);
            }

        String result = 
            JBIAdminCommands
            .SERVER_TARGET_KEY;

        // If there is a list
        // validates the previously selected instance
        // or selects from the list
        if ((null != anInstancesList)
            &&(0 != anInstancesList.size()))
            {
                boolean isPrevValid = false;

                // if there is a previous selected instance,
                // check to see if it is in the list
                if (null != aPrevSelectedInstance)
                    {
                        isPrevValid =
                            anInstancesList.contains(aPrevSelectedInstance);
                        // if the previously selected instance is in the list,
                        // use it
                        if (isPrevValid)
                            {
                                result = aPrevSelectedInstance;
                            }
                    }
                
                // If the previous selection is not valid,
                // picks the "best" selection from the list
                if (!isPrevValid)
                    {
                        boolean isServerInList =
                            anInstancesList
                            .contains(JBIAdminCommands
                                      .SERVER_TARGET_KEY);

                        // if "server" is in the list, 
                        // defaults to using "server"
                        if (isServerInList)
                            {
                                result = 
                                    JBIAdminCommands
                                    .SERVER_TARGET_KEY;
                            }
                        // if "server" is not in the list,
                        // uses the first instance in the list
                        else
                            {
                                result =
                                    (String) anInstancesList
                                    .get(0);
                            }
                    }
            }

        // If the list is empty,
        // uses the default "server" (even if it is invald)
        else
            {
                // (result remains default)
            }
        
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_SELECT_CURRENT_INSTANCE,
                                  result);
            }
        return result;
    }

    /**
     * creates or updates a named component application configuration
     * @param anAction one of: <code>create</code> or <code>update</code>
     * @param anInstanceName a glassfish instance
     * @param aCompName a JBI Component name
     * @param anAppConfigName the application configuration name
     * @param aToBeSavedProps the properties to be saved
     * @return result of the save, containing a key of
     * <code>success-result</code> or <code>failure-result</code>
     */
    public static Properties updateCompAppCompConfig(String anAction,
                                                     String anInstanceName,
                                                     String aCompName,
                                                     String anAppConfigName,
                                                     Properties aToBeSavedProps)
    {
        Properties result =
            new Properties();
        try
            {
                String mgmtMessage = 
                    null;

                // if updating, set new values into existing app config
                if (SharedConstants
                    .APP_CONFIG_ACTION_UPDATE.equals(anAction))
                    {
                        mgmtMessage =
                            sJac.setApplicationConfiguration(aCompName,
                                                             anInstanceName,
                                                             anAppConfigName,
                                                             aToBeSavedProps);
                    }
                // if creating, add new app config
                else
                    {
                        mgmtMessage =
                            sJac.addApplicationConfiguration(aCompName,
                                                             anInstanceName,
                                                             anAppConfigName,
                                                             aToBeSavedProps);
                    }

                result.put(SharedConstants.SUCCESS_RESULT, 
                           mgmtMessage);
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
    	        if (sLog.isLoggable(Level.FINE)) {        	
                   sLog.log(Level.FINE,
                         (CN + "." +
                          MN_UPDATE_COMP_APP_COMP_CONFIG +
                          "(), caught jrEx=" +
                          jrEx),
                         jrEx);
    	        }
    	        
                result.put(SharedConstants.FAILURE_RESULT, 
                           jrEx);
            }        

        return result;
    }

    /**
     * updates a component runtime configuration
     * @param anInstanceName a glassfish instance
     * @param aCompName a JBI Component name
     * @param aToBeSavedProps the properties to be saved
     * @return result of the save, containing a key of
     * <code>success-result</code> or <code>failure-result</code>
     */

    public static Properties updateCompRuntimeCompConfig(String anInstanceName,
                                                         String aCompName,
                                                         Properties aToBeSavedProps)
    {
        Properties result =
            new Properties();
        try
            {
                String mgmtMessage = 
                    sJac.setComponentConfiguration(anInstanceName,
                                                   aCompName,
                                                   aToBeSavedProps);



                result.put(SharedConstants.SUCCESS_RESULT, 
                           mgmtMessage);
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
    	        if (sLog.isLoggable(Level.FINE)) {
                   sLog.log(Level.FINE,
                         (CN + "." +
                          MN_UPDATE_COMP_RUNTIME_COMP_CONFIG +
                          "(), caught jrEx=" +
                          jrEx),
                         jrEx);
    	        }

                result.put(SharedConstants.FAILURE_RESULT, 
                           jrEx);
            }

        return result;
    }
    

    /**
     * ensures that there is only one success or failure
     * i.e. takes warnings from success results and turns
     * them into failure results, to force alert details to be
     * displayed
     * @param aRowProperties containing zero or more success and
     * zero or more failure results
     * @returns Propeties with failure results if any, otherwise
     * success results, if any
     */
    public static Properties adjustResults(Properties aRowProperties)
    {
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("OperationHandlers.adjustResults(" + aRowProperties + ")");
        }

        JBIManagementMessage mgmtMsg = null;
        String failureResult =
            aRowProperties.getProperty(SharedConstants.FAILURE_RESULT);
        if (null == failureResult)
            {
                String failureResult2 =
                    aRowProperties.getProperty(SharedConstants.SUCCESS_RESULT);
                if (null == failureResult2)
                    {
                       if (sLog.isLoggable(Level.FINER)) {
                           sLog.finer("OperationHandlers.adjustResults(...)" +
                                  "no failure or success (?!) --nothing to adjust");
                       }
                    }
                else
                    {
                        mgmtMsg =
                            BeanUtilities.extractJBIManagementMessage(failureResult2);

                        if (sLog.isLoggable(Level.FINER)) {
                           sLog.finer("OperationHandlers.adjustResults(...) mgmtMsg=" + mgmtMsg);
                        }

                        if ((null != mgmtMsg)
                            &&((mgmtMsg.isFailedMsg()
                                ||mgmtMsg.isWarningMsg())))
                            {
                               if (sLog.isLoggable(Level.FINER)) {
                                sLog.finer("OperationHandlers.adjustResults(...)" +
                                          " jbiXmlResults has failure(s) -- adjust failureResult2=" +
                                          failureResult2);
                                }

                                aRowProperties.remove(SharedConstants.SUCCESS_RESULT);
                                String msg = mgmtMsg.getMessage();
                                msg = msg.replaceAll(NEWLINE, BREAK);

                                if (sLog.isLoggable(Level.FINER)) {
                                   sLog.finer("OperationHandlers.adjustResults(...) msg=" + msg);
                                }
                                aRowProperties.setProperty(SharedConstants.FAILURE_RESULT, msg);
                            }
                        else
                            {
                               if (sLog.isLoggable(Level.FINER)) {
                                 sLog.finer("OperationHandlers.adjustResults(...)" +
                                          " jbiXmlResult success --nothing to adjust");
                               }
                            }
                    }
            }
        else
            {
                if (sLog.isLoggable(Level.FINER)) {
                   sLog.finer("OperationHandlers.accumulateResults(...)" +
                          " found failure--accumulate failureResult=" + failureResult);
                }
                mgmtMsg =
                    BeanUtilities.extractJBIManagementMessage(failureResult);
                if (null != mgmtMsg)
                    {
                        aRowProperties.remove(SharedConstants.SUCCESS_RESULT);
                        String msg = mgmtMsg.getMessage();
                        msg = msg.replaceAll(NEWLINE, BREAK);
                        aRowProperties.setProperty(SharedConstants.FAILURE_RESULT, msg);
                    }
                else
                    {
                       if (sLog.isLoggable(Level.FINER)) {
                        sLog.finer("OperationHandlers.accumulateResults(...)" +
                                  "no management message");
                       }
                    }
            }
        return aRowProperties;
    }




    /**
     * prevents instantiation and subclassing
     */
    private JBIUtils()
    {
    }

    private static final String CN = JBIUtils.class.getName(); // not I18n
    private static final String MN_GET_COMP_APP_CONFIG_PROPS = "getCompAppConfigProps"; // not I18n
    private static final String MN_GET_COMP_CONFIG_PROPS = "getCompConfigProps"; // not I18n
    private static final String MN_GET_COMP_METADATA = "getCompMetadata"; // not I18n
    private static final String MN_GET_COMP_STATE = "getCompState"; // not I18n
    private static final String MN_GET_ENDPOINTS = "getEndpoints"; // not I18n
    private static final String MN_GET_INSTANCES_AS_OPTION_ARRAY = 
        "getInstancesAsOptionArray"; // not I18n
    private static final String MN_GET_INSTANCES_AS_STRING_LIST = 
        "getInstancesAsStringList"; // not I18n
    private static final String MN_IS_COMP_STARTED_ON_INSTANCE = "isCompStartedOnInstance"; // not I18n
    private static final String MN_SELECT_CURRENT_INSTANCE = "selectCurrentInstance"; // not I18n
    private static final String MN_UPDATE_COMP_APP_COMP_CONFIG = "updateCompAppCompConfig"; // not I18n
    private static final String MN_UPDATE_COMP_RUNTIME_COMP_CONFIG = "updateCompRuntimeCompConfig"; // not I18n
    private static final String RESULT_ALREADY_LOGGED = "RESULT_ALREADY_LOGGED"; // not I18n

    private static final String BREAK = SharedConstants.HTML_BREAK;
    private static final String NEWLINE = "\r";

    private static JBIAdminCommands sJac;

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();


    static
    {
        sJac = BeanUtilities.getClient();
    }

}
