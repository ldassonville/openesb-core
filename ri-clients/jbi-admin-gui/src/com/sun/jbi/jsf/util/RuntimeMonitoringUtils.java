/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  RuntimeMonitoringUtils.java
 *
 */

package com.sun.jbi.jsf.util;
import com.sun.jbi.ui.common.JBIAdminCommands;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;

/**
 *
 * This class is used to provide utilitiies for the Component
 * Application Configuration use-cases
 *
 **/

public final class RuntimeMonitoringUtils
{

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();

    /**
     * Gets endpoint statistics for specified endpoint on the specified
     * target
     * @param anEndpointName endpoint
     * @param anInstanceName target standalone or cluster instance name
     * @param aNameToStatsMap to add the zero or more names and stats mappings to
     * @param aStatus Properties to receive a status result
     */
    public static void addEndpointStatsToMap(String anEndpointName,
                                             String anInstanceName,
                                             Map<String,CompositeData> aNameToStatsMap,
                                             Properties aStatus)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("RuntimeMonitoringUtils.getEndpointStats(" +
                  anEndpointName + ", " +
                  anInstanceName + ", " +
                  aNameToStatsMap + ", " +
                  aStatus + ")");
    	}

        String targetName =
            ClusterUtilities
            .getInstanceDomainCluster(anInstanceName);

        try
            {
                JBIAdminCommands jac = 
                    BeanUtilities.getClient();

                TabularData statsTD =
                    jac.getEndpointStats(anEndpointName,
                                         targetName);
                
                CompositeData statsCD =
                    CompositeDataUtils.getDataForInstance(statsTD,
                                                          anInstanceName);

                aNameToStatsMap.put(anEndpointName,
                                    statsCD);

                aStatus.put("success-result", anEndpointName);
                
                if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("RuntimeMonitoringUtils.addEndpointStatsToMap(...)" +
                          ", targetName=" + targetName +
                          ", aNameToStatsMap=" + aNameToStatsMap +
                          ", aStatus=" + aStatus);
                }

            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.log(Level.FINE,
                         ("RuntimeMonitoringUtils.addEndpointStatsToMap(...) caught jrEx=" + 
                          jrEx),
                         jrEx);
        	    }
                aStatus.put("failure-result",jrEx);
            }

    }

    /**
     * Gets framework statistics for specified instance, if available
     * @param anInstanceName String instance with target MBean for the component
     * @param aStatus Properties to receive a status result
     * @return TabularData with framework statistics (or null)
     */
    public static TabularData getFrameworkStats(String anInstanceName,
                                                Properties aStatus)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("RuntimeMonitoringUtils.getFrameworkStats(" +
                  anInstanceName + ", " +
                  aStatus + ")");
    	}

        TabularData result = null;

        try
            {
                JBIAdminCommands jac = 
                    BeanUtilities.getClient();

                result =
                    jac.getFrameworkStats(anInstanceName);
                aStatus.put("success-result", anInstanceName);
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.log(Level.FINE,
                         ("RuntimeMonitoringUtils.getFrameworkStats(...) caught jrEx=" + 
                          jrEx),
                         jrEx);
        	    }
                aStatus.put("failure-result",jrEx);
            }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("RuntimeMonitoringUtils.getFrameworkStats(...), result=" +
                  result + 
                  ", aStatus=" + aStatus);
        }

        return result;
    }

    /**
     * Gets NMR statistics for specified instance, if available
     * @param anInstanceName String instance with target MBean for the component
     * @param aStatus Properties to receive a status result
     * @return TabularData with NMR statistics (or null)
     */
    public static TabularData getNMRStats(String anInstanceName,
                                          Properties aStatus)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("RuntimeMonitoringUtils.getNMRStats(" +
                  anInstanceName + ", " +
                  aStatus + ")");
    	}

        TabularData result = null;

        try
            {
                JBIAdminCommands jac = 
                    BeanUtilities.getClient();

                result =
                    jac.getNMRStats(anInstanceName);
                aStatus.put("success-result", anInstanceName);
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.log(Level.FINE,
                         ("RuntimeMonitoringUtils.getNMRStats(...) caught jrEx=" + 
                          jrEx),
                         jrEx);
        	    }
        	    
                aStatus.put("failure-result",jrEx);
            }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("RuntimeMonitoringUtils.getNMRStats(...), result=" +
                  result + 
                  ", aStatus=" + aStatus);
        }

        return result;
    }

    /**
     * prevents subclassing and instantiation
     */
    private RuntimeMonitoringUtils()
    {
    }
}
