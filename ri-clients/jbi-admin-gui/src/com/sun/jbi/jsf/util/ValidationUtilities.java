/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */

/*
 *  ValidationUtilities.java
 */
package com.sun.jbi.jsf.util;

import com.sun.jbi.jsf.bean.ArchiveBean;
import com.sun.jbi.jsf.bean.ConfigProperty;
import com.sun.jbi.jsf.bean.ConfigPropertyGroup;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.sun.org.apache.xpath.internal.XPathAPI;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.faces.context.FacesContext;

/**
 * XML, XSD, XPath related utilities
 *
 * @author   Sun Microsystems Inc.
 */

public final class ValidationUtilities
{

    /**
     * Gets the configuration extension elements, if any
     *
     * @param aJbiDocument  a jbi.xml document
     * @param anElementType one of: <code>Configuration</code> or
     * <code>ApplicationConfiguration</code>
     * @return              a list of <code>ConfigPropertyGroup</code> beans,
     * or null if there are no valid Configuration extension elements found
     * for the specified type.
     */
    public static List<ConfigPropertyGroup> getConfigExtensions(Document aJbiDocument,
                                                                String anElementType)
    {
        List<ConfigPropertyGroup> result = 
            null; // if no valid Configuration extension found for the requested type 

        if (null != aJbiDocument)
            {
                NodeList configElements =
                    aJbiDocument.getElementsByTagNameNS(ESBConstants.NS_CFG,
                                                        anElementType);

                if ((null != configElements)
                    && (1 == configElements.getLength()))
                    {
                	    if (sLog.isLoggable(Level.FINER)){
                        sLog.finer("ValidationUtilities.getConfigExtensions(), found Configuration element");
                	    }

                        ConfigPropertyGroup defaultPropertyGroup =
                            createDefaultPropertyGroup(anElementType);
                        result =
                            new ArrayList();
                        result.add(defaultPropertyGroup);

                        for (int i = 0; i < configElements.getLength(); ++i)
                            {
                                Node configNode =
                                    configElements.item(i);

                                // add ungrouped properties to the default group
                                // add property groups as found
                                // (adding grouped properties under them)

                                for (Node child = configNode.getFirstChild();
                                     child != null;
                                     child = child.getNextSibling())
                                {
                                   if (Node.ELEMENT_NODE == child.getNodeType()) {
                                      if (ESBConstants.NS_CFG.equals (child.getNamespaceURI()))
                                      {
                                         String groupOrPropNodeTagName = child.getLocalName();
                                         if (ESBConstants.ELT_PROP_GROUP.equals (groupOrPropNodeTagName))
                                         {
                                        	 if (sLog.isLoggable(Level.FINEST)){
                                            sLog.finest("ValidationUtilities.getConfigExtensions()" +
                                                      ", found PropertyGroup element");
                                        	 }
                                            ConfigPropertyGroup group =
                                                createPropertyGroup(child);
                                            result.add(group);
                                         }
                                         else if (ESBConstants.ELT_PROP.equals(groupOrPropNodeTagName))
                                             {
                                        	     if (sLog.isLoggable(Level.FINEST)){
                                                 sLog.finest("ValidationUtilities.getConfigExtensions()" +
                                                           ", found ungrouped Property element");
                                        	     }
                                                 addProperty(child, defaultPropertyGroup);
                                             }
                                         else
                                             {
                                        	     if (sLog.isLoggable(Level.FINE)){
                                                 sLog.finest("ValidationUtilities.getConfigExtensions()" +
                                                           ", found unexpected \"" +
                                                           groupOrPropNodeTagName + "\" element");
                                        	     }
                                             }
                                      }
                                      else
                                          {
                                    	     if (sLog.isLoggable(Level.FINEST)){
                                              sLog.finest("ValidationUtilities.getConfigExtensions()" +
                                                        ", found unexpected \"" +
                                                        child.getNodeName() + "\" element");
                                    	     }
                                          }
                                   }
                                }
                            }
                    }
            }

        return result;
    }


    /**
     * Gets the ConfigExtensionJbiDocument attribute of the
     * ValidationUtilities class
     *
     * @return   The ConfigExtensionJbiDocument value
     */
    public static Document getConfigExtensionJbiDocument()
    {
        return getJbiDocument(IS_NS_AWARE);
    }

    /**
     * Gets the JbiDocument attribute of the ValidationUtilities class
     *
     * @return   The JbiDocument value
     */
    public static Document getJbiDocument()
    {
        return getJbiDocument(IS_NOT_NS_AWARE);
    }


    /**
     * determines the JBI Type for a given JBI metadata descriptor (jbi.xml)
     *
     * @param aJbiDescriptor  an XML document representing a valid jbi.xml
     *      file
     * @return                the JBI type <code> binding-component</code>,
     *      <code>service-assembly</code>, <code>service-engine</code>, or
     *      <code>shared-library</code>
     */
    public static String getJbiType(Document aJbiDescriptor)
    {
        String result = null;

        if (null != aJbiDescriptor)
            {
                if (tryXpathExpInDescriptor(XPATH_BC_NAME, XPATH_BC_DESCR, XPATH_BC_VERINFO, aJbiDescriptor))
                    {
                        result = JBIConstants.JBI_BINDING_COMPONENT_TYPE;
                    }
                else if (tryXpathExpInDescriptor(XPATH_SA_NAME, XPATH_SA_DESCR, XPATH_SA_VERINFO, aJbiDescriptor))
                    {
                        result = JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE;
                    }
                else if (tryXpathExpInDescriptor(XPATH_SE_NAME, XPATH_SE_DESCR, XPATH_SE_VERINFO, aJbiDescriptor))
                    {
                        result = JBIConstants.JBI_SERVICE_ENGINE_TYPE;
                    }
                else if (tryXpathExpInDescriptor(XPATH_SL_NAME, XPATH_SL_DESCR, XPATH_SL_VERINFO, aJbiDescriptor))
                    {
                        result = JBIConstants.JBI_SHARED_LIBRARY_TYPE;
                    }
                else
                    {
                	   if (sLog.isLoggable(Level.FINER)){
                        sLog.finer("ValidationUtilities.getJbiType() " +
                                  "cannot find an expected type in supplied descriptor");
                	   }

                        return "unknown jbi  type";
                        // not i18n
                    }
            }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("ValidationUtilities.getJbiType(), result=" + result);
        }
        return result;
    }

    
    
    /**
     * Get the client(browser) locale of jbi.xml
     * 
     * @return client locale of jbi.xml
     */
    private static String getLocaleJBIXMLName() {
        Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        
        String localeJbiXMLName = JBI_MANIFEST;  //default
        String bestFitXMLName = "META-INF/jbi" + "_" + language + "_" + country + ".xml";
        String secondBestXMLName = "META-INF/jbi" + "_" + language + ".xml";
                
        if (hasJBIXML(bestFitXMLName)) {
        	localeJbiXMLName = bestFitXMLName;
        } else if (hasJBIXML(secondBestXMLName)){
        	localeJbiXMLName = secondBestXMLName;
        }
                
    	return localeJbiXMLName;    	
    }
 
    
    /**
     *  find the xml file in the archive
     * 
     * @param xmlName file to look for
     * @return true if the xmlName is found
     */
    
    private static boolean hasJBIXML(String xmlName) {
    	boolean bool = false;
    	ArchiveBean archiveBean = BeanUtilities.getArchiveBean();
        String filePath = archiveBean.getArchiveAbsolutePath();
        ZipInputStream zipFile = null;
        
        if (filePath != null) {
        	
        	try {
            	zipFile = new ZipInputStream(new FileInputStream(filePath));
        	    if (zipFile != null) {
        		    ZipEntry zipEntry = zipFile.getNextEntry();
        		    String name = "";
        		
        		    if (null != zipEntry){
                       name = zipEntry.getName();
                    }
        		
        		    while (null != zipEntry && (!name.equals(xmlName))){
                        zipEntry = zipFile.getNextEntry();
                        if (zipEntry != null){
                            name = zipEntry.getName();
                        }
                    }
        		        		
        		    if (name.equals(xmlName)) {
        			    bool = true;
        			    
        			    if (sLog.isLoggable(Level.FINER)){
        			    sLog.finer("ValidationUtilities.hasJBIXML(), locale " + xmlName + " found");
        			    }
        		    }        		    
        	     }
        	    
        	} catch (Exception ex){        		
        	   //no further action to take, just bypass
            } finally {
                close(zipFile);
            }
        } 
        
    	return bool;    	
    }
    
    

    /**
     * Get the jbi.xml file from given jar/zip file if present.
     *
     * @return   result InputStream containing locale or default jbi.xml data, or empty stream
     *      if no jbi.xml
     */
    public static InputStream getMetaDataEntry()
    {
        ArchiveBean archiveBean = BeanUtilities.getArchiveBean();
        InputStream result =
            new ByteArrayInputStream(new byte[0]);

        int count = 0;
        String name = "";
        ZipEntry zipEntry;

        String filePath = archiveBean.getArchiveAbsolutePath();
        
        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("ValidationUtilities.getMetaDataEntry(), filePath=" + filePath);
        }
        ZipInputStream zipFile = null;

        if (null != filePath)
            {
                try
                    {
                        String localeXMLName = getLocaleJBIXMLName();
                        zipFile = new ZipInputStream(new FileInputStream(filePath));
                        
                        if (sLog.isLoggable(Level.FINER)){
                        sLog.finer("ValidationUtilities.getMetaDataEntry(), zipFile=" + zipFile);
                        }

                        if (null != zipFile)
                            {
                                zipEntry = zipFile.getNextEntry();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                sLog.finer("ValidationUtilities.getMetaDataEntry(), zipEntry=" + zipEntry);
                                }
                                
                                
                                if (null != zipEntry)
                                    {
                                        name = zipEntry.getName();
                                    }
                                while (null != zipEntry && (!name.equals(localeXMLName)))
                                    {
                                        ++count;
                                        zipEntry = zipFile.getNextEntry();
                                        if (zipEntry != null)
                                            {
                                                name = zipEntry.getName();
                                                
                                                if (sLog.isLoggable(Level.FINEST)){
                                                sLog.finest("ValidationUtilities.getMetaDataEntry(), name=" + name);
                                                }
                                            }
                                    }
                            }
                        //zipArchive has the locale or default jbi.xml
                        if (sLog.isLoggable(Level.FINER)){
                        sLog.finer("ValidationUtilities.getMetaDataEntry(), after loop, name=" + name);
                        }

                        if (name.equals(localeXMLName))
                            {
                                ByteArrayOutputStream baos =
                                    new ByteArrayOutputStream();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                sLog.finer("ValidationUtilities.getMetaDataEntry(), baos=" + baos);
                                }

                                byte[] buffer = new byte[BLOCK_SIZE];
                                int len = zipFile.read(buffer);
                                while (0 < len)
                                    {
                                        baos.write(buffer, 0, len);
                                        len = zipFile.read(buffer);
                                    }
                                result =
                                    new ByteArrayInputStream(baos.toByteArray());
                                
                                if (sLog.isLoggable(Level.FINER)){
                                sLog.finer("ValidationUtilities.getMetaDataEntry(), jbi.xml found");
                                }
                            }
                        else
                            {
                                archiveBean.setHasJbiXml(false);
                                
                                if (sLog.isLoggable(Level.FINER)){
                                sLog.finer("ValidationUtilities.getMetaDataEntry(), jbi.xml not found");
                                }
                            }
                    }
                catch (java.util.zip.ZipException zex)
                    {
                	    if (sLog.isLoggable(Level.FINE)){
                        sLog.fine("ValidationUtilities.getMetaDataEntry(), caught zex=" + zex);
                	    }
                        archiveBean.setZipFileReadError(true);
                    }
                catch (java.io.FileNotFoundException fnfex)
                    {
                	    if (sLog.isLoggable(Level.FINE)){
                	    sLog.fine("ValidationUtilities.getMetaDataEntry(), caught fnfex=" + fnfex);
                	    }
                        archiveBean.setFileReadError(true);
                    }
                catch (java.io.IOException ioex)
                    {
                	    if (sLog.isLoggable(Level.FINE)){
                        sLog.fine("ValidationUtilities.getMetaDataEntry(), caught ioex=" + ioex);
                	    }
                        archiveBean.setFileReadError(true);
                    }
                finally
                    {
                        close(zipFile);
                    }
            }
        else
            {
        	    if (sLog.isLoggable(Level.FINER)){        	
                sLog.finer("ValidationUtilities.getMetaDataEntry(), unable to read archive");
        	    }
                archiveBean.setFileReadError(true);
                // no file specified
            }
        
        if (sLog.isLoggable(Level.FINER)){
            sLog.finer("ValidationUtilities.getMetaDataEntry(), result=" + result);
        }
        
        return result;
    }


    /**
     * Check zip file entry.
     *
     * @return   true if the zipfile is empty or invalid
     */
    public static boolean isArchiveEmptyOrInValid()
    {
        ArchiveBean archiveBean = BeanUtilities.getArchiveBean();
        boolean result = false;
        // assume zip file has some entries

        String filePath = archiveBean.getArchiveAbsolutePath();
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("isArchiveEmptyOrInValid(), filePath=" + filePath);
        }

        ZipInputStream zipArchive = null;
        if (null != filePath)
            {
                try
                    {
                        zipArchive
                            = new ZipInputStream
                            (new FileInputStream(filePath));
                        boolean done = false;
                        boolean empty = true;
                        while (!done)
                            {
                                ZipEntry entry = zipArchive.getNextEntry();

                                if (null != entry)
                                    {
                                        empty = false;
                                        
                                        if (sLog.isLoggable(Level.FINEST)){
                                        sLog.finest("isArchiveEmptyOrInValid(), entry.getName()=" + 
                                                    entry.getName());
                                        }
                                    }
                                else
                                    {
                                        done = true;
                                    }
                            }
                        if (empty)
                            {
                                result = true;
                            }
                        
                        if (sLog.isLoggable(Level.FINER)){
                           sLog.finer("isArchiveEmptyOrInValid(), empty=" + empty);
                        }
                    }
                catch (java.util.zip.ZipException zex)
                    {
                        // not a valid archive

                        archiveBean.setZipFileReadError(true);
                        
                        
                        if (sLog.isLoggable(Level.FINE)){
                           sLog.fine("isArchiveEmptyOrInValid() bad archive, zex=" + zex);
                        }
                        result = true;
                    }
                catch (java.io.IOException ioex)
                    {
                        // not a valid file

                        archiveBean.setFileReadError(true);
                        
                        if (sLog.isLoggable(Level.FINE)){
                          sLog.fine("isArchiveEmptyOrInValid() bad file, ioex=" + ioex);
                        }
                        result = true;
                    }
                finally
                    {
                        close(zipArchive);
                    }
            }
        else
            {
                // no file specified

        	    if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("isArchiveEmptyOrInValid() no file");
        	    }
        	    
                archiveBean.setFileReadError(true);
                result = true;
            }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("isArchiveEmptyOrInValid(), result=" + result);
        }

        return result;
    }

    /**
     * Validate the jbi.xml file for well-formedness.
     *
     * @return   true iff the file passes validation
     */
    public static boolean isJbiXmlWellformed()
    {
        boolean result = false;
        try
            {
                DOMParser parser = new DOMParser();
                InputStream istr = getMetaDataEntry();
                InputSource inSrc = new InputSource(istr);
                parser.parse(inSrc);
                istr.close();
                result = true;
            }
        catch (org.xml.sax.SAXParseException spe)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.fine("isJbiXmlWellformed(), caught spe=" + spe);
        	    }
                result = false;
            }
        catch (Exception e)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.fine("isJbiXmlWellformed(), caught e=" + e);
        	    }
                result = false;
            }

        if (sLog.isLoggable(Level.FINER)){
          sLog.finer("isJbiXmlWellformed(), result=" + result);
        }

        return result;
    }

    /**
     * parse XML into a DOM document (optionally namespace aware)
     * @param anXmlInputSource a source of XML to be parsed
     * @param isNsAware set to <code>true</code> when the document to be
     * created must support namespace qualified elements
     * @return a DOM document representing the supplied XML.
     */
    public static Document parseXmlSource(InputSource anXmlInputSource,
                                          boolean isNsAware)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("ValidationUtilities.parseXmlSource(" +
                  anXmlInputSource + 
                  ", " + isNsAware + ")");
    	}
    	
        Document doc;
        try
            {
                DocumentBuilderFactory docBuilderFactory =
                    DocumentBuilderFactory.newInstance();
                docBuilderFactory.setNamespaceAware(isNsAware);
                docBuilderFactory.setValidating(false);
                docBuilderFactory.setExpandEntityReferences(false);
                DocumentBuilder docBuilder =
                    docBuilderFactory.newDocumentBuilder();
                doc = 
                    docBuilder.parse(anXmlInputSource);
            }
        catch (javax.xml.parsers.ParserConfigurationException pcEx)
            {        	
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.fine("getJbiDocument(), caught pcEx.getMessage()=" + pcEx.getMessage());
        	    }
                doc = null;
            }
        catch (org.xml.sax.SAXException saXex)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.fine("getJbiDocument(), caught saXex.getMessage()=" + saXex.getMessage());
        	    }
        	    
                doc = null;
            }
        catch (java.io.IOException ioEx)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.fine("getJbiDocument(), caught ioEx.getMessage()=" + ioEx.getMessage());
        	    }
                doc = null;
            }
        return doc;
    }

    /**
     * parses an XML string into a DOM Document (optionally namespace aware)
     * @param anXmlString to be parsed
     * @param isNsAware if the resulting Document needs to support
     * namespace qualified operations.
     * @return the document or null if the string cannot be parsed
     */
    public static Document parseXmlString(String anXmlString,
                                          boolean isNsAware)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("parseXmlString(" +
                  anXmlString + 
                  "," + isNsAware + ")");
    	}

        Document result = 
            null;

        StringReader xmlStringReader =
            new StringReader(anXmlString);
        
        InputSource inputSource =
            new InputSource(xmlStringReader);

        try
        {
            result = 
                parseXmlSource(inputSource,
                               isNsAware);
        }
        catch (Exception ex)
        {
        	if (sLog.isLoggable(Level.FINE)){
               sLog.log(Level.FINE,
                     ("ValidationUtils.parseXmlString()" +
                      ", caught ex=" +
                      ex),
                     ex);
        	}
        }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("ValidationUtilities.parseXmlString(), result=" + result);
        }

        return result;
    }

    /**
     * parses an XML string into a non-namespace-aware DOM Document 
     * @param anXmlString to be parsed
     * @return the document or null if the string cannot be parsed
     */
    public static Document parseXmlString(String anXmlString)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("parseXmlString(" +
                  anXmlString + ")");
    	}
       
        Document result =
            parseXmlString(anXmlString,
                           false);
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("parseXmlString(), result=" + result);
        }

        return result;
    }

    /**
     * Gets the JbiDocument attribute of the ValidationUtilities class
     *
     * @param isNsAware  Description of Parameter
     * @return           The JbiDocument value
     */
    private static Document getJbiDocument(boolean isNsAware)
    {
        Document doc;

        InputStream inputStream =
            getMetaDataEntry();
        
        InputSource inputSource =
            new InputSource(inputStream);

        doc =
            parseXmlSource(inputSource,
                           isNsAware);

        return doc;
    }

    /**
     * Gets the BooleanAttr attribute of the ValidationUtilities class
     *
     * @param aBooleanAttrName  Description of Parameter
     * @param aNode             Description of Parameter
     * @param aDefault          Description of Parameter
     * @return                  The BooleanAttr value
     */
    private static boolean getBooleanAttr(String aBooleanAttrName,
                                          Node aNode,
                                          boolean aDefault)
    {
        NamedNodeMap attrs =
            aNode.getAttributes();
        Node booleanAttrNode =
            attrs.getNamedItem(aBooleanAttrName);
        boolean result = aDefault;
        if (null != booleanAttrNode)
            {
                String booleanAttrValue =
                    booleanAttrNode.getNodeValue();
                if (Boolean.toString(true).equalsIgnoreCase(booleanAttrValue))
                    {
                        result = true;
                    }
            }
        return result;
    }

    /**
     * Parses the boolean attributes for a configuration property
     * and sets corresponding properties 
     * @param aPropertyNode  The XML node to be parsed
     * @param aProperty      The configuration property to update
     */
    private static void setBooleanAttrs(Node aPropertyNode,
                                        ConfigProperty aProperty)
    {
        NamedNodeMap attrs =
            aPropertyNode.getAttributes();
        if (null != attrs)
            {
                boolean isEncrypted =
                    getBooleanAttr(ESBConstants.ATTR_IS_VALUE_ENCRYPTED,
                                   aPropertyNode,
                                   false);
                aProperty.setIsEncrypted(isEncrypted);

                boolean isRequired =
                    getBooleanAttr(ESBConstants.ATTR_IS_VALUE_REQUIRED,
                                   aPropertyNode,
                                   false);
                aProperty.setIsRequired(isRequired);

                boolean isAppRestartRequired =
                    getBooleanAttr(ESBConstants
                                   .ATTR_IS_APP_RESTART_REQUIRED,
                                   aPropertyNode,
                                   false);
                aProperty.setIsAppRestartRequired(isAppRestartRequired);

                boolean isCompRestartRequired =
                    getBooleanAttr(ESBConstants
                                   .ATTR_IS_COMP_RESTART_REQUIRED,
                                   aPropertyNode,
                                   false);
                aProperty.setIsCompRestartRequired(isCompRestartRequired);

                boolean isServerRestartRequired =
                    getBooleanAttr(ESBConstants
                                   .ATTR_IS_SERVER_RESTART_REQUIRED,
                                   aPropertyNode,
                                   false);
                aProperty.setIsServerRestartRequired(isServerRestartRequired);
            }
    }

    /**
     * Parses the on change message attributes for a configuration property
     * and sets corresponding properties 
     * @param aPropertyNode  The XML node to be parsed
     * @param aProperty      The configuration property to update
     */
    private static void setOnChangeMessage(Node aPropertyNode,
                                           ConfigProperty aProperty)
    {
        NamedNodeMap attrs =
            aPropertyNode.getAttributes();
        if (null != attrs)
            {
                Node onChangeMessageAttrNode =
                    attrs
                    .getNamedItem(ESBConstants
                                  .ATTR_ON_CHANGE_MESSAGE);

                if (null != onChangeMessageAttrNode)
                    {
                        String value =
                            onChangeMessageAttrNode.getNodeValue();
                        if (null != value)
                            {
                                aProperty.setOnChangeMessage(value);
                            }
                    }
            }
    }

    /**
     * Sets the showDisplay attribute to <code>all</code>
     * <code>install</code> or <code>runtime</code>
     *
     * @param aPropertyNode  The property XML node to inspect
     * @param aProperty      The bean to be updated
     */
    private static void setShowDisplay(Node aPropertyNode,
                                       ConfigProperty aProperty)
    {
        String showDisplay =
            ESBConstants
            .ATTR_SHOW_DISPLAY_VALUE_ALL; // default (if not specified)

        NamedNodeMap attrs =
            aPropertyNode.getAttributes();
        if (null != attrs)
            {
                Node showDisplayAttrNode =
                    attrs
                    .getNamedItem(ESBConstants
                                  .ATTR_SHOW_DISPLAY);

                if (null != showDisplayAttrNode)
                    {
                        String value =
                            showDisplayAttrNode.getNodeValue();
                        // ensure showDisplay value is valid
                        if ((ESBConstants
                             .ATTR_SHOW_DISPLAY_VALUE_INSTALL
                             .equals(value))
                            ||((ESBConstants
                                .ATTR_SHOW_DISPLAY_VALUE_RUNTIME
                                .equals(value))))
                            
                            {
                                showDisplay = value;
                            }
                        // else use default if "all" (or if unrecognized)
                    }
                aProperty.setShowDisplay(showDisplay);
            }
    }

    /**
     * Adds a feature to the Property attribute of the ValidationUtilities
     * class
     *
     * @param aPropertyNode         The feature to be added to the Property
     *      attribute
     * @param aConfigPropertyGroup  The feature to be added to the Property
     *      attribute
     */
    private static void addProperty(Node aPropertyNode,
                                    ConfigPropertyGroup aConfigPropertyGroup)
    {
        ConfigProperty prop =
            new ConfigProperty();

        // parse property info

        // set name
        String nameValue =
            parsePropertyNode(aPropertyNode,
                              ESBConstants.ATTR_NAME,
                              "*name unknown*");
        prop.setName(nameValue);

        // set display name
        String displayNameValue =
            parsePropertyNode(aPropertyNode,
                              ESBConstants.ATTR_DISPLAY_NAME,
                              nameValue);
        prop.setDisplayName(displayNameValue);

        // set display description
        String displayDescriptionValue =
            parsePropertyNode(aPropertyNode,
                              ESBConstants.ATTR_DISPLAY_DESCRIPTION,
                              null);
        prop.setDisplayDescription(displayDescriptionValue);

        // set type
        String xsdType =
            parsePropertyNode(aPropertyNode,
                              ESBConstants.ATTR_TYPE,
                              XSD_DEFAULT_TYPE);
        // If the type is a supported type,
        // sets the supported type
        if ((ESBConstants.XSD_TYPE_BOOLEAN.equals(xsdType))
            || (ESBConstants.XSD_TYPE_INT.equals(xsdType))
            || (ESBConstants.XSD_TYPE_POSITIVE_INTEGER.equals(xsdType))
            || (ESBConstants.XSD_TYPE_STRING.equals(xsdType)))
            {
                prop.setXSDType(xsdType);
            }
        // If the type is a nonsupported type,
        // assumes the default (String) type
        else
            {
        	    if (sLog.isLoggable(Level.FINE)){
                    sLog.fine("addProperty() assuming default type for unsupported xsdType=" + xsdType);
        	    }
                prop.setXSDType(XSD_DEFAULT_TYPE);
            }

        // set flag for maxOccurs="unbounded"
        // which means that the property can repeat
        // and therefore is treated as a array/table
        // for viewing/editing purposes
        setArrayIndicator(aPropertyNode,
                          prop);

        // set default value
        String defaultValue =
            parsePropertyNode(aPropertyNode,
                              ESBConstants.ATTR_DEFAULT_VALUE,
                              null);
        // If the property is not an array
        // sets its default value
        if (!prop.getIsVariableArray())
            {
                prop.setValue(defaultValue);
            }
        // If the property is an array
        // adds the current default value to the list 
        else
            {
                // no-op
                // default-values-for-array is not supported
            }

        // set flags for: required field, encrypted field, 
        // server|component|application restart required
        setBooleanAttrs(aPropertyNode,
                        prop);

        // set on-change message if any
        setOnChangeMessage(aPropertyNode,
                           prop);

        // set showDisplay (install|runtime|all)
        setShowDisplay(aPropertyNode,
                       prop);

        // and type/range constraints
        addConstraints(aPropertyNode,
                       prop);

        // add to group
        List<ConfigProperty> props =
            aConfigPropertyGroup.getConfigProperties();
        if (null == props)
            {
                props =
                    new ArrayList<ConfigProperty>();
                aConfigPropertyGroup.setConfigProperties(props);
            }
        props.add(prop);
    }


    /**
     * Creates a default/general configuration property group to contain 
     * configuration properties that are not part of a named group
     * @param anElementType one of: <code>ApplicationConfiguration</code>,
     * or <code>Configuration</code>
     * @return the group to contain not-otherwise-grouped properties
     */
    private static ConfigPropertyGroup createDefaultPropertyGroup(String anElementType)
    {
        ConfigPropertyGroup result =
            new ConfigPropertyGroup();

        // Initialize default PropertyGroup
        result.setName("ungroupedProperties");

        String displayName =
            I18nUtilities.getResourceString("jbi.config.props.general.section.display.name");
        result.setDisplayName(displayName);

        String displayDescription;
        
        // for component install/runtime configuration, use general description
        if (!ESBConstants
            .ELT_APP_CFG
            .equals(anElementType))
            {
                displayDescription =
                    I18nUtilities.getResourceString("jbi.config.props.general.section.display.description");
            }
        // for component application configuration, use component application general description
        else
            {
                displayDescription =
                    I18nUtilities.getResourceString("jbi.edit.comp.app.config.props.pss.properties.label");
            }

        result.setDisplayDescription(displayDescription);

        // Children, if any, are added after the group is returned

        return result;
    }

    /**
     * Adds a feature to the Constraints attribute of the ValidationUtilities
     * class
     *
     * @param aPropertyNode  The feature to be added to the Constraints
     *      attribute
     * @param aProperty      The feature to be added to the Constraints
     *      attribute
     */
    private static void addConstraints(Node aPropertyNode,
                                       ConfigProperty aProperty)
    {
        NodeList propertyChildren =
            aPropertyNode.getChildNodes();

        if ((null != propertyChildren)
            && (0 < propertyChildren.getLength()))
            {
                for (int j = 0; j < propertyChildren.getLength(); ++j)
                    {
                        Node constraintOrOtherNode =
                            propertyChildren.item(j);

                        String nodePrefix = null;
                        String nodeName = null;
                        String constraintOrOtherNodeTagName = null;

                        if (null != constraintOrOtherNode)
                            {
                                short nodeType =
                                    constraintOrOtherNode.getNodeType();
                                // 1==element
                                if (Node.ELEMENT_NODE == nodeType)
                                    {
                                        // e.g. "config" (without ":")
                                        nodePrefix =
                                            constraintOrOtherNode.getPrefix();

                                        // e.g. "config:Constraint"
                                        nodeName =
                                            constraintOrOtherNode.getNodeName();

                                        if (null != nodePrefix)
                                            {
                                                // e.g. "Constraint"
                                                constraintOrOtherNodeTagName =
                                                    nodeName.substring((ESBConstants.NS_PREFIX_SEP.length() +
                                                                        nodePrefix.length()));
                                            }

                                        if (ESBConstants.ELT_CONSTRAINT.equals(constraintOrOtherNodeTagName))
                                            {
                                                NamedNodeMap attrs =
                                                    constraintOrOtherNode.getAttributes();
                                                if (null != attrs)
                                                    {
                                                        Node facetAttrNode =
                                                            attrs.getNamedItem(ESBConstants.ATTR_FACET);
                                                        String facetAttrValue = null;
                                                        if (null != facetAttrNode)
                                                            {
                                                                facetAttrValue =
                                                                    facetAttrNode.getNodeValue();

                                                            }
                                                        Node valueAttrNode =
                                                            attrs.getNamedItem(ESBConstants.ATTR_VALUE);
                                                        String valueAttrValue = null;
                                                        if (null != valueAttrNode)
                                                            {
                                                                valueAttrValue =
                                                                    valueAttrNode.getNodeValue();
                                                            }

                                                        // if we have a maxInclusive numeric value,
                                                        // then setConstraintMaxInclusive
                                                        if (
                                                            ESBConstants
                                                            .CONSTRAINT_FACET_ATTR_MAX_INCLUSIVE
                                                            .equals(facetAttrValue))
                                                            {
                                                                int max = Integer.MAX_VALUE;
                                                                try
                                                                    {
                                                                        max =
                                                                            Integer.parseInt(valueAttrValue);
                                                                    }
                                                                catch (Exception ex)
                                                                    {
                                                                	   if (sLog.isLoggable(Level.FINEST)){
                                                                          sLog.log(Level.FINE,
                                                                                 ("ValidationUtils.addConstraints()" +
                                                                                  ", maxInclusive caught ex=" +
                                                                                  ex),
                                                                                 ex);
                                                                	    }
                                                                    }
                                                                aProperty.setConstraintMaxInclusive(max);
                                                                
                                                                if (sLog.isLoggable(Level.FINEST)){
                                                                    sLog.finest("ValidationUtilities.addConstraints()" +
                                                                          ", maxInclusive=" + max);
                                                                }
                                                            }

                                                        // else if we have a minInclusive numeric value,
                                                        // then setConstraintMinInclusive
                                                        else if (
                                                                 ESBConstants
                                                                 .CONSTRAINT_FACET_ATTR_MIN_INCLUSIVE
                                                                 .equals(facetAttrValue))
                                                            {
                                                                int min = Integer.MIN_VALUE;
                                                                try
                                                                    {
                                                                        min =
                                                                            Integer.parseInt(valueAttrValue);
                                                                    }
                                                                catch (Exception ex)
                                                                    {
                                                                	   if (sLog.isLoggable(Level.FINE)){
                                                                           sLog.log(Level.FINE,
                                                                                 ("ValidationUtils.addConstraints()" +
                                                                                  ", minInclusive caughght ex=" +
                                                                                  ex),
                                                                                 ex);
                                                                	   }
                                                                    }
                                                                aProperty.setConstraintMinInclusive(min);
                                                                
                                                                if (sLog.isLoggable(Level.FINEST)){
                                                                   sLog.finest("ValidationUtilities.addConstraints()" +
                                                                          ", minInclusive=" + min);
                                                                }
                                                            }

                                                        // else if we have an enumeration value,
                                                        // add it to the list
                                                        else if (
                                                                 ESBConstants
                                                                 .CONSTRAINT_FACET_ATTR_ENUMERATION
                                                                 .equals(facetAttrValue))
                                                            {
                                                                // (if the list is empty, create it)
                                                                List<String> choices =
                                                                    aProperty.getConstraintEnumeration();
                                                                if (null == choices)
                                                                    {
                                                                        choices =
                                                                            new ArrayList<String>();

                                                                        aProperty.setConstraintEnumeration(choices);
                                                                    }
                                                                choices.add(valueAttrValue);
                                                                
                                                                if (sLog.isLoggable(Level.FINEST)){
                                                                   sLog.finest("ValidationUtilities.addConstraints()" +
                                                                          ", enumeration=" + valueAttrValue);
                                                                }

                                                            }

                                                        // else what?
                                                        else
                                                            {
                                                        	    if (sLog.isLoggable(Level.FINEST)){
                                                                   sLog.finest("ValidationUtilities.addConstraints()" +
                                                                          ", skipping unsupported facetAttrValue=" +
                                                                          facetAttrValue);
                                                        	    }
                                                            }
                                                    }
                                            }
                                    }
                            }
                    }
            }
    }

    /**
     * Checks for an maxOccurs="unbounded" attribute, which is used
     * to indicate that the property is to be viewed/edited as a
     * variable size list (editable subtable with add/remove)
     *
     * @param aPropertyNode the jbi.xml config:Property element
     * @param aProperty a bean used for displaying/editing the property
     */
    private static void setArrayIndicator(Node aPropertyNode,
                                          ConfigProperty aProperty)
    {
        String maxOccurs =
            parsePropertyNode(aPropertyNode,
                              ESBConstants.ATTR_MAX_OCCURS,
                              ESBConstants.ATTR_MAX_OCCURS_VALUE_DEFAULT);
        if (ESBConstants.ATTR_MAX_OCCURS_VALUE_UNBOUNDED.equals(maxOccurs))
            {
                aProperty.setIsVariableArray(true);
            }
    }

    /**
     * Description of the Method
     *
     * @param aPropertyGroupNode  Description of Parameter
     * @return                    Description of the Returned Value
     */
    private static ConfigPropertyGroup createPropertyGroup(Node aPropertyGroupNode)
    {
        ConfigPropertyGroup result =
            new ConfigPropertyGroup();

        // Initialize from PropertyGroup node attributes
        String groupName =
            parsePropertyNode(aPropertyGroupNode,
                              ESBConstants.ATTR_NAME,
                              "*group name unknown*");
        result.setName(groupName);

        String groupDisplayName =
            parsePropertyNode(aPropertyGroupNode,
                              ESBConstants.ATTR_DISPLAY_NAME,
                              groupName);
        result.setDisplayName(groupDisplayName);

        String groupDisplayDescription =
            parsePropertyNode(aPropertyGroupNode,
                              ESBConstants.ATTR_DISPLAY_DESCRIPTION,
                              null);
        result.setDisplayDescription(groupDisplayDescription);

        // add children configuration properties
        NodeList propertyElements =
            aPropertyGroupNode.getChildNodes();
        if ((null != propertyElements)
            && (0 < propertyElements.getLength()))
            {
                for (int j = 0; j < propertyElements.getLength(); ++j)
                {
                        Node propNode = propertyElements.item(j);
                        
                        if (Node.ELEMENT_NODE == propNode.getNodeType()) 
                        {
                            if (ESBConstants.NS_CFG.equals (propNode.getNamespaceURI()))
                            {
                               String propNodeTagName = propNode.getLocalName();
                               if (ESBConstants.ELT_PROP.equals(propNodeTagName))
                               {
                            	   if (sLog.isLoggable(Level.FINEST)){
                                       sLog.finest("ValidationUtilities.createPropertyGroup() found Property element");
                            	   }
                                    addProperty(propNode, result);
                               }
                               else
                               {
                            	   if (sLog.isLoggable(Level.FINEST)){
                                      sLog.finest("ValidationUtilities.createPropertyGroup() skipping unsupported \"" +
                                           propNodeTagName + "\" element");
                            	   }
                               }
                            }
                            else
                            {
                            	if (sLog.isLoggable(Level.FINEST)){
                                   sLog.finest("ValidationUtilities.createPropertyGroup()" +
                                          ", found unexpected \"" +
                                          propNode.getNodeName() + "\" element");
                            	}
                            }
                         }                                                
                 }
            }

        return result;
    }

    /**
     * determines if a given XPATH expression is found in a given JBI
     * descriptor.
     *
     * @param anXpathNameExpression  Description of Parameter
     * @param anXpathDescExpression  Description of Parameter
     * @param aJbiDescriptor         The jbi.xml DOM Document
     * @return                       true if the XPATH expression is found,
     *      else false
     */
    private static boolean tryXpathExpInDescriptor(String anXpathNameExpression,
                                                   String anXpathDescExpression,
                                                   String anXpthVerInfoExpression,
                                                   Document aJbiDescriptor)
    {
        boolean result = false;

        ArchiveBean archiveBean = BeanUtilities.getArchiveBean();
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("ValidationUtilities.tryXpathExpInDescriptor(" +
                  anXpathNameExpression + ", " +
                  anXpathDescExpression + "," + aJbiDescriptor + ")");
        }

        try
            {
                NodeList nodeList =
                    XPathAPI.selectNodeList(aJbiDescriptor,
                                            anXpathNameExpression);

                if (0 != nodeList.getLength())
                    {
                        NodeList descNodeList = XPathAPI.selectNodeList(aJbiDescriptor,
                                                                        anXpathDescExpression);
                        
                        NodeList verInfoNodeList =XPathAPI.selectNodeList(aJbiDescriptor,
                        		                                         anXpthVerInfoExpression);
                                                                        
                        if ((null != descNodeList.item(0))
                            && (null != descNodeList.item(0).getFirstChild()))
                            {
                                archiveBean.setDescription(descNodeList.item(0).getFirstChild().getNodeValue());
                            }
                        else
                            {
                                archiveBean.setDescription("");
                                // none provided (BT CR 6529558)
                            }
                                                
                        if ((null != verInfoNodeList.item(0)) 
                        	    && (null != verInfoNodeList.item(0).getAttributes())) {
                        	
                        	//take care of legacy attribute "specification-version" or has no VersionInfo attribute
                        	String componentVersion = null;
                        	String buildNumber = null;
                        	if ((verInfoNodeList.item(0).getAttributes()).getNamedItem(COMPONENT_VERSION) != null) {
                            	componentVersion = (verInfoNodeList.item(0).getAttributes()).getNamedItem(COMPONENT_VERSION).getNodeValue();
                            	buildNumber = (verInfoNodeList.item(0).getAttributes()).getNamedItem(BUILD_NUMBER).getNodeValue();                        		
                        	} else if ((verInfoNodeList.item(0).getAttributes()).getNamedItem(SPEC_VERSION) != null) {
                            	componentVersion = (verInfoNodeList.item(0).getAttributes()).getNamedItem(SPEC_VERSION).getNodeValue();
                            	buildNumber = (verInfoNodeList.item(0).getAttributes()).getNamedItem(BUILD_NUMBER).getNodeValue();                        		
                        	}
                        	
                        	if (componentVersion != null) {
                        	   archiveBean.setVersion(componentVersion);
                        	   archiveBean.setBuildNumber(buildNumber);
                        	   archiveBean.setShowVersionInfo(true);
                        	} else {
                        		archiveBean.setShowVersionInfo(false); 
                        	}
                        	
                        } else {
                        	archiveBean.setShowVersionInfo(false);                        	
                        }
                                                                        
                        if ((null != nodeList.item(0))
                            && (null != nodeList.item(0).getFirstChild()))
                            {
                                archiveBean.setJbiName(nodeList.item(0).getFirstChild().getNodeValue());
                                result = true;
                            }
                        else
                            {
                                // result = false; // required name missing
                            }
                    }
            }
        catch (Exception ex)
            {        	
        	    if (sLog.isLoggable(Level.FINE)){
                   sLog.log(Level.FINE,
                         ("ValidationUtilities.tryXpathExpInDescriptor(...)" +
                          ", while parsing jbi.xml, caught ex="),
                         ex);
        	    }
            }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("ValidationUtilities.tryXpathExpInDescriptor(...), result=" + result);
        }
        
        return result;
    }

    /**
     * Description of the Method
     *
     * @param is  Description of Parameter
     */
    private static void close(InputStream is)
    {
        if (is != null)
            {
                try
                    {
                        is.close();
                    }
                catch (java.io.IOException ioe)
                    {
                	    if (sLog.isLoggable(Level.FINE)){
                          sLog.fine("IO Exception error in closing the zip archive" + ioe.getMessage());
                	    }
                    }
            }
    }

    /**
     * Description of the Method
     *
     * @param aPropertyNode  Description of Parameter
     * @param anAttrName     Description of Parameter
     * @param aDefaultValue  Description of Parameter
     * @return               Description of the Returned Value
     */
    private static String parsePropertyNode(Node aPropertyNode,
                                            String anAttrName,
                                            String aDefaultValue)
    {
        NamedNodeMap attrs =
            aPropertyNode.getAttributes();
        String attrValue = aDefaultValue;
        if (null != attrs)
            {
                Node attrNode =
                    attrs.getNamedItem(anAttrName);
                if (null != attrNode)
                    {
                        attrValue = attrNode.getNodeValue();
                    }
            }
        return attrValue;
    }

    /*
     *  Expected location of jbi.xml descriptor file in jbi archive
     */
    private static final String JBI_MANIFEST = 
        "META-INF/jbi.xml"; // not i18n

    private static final int BLOCK_SIZE = 4096;
    private static final String IDENT_EXT_NS = "http://www.sun.com/jbi/descriptor/identification";
    private static final String IDENT_EXT_VERS_INFOG_ELT_NAME = "VersionInfo";
    private static final boolean IS_NS_AWARE = true;
    private static final boolean IS_NOT_NS_AWARE = false;
    private static final String LOGGING_EXT_LOGGER_ELT_NAME = "logger";
    private static final String LOGGING_EXT_LOGGING_ELT_NAME = "Logging";
    private static final String LOGGING_EXT_NS = "http://www.sun.com/jbi/descriptor/logging";
    private static final String SPEC_VERSION = "specification-version";
    private static final String COMPONENT_VERSION = "component-version";
    private static final String BUILD_NUMBER = "build-number";
    
    /**
     * XPATH expression to extract a binding-component name from a JBI Binding
     * Component archive's /META-INF/jbi.xml
     */
    private static final String XPATH_BC_NAME =
        "/jbi/component[@type='binding-component']/identification/name";

    /**
     * XPATH expression to extract a binding-component description from a JBI
     * Binding Component archive's /META-INF/jbi.xml
     */
    private static final String XPATH_BC_DESCR =
        "/jbi/component[@type='binding-component']/identification/description";

    /**
     * XPATH expression to extract a binding-component VersionInfo from a JBI
     * Binding Component archive's /META-INF/jbi.xml
     */
    private static final String XPATH_BC_VERINFO =
        "/jbi/component[@type='binding-component']/identification/VersionInfo";

    /**
     * XPATH expression to extract a service-assembly name from a JBI Service
     * Assembly archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SA_NAME =
        "/jbi/service-assembly/identification/name";

    /**
     * XPATH expression to extract a service-assembly description from a JBI
     * Service Assembly archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SA_DESCR =
        "/jbi/service-assembly/identification/description";

    /**
     * XPATH expression to extract a service-assembly VersionInfo from a JBI
     * Service Assembly archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SA_VERINFO =
        "/jbi/service-assembly/identification/VersionInfo";

    /**
     * XPATH expression to extract a service-engine name from a JBI Service
     * Engine archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SE_NAME =
        "/jbi/component[@type='service-engine']/identification/name";

    /**
     * XPATH expression to extract a service-engine description from a JBI
     * Service Engine archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SE_DESCR =
        "/jbi/component[@type='service-engine']/identification/description";

    /**
     * XPATH expression to extract a service-engine VersionInfo from a JBI
     * Service Engine archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SE_VERINFO =
        "/jbi/component[@type='service-engine']/identification/VersionInfo";

    /**
     * XPATH expression to extract a shared-library name from a JBI Shared
     * Library archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SL_NAME =
        "/jbi/shared-library/identification/name";

    /**
     * XPATH expression to extract a shared-library description from a JBI
     * Shared Library archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SL_DESCR =
        "/jbi/shared-library/identification/description";

    /**
     * XPATH expression to extract a shared-library VersionInfo from a JBI
     * Shared Library archive's /META-INF/jbi.xml
     */
    private static final String XPATH_SL_VERINFO =
        "/jbi/shared-library/identification/VersionInfo";
            
    private static final String XSD_DEFAULT_TYPE = 
        ESBConstants.XSD_TYPE_STRING; // xsd:string

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();

    /**
     * prevents instantiation and subclassing
     */
    private ValidationUtilities()
    {
    }

}
