/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  CompConfigUtils.java
 */
package com.sun.jbi.jsf.util;

import com.sun.jbi.jsf.bean.CompConfigBean;
import com.sun.jbi.jsf.bean.ConfigPropertyGroup;
import com.sun.jbi.jsf.bean.Row;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.webui.jsf.component.PropertySheet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;

/**
 * Component Configuration Utilities.
 *
 * @author   Sun Microsystems Inc.
 */

public final class CompConfigUtils
{

    /**
     * Gets the Configuration attribute of the CompConfigUtils class
     *
     * @param aCompName   Description of Parameter
     * @param anInstance  Description of Parameter
     * @return            The Configuration value
     */
    public static Properties getConfiguration(String aCompName,
                                              String anInstance)
    {
        Properties result = null;
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("CompConfigUtils.getConfiguration(" + aCompName +
                  ", " + anInstance + ")");
        }

        try
            {
                result =
                    sJac.getComponentConfiguration(aCompName,
                                                  anInstance);
                if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("CompConfigUtils.getConfiguration(...), result=" +
                          result);
                }

            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	if (sLog.isLoggable(Level.FINE)){
                sLog.log(Level.FINE,
                         ("CompConfigUtils.getConfiguration(...) caught jrEx=" +
                          jrEx),
                         jrEx);
        	}
            }
        return result;
    }

    /**
     * Gets the ConfigurationData attribute of the CompConfigUtils class
     *
     * @param aCompName   Description of Parameter
     * @param anInstance  Description of Parameter
     * @return            The ConfigurationData value
     */
    public static String getConfigurationData(String aCompName,
                                              String anInstance)
    {
        String result = null;
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("CompConfigUtils.getConfigurationData(" + aCompName +
                  ", " + anInstance + ")");
        }

        try
            {
                result =
                    sJac.retrieveConfigurationDisplayData(aCompName,
                                                         anInstance);

                if (sLog.isLoggable(Level.FINER)){
                    sLog.finer("CompConfigUtils.getConfigurationData(...), result=" +
                          result);
                }

            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	if (sLog.isLoggable(Level.FINE)){
                sLog.log(Level.FINE,
                         ("CompConfigUtils.getConfigurationData(...) caught jrEx=" +
                          jrEx),
                         jrEx);
        	}
            }
        return result;
    }

    /**
     * Gets the ConfigurationSchema attribute of the CompConfigUtils class
     *
     * @param aCompName   Description of Parameter
     * @param anInstance  Description of Parameter
     * @return            The ConfigurationSchema value
     */
    public static String getConfigurationSchema(String aCompName,
                                                String anInstance)
    {
        String result = null;
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("CompConfigUtils.getConfigurationSchema(" + aCompName +
                  ", " + anInstance + ")");
        }

        try
            {
                result =
                    sJac.retrieveConfigurationDisplaySchema(aCompName,
                                                           anInstance);
                if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("CompConfigUtils.getConfigurationSchema(...), result=" +
                          result);
                }

            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	if (sLog.isLoggable(Level.FINE)){
                sLog.log(Level.FINE,
                         ("CompConfigUtils.getConfigurationSchema(...) caught jrEx=" +
                          jrEx),
                         jrEx);
        	}
            }
        return result;
    }

     /**
      * Determines if the requested type of configuration is supported by the
      * specified component on the specified instance.
      *
      * @param aConfigType   a type to be checked, one of: <code>application</code> or
      * <code>runtime</code>
      * @param aCompName     the JBI component name
      * @param aCompType     the JBI component type, one of: <code>binding-component</code> 
      * or <code>service-engine</code>
      * @param anInstanceName the instance name (or target; will be converted to target)
      * @return              true if the specified component (on the target implied by the
      * specified instance) supports the requested configuration type 
      */
     public static boolean isConfigTypeSupported(String aConfigType,
                                                 String aCompName,
                                                 String aCompType,
                                                 String anInstanceName)
     {
         boolean result = false;
         if (JBILogger.isLoggableFiner())
             {
                 Object[] args =
                     {
                         aConfigType,
                         aCompName,
                         aCompType,
                         anInstanceName,
                     };
                 JBILogger.entering(CN, MN_IS_CONFIG_TYPE_SUPPORTED, args);
             }
         try
             {
                 String target =
                     ClusterUtilities
                     .getInstanceDomainCluster(anInstanceName);

                 if (ESBConstants.CONFIG_TYPE_COMP_APP.equals(aConfigType))
                     {
                         result =
                             sJac.isAppConfigSupported(aCompName,
                                                       target);
                     }
                 else if (ESBConstants.CONFIG_TYPE_COMP_RUNTIME.equals(aConfigType))
                     {
                         result =
                             sJac.isComponentConfigSupported(aCompName,
                                                             target);
                     }
                 else 
                     {
                	 if (sLog.isLoggable(Level.FINER)){
                         sLog.finer(CN + "." + MN_IS_CONFIG_TYPE_SUPPORTED + 
                                   "(), unsupported aConfigType=" + aConfigType);
                	 }
                     }
             }
         catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
             {
        	 if (sLog.isLoggable(Level.FINE)){
                 sLog.log(Level.FINE,
                          (CN + "." + MN_IS_CONFIG_TYPE_SUPPORTED + "(), caught jrEx=" +
                           jrEx),
                          jrEx);
        	 }
             }

         if (JBILogger.isLoggableFiner())
             {
                 JBILogger.exiting(CN, MN_IS_CONFIG_TYPE_SUPPORTED, result);
             }
         return result;
     }

     /**
      * Sets the requested configuration type property sheet on the component
      * configuration bean, if possible.
      *
      * @param aConfigType   The new ConfigPS value
      * @param aCompName     the component
      * @param aCompType     one of: <code>binding-component</code> or <code>service-engine</code>
      * @param anInstanceName the instance name (or <code>null</code> for configuration type of
      *      <code>installation</code>
      * @return              true if the specified component (on the specified
      *      instance) returns valid data that can be used to initialize the
      *      property sheet.
      */
     public static boolean setConfigPS(String aConfigType,
                                       String aCompName,
                                       String aCompType,
                                       String anInstanceName)
     {
         boolean result = false;
         if (JBILogger.isLoggableFiner())
             {
                 Object[] args =
                     {
                         aConfigType,
                         aCompName,
                         aCompType,
                         anInstanceName,
                     };
                 JBILogger.entering(CN, MN_SET_CONFIG_PS, args);
             }

         if (ESBConstants.CONFIG_TYPE_COMP_APP.equals(aConfigType))
             {
                 PropertySheet
                     compAppConfigPS;

                 List<ConfigPropertyGroup> configGroups =
                     getMetadataConfigExtensions(aCompName,
                                                 ESBConstants
                                                 .ELT_APP_CFG);

                 String appConfigName =
                     BeanUtilities
                     .getStringPropertyUsingExpression("#{jbiAppConfigName}");


                 // If this component supports application configuration,
                 // creates a property sheet for a new or exiting app config
                 if (null != configGroups)
                     {
                         compAppConfigPS =
                             JSFUtils.getCompAppCompConfigPS(anInstanceName,
                                                             aCompName,
                                                             aCompType,
                                                             appConfigName,
                                                             configGroups);
                         result = true;
                     }
                 // If this component does not support application configuration
                 // create a "No Configuration Available" placeholder property sheet
                 else
                     {
                         compAppConfigPS =
                             JSFUtils.getNoConfigPS(aConfigType,
                                                    aCompName,
                                                    aCompType);
                     }
                 CompConfigBean compConfigBean =
                     BeanUtilities.getCompConfigBean();
                compConfigBean.setCompRuntimeAppConfigPS(compAppConfigPS);

            }
        else if (ESBConstants.CONFIG_TYPE_COMP_INSTALL.equals(aConfigType))
            {
                Document jbiDocument =
                    ValidationUtilities.getConfigExtensionJbiDocument();

                // this code gets called only after successful
                // archive validation, so there will be
                // a jbi.xml document

                List<ConfigPropertyGroup> configGroups =
                    ValidationUtilities.getConfigExtensions(jbiDocument,
                                                            ESBConstants
                                                            .ELT_CFG);

                PropertySheet installConfigPS = 
                    null;
                
                // if there are config groups,
                // creates a (possibly empty) property sheet
                if (null != configGroups)
                    {
                        installConfigPS =
                            JSFUtils.getCompInstallationConfigPS(aCompName,
                                                                 aCompType,
                                                                 configGroups);
                    }
                
                // if no property sheet was created,
                // or one was created but it is empty,
                // creates a placeholder property sheet
                if ((null == installConfigPS)
                    ||(0 == installConfigPS.getSectionCount()))
                    {
                        installConfigPS =
                            JSFUtils.getNoConfigPS(aConfigType,
                                                   aCompName,
                                                   aCompType);
                    }

                CompConfigBean compConfigBean =
                    BeanUtilities.getCompConfigBean();
                compConfigBean.setCompInstallationConfigPS(installConfigPS);
            }
        else if (ESBConstants.CONFIG_TYPE_COMP_RUNTIME.equals(aConfigType))
            {
                PropertySheet
                    runtimeConfigPS;

                String jbiXmlString =
                    JBIUtils.getCompMetadata(aCompName);

                Document jbiDocument =
                    ValidationUtilities.parseXmlString(jbiXmlString,
                                                       IS_NS_AWARE);
                
                List<ConfigPropertyGroup> configGroups =
                    ValidationUtilities.getConfigExtensions(jbiDocument,
                                                            ESBConstants
                                                            .ELT_CFG);

                String target =
                    ClusterUtilities
                    .getInstanceDomainCluster(anInstanceName);

                Properties configProps =
                    JBIUtils.getCompConfigProps(aCompName,
                                                target);

                if (null != configGroups)
                    {
                        runtimeConfigPS =
                            JSFUtils.getCompRuntimeCompConfigPS(anInstanceName,
                                                                aCompName,
                                                                aCompType,
                                                                configGroups);
                        result = true;
                    }
                else
                    {
                        runtimeConfigPS =
                            JSFUtils.getNoConfigPS(aConfigType,
                                                   aCompName,
                                                   aCompType);
                    }
                CompConfigBean compConfigBean =
                    BeanUtilities.getCompConfigBean();
                compConfigBean.setCompRuntimeCompConfigPS(runtimeConfigPS);
            }
        else
            {
            }

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN, MN_SET_CONFIG_PS, result);
            }
        return result;
    }


     /**
      * Sets the installation configuration property sheet to be used with the component
      * installation configuration bean, during manage targets use-cases, if possible.
      *
      * @param aCompName     the component
      * @param aCompType     one of: <code>binding-component</code> or <code>service-engine</code>
      */     
     public static void setManageTargetConfigPS(String aCompName, String aCompType) {    	 
         List<ConfigPropertyGroup> configGroups =
             getMetadataConfigExtensions(aCompName, ESBConstants.ELT_CFG);

         PropertySheet installConfigPS = null;
         
         // if there are config groups,
         // creates a (possibly empty) property sheet
         if (null != configGroups)
             {
                 installConfigPS =
                     JSFUtils.getCompInstallationConfigPS(aCompName,
                                                          aCompType,
                                                          configGroups);
             }
         
         // if no property sheet was created,
         // or one was created but it is empty,
         // creates a placeholder property sheet
         if ((null == installConfigPS)
             ||(0 == installConfigPS.getSectionCount()))
             {
                 installConfigPS =
                     JSFUtils.getNoConfigPS(ESBConstants.CONFIG_TYPE_COMP_INSTALL,
                                            aCompName,
                                            aCompType);
             }

         CompConfigBean compConfigBean =
             BeanUtilities.getCompConfigBean();
         compConfigBean.setCompInstallationConfigPS(installConfigPS);         
     }
     
     
    /**
     * Gets a (possibly empty) list of Configuration Property groups for a component
     * @param aCompName JBI component that may or may not support Configuration extensions
     * @param anElementType one of: <code>ApplicationConfiguration</code>, 
     * <code>ApplicationVariable</code>, or <code>Configuration</code>
     * @return list of property groups found in the JBI metadata (jbi.xml) for the
     * specified component, or null if the component's metadata doesn't have any
     * coniguration extension.
     */
    public static List<ConfigPropertyGroup> getMetadataConfigExtensions(String aCompName,
                                                                        String anElementType)
    {
        String jbiXmlString =
            JBIUtils.getCompMetadata(aCompName);
        
        Document jbiDocument =
            ValidationUtilities.parseXmlString(jbiXmlString,
                                               IS_NS_AWARE);
        
        List<ConfigPropertyGroup> configGroups =
            ValidationUtilities.getConfigExtensions(jbiDocument,
                                                    anElementType);
        return configGroups;
    }

    /**
     * prevents instantiation and subclassing
     */
    private CompConfigUtils()
    {
    }

    private static final boolean IS_NS_AWARE = true;

    private static final String CN = CompConfigUtils.class.getName();
    private static final String MN_GET_COMP_STATE = "getCompState";
    private static final String MN_IS_CONFIG_TYPE_SUPPORTED = "isConfigTypeSupported";
    private static final String MN_SET_CONFIG_PS = "setConfigPS";

    private static JBIAdminCommands sJac;

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();

    static
    {
        sJac = BeanUtilities.getClient();
    }

}

