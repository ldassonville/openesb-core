/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.jbi.jsf.handlers;

import com.sun.jbi.jsf.bean.AlertBean;
import com.sun.jbi.jsf.bean.RuntimeStatsBean;
import com.sun.jbi.jsf.bean.ServiceUnitBean;
import com.sun.jbi.jsf.bean.ShowBean;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.ClusterUtilities;
import com.sun.jbi.jsf.util.I18nUtilities;
import com.sun.jbi.jsf.util.JBIConstants;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JBIUtils;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jsftemplating.annotation.Handler;
import com.sun.jsftemplating.annotation.HandlerInput;
import com.sun.jsftemplating.annotation.HandlerOutput;
import com.sun.jsftemplating.layout.descriptors.handler.HandlerContext;
import com.sun.webui.jsf.model.Option;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sun.enterprise.tools.admingui.util.GuiUtil;

/**
 * Provides jsftemplating handlers for Statistics/Monitorung use-cases
 */
public final class StatsHandlers
{
    private static Logger sLog = JBILogger.getInstance();

    /**
     * <p> This method sets the component statistics for the specified component
     * on the specified instance into the ShowBean.
     * <p> Input  value: "compName" -- Type: <code> java.lang.String</code> Name of the component
     * <p> Input  value: "compType" -- Type: <code> java.lang.String</code> one of:
     * <code>binding-component</code> or <code>service-engine<code>
     * <p> Input value: "instanceName" -- Type: <code>java.lang.String</code></p>
     * <p> Output value: "alertDetails" -- Type: <code>java.lang.String</code>alert Details, 
     * or empty String</p>
     * <p> Output value: "alertSummary" -- Type: <code>java.lang.String</code>alert Summary, 
     * or empty String</p>
     * <p> Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>true if
     * an alert is needed</p>
     * <p>Output value: "showStartButton" -- Type: <code>java.lang.Boolean</code> show start 
     * button indicator</p>
     * @param handlerCtx The HandlerContext.
     */
    @Handler (id="jbiSetCompStatsForInstance",
              input={
                  @HandlerInput (name="compName", type=String.class, required=true),
                  @HandlerInput (name="compType", type=String.class, required=true),
                  @HandlerInput (name="instanceName",  type=String.class, required=true)},
              output={
                  @HandlerOutput(name="alertDetails", type=String.class) ,
                  @HandlerOutput(name="alertSummary", type=String.class) ,
                  @HandlerOutput(name="isAlertNeeded", type=Boolean.class),
                  @HandlerOutput(name="showStartButton", type=Boolean.class)
              })
    
        public static void  jbiSetCompStatsForInstance(HandlerContext handlerCtx)
    {
        String compName = (String)handlerCtx.getInputValue ("compName");
        String compType = (String)handlerCtx.getInputValue ("compType");
        String instanceName = (String)handlerCtx.getInputValue ("instanceName");
        
        if (sLog.isLoggable(Level.FINER)) {        
           sLog.finer("StatsHandlers.jbiSetCompStatsForInstance(...), compName=" +
                  compName + ", compType=" + compType + ", instanceName=" + instanceName);
        }

        AlertBean alertBean = BeanUtilities.getAlertBean();
        String alertDetails =
            AlertBean.ALERT_DETAIL_NONE;
        String alertSummary =
            AlertBean.ALERT_SUMMARY_NONE;
        String currSelectedInstance = null;
        List<String> instanceNamesList = null;
        String instanceState = null;
        boolean isAlertNeeded = false;
        boolean isAlertSeeLogEpilogSkipped = false;
        boolean isCompStartedOnInstance = false;
        boolean showStartButton = false;
        boolean isInstalledOnZeroTargets = false;
        boolean isInstanceRunning = false;
        String prevSelectedInstance = instanceName;

        Option[] instancesList =
            JBIUtils.getInstancesAsOptionArray(compName,
                                               compType);        
        ShowBean showBean =
            BeanUtilities.getShowBean();
        showBean.setCompStatsProps(compName,
                                   instanceName);

        // If  there is a pending alert,
        // shows the pending alert summary and details, if any 
        // (If there is an alert pending but missing a summary or details,
        // provides internal error defaults)
        if (AlertBean.isSessionAlertNeeded())
            {
                isAlertNeeded = true;
                alertSummary = 
                    AlertBean
                    .getSessionAlertSummary(I18nUtilities
                                            .getResourceString("jbi.comp.stats.internal.error"));
                alertDetails = 
                    AlertBean
                    .getSessionAlertDetail(I18nUtilities
                                           .getResourceString("jbi.comp.stats.internal.error.details.unknown"));
                isAlertSeeLogEpilogSkipped = true;
            }
        
        // If no alert needed so far, 
        // determines the currently selected instance, if possible
        if (!isAlertNeeded)
            {
                instanceNamesList = 
                    JBIUtils.getInstancesAsStringList(compName,
                                                      compType);
                
                // If there are no instances to choose from
                // indicates that the component has no targets
                if (0 == instanceNamesList.size())
                    {
                        // If running in a cluster-profile domain
                        // indicates taht there are no targets
                        if (IS_CLUSTER_PROFILE)
                            {
                                isInstalledOnZeroTargets = true;
                            }
                        // If running in a developer-profile domain
                        // there must be a target ("server")
                    }
            
                // If the component has targets
                // determines the currently selected instance and its state
                if (!isInstalledOnZeroTargets)
                    {
                        // 1. (uses the prevSelectedInstance if valid, regardless of component 
                        // state; if prevSelectedInstance is null or invalid, selects "server" 
                        // (if valid) or the "first" instance or (if none), the previous
                        // (even if invalid)
                        currSelectedInstance =
                            JBIUtils.selectCurrentInstance(instanceNamesList,

                                                           prevSelectedInstance);

                        // 2. determines if the instance is running
                        // Note: since this code only runs on "server"
                        // if that is the current instance it must be running
                        instanceState =
                            ClusterUtilities
                            .getInstanceState(currSelectedInstance);
                        if ((JBIAdminCommands
                             .SERVER_TARGET_KEY
                             .equals(currSelectedInstance))
                            ||(SharedConstants
                               .STATE_RUNNING
                               .equals(instanceState)))
                            {
                                isInstanceRunning = true;
                            }
                    }
                // If the component has no targets
                // shows a "Component Has No Targets" alert
                else
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.stats.cannot.show.stats");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.stats.comp.has.no.targets");
                        isAlertSeeLogEpilogSkipped = true;
                    }
            }

        // If no alert needed so far,
        // determines if the component is started
        if (!isAlertNeeded)
            {
                // If the currently selected instance is running,
                // gets the state of the component on the currently selected instance
                if (isInstanceRunning)
                    {
                        Properties statusProps = 
                            new Properties();
                        
                        isCompStartedOnInstance =
                            JBIUtils.isCompStartedOnInstance(compName,
                                                             compType,
                                                             currSelectedInstance,
                                                             statusProps);
                    }
                // If the instance is not running,
                // shows an "Instance Not Running" alert
                else
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.stats.cannot.show.stats");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.stats.instance.not.running");
                        isAlertSeeLogEpilogSkipped = true;
                    }
            }

        if (!isAlertNeeded)
            {

                // If the component is not started,
                // shows a "Component Not Started" alert
                if (!isCompStartedOnInstance)
                    {
                	    showStartButton = true;
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.stats.cannot.show.stats");
                        String compNotStartedKey;
                        if (ClusterUtilities.isClusterProfile())
                            {
                                compNotStartedKey =
                                    "jbi.comp.stats.comp.not.started";
                            }
                        else
                            {
                                compNotStartedKey =
                                    "jbi.comp.stats.comp.not.started.pe";
                            }
                        alertDetails =
                            I18nUtilities
                            .getResourceString(compNotStartedKey);
                        isAlertSeeLogEpilogSkipped = true;
                    }
            }
        else
            {
            }

        if (!isAlertSeeLogEpilogSkipped)
            {
                alertDetails = 
                    BeanUtilities
                    .addAlertFooterMessage(alertDetails);
            }

        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("showStartButton", showStartButton);
        
        if (sLog.isLoggable(Level.FINE)) {        
        sLog.fine("StatsHandlers.jbiSetCompStatsForInstance(...)" +
                  ", instancesList=" + instancesList +
                  ", currSelectedInstance=" + currSelectedInstance +
                  ", isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails +
                  ", showStartButton" + showStartButton +
                  "");
        }

    }

    /**
     *  <p> This method sets the runtime monitoring framework statistics
     *  <p> Input value: "instanceName" -- Type: <code>java.lang.String</code></p>
     *  @param  handlerCtx The HandlerContext.
     */
    @Handler (id="jbiSetFrameworkStatsForInstance",
              input={
                  @HandlerInput (name="instanceName",  type=String.class, required=true)})

        public static void  jbiSetFrameworkStatsForInstance(HandlerContext handlerCtx)
    {
        String instanceName = (String)handlerCtx.getInputValue ("instanceName");
        
        if (sLog.isLoggable(Level.FINE)) {        
        sLog.fine("StatsHandlers.jbiSetFrameworkStatsForInstance(...), instanceName=" + 
                  instanceName);
        }
        
        RuntimeStatsBean runtimeStatsBean = BeanUtilities.getRuntimeStatsBean();
        runtimeStatsBean.setInstanceName(instanceName);
    }



    /**
     *  <p> This method sets the runtime monitoring NMR statistics
     *  <p> Input value: "instanceName" -- Type: <code>java.lang.String</code></p>
     *  @param  handlerCtx The HandlerContext.
     */
    @Handler (id="jbiSetNMRStatsForInstance",
              input={
                  @HandlerInput (name="instanceName",  type=String.class, required=true)})

        public static void  jbiSetNMRStatsForInstance(HandlerContext handlerCtx)
    {
        String instanceName = (String)handlerCtx.getInputValue ("instanceName");
        
        if (sLog.isLoggable(Level.FINER)) {        
          sLog.finer("StatsHandlers.jbiSetNMRStatsForInstance(...), instanceName=" + 
                  instanceName);
        }
        RuntimeStatsBean runtimeStatsBean = BeanUtilities.getRuntimeStatsBean();
        runtimeStatsBean.setInstanceName(instanceName);
    }


    /**
     * <p> This method sets the statistics for the specified Service Assembly
     * on the specified instance into the ShowBean.
     * <p> Input  value: "saName" -- Type: <code> java.lang.String</code> JBI
     * Service Assembly name
     * <p> Input value: "instanceName" -- Type: <code>java.lang.String</code></p>
     * @param  handlerCtx The HandlerContext.
     */
    @Handler (id="jbiSetSaStatsForInstance",
              input={
                  @HandlerInput (name="saName", type=String.class, required=true),
                  @HandlerInput (name="prevSelectedInstance", type=String.class, required=true)
              },
              output={
                  @HandlerOutput(name="instancesList", type=Option[].class),
                  @HandlerOutput(name="isInstanceDropDownEnabled", type=Boolean.class),
                  @HandlerOutput(name="currSelectedInstance", type=String.class),
                  @HandlerOutput(name="isAlertNeeded", type=Boolean.class),
                  @HandlerOutput(name="alertSummary", type=String.class),
                  @HandlerOutput(name="alertDetails", type=String.class),
                  @HandlerOutput(name="installedOnZeroTargets", type=Boolean.class)                  
              })

        public static void  jbiSetSaStatsForInstance(HandlerContext handlerCtx)
    {
        String saName = (String)handlerCtx.getInputValue ("saName");
        String prevSelectedInstance = (String) handlerCtx.getInputValue("prevSelectedInstance");

        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("StatsHandlers.jbiSetSaStatsForInstance(...)" +
                  ", saName=" + saName + ", prevSelectedInstance=" + prevSelectedInstance);
        }
 
        AlertBean alertBean = BeanUtilities.getAlertBean();        
        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE; 
        String currSelectedInstance = prevSelectedInstance;
        boolean isAlertNeeded = false;
        boolean isInstanceDropDownEnabled = false;
        boolean installedOnZeroTargets = true;
        
        ShowBean showBean = BeanUtilities.getShowBean();
        showBean.setSaStatsProps(saName, prevSelectedInstance);
        
        Option[] instancesList =
            JBIUtils.getInstancesAsOptionArray(saName, JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE);
        List<String> instanceNamesList = 
            JBIUtils.getInstancesAsStringList(saName, JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE);

        if ( instancesList != null && instancesList.length > 0) {
        	installedOnZeroTargets = false;
        	        
	        // determine the currently selected instance
	        // using either the previously selected instance if it is in the list,
	        // or the default "server" target if it is in the list
	        // or the first instance in the list if it is non empty
	        // or null if the list is empty
	        currSelectedInstance =
	            JBIUtils.selectCurrentInstance(instanceNamesList, prevSelectedInstance);
	        	        
            // determines if the instance is running
            String instanceState = ClusterUtilities.getInstanceState(currSelectedInstance);
            if (SharedConstants.STATE_RUNNING.equals(instanceState)) {
                showBean.setSaStatsProps(saName, currSelectedInstance);
            } else {            	
                isAlertNeeded = true;
                alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                alertSummary = I18nUtilities.getResourceString("jbi.sa.stats.cannot.show.stats");
                alertDetails = I18nUtilities.getResourceString("jbi.sa.stats.instance.not.running");
            }
	        
        } else {        	
            isAlertNeeded = true;
            alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
            alertSummary = I18nUtilities.getResourceString("jbi.sa.not.installed.to.target.summary");
            Object[] args = {saName};
            alertDetails = 
                GuiUtil.getMessage(I18nUtilities.getResourceString("jbi.sa.not.installed.to.target.details"), args);        	
        }
        
         
        
        handlerCtx.setOutputValue("instancesList", instancesList);
        handlerCtx.setOutputValue("isInstanceDropDownEnabled", isInstanceDropDownEnabled);
        handlerCtx.setOutputValue("currSelectedInstance", currSelectedInstance);
        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("installedOnZeroTargets", installedOnZeroTargets);
        
        if (sLog.isLoggable(Level.FINE)) {        
        sLog.fine("StatsHandlers.jbiSetSaStatsForInstance(...)" +
                  ", instancesList=" + instancesList +
                  ", isInstanceDropDownEnabled=" + isInstanceDropDownEnabled +
                  ", currSelectedInstance=" + currSelectedInstance +
                  ", isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails +
                  ", installedOnZeroTargets=" + installedOnZeroTargets +
                  "");
        }

    }
    /**
     * <p> This method sets the statistics for the specified Service Unit
     * on the specified instance into the ServiceUnitBean.
     * <p> Input  value: "saName" -- Type: <code> java.lang.String</code> JBI
     * Service Assembly name
     * <p> Input  value: "suName" -- Type: <code> java.lang.String</code> JBI
     * Service Unit name
     * <p> Input value: "instanceName" -- Type: <code>java.lang.String</code></p>
     * @param  handlerCtx The HandlerContext.
     */
    @Handler (id="jbiSetSuStatsForInstance",
              input={
                  @HandlerInput (name="saName", type=String.class, required=true),
                  @HandlerInput (name="suName", type=String.class, required=true),
                  @HandlerInput (name="prevSelectedInstance", type=String.class, required=true)
              },
              output={
                  @HandlerOutput(name="instancesList", type=Option[].class),
                  @HandlerOutput(name="isInstanceDropDownEnabled", type=Boolean.class),
                  @HandlerOutput(name="currSelectedInstance", type=String.class),
                  @HandlerOutput(name="isAlertNeeded", type=Boolean.class),
                  @HandlerOutput(name="alertSummary", type=String.class),
                  @HandlerOutput(name="alertDetails", type=String.class),
                  @HandlerOutput(name="installedOnZeroTargets", type=Boolean.class)
              })

        public static void  jbiSetSuStatsForInstance(HandlerContext handlerCtx)
    {
        String saName = (String)handlerCtx.getInputValue ("saName");
        String suName = (String)handlerCtx.getInputValue ("suName");
        String prevSelectedInstance = (String) handlerCtx.getInputValue("prevSelectedInstance");

        if (sLog.isLoggable(Level.FINER)) {        
            sLog.finer("StatsHandlers.jbiSetSuStatsForInstance(...)" +
                  ", saName=" + saName + 
                  ", suName=" + suName + 
                  ", prevSelectedInstance=" + prevSelectedInstance);
        }

        AlertBean alertBean = BeanUtilities.getAlertBean();
        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE; 
        String currSelectedInstance = prevSelectedInstance;
        boolean isAlertNeeded = false;
        boolean isInstanceDropDownEnabled = false;
        boolean installedOnZeroTargets = true;
        
        ServiceUnitBean suBean = BeanUtilities.getServiceUnitBean();
        suBean.setSuStatsProps(saName, suName, prevSelectedInstance);
        
        Option[] instancesList =
            JBIUtils.getInstancesAsOptionArray(saName, JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE);
        List<String> instanceNamesList = 
            JBIUtils.getInstancesAsStringList(saName, JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE);

        if ( instancesList != null && instancesList.length > 0) {
        	installedOnZeroTargets = false;
        	        
	        // determine the currently selected instance
	        // using either the previously selected instance if it is in the list,
	        // or the default "server" target if it is in the list
	        // or the first instance in the list if it is non empty
	        // or null if the list is empty
	        currSelectedInstance =
	            JBIUtils.selectCurrentInstance(instanceNamesList, prevSelectedInstance);
	
            // determines if the instance is running
            String instanceState = ClusterUtilities.getInstanceState(currSelectedInstance);
            if (SharedConstants.STATE_RUNNING.equals(instanceState)){
                suBean.setSuStatsProps(saName, suName, currSelectedInstance);
            } else {            	
                isAlertNeeded = true;
                alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                alertSummary = I18nUtilities.getResourceString("jbi.sa.stats.cannot.show.stats");
                alertDetails = I18nUtilities.getResourceString("jbi.sa.stats.instance.not.running");
            }
	        	        	        
        } else {
            isAlertNeeded = true;
            alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
            alertSummary = I18nUtilities.getResourceString("jbi.sa.su.not.installed.to.target.summary");
            Object[] args = {saName, suName};
            alertDetails = 
                GuiUtil.getMessage(I18nUtilities.getResourceString("jbi.sa.su.not.installed.to.target.details"), args);

        }
        
        handlerCtx.setOutputValue("instancesList", instancesList);
        handlerCtx.setOutputValue("isInstanceDropDownEnabled", isInstanceDropDownEnabled);
        handlerCtx.setOutputValue("currSelectedInstance", currSelectedInstance);
        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("installedOnZeroTargets", installedOnZeroTargets);
        
        if (sLog.isLoggable(Level.FINE)) {        
        sLog.fine("StatsHandlers.jbiSetSuStatsForInstance(...)" +
                  ", instancesList=" + instancesList +
                  ", isInstanceDropDownEnabled=" + isInstanceDropDownEnabled +
                  ", currSelectedInstance=" + currSelectedInstance +
                  ", isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails +
                  ", installedOnZeroTargets=" + installedOnZeroTargets +
                  "");
        }
    }

    private static final boolean IS_CLUSTER_PROFILE =
        ClusterUtilities.isClusterProfile();

    /**
     * prevents subclassing or instantiation
     */
    private StatsHandlers()
    {
    }
}

