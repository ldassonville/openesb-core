/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
package com.sun.jbi.jsf.bean;

import com.sun.jbi.jsf.util.ESBConstants;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JSFUtils;
import com.sun.webui.jsf.component.PropertySheet;
import java.util.Properties;

/**
 * A JavaServer Faces managed bean that supports: installation-time component
 * configuration, runtime component configuration, and runtime component
 * application configuration. <p>
 *
 * For installation-time component configuration this bean uses the jbi.xml
 * from a not-yet-installed component archive. If this jbi.xml uses a
 * schema-valid configuration extension element and property subelements,
 * those properties that are to be displayed during installation are used to
 * determine how to display editable installation configuration properties,
 * which, when accepted or altered, this bean then passes to the installer for
 * use when setting the component's installation extension configuration
 * MBean's corresponding attributes. </p> <p>
 *
 * For runtime component configuration, this bean uses the common client APIs
 * to determine if the component supports configuration and to get the schema
 * and extension elements. If the component supports configuration and
 * provides extension elements, those properties that are to be displayed
 * during runtime configuration are used to determine how to display editiable
 * runtime configuration properties, which when altered and saved, this bean
 * the passes to the common client for use when setting the component's
 * runtime configuration extension MBean's corresponding attributes.
 *
 * @author   Sun Microsystems Inc.
 */
public class CompConfigBean
{

    /**
     * Gets the CompInstallationConfigPS attribute of the CompConfigBean
     * object
     *
     * @return   The CompInstallationConfigPS value
     */
    public PropertySheet getCompInstallationConfigPS()
    {
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_GET_COMP_INSTALLATION_CONFIG_PS,
                                   "mCompInstallationConfigPS=" + mCompInstallationConfigPS);
            }
        return mCompInstallationConfigPS;
    }

    /**
     * Gets the properties from the component installation 
     * configuration property sheet, if any.
     * @return a possibly empty set of properties
     */
    public Properties getCompInstallationConfigProperties()
    {
        Properties result =
            new Properties();
        JSFUtils.decodeCompConfigProperties(mCompInstallationConfigPS,
                                            result);
        return result;
    }

    /**
     * Gets the properties from the component runtime 
     * application configuration property sheet, if any.
     * @return a possibly empty set of properties
     */
    public Properties getCompAppConfigProperties()
    {
        Properties result =
            new Properties();
        JSFUtils.decodeCompAppConfigProperties(mCompRuntimeAppConfigPS,
                                               result);
        return result;
    }

    /**
     * Gets the properties from the component runtime 
     * configuration property sheet, if any.
     * @return a possibly empty set of properties
     */
    public Properties getCompRuntimeConfigProperties()
    {
        Properties result =
            new Properties();
        JSFUtils.decodeCompConfigProperties(mCompRuntimeCompConfigPS,
                                            result);
        return result;
    }

    /**
     * Gets the CompRuntimeAppConfigPS attribute of the CompConfigBean object
     *
     * @return   The CompRuntimeAppConfigPS value
     */
    public PropertySheet getCompRuntimeAppConfigPS()
    {
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_GET_COMP_RUNTIME_APP_CONFIG_PS,
                                   "mCompRuntimeAppConfigPS=" + mCompRuntimeAppConfigPS);
            }
        return mCompRuntimeAppConfigPS;
    }

    /**
     * Gets the CompRuntimeAppVarsPS attribute of the CompConfigBean object
     *
     * @return   The CompRuntimeAppVarsPS value
     */
    public PropertySheet getCompRuntimeAppVarsPS()
    {
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_GET_COMP_RUNTIME_APP_VARS_PS,
                                   "mCompRuntimeAppVarsPS=" + mCompRuntimeAppVarsPS);
            }
        return mCompRuntimeAppVarsPS;
    }

    /**
     * Gets the CompRuntimeCompConfigPS attribute of the CompConfigBean object
     *
     * @return   The CompRuntimeCompConfigPS value
     */
    public PropertySheet getCompRuntimeCompConfigPS()
    {
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_GET_COMP_RUNTIME_COMP_CONFIG_PS,
                                   "mCompRuntimeCompConfigPS=" + mCompRuntimeCompConfigPS);
            }
        return mCompRuntimeCompConfigPS;
    }

    /**
     * returns the constant to use for setting/saving 
     * component application configuration
     * @return <code>config-type-comp-app</code>
     * Note: even though this method is stateless
     * it must not be declared static (in order for
     * managed bean introspection to find it).
     */
    public String getTYPE_APPLICATION()
    {
        return ESBConstants.CONFIG_TYPE_COMP_APP;
    }

    /**
     * returns the constant to use for setting/saving 
     * component installation configuration
     * @return <code>config-type-comp-install</code>
     * Note: even though this method is stateless
     * it must not be declared static (in order for
     * managed bean introspection to find it).
     */
    public String getTYPE_INSTALL()
    {
        return ESBConstants.CONFIG_TYPE_COMP_INSTALL;
    }

    /**
     * returns the constant to use for setting/saving 
     * component runtime configuration
     * @return <code>config-type-comp-runtime</code>
     * Note: even though this method is stateless
     * it must not be declared static (in order for
     * managed bean introspection to find it).
     */
    public String getTYPE_RUNTIME()
    {
        return ESBConstants.CONFIG_TYPE_COMP_RUNTIME;
    }


    /**
     * Sets the CompInstallationConfigPS attribute of the CompConfigBean
     * object
     *
     * @param anInstallationConfigPS  The new CompInstallationConfigPS value
     */
    public void setCompInstallationConfigPS(PropertySheet anInstallationConfigPS)
    {
        mCompInstallationConfigPS =
            anInstallationConfigPS;
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_SET_COMP_INSTALLATION_CONFIG_PS,
                                   "mCompInstallationConfigPS=" + mCompInstallationConfigPS);
            }
    }

    /**
     * required by JavaServer Faces introspection, but never invoked
     * @param anIgnoredArgument which are ignored if this method was invoked
     */
    public void setCompInstallationConfigProperties(Properties anIgnoredArgument)
    {
        // required, but not used
    }

    /**
     * Sets the CompRuntimeAppConfigPS attribute of the CompConfigBean object
     *
     * @param aCompRuntimeAppConfigPS  The new CompRuntimeAppConfigPS value
     */
    public void setCompRuntimeAppConfigPS(PropertySheet aCompRuntimeAppConfigPS)
    {
        mCompRuntimeAppConfigPS =
            aCompRuntimeAppConfigPS;
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_SET_COMP_RUNTIME_APP_CONFIG_PS,
                                   "mCompRuntimeAppConfigPS=" + mCompRuntimeAppConfigPS);
            }
    }

    /**
     * Sets the CompRuntimeAppVarsPS attribute of the CompConfigBean object
     *
     * @param aCompRuntimeAppVarsPS  The new CompRuntimeAppVarsPS value
     */
    public void setCompRuntimeAppVarsPS(PropertySheet aCompRuntimeAppVarsPS)
    {
        mCompRuntimeAppVarsPS =
            aCompRuntimeAppVarsPS;
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_SET_COMP_RUNTIME_APP_VARS_PS,
                                   "mCompRuntimeAppVarsPS=" + mCompRuntimeAppVarsPS);
            }
    }

    /**
     * Sets the CompRuntimeCompConfigPS attribute of the CompConfigBean object
     *
     * @param aCompRuntimeCompConfigPS  The new CompRuntimeCompConfigPS value
     */
    public void setCompRuntimeCompConfigPS(PropertySheet aCompRuntimeCompConfigPS)
    {
        mCompRuntimeCompConfigPS =
            aCompRuntimeCompConfigPS;
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.logFiner(CN, MN_SET_COMP_RUNTIME_COMP_CONFIG_PS,
                                   "mCompRuntimeCompConfigPS=" + 
                                   mCompRuntimeCompConfigPS);
            }
    }

    private static final String CN = CompConfigBean.class.getName();
    private static final String MN_GET_COMP_INSTALLATION_CONFIG_PS =
        "getCompInstallationConfigPS";
    private static final String MN_GET_COMP_RUNTIME_APP_CONFIG_PS =
        "getCompRuntimeAppConfigPS";
    private static final String MN_GET_COMP_RUNTIME_APP_VARS_PS =
        "getCompRuntimeAppVarsPS";
    private static final String MN_GET_COMP_RUNTIME_COMP_CONFIG_PS =
        "getCompRuntimeCompConfigPS";
    private static final String MN_SET_COMP_INSTALLATION_CONFIG_PS =
        "setCompInstallationConfigPS";
    private static final String MN_SET_COMP_RUNTIME_APP_CONFIG_PS =
        "setCompRuntimeAppConfigPS";
    private static final String MN_SET_COMP_RUNTIME_APP_VARS_PS =
        "setCompRuntimeAppVarsPS";
    private static final String MN_SET_COMP_RUNTIME_COMP_CONFIG_PS =
        "setCompRuntimeCompConfigPS";

    /**
     */
    private PropertySheet mCompInstallationConfigPS;

    /**
     */
    private PropertySheet mCompRuntimeAppConfigPS;

    /**
     */
    private PropertySheet mCompRuntimeAppVarsPS;

    /**
     */
    private PropertySheet mCompRuntimeCompConfigPS;


}
