/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  JBIConfigBean.java
 */
package com.sun.jbi.jsf.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import com.sun.jbi.jsf.util.ConnectionUtilities;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JBITreeExtension;
import com.sun.jbi.ui.common.JBIAdminCommands;

/**
 * JavaServer Faces managed bean that loads the other (psuedo) managed beans
 * and checks for JBI runtime and JBI admin-gui extensions.
 *
 * @author   Sun Microsystems Inc.
 */
public class JBIConfigBean
{

    /**
     * Gets the JBI common client, checks for JBI runtime availability, and
     * checks-for/initializes any (optional) JBI admin-gui navigation tree
     * extension (such as one added by a Java CAPS installation).
     */
    public JBIConfigBean()
    {
        boolean isJbiRuntimeEnabled = false;

        try
        {
            mCachedClient = (new ConnectionUtilities()).getClient();
            isJbiRuntimeEnabled = mCachedClient.isJBIRuntimeEnabled();
            
            if (sLog.isLoggable(Level.FINEST)) {
                sLog.finest("JBIConfigBean.<init>(): isJbiRuntimeEnabled=" +
                        isJbiRuntimeEnabled + ", mCachedClient=" +
                        mCachedClient);
            }
            if (isJbiRuntimeEnabled)
            {
                boolean isExtensionInitialized = initExtensionManagedBeans();
                if (isExtensionInitialized)
                {
                    mIsJbiEnabled = true;

                    // check for any JBI extension subtree
                    initJBITreeExtension();
                }
            }
        }
        catch (Exception ex)
        {
        	if (sLog.isLoggable(Level.FINE)){
            sLog.log(Level.FINE,
                        "JBIConfigBean.isJbiEnabled() caught exception",
                        ex);
        	}
        }
        
        if (sLog.isLoggable(Level.FINEST)) {
            sLog.finest("JBIConfigBean.<init>(): mIsJbiEnabled=" +
                    mIsJbiEnabled);
        }
    }

    /**
     * get the common client for JBI administration queries and operations
     *
     * @return   the JBI Administration common client
     */
    public JBIAdminCommands getClient()
    {
        if (sLog.isLoggable(Level.FINEST)) {
            sLog.finest("JBIConfigBean.getClient() result=" + mCachedClient);
        }
        return mCachedClient;
    }

    /**
     * checks for a usable JBI runtime (JBI's JSF tags use this to
     * conditionally render JBI views; no runtime (or any missing required jar
     * files) results in no JBI tree node or tabs in the console
     *
     * @return   true if the JBI console implementation, common client, and
     *      runtime are all available
     */
    public boolean isJbiEnabled()
    {
        if (sLog.isLoggable(Level.FINEST)) {
            sLog.finest("JBIConfigBean.isJbiEnabled(): result=" + mIsJbiEnabled);
        }
        
        return mIsJbiEnabled;
    }

    /**
     * returns the admin-gui navigation tree extension, if any.
     *
     * @return   the extension or null
     */
    public JBITreeExtension getJBITreeExtension()
    {
        if (sLog.isLoggable(Level.FINEST)) {
            sLog.finest("JBIConfigBean.getJBITreeExtension(), result=" + mJBITreeExtension);
        }
        return mJBITreeExtension;
    }

    //Get Logger to log fine mesages for debugging
    private static Logger sLog = JBILogger.getInstance();

    private static final String[][] EXTENSION_MANAGED_BEANS = {
                {"ArchiveBean", "com.sun.jbi.jsf.bean.ArchiveBean"},
                {"AlertBean", "com.sun.jbi.jsf.bean.AlertBean"},
                {"CompConfigBean", "com.sun.jbi.jsf.bean.CompConfigBean"},
                {"DeletionBean", "com.sun.jbi.jsf.bean.DeletionBean"},
                {"InstallationBean", "com.sun.jbi.jsf.bean.InstallationBean"},
                {"ListBean", "com.sun.jbi.jsf.bean.ListBean"},
                {"LoggingBean", "com.sun.jbi.jsf.bean.LoggingBean"},
                {"OperationBean", "com.sun.jbi.jsf.bean.OperationBean"},
                {"RuntimeConfigurationBean", "com.sun.jbi.jsf.bean.RuntimeConfigurationBean"},
                {"RuntimeStatsBean", "com.sun.jbi.jsf.bean.RuntimeStatsBean"},
                {"ServiceUnitBean", "com.sun.jbi.jsf.bean.ServiceUnitBean"},
                {"ShowBean", "com.sun.jbi.jsf.bean.ShowBean"},
                {"TargetBean", "com.sun.jbi.jsf.bean.TargetBean"},
                {"UploadCopyRadioBean", "com.sun.jbi.jsf.bean.UploadCopyRadioBean"},
                };
    private static final int INDEX_BEAN_NAME = 0;
    private static final int INDEX_CLASS_NAME = 1;

    private static final String TREE_EXTENSION_CLASS_NAME =
                "com.sun.jbi.jsf.util.JBITreeExtensionImpl";

    /**
     * loads extension-managed (formerly JavaServer Faces managed) beans.
     *
     * @return   true if no exceptions occur
     */
    private boolean initExtensionManagedBeans()
    {
        boolean result = true;
        // assume success
        try
        {
            for (int i = 0; i < EXTENSION_MANAGED_BEANS.length; ++i)
            {
                String beanName = EXTENSION_MANAGED_BEANS[i][INDEX_BEAN_NAME];
                String className = EXTENSION_MANAGED_BEANS[i][INDEX_CLASS_NAME];
                Class beanClass = Class.forName(className);
                Object bean = beanClass.newInstance();

                if (sLog.isLoggable(Level.FINEST)) {
                    sLog.finest("JBIConfigBean.initExensionManagedBeans() beanName=" + beanName +
                            ", className=" + className + ", bean=" + bean);
                }
                
                FacesContext facesContext = FacesContext.getCurrentInstance();
                facesContext.getExternalContext().getSessionMap().put(beanName, bean);
            }
        }
        catch (Exception ex)
        {
            result = false;
            // indicate failure
            if (sLog.isLoggable(Level.FINE)){
            sLog.log(Level.FINE,
                        "JBIConfigBean.initExtensionManagedBeans() caught Exception",
                        ex);
            }
        }
        
        if (sLog.isLoggable(Level.FINEST)) {
            sLog.finest("JBIConfigBean.initExensionManagedBeans() result=" + result);
        }
        
        return result;
    }

    /**
     * initializes the (optional) admin-gui navigation tree extension, if any.
     * A ClassNotFoundException is expected if there is no tree extension
     * class found in the classpath (when Java CAPS is installed this would be
     * in the optional glassfish/jbi/lib/jbi-gui-ext.jar file).
     */
    private void initJBITreeExtension()
    {

        mJBITreeExtension = null;

        try
        {
            Class treeExtensionClass = Class.forName(TREE_EXTENSION_CLASS_NAME);
            mJBITreeExtension =
                        (JBITreeExtension) treeExtensionClass.newInstance();
        }
        catch (Exception ex)
        {        	
        	if (sLog.isLoggable(Level.FINE)) {
               sLog.log(Level.FINE,
                        ("JBIConfigBean.initJBITreeExtension() caught ex=" + ex),
                        ex);
        	}
        }

        if (sLog.isLoggable(Level.FINEST)) {
            sLog.finest("JBIConfigBean.initJBITreeExtension(), mJBITreeExtension=" +
                    mJBITreeExtension);
        }
    }

    /**
     * cached JBI client and its JMX connection (prevents garbage collection
     * during life of web console)
     */
    private JBIAdminCommands mCachedClient;

    /**
     * JBI is enabled when the jbi-admin-gui and jbi-admin-common jar files
     * are both present
     */
    private boolean mIsJbiEnabled = false;

    /**
     * JBI tree extension is non-null if an implementation is found in the
     * classpath
     */
    private JBITreeExtension mJBITreeExtension = null;

}
