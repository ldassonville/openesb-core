/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  AlertBean.java
 */
package com.sun.jbi.jsf.bean;

import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.JBILogger;

/**
 * Holds alert type (for icon display), summary, details, and display state.
 *
 * @author   Sun Microsystems Inc.
 */
public class AlertBean
{

    /**
     * Tracks display of an alert
     */
    public static final int ALERT_COUNT_NONE = 0;

    /**
     * Represents emptied details
     */
    public static final String ALERT_DETAIL_NONE = "";
    // not I18n

    /**
     * Represents emptied summary
     */
    public static final String ALERT_SUMMARY_NONE = "";
    // not I18n

    /**
     * <code>error</code> icon alert display
     */
    public static final String ALERT_TYPE_ERROR = "error";
    // not I18n

    /**
     * <code>info</code> icon alert display
     */
    public static final String ALERT_TYPE_INFO = "info";
    // not I18n

    /**
     * an uninitialized alert type
     */
    public static final String ALERT_TYPE_NONE = "";
    // not I18n

    /**
     * <code>success</code> icon alert display
     */
    public static final String ALERT_TYPE_SUCCESS = "success";
    // not I18n

    /**
     * <code>warning</code> icon alert display
     */
    public static final String ALERT_TYPE_WARNING = "warning";
    // not I18n

    /**
     * Gets the count to determine if the alert needs to be, or already has
     * been, displayed.
     *
     * @return   zero if there is no need to display the alert
     */
    public int getAlertCount()
    {
        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_GET_ALERT_COUNT,
                        "mAlertCount=" + mAlertCount);
        }
        return mAlertCount;
    }

    /**
     * Gets alert details
     *
     * @return   the I18n details to display in an alert
     */
    public String getAlertDetail()
    {
        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_GET_ALERT_DETAIL,
                        "mAlertDetail=" + mAlertDetail);
        }
        return mAlertDetail;
    }

    /**
     * Gets alert summary
     *
     * @return   the I18n summary to display in an alert
     */
    public String getAlertSummary()
    {
        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_GET_ALERT_SUMMARY,
                        "mAlertSummary=" + mAlertSummary);
        }
        return mAlertSummary;
    }

    /**
     * Gets alert type
     *
     * @return   the type of alert icon to display, one of: <code>info</code>,
     *      <code>warning</code>, <code>error</code> (defaults to <code>error</code>
     *      if no alert type was set.
     */
    public String getAlertType()
    {
        if (ALERT_TYPE_NONE.equals(mAlertType))
        {
            mAlertType = ALERT_TYPE_ERROR;
        }

        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_GET_ALERT_TYPE,
                        "mAlertType=" + mAlertType);
        }

        return mAlertType;
    }

    /**
     * Sets the count
     *
     * @param anAlertCount  the new count, nonzero if the alert is to be
     *      displayed, and zero when the alert has been displayed.
     */
    public void setAlertCount(int anAlertCount)
    {
        mAlertCount = anAlertCount;

        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_SET_ALERT_COUNT,
                               ", mAlertCount=" + mAlertCount);
        }
    }

    /**
     * Sets the details
     *
     * @param aAlertDetail  an I18n details message string
     */
    public void setAlertDetail(String aAlertDetail)
    {
        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_SET_ALERT_DETAIL,
                        "mAlertDetail=" + mAlertDetail);
        }
        mAlertDetail = aAlertDetail;
    }

    /**
     * Sets summary
     *
     * @param aAlertSummary  the I18n summary message string
     */
    public void setAlertSummary(String aAlertSummary)
    {
        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_SET_ALERT_SUMMARY,
                        "mAlertSummary=" + mAlertSummary);
        }
        mAlertSummary = aAlertSummary;
    }

    /**
     * Sets the type of icon to display
     *
     * @param aAlertType  an icon type, one of: <code>info</code>, <code>warning</code>
     *      , or <code>error</code>. If not set, the icon defaults to <code>error</code>
     *      .
     */
    public void setAlertType(String aAlertType)
    {
        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_SET_ALERT_TYPE,
                        "mAlertType=" + mAlertType);
        }
        mAlertType = aAlertType;
    }

    /**
     * deterimes if a pending alert, in the current session,
     * needs to be displayed 
     * @return true if <code>#{isJbiAlertNeeded}</code> is true
     */
    public static boolean isSessionAlertNeeded()
    {
        boolean result = false;
        String isJbiAlertNeededString = 
            BeanUtilities
            .getStringPropertyUsingExpression("#{isJbiAlertNeeded}");
        
        if (Boolean
            .TRUE
            .toString()
            .equalsIgnoreCase(isJbiAlertNeededString))
            {
                result = true;
            }
        return result;
    }

    /**
     * Gets the pending alert detail message from the session, if any.
     * @return the, possibly empty string, with alert details.
     */
    public static String getSessionAlertDetail(String aDefault)
    {
        String result = 
            BeanUtilities
            .getStringPropertyUsingExpression("#{jbiAlertDetails}");
        if ((null == result)
            ||(ALERT_DETAIL_NONE.equals(result)))
            {
                result = aDefault;
            }
        return result;
    }

    /**
     * Gets the pending alert summary message from the session, if any.
     * @return the, possibly empty string, with alert summary.
     */
    public static String getSessionAlertSummary(String aDefault)
    {
        String result = 
            BeanUtilities
            .getStringPropertyUsingExpression("#{jbiAlertSummary}");
        if ((null == result)
            ||(ALERT_SUMMARY_NONE.equals(result)))
            {
                result = aDefault;
            }

        return result;
    }

    private static final String CN = AlertBean.class.getName();
    private static final String MN_GET_ALERT_COUNT = "getAlertCount()";
    private static final String MN_GET_ALERT_DETAIL = "getAlertDetail()";
    private static final String MN_GET_ALERT_SUMMARY = "getAlertSummary()";
    private static final String MN_GET_ALERT_TYPE = "getAlertType()";
    private static final String MN_SET_ALERT_COUNT = "setAlertCount()";
    private static final String MN_SET_ALERT_DETAIL = "setAlertDetail()";
    private static final String MN_SET_ALERT_SUMMARY = "setAlertSummary()";
    private static final String MN_SET_ALERT_TYPE = "setAlertType()";

    private int mAlertCount = ALERT_COUNT_NONE;
    private String mAlertDetail = ALERT_DETAIL_NONE;
    private String mAlertSummary = ALERT_SUMMARY_NONE;
    private String mAlertType = ALERT_TYPE_NONE;
}
