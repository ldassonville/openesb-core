/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  InstallationBean.java
 */

package com.sun.jbi.jsf.bean;

import com.sun.data.provider.TableDataProvider;
import com.sun.data.provider.impl.ObjectListDataProvider;
import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.CompositeDataUtils;
import com.sun.jbi.jsf.util.I18nUtilities;
import com.sun.jbi.jsf.util.JBIConstants;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIVerifierReportItemNames;
import com.sun.webui.jsf.model.Option;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;

/**
 * Description of the Class
 *
 * @author   Sun Microsystems Inc.
 */
public class InstallationBean
{

    /**
     * Description of the Field
     */
    public static final String DEFAULT_PE_TARGET = JBIAdminCommands.SERVER_TARGET_KEY;


    /**
     * Gets the ResultDetails attribute of the InstallationBean object
     *
     * @return   The ResultDetails value
     */
    public String getResultDetails()
    {
        // only once
        mIsAlertNeeded = false;
        
        if (sLog.isLoggable(Level.FINEST)){
          sLog.finest("InstallationBean.getResultDetails()=" + mResultDetails + ")");
        }
        return mResultDetails;
    }

    /**
     * Gets the ResultSummary attribute of the InstallationBean object
     *
     * @return   The ResultSummary value
     */
    public String getResultSummary()
    {
        if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("InstallationBean.getResultSummary()=" + mResultSummary + ")");
        }
        return mResultSummary;
    }

    /**
     * Gets the IsAlertNeeded attribute of the InstallationBean object
     *
     * @return   The IsAlertNeeded value
     */
    public Boolean getIsAlertNeeded()
    {
        Boolean result = Boolean.valueOf(mIsAlertNeeded); 
        if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("InstallationBean.isAlertNeeded()=" + result + ")");
        }
        return result;
    }


    /**
     * Get the mUploadSelected variable for cleaning up temporary uploaded
     * files
     *
     * @return   mUploadSelected boolean false if copy path was selected
     */

    public boolean getUploadSelected()
    {
        if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("InstallationBean.getUploadSelected()=" + mUploadSelected + ")");
        }
        return mUploadSelected;
    }


    /**
     * Gets the TargetNames attribute of the InstallationBean object
     *
     * @return   The TargetNames value
     */
    public String[] getTargetNames()
    {
        return mTargetNames;
    }

    /**
     * Set the mUploadSelected variable true for cleaning up temporary
     * uploaded files
     *
     * @param uploadSelected  boolean false if copy path was selected
     */
    public void setUploadPathSelected(boolean uploadSelected)
    {
        if (sLog.isLoggable(Level.FINEST)){    	
           sLog.finest("InstallationBean.setUploadSelected(" + uploadSelected + ")");
        }
        
        mUploadSelected = uploadSelected;
    }


    /**
     * Sets the TargetNames attribute of the InstallationBean object
     *
     * @param aTargetNames  The new TargetNames value
     */
    public void setTargetNames(String[] aTargetNames)
    {
        mTargetNames = aTargetNames;
    }

    /**
     * Set the mUploadSelected variable true for cleaning up temporary
     * uploaded files
     *
     * @param aSource  Properties containing the parameters
     * @param aTarget  the name of the target to install to
     * @return         result Properteis contain the result information
     */
    public Properties installValidatedArchive(Properties aSource, String aTarget)
    {
        Properties result = aSource;
        String jbiType = aSource.getProperty("type");
        String archiveName = aSource.getProperty("archiveName");
        String archivePath = aSource.getProperty("path");

        result = tryInstallToOneTarget(archiveName,
                                       archivePath,
                                       jbiType,
                                       aTarget,
                                       result);
        return result;
    }


    /**
     * Description of the Method
     *
     * @param aSource  Description of Parameter
     * @return         Description of the Returned Value
     */
    public Properties installValidatedArchive(Properties aSource)
    {
        Properties result = aSource;

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("InstallationBean.installValidatedArchive(" + aSource + "), mTargetNames=" + mTargetNames);
        }

        String archivePath = aSource.getProperty("path");
        String jbiType = aSource.getProperty("type");
        String archiveName = aSource.getProperty("archiveName");

        if (null != mTargetNames)
            {
                /*
                 *  For PE with clusters (or EE) this list could have zero targets, implying domain only
                 */
                if (0 == mTargetNames.length)
                    {
                        // DOMAIN-only

                        if (sLog.isLoggable(Level.FINE)){
                           sLog.fine("InstallationBean.installValidatedArchive(" + aSource + ") install to \"domain\"");
                        }
                        result = tryInstallToOneTarget(null,
                                                       archivePath,
                                                       jbiType,
                                                       JBIAdminCommands.DOMAIN_TARGET_KEY,
                                                       result);
                    }
                /*
                 *  For PE without clusters this list will have one "server" target only
                 *  For PE with clusters (or EE) this list will have one or more named targets only
                 */
                else
                    {
                        for (int i = 0; i < mTargetNames.length; ++i)
                            {
                        	   if (sLog.isLoggable(Level.FINE)){
                                  sLog.fine("InstallationBean.installValidatedArchive(" + aSource + 
                                          ") install to \"" + mTargetNames[i] + "\"");
                        	   }
                                result = tryInstallToOneTarget(archiveName,
                                                               archivePath,
                                                               jbiType,
                                                               mTargetNames[i],
                                                               result);
                            }
                    }
            }
    
        return result;
    }


    /**
     * Verifies the application "environment" for a specified JBI
     * Service Assembly on a specified target.  This includes checking
     * that all required components are installed on the specified 
     * target, and checks that endpoints can be resolved, checks for
     * missing application configurations, checks for missing
     * application variables, and may perform other special-case checks.
     *
     * @param anArchivePath the path to a JBI Service Assembly archive
     * to be verified
     * @param aTarget the GlassFish target name to be verified
     * against 
     * @return a Properties object with either a <code>success-result</code>
     * key or a <code>failure-result</code> key (which maps to error 
     * detail messages to be displayed in an alert)
     */
    public Properties runAppConfigVerifier(String anArchivePath,
                                           String aTarget)
    {
        Properties result =
            new Properties();
        boolean isVerified = true;

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("InstallationBean.runAppConfigVerifier(" + anArchivePath +
                  ", " + aTarget + ")");
        }

        String details = "";

        // Verifies the Service Assembly archive for the specified target
        // and gets back verification results to analyze
        CompositeData compData = null;
        try
            {
                JBIAdminCommands jac = BeanUtilities.getClient();

                compData =
                    jac.verifyApplication(anArchivePath,
                                          aTarget,
                                          false,
                                          null,
                                          false);
            }
        // If the common client call fails,
        // shows an alert with the exception message
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	   if (sLog.isLoggable(Level.FINE)){
                sLog.log(Level.FINE,
                         ("ShowBean.check() caught jrEx=" +
                          jrEx),
                         jrEx);
        	   }
                result.put("failure-result", jrEx.toString());
            }


        // If all required components are installed,
        // checks for unresolved endpoints, unresolvable endpoints,
        // missing application variables, missing application configutations
        // and any special-case validations (e.g. JavaEE SE)
        Object allComponentsInstalled =
            compData.get("AllComponentsInstalled");
        Object endpointInfo = null;
        if ((null != allComponentsInstalled)
            && (allComponentsInstalled instanceof Boolean)
            && (((Boolean) allComponentsInstalled).booleanValue()))
            {
                // check for unsatisfied app config
                
                endpointInfo =
                    compData.get("EndpointInfo");
            }
                
        // If one or more required components is not intalled
        // adds a required component not installed message to the alert details
        else
            {
                isVerified = false;
                Object missingComponentsList =
                    compData.get("MissingComponentsList");
                if ((null != missingComponentsList)
                    && (missingComponentsList instanceof String[])
                    && (0 < ((String[]) missingComponentsList).length))
                    {
                        String[] missingComponents =
                            (String[]) missingComponentsList;
                        for (int i = 0; i < missingComponents.length; ++i)
                            {
                                Object[] args =
                                    {missingComponents[i],
                                     aTarget};
                                String compNotInstKey =
                                    "jbi.app.verification.required.component.not.installed";
                                details +=
                                    GuiUtil
                                    .getMessage(I18nUtilities
                                                .getResourceString(compNotInstKey),
                                                args) + 
                                    SharedConstants
                                    .HTML_BREAK;
                            }
                    }
                // If not all components are installed
                // but no unintalled-components are listed,
                // displays an unknown failure reason
                else
                    {
                        details +=
                            I18nUtilities
                            .getResourceString("jbi.app.verification.failure.reason.unknown");
                    }
                result.put("failure-result", details);
            }


        // If no error so far
        // and endpoint information is available
        // validates endpoints
        if (isVerified
            && (null != endpointInfo)
            && (endpointInfo instanceof CompositeData[])
            && (0 < ((CompositeData[]) endpointInfo).length))
            {
                String[] endpointNames =
                    {
                        "EndpointName",
                        "ServiceUnitName",
                        "ComponentName",
                        "Status",
                    };
                CompositeData[] endpointInfos =
                    (CompositeData[]) endpointInfo;
                for (int i = 0; i < endpointInfos.length; ++i)
                    {
                        Object[] endpointValues =
                            endpointInfos[i].getAll(endpointNames);
                        String epName =
                            (String) endpointValues[0];
                        String epSuName =
                            (String) endpointValues[1];
                        String epCompName =
                            (String) endpointValues[2];
                        String epStatus =
                            (String) endpointValues[3];
                        
                        // If an endpoint is unresolved,
                        // adds an unresolved endpoint message to the alert details
                        if ("UNRESOLVED".equals(epStatus))
                            {
                                isVerified = false;
                                Object[] args =
                                    {epName,
                                     epSuName,
                                     epCompName,
                                     aTarget};
                                String unresolvedEpKey =
                                    "jbi.app.verification.unresolved.endpoint";
                                details +=
                                    GuiUtil
                                    .getMessage(I18nUtilities
                                                .getResourceString(unresolvedEpKey),
                                                args) + 
                                    SharedConstants
                                    .HTML_BREAK;
                            }

                        // If an endpoint status is neither unresolved nor resolved
                        // adds an cannot resolve endpoint message to the alert details
                        // (this could be due to the runtime start-on-verify flag being
                        // turned off, and a required component not yet being started)
                        else if (!"RESOLVED".equals(epStatus))
                            {
                                isVerified = false;
                                Object[] args =
                                    {epName,
                                     epSuName,
                                     epCompName,
                                     aTarget};
                                String cannotResolveEpKey =
                                    "jbi.app.verification.cannot.resolve.endpoint";
                                details +=
                                    GuiUtil
                                    .getMessage(I18nUtilities
                                                .getResourceString(cannotResolveEpKey),
                                                args) + 
                                    SharedConstants
                                    .HTML_BREAK;
                            }
                        
                        if (sLog.isLoggable(Level.FINER)){
                           sLog.finer("InstallationBean.runAppConfigVerifier(" +
                                  anArchivePath + ", " + aTarget + "), epName=" +
                                  epName + ", epSuName=" + epSuName + ", epCompName=" +
                                  epCompName + ", epStatus=" + epStatus);
                        }
                        
                        
                        // check for missing application configurations, if any
                        String[] missingAppConfigNamesAry = 
                            CompositeDataUtils
                            .findCompositeStringArray((CompositeData)
                                                      ((CompositeData[])endpointInfo)[i],
                                                      "MissingApplicationConfigurations");
                        if ((null != missingAppConfigNamesAry)
                            &&(0 < missingAppConfigNamesAry.length))
                            
                            {
                                isVerified = false;
                                String names = "";
                                for (String missingAppConfigName : missingAppConfigNamesAry)
                                    {
                                        names +=
                                            missingAppConfigName + " ";
                                    }
                                Object[] args =
                                    {names,};
                                String missingAppConfigsKey =
                                    "jbi.app.verification.missing.app.configs";
                                
                                details +=
                                    GuiUtil
                                    .getMessage(I18nUtilities
                                                .getResourceString(missingAppConfigsKey),
                                                args) + 
                                    SharedConstants
                                    .HTML_BREAK;
                                
                            }
                        
                        // check for missing application variables, if any
                        String[] missingAppVarsNamesAry = 
                            CompositeDataUtils
                            .findCompositeStringArray((CompositeData)
                                                      ((CompositeData[])endpointInfo)[i],
                                                      "MissingApplicationVariables");
                        if ((null != missingAppVarsNamesAry)
                            &&(0 < missingAppVarsNamesAry.length))
                            
                            {
                                isVerified = false;
                                String names = "";
                                for (String missingAppVarsName : missingAppVarsNamesAry)
                                    {
                                        names +=
                                            missingAppVarsName + " ";
                                    }
                                Object[] args =
                                    {names,};
                                String missingAppVarsKey =
                                    "jbi.app.verification.missing.app.vars";
                                
                                details +=
                                    GuiUtil
                                    .getMessage(I18nUtilities
                                                .getResourceString(missingAppVarsKey),
                                                args) + 
                                    SharedConstants
                                    .HTML_BREAK;
                                
                            }
                        
                    }
                
                // Check for special-case errors: 
                // Processes the JavaEE SE verification report, if any
                CompositeType compType = compData.getCompositeType();
                Set compItemSet = compType.keySet();
                CompositeData[] reports = null;
                if (compItemSet.contains("JavaEEVerificationReport"))
                    {
                        reports = 
                            (CompositeData[]) compData.get("JavaEEVerificationReport");
                    }
                
                // If any reports exist,
                // Verifies that the status is success for each report
                if ((null != reports) 
                    && (0 < reports.length))
                    {
                        for (int reportIndex = 0; reportIndex < reports.length; ++reportIndex)
                            {
                                String suName = 
                                    CompositeDataUtils
                                    .findCompositeItem(reports[reportIndex],
                                                       "ServiceUnitName");
                                boolean isSuNameShown = 
                                    false;

                                TabularData reportTable = 
                                    (TabularData) reports[reportIndex]
                                    .get("JavaEEVerifierReport");
                                
                                Set rows = 
                                    reportTable.keySet();
                                
                                int rowCount = 0;
                                for (Object row : rows)
                                    {
                                        ++rowCount;

                                        Object[] key = 
                                            ((List)row).toArray();
                                        
                                        CompositeData suCompData = 
                                            reportTable.get(key);
                                        
                                        if (null == suCompData)
                                            {
                                                continue;
                                            }

                                        CompositeType suCompType = 
                                            suCompData.getCompositeType();
                                        Set suCompItemSet = 
                                            suCompType.keySet();
                                        
                                        Iterator itr = 
                                            suCompItemSet.iterator();
                                        
                                        String perReportLogMsg = "";
                                        String perReportErrorMessage = "";
                                        while (itr.hasNext())
                                            {          
                                                String keyName = 
                                                    (String) itr.next();
                                                perReportLogMsg += 
                                                    keyName +
                                                    "=" +
                                                    suCompData
                                                    .get(keyName) +
                                                    "; ";

                                                // If the current key is not "_status"
                                                // continues processing with the next key.
                                                if (!JBIVerifierReportItemNames
                                                    .JAVAEE_VERIFIER_ITEM_STATUS_KEY
                                                    .equals(keyName))
                                                    {
                                                        continue;
                                                    }
                                                
                                                // (If the current key is "_status")
                                                // gets the status value
                                                String status =
                                                    (String) suCompData
                                                    .get(JBIVerifierReportItemNames
                                                         .JAVAEE_VERIFIER_ITEM_STATUS_KEY);

                                                // If the current status is "0"
                                                // continues processing with the next key
                                                if (JBIVerifierReportItemNames
                                                    .JAVAEE_VERIFIER_ITEM_STATUS_OKAY
                                                    .equals(status))
                                                    {
                                                        continue;
                                                    }

                                                // (If the current status is not "0")
                                                // indicates that verification failed,
                                                // and adds messages to the alert detail
                                                isVerified = false;
                                                
                                                // Only adds the SU name summary error message
                                                // prior to first message (if there are any details)
                                                if (!isSuNameShown)
                                                    {
                                                        details +=
                                                            perSuErrorMessage(suName) +
                                                            SharedConstants
                                                            .HTML_BREAK;
                                                        isSuNameShown = 
                                                            true;
                                                    }

                                                // Adds the error message detail 
                                                perReportErrorMessage =
                                                    (String) suCompData
                                                    .get(JBIVerifierReportItemNames
                                                         .JAVAEE_VERIFIER_ITEM_MESSAGE_KEY);
                                                
                                                details +=
                                                    perReportErrorMessage +
                                                    SharedConstants
                                                    .HTML_BREAK;
                                            }
                                        
                                        if (sLog.isLoggable(Level.FINER)){                                        
                                           sLog.finer("InstallationBean.runAppConfigVerifier()" +
                                                  ", JavaEEReport reportIndex=" + reportIndex +
                                                  ", rowCount=" + rowCount +
                                                  ", suName=" + suName + 
                                                  ", perReportLogMsg: " + perReportLogMsg);
                                        }
                                    }
                            }
                    }
        
                if (sLog.isLoggable(Level.FINER)){                
                  sLog.finer("InstallationBean.runAppConfigVerifier(" + anArchivePath +
                          ", " + aTarget + "), compData=" + compData +
                          ", isVerified=" + isVerified);
                }
        
                if (isVerified)
                    {
                        result.put("success-result", compData.toString());
                    }
                else
                    {
                        details +=
                            SharedConstants
                            .HTML_BREAK;
                        result.put("failure-result", details);
                    }
            }
        
        if (sLog.isLoggable(Level.FINE)){
           sLog.fine("InstallationBean.runAppConfigVerifier(" + anArchivePath +
                  ", " + aTarget + "), result=" + result);
        }
        
        return result;
    }

    //Get Logger to log fine mesages for debugging
    private static Logger sLog = JBILogger.getInstance();

    private static String perSuErrorMessage(String anSuName)
    {
        String perSuErrorMessageKey =
            "jbi.app.verification.java.ee.report";
        Object[] args = {anSuName,};
        
        String result =
            GuiUtil
            .getMessage(I18nUtilities
                        .getResourceString(perSuErrorMessageKey),
                        args);
        return result;
    }



    /**
     * Description of the Method
     *
     * @param anArchiveName  Description of Parameter
     * @param anArchivePath  Description of Parameter
     * @param aJbiType       Description of Parameter
     * @param aTargetName    Description of Parameter
     * @param aSource        Description of Parameter
     * @return               Description of the Returned Value
     */
    private Properties tryInstallToOneTarget(String anArchiveName,
                                             String anArchivePath,
                                             String aJbiType,
                                             String aTargetName,
                                             Properties aSource)
    {
        Properties result = aSource;

        mIsAlertNeeded = false;
        String installResult = "unknown";

        try
            {
                installResult = installValidatedArchiveToTarget(anArchiveName,
                                                                anArchivePath,
                                                                aJbiType,
                                                                aTargetName);
                result.setProperty(SharedConstants.SUCCESS_RESULT, installResult);                
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
            {
        	    if (sLog.isLoggable(Level.FINE)){
                sLog.log(Level.FINE,
                         ("InstallationBean.tryInstallToOneTarget() caught jbiRemoteEx=" +
                          jbiRemoteEx),
                         jbiRemoteEx);
        	    }

                JBIManagementMessage mgmtMsg = BeanUtilities.extractJBIManagementMessage(jbiRemoteEx);
                if (mgmtMsg == null)
                    {
                        String internalErrorMsg = 
                            I18nUtilities
                            .getResourceString("jbi.internal.error.invalid.remote.exception");
                        result.setProperty(SharedConstants.INTERNAL_ERROR, internalErrorMsg);
                    }
                else
                    {
                        String msg = mgmtMsg.getMessage();
                        result.setProperty(SharedConstants.FAILURE_RESULT, msg);
                    }
            }
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("InstallationBean.tryInstallToOneTarget(" + aTargetName +
                  ", " + aJbiType + ", <properties>), result=" + result);
        }
        return result;
    }


    /**
     * installs a valid archive to a target
     * @param anArchiveName the archive name
     * @param anArchivePath the path to the archive
     * @param aJbiType a JBI type, one of: <code>binding-component</code>,
     * <code>service-engine</code>, or <code>shared-library</code>
     * @param aTarget a glassfish target
     * @return result of the common client call
     * @exception com.sun.jbi.ui.common.JBIRemoteException if the installation fails
     */
    private String installValidatedArchiveToTarget(String anArchiveName,
                                                   String anArchivePath,
                                                   String aJbiType,
                                                   String aTarget)
        throws com.sun.jbi.ui.common.JBIRemoteException
    {
        String result = "";
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("InstallationBean.installValidatedArihiveToTarget(" + anArchivePath + 
                  ", " + aJbiType + ", " + aTarget);
        }

        JBIAdminCommands jac = BeanUtilities.getClient();

        if (null != jac)
            {
                if ((JBIConstants.JBI_BINDING_COMPONENT_TYPE.equals(aJbiType))
                    || (JBIConstants.JBI_SERVICE_ENGINE_TYPE.equals(aJbiType)))
                    {
                        CompConfigBean compConfigBean =
                            BeanUtilities.getCompConfigBean();

                        Properties configProps =
                            compConfigBean.getCompInstallationConfigProperties();

                        if (configProps == null || configProps.size() == 0)
                            {
                                if (anArchiveName != null)
                                    {
                                        result = jac.installComponentFromDomain(anArchiveName, aTarget);
                                    }
                                else
                                    {
                                        result = jac.installComponent(anArchivePath, aTarget);
                                    }

                            }
                        else
                            {
                                if (anArchiveName != null)
                                    {
                                        result = jac.installComponentFromDomain(anArchiveName,
                                                                                configProps, aTarget);
                                    }
                                else
                                    {
                                        result = jac.installComponent(anArchivePath, configProps,
                                                                      aTarget);
                                    }
                            }

                    }
                else if (JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE.equals(aJbiType))
                    {
                        if (anArchiveName != null)
                            {
                                result = jac.deployServiceAssemblyFromDomain(anArchiveName, aTarget);
                            }
                        else
                            {
                                result = jac.deployServiceAssembly(anArchivePath, aTarget);
                            }
                    }
                else if (JBIConstants.JBI_SHARED_LIBRARY_TYPE.equals(aJbiType))
                    {
                        if (anArchiveName != null)
                            {
                                result = jac.installSharedLibraryFromDomain(anArchiveName, aTarget);
                            }
                        else
                            {
                                result = jac.installSharedLibrary(anArchivePath, aTarget);
                            }
                    }
            }
        else
            {
                if (sLog.isLoggable(Level.FINER)){
        	       sLog.finer("InstallationBean.installValidatedArchiveToTarget() jac=null");
                }
                
                mIsAlertNeeded = true;
                mResultSummary = I18nUtilities.getResourceString("jbi.ee.install.impossible");
                mResultDetails = I18nUtilities.getResourceString("jbi.ee.install.impossible.details");
            }

        if (sLog.isLoggable(Level.FINE)){
          sLog.fine("InstallationBean.installValdidatedArchiveToTargetToTarget(...), result=" + result);
        }
        return result;
    }

    private Option[] mAllTargets = null;
    private String[] mTargetNames = new String[]{DEFAULT_PE_TARGET};
    private boolean mIsAlertNeeded;
    private String mResultDetails;
    private String mResultSummary;
    private boolean mUploadSelected;
}

