/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  ServiceUnitBean.java
 */

package com.sun.jbi.jsf.bean;

import com.sun.data.provider.TableDataProvider;
import com.sun.data.provider.impl.ObjectListDataProvider;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.ClusterUtilities;
import com.sun.jbi.jsf.util.JBIConstants;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JSFUtils;
import com.sun.jbi.jsf.util.SaStatsUtils;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.webui.jsf.component.PropertySheet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.openmbean.TabularData;

/**
 * Provides properties used to populate JBI Show Serice Unit
 * view properties and metadata
 */
public class ServiceUnitBean
{
    /**
     * default result for queries when no data found
     */
    private final static String DEFAULT_RESULT = "";

    //Get logger to log fine, info level messages in server.log file
    private Logger sLog = JBILogger.getInstance();

    /**
     * Constructs an uninitialized Service Unit Bean.
     * To complete initialization, call the setServiceUnitName
     * and setServiceAssemblyName methods.
     * The description and deployment descriptor are derived.
     */
    public ServiceUnitBean()
    {
        mJac = BeanUtilities.getClient();
    }


    // getters

    /**
     * get contents of /META-INF/jbi.xml for this service unit
     * @return the JBI deployment descriptor in a (validated) XML String
     */
    public String getDeploymentDescriptor()
    {
        String result = DEFAULT_RESULT;
        try
        {
            if ( null!= mJac )
            {
            	if (sLog.isLoggable(Level.FINER)) {
                    sLog.finer("ServiceUnitBean.getDeploymentDescriptor(), mJac=" + 
			            mJac + ", mServiceUnitName=" + mServiceUnitName);
            	}
            	
		        result = 
                    mJac.getServiceUnitDeploymentDescriptor(mServiceAssemblyName,
							    mServiceUnitName);
            }
        }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
        	    if (sLog.isLoggable(Level.FINE)) {
                   sLog.log(Level.FINE,
                         ("ServiceUnitBean.getDeploymentDescriptor()" + 
                          ", mServiceAssemblyName=" + mServiceAssemblyName +
                          ", caught jrEx=" + jrEx),
                         jrEx);
        	    }
            }
        
    	if (sLog.isLoggable(Level.FINER)) {        
            sLog.finer("ServiceUnitBean.getDeploymentDescriptor(),result=" + result);
    	}
    	
        return result;
    }


    /**
     * get the list of service units
     * @return a List contain the service units
     */
    public List getServiceUnitsList()
    {
        return mServiceUnitsList;
    }


    /**
     * get the service assembly name
     * @return a String containing the service assembly name
     */
    public String getServiceAssemblyName ()
    {
        return mServiceAssemblyName;
    }


    /**
     * get the target component name
     * @return a String containing the target component name
     */
    public String getTargetComponent ()
    {
        return mTargetComponent;
    }


    /**
     * get the component type
     * @return a String containing component type
     */
    public String getComponentType ()
    {
        return mComponentType;
    }


    /**
     * get the service unit description
     * @return a String containing component type
     */
    public String getDescription ()
    {
        return mDescription;
    }

    /**
     * get the service unit state
     * @return a String the service unit state
     */
    public String getState()
    {
        return mState;
    }


    /**
     * get the service unit name
     * @return a String containing component type
     */
    public String getName ()
    {
        return mServiceUnitName;
    }

    
    /**
     * get the component type icon
     * @return a String containing the icon for the component
     */
    public String getComponentTypeIcon()
    {
        String iconFileName = "";
        if (mComponentType.equals(JBIConstants.JBI_BINDING_COMPONENT_TYPE))
        {
            iconFileName = "JBIBindingComponent.gif";
        }
        else if (mComponentType.equals(JBIConstants.JBI_SERVICE_ENGINE_TYPE))
        {
            iconFileName = "JBIServiceEngine.gif";
        }
        return iconFileName;
    }

    public PropertySheet getServiceUnitStatsPS()
    {
         PropertySheet result = 
             null;

         String targetName =
             ClusterUtilities
             .getInstanceDomainCluster(mSaInstanceName);

         TabularData saTabularData =
             SaStatsUtils.getStats(targetName,
                                   mServiceAssemblyName);

         result = 
             JSFUtils.getSuStatsPS(saTabularData,
                                   mSaInstanceName,
                                   mServiceAssemblyName,
                                   mServiceUnitName);
         return result;
    }

    public TableDataProvider getServiceUnitsTableData()
    {
        TableDataProvider result = new ObjectListDataProvider(mServiceUnitsList);
        
    	if (sLog.isLoggable(Level.FINEST)) {        
            sLog.finest("ShowBean.getServiceUnitsTableData(): result=" + result);
    	}
    	
        return result;
    }


    // setters


    /**
     * set the service units list
     * @param aServiceUnitsListt a List of zero or more service units. 
     */
    public void setServiceUnitsList(List aServiceUnitsList)
    {
    	if (sLog.isLoggable(Level.FINEST)) {    	
            sLog.finest("ShowBean.setServiceUnitsList(" + aServiceUnitsList + ")");
    	}
    	
        mServiceUnitsList = aServiceUnitsList;
    }

    public  void setServiceUnitStatsPS(PropertySheet aPropertySheet)
    {
        // no-op
    }


    public void setServiceAssemblyName (String aName)
    {
        mServiceAssemblyName = aName;
    }


    public void setTargetComponent (String aName)
    {
        mTargetComponent = aName;
    }


    public void setComponentType (String aType)
    {
        mComponentType = aType;
    }


    public void setDescription (String aDescription)
    {
        mDescription = aDescription;
    }


    public void setName (String aServiceUnitName)
    {
        mServiceUnitName = aServiceUnitName;
    }

    public void setState (String aState)
    {
        mState = aState;
    }
    
    public void setSuStatsProps(String anSaName,
                                String anSuName,
                                String anInstanceName)
    {
        mServiceAssemblyName = anSaName;
        mServiceUnitName = anSuName;
        mSaInstanceName = anInstanceName;
    }

    // private methods


    // member variables

    /**
     * cached JBI Admin Commands client
     */ 
    private JBIAdminCommands mJac;

    /**
     * List containing the list of SelectableServiceUnitInfo objects
     */ 
    private List mServiceUnitsList = new ArrayList();

    /**
     * The Containing Service Assembly Name
     */ 
    private String mServiceAssemblyName = null;

    /**
     * The Target Component Name
     */ 
    private String mTargetComponent = null;

    /**
     * The Target Component Type
     */ 
    private String mComponentType = null;
    
    /**
     * The Description of the Service Unit
     */ 
    private String mDescription = null;

    /**
     * The instance name
     */
    private String mSaInstanceName;

    /**
     * The Name of the service unit
     */ 
    private String mServiceUnitName = null;
    
    
    /**
     * The state of the service unit
     */
     
    private String mState = null;

}
