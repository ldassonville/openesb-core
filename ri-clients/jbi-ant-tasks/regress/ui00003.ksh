#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00003.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

export TEST_ANT_CMD
TEST_ANT_CMD="asant -emacs -q -lib $JV_JBI_HOME/lib/jbi-ant-tasks.jar -f ui00003.xml "

#
# test task attributes
#

test_task_attributes()
{
$TEST_ANT_CMD test-required-attributes
$TEST_ANT_CMD test-default-attributes
$TEST_ANT_CMD test-failonerror
}


run_test()
{
build_test_artifacts
test_task_attributes
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed -e '/^$/d'

exit 0
