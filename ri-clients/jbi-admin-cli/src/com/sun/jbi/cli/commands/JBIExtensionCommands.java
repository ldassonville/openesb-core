/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.jbi.cli.commands;

import com.sun.jbi.cli.commands.JBICommandConstants;
import com.sun.enterprise.cli.framework.CommandValidationException;
import com.sun.enterprise.cli.framework.CommandException;
import com.sun.enterprise.cli.framework.CLILogger;
import com.sun.jbi.ui.client.JBIAdminCommandsClientFactory;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.esb.management.common.ManagementRemoteException;
import javax.management.MBeanServerConnection;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import java.util.Map;
import java.util.Set;
import java.util.Properties;
import java.util.Enumeration;
import java.util.logging.Level;
import java.io.FileInputStream;
import java.io.IOException;


/**
 *  Will perform command on a component
 *  (service engine, binding component) or a service assembly.
 *  @version  $Revision: 1.28 $
 */
public class JBIExtensionCommands extends JBICommand
{

    // Log Level default value
    private static final String LOG_LEVEL_DEFAULT = "default";

    /**
     *  A method that Executes the command
     *  @throws CommandException
     */
    public void runCommand() throws CommandException
    {
        String result = "";
        String successKey = "";
        boolean successMsgFlag = false;
        try
        {
            // Retrieve the upload boolean option value
            boolean isUpload = getBooleanOption(JBICommandConstants.UPLOAD_OPTION);
            // Perform the pre run initialization
            if (preRunInit(isUpload))
            {
                // Retrieve the mJBICmds object which was created during the preRunInit
                mJBICmds = (JBIAdminCommands) getJBIAdminCommands();

                if (name.equals(JBICommandConstants.UPGRADE_JBI_COMPONENT))
                {
                    upgradeComponent();
                }

                else if (name.equals(JBICommandConstants.SHOW_JBI_SERVICE_ASSEMBLY))
                {
                    showServiceAssembly();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SHOW_JBI_SHARED_LIBRARY))
                {
                    showSharedLibrary();
                    successMsgFlag = true;
                }

                else if ((name.equals(JBICommandConstants.SHOW_JBI_SERVICE_ENGINE)) ||
                (name.equals(JBICommandConstants.SHOW_JBI_BINDING_COMPONENT)))
                {
                    showComponent();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SHOW_JBI_RUNTIME_CONFIGURATION))
                {
                    showRuntimeConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SHOW_JBI_APPLICATION_CONFIGURATION))
                {
                    showApplicationConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SHOW_JBI_RUNTIME_LOGGERS))
                {
                    showRuntimeLoggers();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SHOW_JBI_STATISTICS))
                {
                    showStatistics();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SET_JBI_RUNTIME_LOGGER))
                {
                    setRuntimeLoggers();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SET_JBI_COMPONENT_LOGGER))
                {
                    setComponentLoggers();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SET_JBI_RUNTIME_CONFIGURATION))
                {
                    setRuntimeConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.SET_JBI_COMPONENT_CONFIGURATION))
                {
                    setComponentConfiguration();
                    successMsgFlag = false;
                }

                else if (name.equals(JBICommandConstants.CREATE_APPLICATION_CONFIGURATION))
                {
                    createApplicationConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.UPDATE_APPLICATION_CONFIGURATION))
                {
                    updateApplicationConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.CREATE_APPLICATION_VARIABLE))
                {
                    createApplicationVariable();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.UPDATE_APPLICATION_VARIABLE))
                {
                    updateApplicationVariable();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.DELETE_APPLICATION_CONFIGURATION))
                {
                    deleteApplicationConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.DELETE_APPLICATION_VARIABLE))
                {
                    deleteApplicationVariable();
                    successMsgFlag = true;
                }

                else if ((name.equals(JBICommandConstants.LIST_SERVICE_ENGINES)) ||
                         (name.equals(JBICommandConstants.LIST_BINDING_COMPONENTS)))
                {
                    listComponents();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.LIST_SHARED_LIBRARIES))
                {
                    listSharedLibraries();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.LIST_SERVICE_ASSEMBLIES))
                {
                    listServiceAssemblies();
                    successMsgFlag = true;
                }
                
                else if (name.equals(JBICommandConstants.LIST_APPLICATION_CONFIGURATIONS))
                {
                    listApplicationConfiguration();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.LIST_APPLICATION_VARIABLES))
                {
                    listApplicationVariables();
                    successMsgFlag = true;
                }

                else if ((name.equals(JBICommandConstants.START_COMPONENT)) ||
                         (name.equals(JBICommandConstants.STOP_COMPONENT)) ||
                         (name.equals(JBICommandConstants.SHUT_DOWN_COMPONENT)) ||
                         (name.equals(JBICommandConstants.START_SERVICE_ASSEMBLY)) ||
                         (name.equals(JBICommandConstants.STOP_SERVICE_ASSEMBLY)) ||
                         (name.equals(JBICommandConstants.SHUT_DOWN_SERVICE_ASSEMBLY)))
                {
                    lifecycleCommand();
                    successMsgFlag = false;
                }


                else if ((name.equals(JBICommandConstants.INSTALL_COMPONENT)) ||
                         (name.equals(JBICommandConstants.INSTALL_SHARED_LIBRARY)) ||
                         (name.equals(JBICommandConstants.DEPLOY_SERVICE_ASSEMBLY)))
                {
                    installCommands();
                    successMsgFlag = false;
                }

                else if ((name.equals(JBICommandConstants.UNINSTALL_COMPONENT)) ||
                         (name.equals(JBICommandConstants.UNINSTALL_SHARED_LIBRARY)))

                {
                    uninstallCommand();
                    successMsgFlag = false;
                }

                else if (name.equals(JBICommandConstants.UNDEPLOY_SERVICE_ASSEMBLY))
                {
                    undeployJBIServiceAssembly();
                    successMsgFlag = false;
                }

                else if (name.equals(JBICommandConstants.VERIFY_JBI_APPLICATION_ENVIRONMENT))
                {
                    verifyApplicationEnvironment();
                    successMsgFlag = true;
                }

                else if (name.equals(JBICommandConstants.EXPORT_JBI_APPLICATION_ENVIRONMENT))
                {
                    exportApplicationEnvironment();
                    successMsgFlag = true;
                }

                // Display the default success message it terse is false
                if (successMsgFlag)
                {
                    CLILogger.getInstance().printDetailMessage (
                    getLocalizedString ("CommandSuccessful",new Object[] {name} ));
                }
            }
        }

        // Will process any of the following exception that might of been thrown:
        // JBIRemoteException, CommandException or a CommandValidationException
        // If the Exception is truly an Error (not a Warning), a new CommandException
        // will be thrown, and the cli framework will process the error and perform
        // the System.exit(1).
        catch (Exception e)
        {
            //System.out.println ("------------ Debug Info -----------");
            //System.out.println ("Exception: " + e);
            //System.out.println ("Message: " + e.getMessage());
            //System.out.println ("-----------------------------------");
            processTaskException(e);
        }
    }


    /**
     * Will list the application configurations for the specified component.
     * @throws JBIRemoteException
     */
    private void upgradeComponent() throws JBIRemoteException, CommandException
    {
        // Retrieve the operand "componentName"
        String  compName = (String) getOperands().get(0);

        // Retrieve the required operand "upgradefile"
        String archiveFilePath = (String) getOption(JBICommandConstants.UPGRADE_FILE_OPTION);

        // In addition to validating the archiveFilePath,
        // this converts a relative file name to an absolute path based on
        // the current working directory when the asadmin command was issued
        String errorKey = "JBIUpgradeFileNotFound";
        archiveFilePath = validateFilePath (errorKey, archiveFilePath);

        String result = mJBICmds.updateComponent(compName, archiveFilePath);
        processJBIAdminResult (result);
    }


    /**
     * Will list the application variables for the specified component.
     * @throws JBIRemoteException
     */
    private void listApplicationVariables () throws JBIRemoteException
    {
        String tgtName    = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName   = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        Properties result = mJBICmds.getApplicationVariables (compName,tgtName);
        if (result.size() == 0)
        {
            CLILogger.getInstance().printDetailMessage(getLocalizedString("NoElementsToList"));
        } else
        {
            displayProperties(result,0);
        }
    }


    /**
     * Will list the application configurations for the specified component.
     * @throws JBIRemoteException
     */
    private void listApplicationConfiguration () throws JBIRemoteException
    {
        String tgtName    = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName   = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String[] list = mJBICmds.listApplicationConfigurationNames (compName, 
                        tgtName);
        if (list.length == 0)
        {
            CLILogger.getInstance().printDetailMessage(getLocalizedString("NoElementsToList"));
        } else
        {
            for (int i=0; i<list.length; i++)
            {
                displayMessage (list[i],0);
            }
        }
    }


    /**
     * Will verify application (service-assembly).
     * @throws JBIRemoteException, CommandValidationException
     */
    private void verifyApplicationEnvironment () throws JBIRemoteException,
                                                        CommandException
    {
        String templateDir    = (String) getOption(JBICommandConstants.TEMPLATE_DIR_OPTION);
        String tgtName        = (String) getOption(JBICommandConstants.TARGET_OPTION);
        boolean includeDeploy =  getBooleanOption(JBICommandConstants.INCLUDE_DEPLOY_OPTION);
        String operand        = (String) getOperands().get(0);

        // Set the geterate template flag based on if the use specified
        // a template directory.
        boolean generateTemplate = false;
        if ((templateDir != null) && (templateDir.length() > 0))
        {
            generateTemplate = true;
        }
        CompositeData result = mJBICmds.verifyApplication (operand,
                               tgtName,
                               generateTemplate,
                               templateDir,
                               includeDeploy);
        processVerifyResults(result);

        // If template files were created, we need to convert the files from the
        // ant syntax to asadmin syntax.
        if (generateTemplate)
        {
            convertTemplateFilesToAsadmin (templateDir);
            CLILogger.getInstance().printDetailMessage (
            getLocalizedString ("JBIVerifySuccessMessage",new Object[] {templateDir}));
        }
    }


    /**
     * Will export application (service-assembly).
     * @throws JBIRemoteException, CommandValidationException
     */
    private void exportApplicationEnvironment () throws JBIRemoteException,
                                                        CommandException
    {
        String configDir = (String) getOption(JBICommandConstants.CONFIG_DIR_OPTION);
        String tgtName   = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String operand   = (String) getOperands().get(0);

        String result = mJBICmds.exportApplicationConfiguration (operand,
                        tgtName,
                        configDir);

        convertTemplateFilesToAsadmin (configDir);
        CLILogger.getInstance().printDetailMessage (
        getLocalizedString ("JBIExportSuccessMessage",new Object[] {configDir}));
    }


    /**
     * Will show the jbi statistics.
     * @throws JBIRemoteException, CommandValidationException
     */
    private void showStatistics () throws JBIRemoteException,CommandException
    {
        String compName     =  (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String endpointName =  (String) getOption(JBICommandConstants.ENDPOINT_OPTION);
        String saName       =  (String) getOption(JBICommandConstants.SERVICE_ASSEMBLY_OPTION);
        String tgtName      =  (String) getOption(JBICommandConstants.TARGET_OPTION);
        boolean framework   =  getBooleanOption(JBICommandConstants.FRAMEWORK_OPTION);
        boolean nmr         =  getBooleanOption(JBICommandConstants.NMR_OPTION);

        boolean processFlag = false;

        if (framework)
        {
            processFlag = true;
            TabularData result = mJBICmds.getFrameworkStats(tgtName);
            processFrameworkResults(result);
        }

        if (nmr)
        {
            processFlag = true;
            TabularData result = mJBICmds.getNMRStats(tgtName);
            processNMRResults(result);
        }

        if ((compName != null) && (compName.length() > 0))
        {
            processFlag = true;
            TabularData result = mJBICmds.getComponentStats(compName,tgtName);
            processComponentResults (result, compName);
        }

        if ((endpointName != null) && (endpointName.length() > 0))
        {
            processFlag = true;
            TabularData result = mJBICmds.getEndpointStats(endpointName,tgtName);
            processEndpointResults(result,endpointName);
        }

        if ((saName != null) && (saName.length() > 0))
        {
            processFlag = true;
            TabularData result = mJBICmds.getServiceAssemblyStats(saName,tgtName);
            processServiceAssemblyResults(result,saName);
        }

        // Display the error message if no statistic option was specified
        if (!(processFlag))
        {
            CLILogger.getInstance().printMessage (
            getLocalizedString ("JBINoStatisticsOptionSpecified",new Object[] {name}));
        }
    }


    /**
     * Will show the configurations for the specified application configuration.
     * @throws JBIRemoteException, CommandValidationException
     */
    private void showApplicationConfiguration () throws JBIRemoteException
    {
        String tgtName  = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand  = (String) getOperands().get(0);
        Properties result = mJBICmds.getApplicationConfiguration (compName,tgtName,operand);
        displayProperties(result,0);
    }


    /**
     * Will delete the Application Configuration that was specified on the command line.
     * @throws JBIRemoteException
     */
    private void deleteApplicationConfiguration () throws JBIRemoteException,
                                                          CommandException
    {
        String tgtName  = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand  = (String) getOperands().get(0);
        String result = mJBICmds.deleteApplicationConfiguration (compName,tgtName,operand);
        processJBIAdminResult (result);
    }


    /**
     * Will delete the Application Variable(s) that was specified on the command line.
     * @throws JBIRemoteException
     */
    private void deleteApplicationVariable () throws JBIRemoteException,
                                                     CommandException
    {
        String tgtName  = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand  = (String) getOperands().get(0);
        String variables[] = operand.split(",");

        for (int i=0; i<variables.length; i++)
        {
            variables[i] = variables[i].trim();
        }
        String result = mJBICmds.deleteApplicationVariables (compName,tgtName,variables);
        processJBIAdminResult (result);
    }


    /**
     * Will update Application Variable(s).
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void updateApplicationVariable () throws JBIRemoteException,
                                                     CommandValidationException,
                                                     CommandException
    {
        String tgtName  = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand  = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }
        String result = mJBICmds.setApplicationVariables (compName, tgtName, properties);
        processJBIAdminResult (result);
    }


    /**
     * Will create Application Variable(s).
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void createApplicationVariable () throws JBIRemoteException, 
                                                     CommandValidationException,
                                                     CommandException
    {
        String tgtName    = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName   = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand    = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }

        properties = insertType (properties);
        String result = mJBICmds.addApplicationVariables (compName, tgtName, properties);
        processJBIAdminResult (result);
    }


    /**
     * Will update an Application Configuration.
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void updateApplicationConfiguration () throws JBIRemoteException, 
                                                          CommandValidationException,
                                                          CommandException
    {
        String tgtName    = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName   = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String configName = (String) getOption(JBICommandConstants.CONFIG_NAME_OPTION);
        String operand    = (String) getOperands().get(0);

        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }

        String result = mJBICmds.setApplicationConfiguration (compName, tgtName, configName, properties);
        processJBIAdminResult (result);
    }


    /**
     * Will create an Application Configuration.
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void createApplicationConfiguration () throws JBIRemoteException, 
                                                          CommandValidationException,
                                                          CommandException
    {
        String tgtName    = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName   = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String configName = (String) getOption(JBICommandConstants.CONFIG_NAME_OPTION);
        String operand    = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }
        String result = mJBICmds.addApplicationConfiguration (compName, tgtName, configName, properties);
        processJBIAdminResult (result);
    }


    /**
     * Will set a component logger level value(s).
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void setComponentLoggers() throws JBIRemoteException, 
                                              CommandValidationException,
                                              CommandException
    {
        String tgtName  = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand  = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }
        Enumeration e = properties.propertyNames();
        while (e.hasMoreElements())
        {
            String loggerName = (String) e.nextElement();
            String loggerLevel = (String) properties.getProperty(loggerName);
            Level logLevel = null;
            if (!(loggerLevel.equalsIgnoreCase(LOG_LEVEL_DEFAULT)))
            {
                logLevel = Level.parse(loggerLevel);
            }
            mJBICmds.setComponentLoggerLevel(compName,
                                             loggerName,
                                             logLevel,
                                             tgtName,
                                             tgtName);
        }
    }


    /**
     * Will set the runtime logger level value(s).
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void setRuntimeLoggers() throws JBIRemoteException, 
                                            CommandValidationException,
                                            CommandException
    {
        String tgtName = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String operand = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }
        Enumeration e = properties.propertyNames();
        while (e.hasMoreElements())
        {
            String loggerName = (String) e.nextElement();
            String loggerLevel = (String) properties.getProperty(loggerName);
            Level logLevel = null;
            if (!(loggerLevel.equalsIgnoreCase(LOG_LEVEL_DEFAULT)))
            {
                logLevel = Level.parse(loggerLevel);
            }
            mJBICmds.setRuntimeLoggerLevel(loggerName,logLevel,tgtName);
        }
    }

    /**
     * Will set a component configuration value(s).
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void setComponentConfiguration() throws JBIRemoteException, 
                                                    CommandValidationException,
                                                    CommandException
    {
        String tgtName = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String compName = (String) getOption(JBICommandConstants.COMPONENT_OPTION);
        String operand = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }
        String result = mJBICmds.setComponentConfiguration(tgtName,compName,properties);
        processJBIAdminResult (result);
    }


    /**
     * Will set the runtime configuration value(s).
     * @throws JBIRemoteException,CommandValidationException,CommandException
     */
    private void setRuntimeConfiguration() throws JBIRemoteException, 
                                                  CommandValidationException,
                                                  CommandException
    {
        String tgtName = (String) getOption(JBICommandConstants.TARGET_OPTION);
        String operand = (String) getOperands().get(0);
        Properties properties = new Properties();
        try
        {
            properties.load(new FileInputStream(operand));
        } catch (IOException e)
        {
            properties = createPropertiesParam(operand);
        }
        mJBICmds.setRuntimeConfiguration(properties,tgtName);
    }


    /**
     * Will display the runtime configuration values.
     * @throws JBIRemoteException
     */
    private void showRuntimeConfiguration() throws JBIRemoteException
    {
        String tgtName = (String) getOption(JBICommandConstants.TARGET_OPTION);
        Properties result = mJBICmds.getRuntimeConfiguration(tgtName);
        displayProperties(result,0);
    }


    /**
     * Will display the Runtime logger levels.
     * @throws JBIRemoteException
     */
    private void showRuntimeLoggers() throws JBIRemoteException
    {
        String tgtName = (String) getOption(JBICommandConstants.TARGET_OPTION);
        Map result = mJBICmds.getRuntimeLoggerLevels(tgtName);
        if ( result.containsValue(null)) {
            processLogLevelValues(result);
        }
        displayMap(result,0);
    }

    /**
     * If the log level is null then replace it with DEFAULT.
     */
    private void processLogLevelValues(Map map)
    {
        Set <String> keys = map.keySet();
        for (String key : keys) {
            if ( map.get(key) == null ) {
                map.put(key, LOG_LEVEL_DEFAULT.toUpperCase());
            }
        }
    }

    /**
     * Will display information for the specified service assembly.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void showServiceAssembly() throws JBIRemoteException, 
                                              CommandValidationException,
                                              CommandException
    {
        String saName      = (String) getOperands().get(0);
        String tgtName     = (String) getOption(JBICommandConstants.TARGET_OPTION);
        boolean descriptor = getBooleanOption(JBICommandConstants.DESCRIPTOR_OPTION);

        String result = mJBICmds.showServiceAssembly(saName,"","",tgtName);
        processShowAssemblyResult(result,saName);

        if (descriptor)
        {
            displayOptionHeader("JBIShowComponentDescriptorHeader",0);
            result = mJBICmds.getServiceAssemblyDeploymentDescriptor(saName);
            displayDescriptor(result);
        }

    }


    /**
     * Will display information for the specified shared library.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void showSharedLibrary() throws JBIRemoteException, 
                                            CommandValidationException,
                                            CommandException
    {
        String compName = (String) getOperands().get(0);
        String tgtName  = (String) getOption(JBICommandConstants.TARGET_OPTION);
        boolean descriptor = getBooleanOption(JBICommandConstants.DESCRIPTOR_OPTION);

        String result = mJBICmds.showSharedLibrary(compName,"",tgtName);
        processShowLibraryResult(result,compName);         

        if (descriptor)
        {
            displayOptionHeader("JBIShowComponentDescriptorHeader",0);
            result = mJBICmds.getSharedLibraryInstallationDescriptor(compName);
            displayDescriptor(result);
        }
    }


    /**
     * Will display information for the specified component.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void showComponent() throws JBIRemoteException, 
                                        CommandValidationException,
                                        CommandException
    {
        String  compName      = (String) getOperands().get(0);
        String  tgtName       = (String) getOption(JBICommandConstants.TARGET_OPTION);
        boolean loggers       = getBooleanOption(JBICommandConstants.LOGGERS_OPTION);
        boolean configuration = getBooleanOption(JBICommandConstants.CONFIGURATION_OPTION);
        boolean descriptor    = getBooleanOption(JBICommandConstants.DESCRIPTOR_OPTION);

        // Retrieve and display the general information
        String resultStr = "";
        if (name.equals(JBICommandConstants.SHOW_JBI_SERVICE_ENGINE))
        {
            resultStr = mJBICmds.showServiceEngine (compName,"","","",tgtName);
        } else
        {
            resultStr = mJBICmds.showBindingComponent (compName,"","","",tgtName);
        }
        processShowComponentResult(resultStr,compName);

        if (loggers)
        {
            displayOptionHeader("JBIShowComponentLoggerHeader",0);
            Map result = mJBICmds.getComponentLoggerLevels (compName,tgtName,tgtName);
            displayMap(result,0);
        }

        if (configuration)
        {
            displayOptionHeader("JBIShowComponentConfigurationHeader",0);
            Properties result = mJBICmds.getComponentConfiguration(compName,tgtName);
            if (result.size() == 0)
            {
                if (mInfo.getState().equals(JBIComponentInfo.SHUTDOWN_STATE))
                {
                    CLILogger.getInstance().printMessage (
                    getLocalizedString ("JBIShowComponentConfigShutDownWarning"));
                }
            } else
            {
                displayProperties(result,0);
            }
        }

        if (descriptor)
        {
            displayOptionHeader("JBIShowComponentDescriptorHeader",0);
            String result = mJBICmds.getComponentInstallationDescriptor(compName);
            displayDescriptor(result);
        }

        displayMessage (" ",0);
    }


    /**
     * Will display information for the specified component.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void installCommands () throws JBIRemoteException,
                                           CommandValidationException,
                                           CommandException
    {
        String  operand = (String) getOperands().get(0);
        String  tgtName = (String) getOption(JBICommandConstants.TARGET_OPTION);

        String  result = "";
        String  successKey = "";
        String  errorKey = "";
        boolean installFromDomain = false;

        // Make sure the file specified is valid. If it is not a valid file, then
        // we should try to install/deploy the component/assembly from the domain.
        // If that should fail, then we will throw the original commandException,
        // saying that the filePath specified was not valid.
        try
        {
            if (name.equals(JBICommandConstants.DEPLOY_SERVICE_ASSEMBLY))
            {
                errorKey = "JBIDeloymentFileNotFound";
            } 
            else
            {
                errorKey = "JBIInstallationFileNotFound";
            }
            operand = validateFilePath (errorKey,operand);
        } 
        catch (CommandException ce)
        {
            if (!(tgtName.equalsIgnoreCase(JBIAdminCommands.DOMAIN_TARGET_KEY)))
            {
                installFromDomain = true;

                if (name.equals(JBICommandConstants.INSTALL_COMPONENT))
                {
                    result = mJBICmds.installComponentFromDomain(operand,tgtName);
                    successKey = "JBISuccessInstallDomainComponent";
                }

                else if (name.equals(JBICommandConstants.INSTALL_SHARED_LIBRARY))
                {
                    result = mJBICmds.installSharedLibraryFromDomain(operand,tgtName);
                    successKey = "JBISuccessInstallDomainSharedLibrary";
                }

                else if (name.equals(JBICommandConstants.DEPLOY_SERVICE_ASSEMBLY))
                {
                    result = mJBICmds.deployServiceAssemblyFromDomain(operand,tgtName);
                    successKey = "JBISuccessDeployServiceAssembly";
                }
                processJBIAdminResult (result, successKey);
            }
        }

        if (!(installFromDomain))
        {
            // Using the command name, we'll determine how to process the command
            if (name.equals(JBICommandConstants.INSTALL_COMPONENT))
            {

                // When installing a component, two api's exist, one when installing
                // a component with no associated properties, and one when installing
                // a component with associated properties.
                Properties properties = getAnyProperties();
                result = mJBICmds.installComponent(operand,properties,tgtName);
                successKey = "JBISuccessInstallComponent";
            }

            else if (name.equals(JBICommandConstants.INSTALL_SHARED_LIBRARY))
            {
                result = mJBICmds.installSharedLibrary (operand,tgtName);
                successKey = "JBISuccessInstallSharedLibrary";
            }

            else if (name.equals(JBICommandConstants.DEPLOY_SERVICE_ASSEMBLY))
            {
                result = mJBICmds.deployServiceAssembly (operand,tgtName);
                successKey = "JBISuccessDeployServiceAssembly";
            }
            processJBIAdminResult (result, successKey);
        }
    }


    /**
     * Will uninstall a component or a shared library.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void uninstallCommand() throws JBIRemoteException, 
                                           CommandValidationException
    {
        String result = "";
        String successKey = "";

        String  compName    = (String) getOperands().get(0);
        String  tgtName     = getOption(JBICommandConstants.TARGET_OPTION);
        boolean force       = getBooleanOption(JBICommandConstants.FORCE_OPTION);
        boolean keepArchive = getBooleanOption(JBICommandConstants.KEEP_ARCHIVE_OPTION);

        if (name.equals(JBICommandConstants.UNINSTALL_COMPONENT))
        {
            result = ((JBIAdminCommands) mJBICmds).uninstallComponent(
                    compName,
                    force,
                    keepArchive,
                    tgtName);
            successKey = "JBISuccessUninstallComponent";
        }

        else if (name.equals(JBICommandConstants.UNINSTALL_SHARED_LIBRARY))
        {
            result = ((JBIAdminCommands) mJBICmds).uninstallSharedLibrary(
                    compName,
                    force,
                    keepArchive,
                    tgtName);
            successKey = "JBISuccessUninstallSharedLibrary";
        }

        // Display the success message
        CLILogger.getInstance().printDetailMessage (
        getLocalizedString (successKey,new Object[] {compName}));

    }


    /**
     * Will undeploy a service assembly.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void undeployJBIServiceAssembly() throws JBIRemoteException, 
                                                     CommandValidationException
    {
        String  compName    = (String) getOperands().get(0);
        String  tgtName     = getOption(JBICommandConstants.TARGET_OPTION);
        boolean force       = getBooleanOption(JBICommandConstants.FORCE_OPTION);
        boolean keepArchive = getBooleanOption(JBICommandConstants.KEEP_ARCHIVE_OPTION);

        String result = ((JBIAdminCommands) mJBICmds).undeployServiceAssembly(
                        compName,
                        force,
                        keepArchive,
                        tgtName);
        String successKey = "JBISuccessUndeployServiceAssembly";

        // Display the success message
        CLILogger.getInstance().printDetailMessage (
        getLocalizedString (successKey,new Object[] {compName}));

    }


    /**
     * Will list the jbi components.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void listComponents() throws JBIRemoteException,
                                         CommandValidationException
    {
        // Retrieve the options used for this command
        String  tgtName        = getOption(JBICommandConstants.TARGET_OPTION);
        String  libraryName    = getOption(JBICommandConstants.LIBRARY_NAME_OPTION);                
        String  assemblyName   = getOption(JBICommandConstants.ASSEMBLY_NAME_OPTION);  
        String  lifecycleState = getOption(JBICommandConstants.LIFECYCLE_STATE_OPTION,mValidStates);                        

        String result = "";

        if (name.equals(JBICommandConstants.LIST_SERVICE_ENGINES))
        {
            result = ((JBIAdminCommands) mJBICmds).listServiceEngines(
                     lifecycleState,
                     libraryName,
                     assemblyName,
                     tgtName);
            processJBIAdminComponentListResult(result);
        }

        else if (name.equals(JBICommandConstants.LIST_BINDING_COMPONENTS))
        {
            result = ((JBIAdminCommands) mJBICmds).listBindingComponents(
                     lifecycleState,
                     libraryName,
                     assemblyName,
                     tgtName);
            processJBIAdminComponentListResult(result);
        }

    }


    /**
     * Will list the shared libraries.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void listSharedLibraries() throws JBIRemoteException,
                                              CommandValidationException
    {
        // Retrieve the options used for this command
        String  tgtName   = getOption(JBICommandConstants.TARGET_OPTION);
        String  compName  = getOption(JBICommandConstants.COMPONENT_NAME_OPTION);  

        String result = ((JBIAdminCommands) mJBICmds).listSharedLibraries(
                 compName,
                 tgtName);
        processJBIAdminComponentListResult(result);
       
    }


    /**
     * Will list the shared libraries.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void listServiceAssemblies() throws JBIRemoteException,
                                                CommandValidationException
    {
        // Retrieve the options used for this command
        String  tgtName        = getOption(JBICommandConstants.TARGET_OPTION);
        String  compName       = getOption(JBICommandConstants.COMPONENT_NAME_OPTION);  
        String  lifecycleState = getOption(JBICommandConstants.LIFECYCLE_STATE_OPTION,mValidStates);                        

        String result = ((JBIAdminCommands) mJBICmds).listServiceAssemblies(
                 lifecycleState,
                 compName,
                 tgtName);
        processJBIAdminAsseblyListResult(result);
       
    }



    /**
     * Will list the shared libraries.
     * @throws JBIRemoteException,CommandValidationException
     */
    private void lifecycleCommand() throws JBIRemoteException,
                                           CommandValidationException,
                                           CommandException
    {
        String result = "";
        String successKey = "";

        String  tgtName  = getOption(JBICommandConstants.TARGET_OPTION);
        boolean force    = getBooleanOption(JBICommandConstants.FORCE_OPTION);
        String  compName = (String) getOperands().get(0);

        if (name.equals(JBICommandConstants.START_COMPONENT)) {
            result = ((JBIAdminCommands) mJBICmds).startComponent(
                compName,
                tgtName);
            successKey = "JBISuccessStartedComponent";
        }

        else if (name.equals(JBICommandConstants.STOP_COMPONENT)) {
            result = ((JBIAdminCommands) mJBICmds).stopComponent(
                compName,
                tgtName);
            successKey = "JBISuccessStoppedComponent";
        }

        else if (name.equals(JBICommandConstants.SHUT_DOWN_COMPONENT)) {
            result = ((JBIAdminCommands) mJBICmds).shutdownComponent(
                compName,
                force,
                tgtName);
            successKey = "JBISuccessShutDownComponent";
        }
        else if (name.equals(JBICommandConstants.START_SERVICE_ASSEMBLY)) {
            result = ((JBIAdminCommands) mJBICmds).startServiceAssembly(
                compName,
                tgtName);
            successKey = "JBISuccessStartAssemblyComponent";
        }

        else if (name.equals(JBICommandConstants.STOP_SERVICE_ASSEMBLY)) {
            result = ((JBIAdminCommands) mJBICmds).stopServiceAssembly(
                compName,
                tgtName);
            successKey = "JBISuccessStoppedAssembly";
        }

        else if (name.equals(JBICommandConstants.SHUT_DOWN_SERVICE_ASSEMBLY)) {
            result = ((JBIAdminCommands) mJBICmds).shutdownServiceAssembly(
                compName,
                force,
                tgtName);
            successKey = "JBISuccessShutDownAssembly";
        }

        processJBIAdminResult (result, successKey);
    }

}
