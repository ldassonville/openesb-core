#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#
#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for regression tests.


#-------------------------------------------------------------------------
# Use the Global regress setup.
#-------------------------------------------------------------------------
. $SRCROOT/antbld/regress/common_defs.ksh

#-------------------------------------------------------------------------
# Create pointer to the test directory (different for the two environments)
#-------------------------------------------------------------------------
export UI_REGRESS_DIST_DIR
JV_SVC_TEST_CLASSES=$JV_SVC_BLD/regress
if [ $OPENESB_BUILD -eq 1 ]; then
    UI_REGRESS_DIST_DIR=$JV_SVC_TEST_CLASSES/testdata
else
    UI_REGRESS_DIST_DIR=$JV_SVC_TEST_CLASSES/dist
fi

#-------------------------------------------------------------------------
# Create the pointer to the asadmin script file.  We do this so we can override
#-------------------------------------------------------------------------
export TEST_ASADMIN
TEST_ASADMIN=$AS8BASE/bin/asadmin
if [ "$FORTE_PORT" = "cygwin" -o "$FORTE_PORT" = "nt" ]; then
    TEST_ASADMIN=${JV_SVC_BLD}/regress/test_asadmin.bat
fi  

#-------------------------------------------------------------------------
# Set the password file
#-------------------------------------------------------------------------
export PASSWORD_FILE
PASSWORD_FILE=$AS8BASE/passwords

#-------------------------------------------------------------------------
# Set the port number
#-------------------------------------------------------------------------
export ADMIN_PORT
ADMIN_PORT=$ASADMIN_PORT

#-------------------------------------------------------------------------
# Set the user
#-------------------------------------------------------------------------
export ADMIN_USER
ADMIN_USER=$AS_ADMIN_USER

#-------------------------------------------------------------------------
# Will remove any service assemblies, components or libraries that may
# be in the repository.  Note the target needs to be specified
#-------------------------------------------------------------------------
cleanup()
{
  $AS8BASE/bin/asadmin shut-down-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sa4 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS PingApp >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SA1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SA2 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly  --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_assembly_unit_1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly  --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_sa4 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly  --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force PingApp >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly  --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force SA1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly  --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force SA2 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_binding1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_binding2 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine2 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine3 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine4 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli-config-binding >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force test-component >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force test-component1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin shut-down-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force SimpleTestEngine >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_binding1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_binding2 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine2 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine3 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli_test_engine4 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force cli-config-binding >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force test-component >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force test-component1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-component        --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --force SimpleTestEngine >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library   --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  cli_test_sns1 >> $TEMP_OUTPUT_FILE 2>&1
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library   --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS  cli_test_sns2 >> $TEMP_OUTPUT_FILE 2>&1
}


#-------------------------------------------------------------------------
# Will start a component, redirecting the output to the TEMP file.
#-------------------------------------------------------------------------
start_component()
{
  $AS8BASE/bin/asadmin start-jbi-component --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $2 >> $TEMP_OUTPUT_FILE 2>&1
}


#-------------------------------------------------------------------------
# Will shut down a component, redirecting the output to the TEMP file.
#-------------------------------------------------------------------------
shut_down_component()
{
  $AS8BASE/bin/asadmin shut-down-jbi-component --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $2 >> $TEMP_OUTPUT_FILE 2>&1
}


#-------------------------------------------------------------------------
# Will install a component, redirecting the output to the TEMP file.
#-------------------------------------------------------------------------
install_component()
{
  $AS8BASE/bin/asadmin install-jbi-component --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $2 >> $TEMP_OUTPUT_FILE 2>&1
}


#-------------------------------------------------------------------------
# Will install a shared library, redirecting the output to the TEMP file.
#-------------------------------------------------------------------------
install_shared_library()
{
  $AS8BASE/bin/asadmin install-jbi-shared-library --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $2 >> $TEMP_OUTPUT_FILE 2>&1
}


#-------------------------------------------------------------------------
# Will start the service assembly, redirecting the output to the TEMP file.
#-------------------------------------------------------------------------
start_assembly()
{
  $AS8BASE/bin/asadmin start-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $2 
}

#-------------------------------------------------------------------------
# Will deploy the service assembly, redirecting the output to the TEMP file.
#-------------------------------------------------------------------------
deploy_assembly()
{
  $AS8BASE/bin/asadmin deploy-jbi-service-assembly --target $1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $2 
}


#-------------------------------------------------------------------------
# Build the test artifacts (i.e. all the archive files used for testing)
#-------------------------------------------------------------------------
build_test_artifacts()
{
    echo "-------------------------------------------------------------------"
    echo " Building the test artifacts"
    echo "-------------------------------------------------------------------"
    ant -emacs -q -f $JV_SVC_REGRESS/scripts/build-test-components.ant >> $TEMP_OUTPUT_FILE
    return 0
}


#-------------------------------------------------------------------------
# Build the artifacts used for application configuration tests
#-------------------------------------------------------------------------
build_test_application_artifacts()
{
    echo "-------------------------------------------------------------------"
    echo " Building and packaging the test component cli-config-binding"
    echo "-------------------------------------------------------------------"
    ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $JV_SVC_REGRESS/scripts/build-cli-config-binding.ant pkg.test.component >> $TEMP_OUTPUT_FILE 2>&1
    return 0
}


#-------------------------------------------------------------------------
# Build the artifacts used for application configuration tests
#-------------------------------------------------------------------------
build_test_engine_with_endpoints_artifacts()
{
    echo "-------------------------------------------------------------------"
    echo " Building and packaging the test simple test engine with endpoints"
    echo "-------------------------------------------------------------------"
    ant -q  -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f jbi-admin-cli00013.xml   
    #ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $JV_SVC_REGRESS/scripts/build-test-engine-with-endpoints.ant >> $TEMP_OUTPUT_FILE 2>&1
    return 0
}


#----------------------------------------------------------------------------------
# Will echo some verbage that explains what this regression test is doing
#----------------------------------------------------------------------------------
display_test_info()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start Test : $TEST_NAME"
  echo " Description: $TEST_DESCRIPTION"
  echo "-------------------------------------------------------------------"
}


#----------------------------------------------------------------------------------
# Perform the test initialization, which consist of echoing the test description,
# building the test artifacts and creating the temporary output file.
#----------------------------------------------------------------------------------
initilize_test()
{
 create_temp_output_file
 display_test_info
 build_test_artifacts
}


#-------------------------------------------------------------------------
# Create the tempory output file.  This file will be used to redirect
# the vairable output too.  This is mostly used when setting up the
# test environment before running the test.
#-------------------------------------------------------------------------
create_temp_output_file()
{
  TEMP_OUTPUT_FILE="$JV_SVC_BLD/tmp/$TEST_NAME.out"
  mkdir -p $JV_SVC_BLD/tmp
  #echo "test_setup (logging to ..$TEMP_OUTPUT_FILE)"
  #echo "test_setup" > $TEMP_OUTPUT_FILE 2>&1
}


#-------------------------------------------------------------------------
# Filter list on specified service assembly name.  This is a safe way
# to test the list.
#-------------------------------------------------------------------------
list_service_assembly()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assembly $1"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies | grep $1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep $1
}


#-------------------------------------------------------------------------
# Filter list on specified shared library name.  This is a safe way
# to test the list.
#-------------------------------------------------------------------------
list_shared_libraries()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Shared Library $1"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-shared-library | grep $1"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep $1
}


#-------------------------------------------------------------------------
# Filter list on specified binding component name.  This is a safe way
# to test the list.
#-------------------------------------------------------------------------
list_binding_components()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Binding Component $1"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-binding-component | grep $1"
  $AS8BASE/bin/asadmin list-jbi-binding-components --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep $1
}


#-------------------------------------------------------------------------
# Filter list on specified service engine name.  This is a safe way
# to test the list.
#-------------------------------------------------------------------------
list_service_engines()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Engine $1"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-engines | grep $1"
  $AS8BASE/bin/asadmin list-jbi-service-engines --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep $1
}


#-------------------------------------------------------------------------
# Perform a clean install --- Currently not used ---
#-------------------------------------------------------------------------
clean_jbi_install() 
{
    rm $JBI_DOMAIN_ROOT/logs/server.log
    rm $JBI_DOMAIN_ROOT/logs/.jbi_admin*
    rm -rf $JBI_DOMAIN_ROOT/jbi/engines
    rm -rf $JBI_DOMAIN_ROOT/jbi/bindings
    rm -rf $JBI_DOMAIN_ROOT/jbi/sharedlibraries
    rm -rf $JBI_DOMAIN_ROOT/jbi/repository
    rm -rf $JBI_DOMAIN_ROOT/jbi/system/deployment
    rm -rf $JBI_DOMAIN_ROOT/jbi/Trash
    mkdir $JBI_DOMAIN_ROOT/jbi/repository
    mkdir $JBI_DOMAIN_ROOT/jbi/system/deployment
    return 0;
}
