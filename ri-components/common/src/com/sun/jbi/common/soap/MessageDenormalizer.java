/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageDenormalizer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common.soap;

import javax.jbi.messaging.Fault;
import javax.jbi.messaging.NormalizedMessage;


/**
 * This object is used by <code>MessageProcessor</code> implementations to
 * denormalize a JBI NormalizedMessage and convert it into a SOAP Message.
 * The object maps the Normalized Message's message content to SOAP:Body element in the
 * SOAP Message. The SOAP:header elements are extracted from a Normalized Message's
 * message context property "SoapHeader".
 *
 * @author Sun Microsystems, Inc.
 */
public interface MessageDenormalizer
{
    /**
     * Converts a normalized message to a SOAP Message.
     *
     * @param normalizedMessage message to be denormalized.
     * @param operation operation invoked
     * @param isResponse indicates if a response messages needs to be generated
     *
     * @return the SOAP Message.
     */
    SOAPWrapper denormalizeMessage(
        NormalizedMessage normalizedMessage, Operation operation, boolean isResponse);

    /**
     * Converts a fault mesage to a SOAP Message using the specified fault code.
     *
     * @param faultMessage fault message.
     *
     * @return a new SOAPWrapper instance which contains the SOAP fault Message.
     */
    SOAPWrapper denormalizeFaultMessage(Fault faultMessage);

    /**
     * Converts an exception to a SOAP Message. It uses the Server fault code in the soap
     * namespace.
     *
     * @param exception exception instance
     *
     * @return denormalized exception instance.
     */
    SOAPWrapper denormalizeMessage(Exception exception);

    /**
     * Converts an exception to a SOAP Message. It uses the faultCode passed. The code
     * expects the faultcode passed to be part of the soap namespace.
     *
     * @param exception exception instance
     * @param faultCode fault code
     *
     * @return denormalized exception instance.
     */
    SOAPWrapper denormalizeMessage(Exception exception, String faultCode);
}
