/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageNormalizer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common.soap;

import javax.jbi.JBIException;
import javax.jbi.messaging.NormalizedMessage;


/**
 * This object is used by <code>MessageProcessor</code> implementations to normalize
 * SOAP Message into JBI Normalized Message. The object maps
 * the SOAP:Body element in the SOAP Message to the Normalized Message's message
 * content. The SOAP:header elements are mapped to a property in the Normalized Message
 * MessageContext.  Protocol specific data will not be normalized.
 *
 * @author Sun Microsystems, Inc.
 */
public interface MessageNormalizer
{
    /**
     * Converts a SOAP Message to a NormalizedMessage format.
     *
     * @param soapWrapper request message.
     * @param normalizedMessage jbi specific format.
     * @param operation operation requested.
     *
     * @throws JBIException when the message cannot be normalized.
     */
    void normalizeMessage(
        SOAPWrapper soapWrapper, NormalizedMessage normalizedMessage, Operation operation)
        throws JBIException;
}
