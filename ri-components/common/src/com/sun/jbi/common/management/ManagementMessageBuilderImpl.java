/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementMessageBuilderImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common.management;

import com.sun.jbi.common.DOMUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.StringWriter;

import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * BuildManagementMessageImpl provides utility methods to build messages (as XML string)
 * that are returned to the clients by the JBI Framework.
 *
 * @author Sun Microsystems, Inc.
 */
public class ManagementMessageBuilderImpl implements ManagementMessageBuilder
{
    /**
     * String to represent UNKNOWN elements
     */
    private static final String MISSING_DATA_STRING = "UNKNOWN";

    /**
     * String to represent INVALID input
     */
    private static final String CANNOT_BUILD_MESSAGE_STRING = 
        "CANNOT_BUILD_MESSAGE_INVALID_DATA";
    
    /**
     * Max. nesting level
     */
    private static final int MAX_NESTING_LEVEL = 32;    

    /**
     * Internal handle to the logger instance. 
     */
    private Logger mLogger = null;

    /**
     * Creates a new <CODE>BuildManagementMessageImpl</CODE> instance.
     */
    public ManagementMessageBuilderImpl()
    {
        mLogger = Logger.getLogger("com.sun.jbi.common");
    }

    /**
     * Return an XML string of the task result(either status of exception).  This is the
     * task executed by the component
     *
     * @param cmObj Status details of the component
     *
     * @return XML string with task status
     *
     * @throws Exception If fails to build XML string
     */
    public String buildComponentMessage(ComponentMessageHolder cmObj)
        throws Exception
    {
        String mMsgTypeToBuild = null;
        String compTaskMsg = null;

        try
        {
            mMsgTypeToBuild = cmObj.getComponentMessageType();

            if (mMsgTypeToBuild.equalsIgnoreCase("STATUS_MSG"))
            {
                compTaskMsg = buildComponentTaskStatusMessage(cmObj);
            }

            if (mMsgTypeToBuild.equalsIgnoreCase("EXCEPTION_MSG"))
            {
                compTaskMsg = buildComponentTaskExceptionMessage(cmObj);
            }

            if (compTaskMsg == null)
            {
                compTaskMsg = CANNOT_BUILD_MESSAGE_STRING;
            }
        }
        catch (Exception e)
        {
            throw e;
        }

        return compTaskMsg;
    }

    /**
     * Return an XML string of the task status. This is the task executed by the
     * component
     *
     * @param mmObj Status details of the component
     *
     * @return XML string with task status
     *
     * @throws Exception If fails to build XML string
     */
    public String buildComponentTaskStatusMessage(ComponentMessageHolder mmObj)
        throws Exception
    {
        String cmpStatMsg = null;
        Document doc = null;
        String locToken = null;
        String locMessage = null;
        String locParam = null;

        try
        {
            String componentName = mmObj.getComponentName();

            if (componentName == null)
            {
                componentName = MISSING_DATA_STRING;
            }

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.newDocument();

            Element elem = doc.createElement("component-task-result");
            Element compNameElem = doc.createElement("component-name");
            Element compTaskRsltDtlsElem =
                doc.createElement("component-task-result-details");

            DOMUtil.UTIL.setTextData(compNameElem, componentName);

            Element taskRsltDtlsElem = buildStatusXMLFragment(doc, mmObj);
            compTaskRsltDtlsElem.appendChild(taskRsltDtlsElem);
            elem.appendChild(compNameElem);
            elem.appendChild(compTaskRsltDtlsElem);
            doc.appendChild(elem);

            StringWriter sw = new StringWriter();
            cmpStatMsg = DOMUtil.UTIL.DOM2String(doc, sw);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        return cmpStatMsg;
    }

    /**
     * Return an XML string of the task status. This is the task executed by the
     * framework
     *
     * @param doc XML document which needs to be built
     * @param mmObj name of the task
     *
     * @return Element with framework task status
     *
     * @throws Exception If fails to build framework task status
     */
    public Element buildFrameworkTaskStatusMessage(
        Document doc,
        ManagementMessageHolder mmObj
    ) throws Exception
    {
        Element elem = null;

        try
        {
            String frmwkLocale = mmObj.getFrameworkLocale();

            if (frmwkLocale == null)
            {              
                String errMsg = "Framework Locale cannot be null";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            elem = doc.createElement("jbi-task-result");

            Element frmwkTaskRsltElem = doc.createElement("frmwk-task-result");
            Element frmwkTaskRsltDtlsElem =
                doc.createElement("frmwk-task-result-details");
            Element frmwkLocaleElem = doc.createElement("locale");

            DOMUtil.UTIL.setTextData(frmwkLocaleElem, frmwkLocale);

            Element taskRsltDtlsElem = buildStatusXMLFragment(doc, mmObj);
            frmwkTaskRsltDtlsElem.appendChild(taskRsltDtlsElem);
            frmwkTaskRsltDtlsElem.appendChild(frmwkLocaleElem);
            frmwkTaskRsltElem.appendChild(frmwkTaskRsltDtlsElem);
            elem.appendChild(frmwkTaskRsltElem);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        return elem;
    }

    /**
     * DOCUMENT ME!
     *
     * @param doc DOCUMENT ME!
     * @param mmObj DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    private Element buildStatusXMLFragment(
        Document doc,
        ComponentMessageHolder mmObj
    ) throws Exception
    {
        String cmpStatMsg = null;
        String locToken = null;
        String locMessage = null;
        String[] locParam = null;
        Element taskRsltDtlsElem = null;

        try
        {
            String taskName = mmObj.getTaskName();

            if (taskName == null)
            {
                /* Note : Since this is a helper class which can
                 * be instantiated by the component, JBI resource
                 * bundle cannot be used. Hence strings are hard coded.
                */
                String errMsg =
                    "JBIMA0451: BuildManagementMessage : Task name cannot be null ";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            String taskResult = mmObj.getTaskResult();

            if (taskResult == null)
            {
                /* Note : Since this is a helper class which can
                 * be instantiated by the component, JBI resource
                 * bundle cannot be used. Hence strings are hard coded.
                */
                String errMsg =
                    "JBIMA0456: BuildManagementMessage : Task result cannot be null ";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            String messageType = mmObj.getStatusMessageType();

            if (messageType != null)
            {
                locToken = mmObj.getLocToken(1);

                if (locToken == null)
                {
                    locToken = MISSING_DATA_STRING;
                }

                locMessage = mmObj.getLocMessage(1);

                if (locMessage == null)
                {
                    locMessage = MISSING_DATA_STRING;
                }

                locParam = (String[]) mmObj.getLocParam(1);
            }

            Element[] msgLocInfoElem = new Element[MAX_NESTING_LEVEL];
            Element[] locTokenElem = new Element[MAX_NESTING_LEVEL];
            Element[] locMessageElem = new Element[MAX_NESTING_LEVEL];
            Element[] taskStatMsgElem = new Element[MAX_NESTING_LEVEL];
            taskRsltDtlsElem = doc.createElement("task-result-details");

            Element taskIdElem = doc.createElement("task-id");
            Element taskRsltStatElem = doc.createElement("task-result");
            Element msgTypeElem = doc.createElement("message-type");
            taskStatMsgElem[0] = doc.createElement("task-status-msg");
            msgLocInfoElem[0] = doc.createElement("msg-loc-info");
            locTokenElem[0] = doc.createElement("loc-token");
            locMessageElem[0] = doc.createElement("loc-message");

            DOMUtil.UTIL.setTextData(taskIdElem, taskName);
            DOMUtil.UTIL.setTextData(taskRsltStatElem, taskResult);
            taskRsltDtlsElem.appendChild(taskIdElem);
            taskRsltDtlsElem.appendChild(taskRsltStatElem);
            int j = 0;

            if (messageType != null)
            {
                DOMUtil.UTIL.setTextData(msgTypeElem, messageType);

                while ((locToken != null) && (locMessage != null))
                {
                    DOMUtil.UTIL.setTextData(locTokenElem[j], locToken);
                    DOMUtil.UTIL.setTextData(locMessageElem[j], locMessage);
                    taskRsltDtlsElem.appendChild(msgTypeElem);
                    msgLocInfoElem[j].appendChild(locTokenElem[j]);
                    msgLocInfoElem[j].appendChild(locMessageElem[j]);

                    if (locParam != null)
                    {
                        Element[] locParamElem = new Element[MAX_NESTING_LEVEL];

                        for (int k = 0; k < locParam.length; k++)
                        {
                            locParamElem[k] = doc.createElement("loc-param");
                            DOMUtil.UTIL.setTextData(locParamElem[k], locParam[k]);
                            msgLocInfoElem[j].appendChild(locParamElem[k]);
                        }
                    }

                    taskStatMsgElem[j].appendChild(msgLocInfoElem[j]);
                    taskRsltDtlsElem.appendChild(taskStatMsgElem[j]);

                    j++;
                    locToken = mmObj.getLocToken(j + 1);
                    locMessage = mmObj.getLocMessage(j + 1);
                    locParam = mmObj.getLocParam(j + 1);
                    taskStatMsgElem[j] = doc.createElement("task-status-msg");
                    msgLocInfoElem[j] = doc.createElement("msg-loc-info");
                    locTokenElem[j] = doc.createElement("loc-token");
                    locMessageElem[j] = doc.createElement("loc-message");
                }          
                
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        return taskRsltDtlsElem;
    }

    /**
     * DOCUMENT ME!
     *
     * @param doc DOCUMENT ME!
     * @param mmObj DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    public Element buildExceptionXMLFragment(
        Document doc,
        ComponentMessageHolder mmObj
    ) throws Exception
    {
        String cmpStatMsg = null;
        String locToken = null;
        String locMessage = null;
        String[] locParam = null;
        Throwable exObj = null;
        Element taskRsltDtlsElem = null;

        try
        {
            String taskName = mmObj.getTaskName();

            if (taskName == null)
            {
                /* Note : Since this is a helper class which can
                 * be instantiated by the component, JBI resource
                 * bundle cannot be used. Hence strings are hard coded.
                */
                String errMsg =
                    "JBIMA0452: BuildManagementMessage : Task name cannot be null ";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            String taskResult = mmObj.getTaskResult();

            if (taskResult == null)
            {
                /* Note : Since this is a helper class which can
                 * be instantiated by the component, JBI resource
                 * bundle cannot be used. Hence strings are hard coded.
                 *
                */
                String errMsg =
                    "JBIMA0453: BuildManagementMessage : Task result cannot be null ";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            String messageType = mmObj.getExceptionMessageType();

            locToken = mmObj.getLocToken(1);

            if (locToken == null)
            {
                locToken = MISSING_DATA_STRING;
            }

            locMessage = mmObj.getLocMessage(1);

            locParam = mmObj.getLocParam(1);

            exObj = mmObj.getExceptionObject();

            taskRsltDtlsElem = doc.createElement("task-result-details");

            Element taskIdElem = doc.createElement("task-id");
            Element taskRsltStatElem = doc.createElement("task-result");
            Element msgTypeElem = doc.createElement("message-type");
            DOMUtil.UTIL.setTextData(taskIdElem, taskName);
            DOMUtil.UTIL.setTextData(taskRsltStatElem, taskResult);

            if (messageType != null)
            {
                DOMUtil.UTIL.setTextData(msgTypeElem, messageType);
                taskRsltDtlsElem.appendChild(msgTypeElem);
            }

            taskRsltDtlsElem.appendChild(taskIdElem);
            taskRsltDtlsElem.appendChild(taskRsltStatElem);

            if (messageType != null)
            {
                DOMUtil.UTIL.setTextData(msgTypeElem, messageType);
                taskRsltDtlsElem.appendChild(msgTypeElem);
            }

            if (exObj == null)
            {
                Element exInfoElem = doc.createElement("exception-info");
                Element msgLocInfoElem = doc.createElement("msg-loc-info");
                Element locTokenElem = doc.createElement("loc-token");
                Element locMessageElem = doc.createElement("loc-message");
                Element nestingLevelElem = doc.createElement("nesting-level");
                Element stckTraceElem = doc.createElement("stack-trace");

                DOMUtil.UTIL.setTextData(locTokenElem, locToken);

                if (locMessage != null)
                {
                    DOMUtil.UTIL.setTextData(locMessageElem, locMessage);
                }
                else
                {
                    DOMUtil.UTIL.setTextData(locMessageElem, MISSING_DATA_STRING);
                }

                DOMUtil.UTIL.setTextData(nestingLevelElem, MISSING_DATA_STRING);
                DOMUtil.UTIL.setTextData(stckTraceElem, MISSING_DATA_STRING);
                
                msgLocInfoElem.appendChild(locTokenElem);
                msgLocInfoElem.appendChild(locMessageElem);
                
                if (locParam != null)
                {
                    Element[] locParamElem = new Element[MAX_NESTING_LEVEL];

                    for (int i = 0; i < locParam.length; i++)
                    {
                        locParamElem[i] = doc.createElement("loc-param");
                        DOMUtil.UTIL.setTextData(locParamElem[i], locParam[i]);
                        msgLocInfoElem.appendChild(locParamElem[i]);
                    }
                }

                exInfoElem.appendChild(nestingLevelElem);
                exInfoElem.appendChild(msgLocInfoElem);
                exInfoElem.appendChild(stckTraceElem);
                taskRsltDtlsElem.appendChild(exInfoElem);
            }
            else
            {
                int nestingLevel = 1;
                Element[] exInfoElem = new Element[MAX_NESTING_LEVEL];
                Element[] msgLocInfoElem = new Element[MAX_NESTING_LEVEL];
                Element[] locTokenElem = new Element[MAX_NESTING_LEVEL];
                Element[] locMessageElem = new Element[MAX_NESTING_LEVEL];
                Element[] nestingLevelElem = new Element[MAX_NESTING_LEVEL];
                Element[] stckTraceElem = new Element[MAX_NESTING_LEVEL];

                while (exObj != null)
                {
                    exInfoElem[nestingLevel] = doc.createElement("exception-info");
                    msgLocInfoElem[nestingLevel] = doc.createElement("msg-loc-info");
                    locTokenElem[nestingLevel] = doc.createElement("loc-token");
                    locMessageElem[nestingLevel] = doc.createElement("loc-message");
                    nestingLevelElem[nestingLevel] = doc.createElement("nesting-level");
                    stckTraceElem[nestingLevel] = doc.createElement("stack-trace");

                    StackTraceElement[] stckTrElem = exObj.getStackTrace();
                    StringBuffer sb = new StringBuffer("");

                    if (stckTrElem != null)
                    {
                        for (int i = 0; i < stckTrElem.length; i++)
                        {
                            String stckTrace = stckTrElem[i].toString();
                            sb.append(stckTrace);
                            sb.append("\n");
                        }
                    }

                    if (nestingLevel == 1)
                    {
                        DOMUtil.UTIL.setTextData(locTokenElem[nestingLevel], locToken);

                        if (locMessage == null)
                        {
                            DOMUtil.UTIL.setTextData(
                                locMessageElem[nestingLevel],
                                exObj.getMessage()
                            );
                        }
                        else
                        {
                            DOMUtil.UTIL.setTextData(
                                locMessageElem[nestingLevel],
                                locMessage
                            );
                        }
                    }
                    else
                    {
                        String errMsgWithToken = exObj.getMessage();
                        String token = errMsgWithToken.substring(0, 9);
                        String errMsg = null;

                        if (token.startsWith("JBI"))
                        {
                            errMsg = errMsgWithToken.substring(11);
                        }
                        else
                        {
                            token = MISSING_DATA_STRING;
                            errMsg = errMsgWithToken;
                        }

                        DOMUtil.UTIL.setTextData(locTokenElem[nestingLevel], token);
                        DOMUtil.UTIL.setTextData(locMessageElem[nestingLevel], errMsg);
                        
                    }

                    DOMUtil.UTIL.setTextData(
                        nestingLevelElem[nestingLevel],
                        Integer.toString(nestingLevel)
                    );
                    DOMUtil.UTIL.setTextData(stckTraceElem[nestingLevel], sb.toString());

                    msgLocInfoElem[nestingLevel].appendChild(locTokenElem[nestingLevel]);
                    msgLocInfoElem[nestingLevel].appendChild(
                        locMessageElem[nestingLevel]
                    );
                    
                    if (nestingLevel == 1)
                    {
                        if (locParam != null)
                        {
                            Element[] locParamElem = new Element[MAX_NESTING_LEVEL];

                            for (int i = 0; i < locParam.length; i++)
                            {
                                locParamElem[i] = doc.createElement("loc-param");
                                DOMUtil.UTIL.setTextData(locParamElem[i], locParam[i]);
                                msgLocInfoElem[nestingLevel].appendChild(locParamElem[i]);
                            }
                        }
                    }
                    else
                    {
                        Element locParamElement = doc.createElement("loc-param");
                        DOMUtil.UTIL.setTextData(locParamElement, MISSING_DATA_STRING);
                        msgLocInfoElem[nestingLevel].appendChild(locParamElement);
                    }
                    
                    exInfoElem[nestingLevel].appendChild(nestingLevelElem[nestingLevel]);
                    exInfoElem[nestingLevel].appendChild(msgLocInfoElem[nestingLevel]);
                    exInfoElem[nestingLevel].appendChild(stckTraceElem[nestingLevel]);
                    taskRsltDtlsElem.appendChild(exInfoElem[nestingLevel]);

                    nestingLevel++;
                    exObj = exObj.getCause();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        return taskRsltDtlsElem;
    }

    /**
     * Constructs an XML string of the component exception.
     *
     * @param mmObj HashMap containing component exception information
     *
     * @return XML string representing component exception
     *
     * @throws Exception If fails to build component exception message
     */
    public String buildComponentTaskExceptionMessage(ComponentMessageHolder mmObj)
        throws Exception
    {
        String cmpStatMsg = null;
        Document doc = null;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.newDocument();

            Element elem = doc.createElement("component-task-result");
            Element compNameElem = doc.createElement("component-name");
            Element compTaskRsltDtlsElem =
                doc.createElement("component-task-result-details");

            String componentName = mmObj.getComponentName();

            if (componentName == null)
            {
                String errMsg =
                    "JBIMA0457: BuildManagementMessage : Component name cannot be null ";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            DOMUtil.UTIL.setTextData(compNameElem, componentName);

            Element taskRsltDtlsElem = buildExceptionXMLFragment(doc, mmObj);

            compTaskRsltDtlsElem.appendChild(taskRsltDtlsElem);
            elem.appendChild(compNameElem);
            elem.appendChild(compTaskRsltDtlsElem);
            doc.appendChild(elem);

            StringWriter sw = new StringWriter();
            cmpStatMsg = DOMUtil.UTIL.DOM2String(doc, sw);
        }
        catch (Exception e)
        {
            throw e;
        }

        return cmpStatMsg;
    }

    /**
     * Constructs an XML string of the framework exception.
     *
     * @param doc Document that needs to be built
     * @param mmObj HashMap containing component exception information
     *
     * @return XML string representing framework exception
     *
     * @throws Exception If fails to build framework exception message
     */
    public Element buildFrameworkExceptionMessage(
        Document doc,
        ManagementMessageHolder mmObj
    ) throws Exception
    {
        Element elem = null;

        try
        {
            String isCauseFrmwk = mmObj.isCauseFramework();

            if (isCauseFrmwk == null)
            {
                isCauseFrmwk = MISSING_DATA_STRING;
            }

            String frmwkLocale = mmObj.getFrameworkLocale();

            if (frmwkLocale == null)
            {
                /* Note : Since this is a helper class which can
                 * be instantiated by the component, JBI resource
                 * bundle cannot be used. Hence strings are hard coded.
                */
                String errMsg =
                    "JBIMA0454: BuildManagementMessage : Framework Locale cannot" +
                    " be null ";
                mLogger.severe(errMsg);
                throw new Exception(errMsg);
            }

            elem = doc.createElement("frmwk-task-result");

            Element frmwkTaskRsltDtlsElem =
                doc.createElement("frmwk-task-result-details");
            Element frmwkLocaleElem = doc.createElement("locale");
            Element isCauseFrmwkElem = doc.createElement("is-cause-framework");
            DOMUtil.UTIL.setTextData(isCauseFrmwkElem, isCauseFrmwk);
            DOMUtil.UTIL.setTextData(frmwkLocaleElem, frmwkLocale);

            Element taskRsltDtlsElem = buildExceptionXMLFragment(doc, mmObj);

            frmwkTaskRsltDtlsElem.appendChild(frmwkLocaleElem);
            frmwkTaskRsltDtlsElem.appendChild(taskRsltDtlsElem);
            elem.appendChild(frmwkTaskRsltDtlsElem);
            elem.appendChild(isCauseFrmwkElem);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        return elem;
    }

    /**
     * Returns a org.w3c.dom.Element.
     *
     * @return org.w3c.dom.Document
     *
     * @throws Exception If fails to build Document
     */
    public Document createDocument() throws Exception
    {
        Document doc = null;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.newDocument();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        return doc;
    }

    /**
     * Constructs an XML string of the framework exception.
     *
     * @param mmObj name of the task
     *
     * @return XML string representing framework exception message
     */
    public String buildCompleteExceptionMessage(ManagementMessageHolder mmObj)
    {
        String jbiTaskStr = null;      
        jbiTaskStr = buildJbiTask(mmObj);

        return jbiTaskStr;
    }

    /**
     * Constructs an XML string of the framework exception.
     *
     * @param mmObj HashMap containing framework exception information
     *
     * @return XML string representing framework exception
     */
    public String buildJbiTask(ManagementMessageHolder mmObj)
    {
        String jbiTask = null;

        try
        {
            Document doc = createDocument();
            Element elem = doc.createElement("jbi-task");
            elem.setAttribute("version", "1.0");
            elem.setAttribute("xmlns", "http://java.sun.com/xml/ns/jbi/management-message");
            elem.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            elem.setAttribute(
                "xsi:schemaLocation",
                "http://java.sun.com/xml/ns/jbi/management-message ./managementMessage.xsd"
            );

            Element jbiTaskRsltElem = doc.createElement("jbi-task-result");
            Element frmwkExElem = buildFrameworkExceptionMessage(doc, mmObj);
            elem.appendChild(jbiTaskRsltElem);
            jbiTaskRsltElem.appendChild(frmwkExElem);
            doc.appendChild(elem);

            StringWriter sw = new StringWriter();
            jbiTask = DOMUtil.UTIL.DOM2String(doc, sw);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return jbiTask;
    }
}
