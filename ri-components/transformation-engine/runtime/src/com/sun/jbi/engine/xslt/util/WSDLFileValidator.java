/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLFileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.BindingOperation;
import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.Interface;
import com.sun.jbi.wsdl2.InterfaceOperation;
import com.sun.jbi.wsdl2.WsdlFactory;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.io.File;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

import com.sun.jbi.engine.xslt.TransformationEngineContext;

/**
 * This class is uses to validate the configuration file supplied during
 * deployment conforms to the schema.
 *
 * @author Sun Microsystems, Inc.
 */
public final class WSDLFileValidator
    extends UtilBase
{
    /**
     * Description object
     */
    private static Description sDefinition;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     *    
     */
    private String mCurValue = "";

    /**
     * Name of the file to parse.
     */
    private String mFileName;

    /**
     * Translator for i18n messages.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new WSDLFileValidator object.
     *
     * @param supath Service Unit path
     * @param wsdlFileName WSDL 2.0 deployment file name
     */
    public WSDLFileValidator(String supath, String wsdlFileName)
    {
        mFileName = wsdlFileName;
        //mFileName = supath + File.separatorChar + ConfigData.WSDL_FILE_NAME;
        //mFileName = supath + File.separatorChar + wsdlFileName;

        mLog = TransformationEngineContext.getInstance().getLogger("");
        mStringTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        
        mLog.finer("WSDL 20 depl file name: "+mFileName);
    }

    /**
     * Returns the document object obtained as a result of parsing.
     *
     * @return document object
     */
    public Description getDocument()
    {
        return sDefinition;
    }

    /**
     * DOCUMENT ME!
     */
    /**
     * This method has to be invoked to check the validity of the input
     * document.
     */
    public void validate()
    {
        parse();

        if (isValid())
        {
            validateEndpoints();
        }

        //mLog.info(mStringTranslator.getString(CONFIG_FILE_CHECK) + isValid());
        mLog.info("Checking WSDL file: " + isValid());
    }

    /**
     * DOCUMENT ME!
     *
     * @param df NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    private boolean isTEBinding(DocumentFragment df)
    {
        NamedNodeMap n = df.getFirstChild().getAttributes();

        for (int g = 0; g < n.getLength(); g++)
        {
            if (n.item(g).getLocalName().equals("type"))
            {
                if (n.item(g).getNodeValue().equals(ConfigData.SERVICE_ENGINE_TYPE))
                {
                    mLog.info("Found TE Endpoint.");

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * DOCUMENT ME!
     *
     * @param ep NOT YET DOCUMENTED
     */
    private void checkEndpoint(Endpoint ep)
    {
        DocumentFragment docfrag = ep.toXmlDocumentFragment();

        Node epnode = docfrag.getFirstChild();
        NamedNodeMap epattributes = epnode.getAttributes();
        int direction = findDirection(ep);

        if (direction == 0)
        {
            mLog.info("Inbound");
        }

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName() + getError());
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param map NOT YET DOCUMENTED
     * @param attr NOT YET DOCUMENTED
     */
    private void checkRequired(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode =
            map.getNamedItemNS(ConfigData.SERVICE_ENGINE_TYPE, attr);

        if ((attrnode == null))
        {
            setValid(false);
            setError(getError() + "\n\t" + "Required attribute \"" + attr
                + "\" not found");

            return;
        }

        try
        {
            mCurValue = attrnode.getNodeValue().trim();

            if (mCurValue.equals(""))
            {
                setValid(false);
                setError(getError() + "\n\t" + "Required attribute \"" + attr
                    + "\" has to have a value, cannot be null");
            }
        }
        catch (Exception e)
        {
            setValid(false);
            setError(getError() + "\n\t" + "Cannot get attribute value of "
                + attr);
            setException(e);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param ep NOT YET DOCUMENTED
     *
     * @return NOT YET DOCUMENTED
     */
    private int findDirection(Endpoint ep)
    {
        Binding b = ep.getBinding();
        Interface bindingif = b.getInterface();

        for (int oper = 0; oper < b.getOperationsLength(); oper++)
        {
            BindingOperation boper = b.getOperation(oper);
            QName bindingopername = boper.getInterfaceOperation();
            int ifoperlength = bindingif.getOperationsLength();

            for (int j = 0; j < ifoperlength; j++)
            {
                InterfaceOperation op = bindingif.getOperation(j);
                String pat = op.getPattern();

                if (pat.trim().equals(ConfigData.IN_OUT)
                        || pat.trim().equals(ConfigData.IN_ONLY)
                        || pat.trim().equals(ConfigData.ROBUST_IN_ONLY)
                        || pat.trim().equals(ConfigData.IN_OPTIONAL_OUT))
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        }

        return -1;
    }

    /**
     * Parses the input WSDL file.
     */
    private void parse()
    {
        try
        {
	    com.sun.jbi.component.ComponentContext ctx =
		 (com.sun.jbi.component.ComponentContext)
			 TransformationEngineContext.getInstance().getContext();
	    WsdlFactory wsdlFactory = ctx.getWsdlFactory();
            com.sun.jbi.wsdl2.WsdlReader wsdlRdr = wsdlFactory.newWsdlReader();
            sDefinition = wsdlRdr.readDescription(mFileName);
        }
        catch (Exception e)
        {
            setError("WSDL file " + mFileName + " parse error, reason "
                + e.getMessage());
            setException(e);
            setValid(false);
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void validateEndpoints()
    {
        int services = sDefinition.getServicesLength();
        boolean found = false;

        for (int i = 0; i < services; i++)
        {
            try
            {
                com.sun.jbi.wsdl2.Service ser = sDefinition.getService(i);

                //mLog.info("Service s = "+ser.toXmlString());
                int endpoints = ser.getEndpointsLength();

                for (int k = 0; k < endpoints; k++)
                {
                    Endpoint ep = ser.getEndpoint(k);

                    //mLog.info("Endpoint = "+ep.toXmlString());
                    Binding b = ep.getBinding();

                    //mLog.info("Binding b= "+b.toXmlString());
                    if (b == null)
                    {
                        //mLog.info("NULL Document Fragment. Binding.");
                        mLog.info("NULL Binding Document Fragment. ");

                        continue;
                    }

                    if (!isTEBinding(b.toXmlDocumentFragment()))
                    {
                        continue;
                    }

                    found = true;

                    //checkEndpoint(ep);
                    if (!isValid())
                    {
                        setError("\n\tBinding " + " : " + b.getName()
                            + getError());

                        return;
                    }
                }
            }
            catch (Exception e)
            {
                setError("Error occurred during endpoint validation "
                    + e.getMessage());
                setException(e);
                setValid(false);

                return;
            }
        }

        if (!found)
        {
            setError(getError() + "None of endpoints can be supported by TE");
            setValid(false);
        }
    }
}
