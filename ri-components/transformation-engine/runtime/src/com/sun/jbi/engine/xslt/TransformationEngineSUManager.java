/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TransformationEngineSUManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.util.DeployHelper;
import com.sun.jbi.engine.xslt.util.DeploymentLister;
import com.sun.jbi.engine.xslt.util.ServiceManager;
import com.sun.jbi.engine.xslt.util.StringTranslator;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.component.ServiceUnitManager;
import com.sun.jbi.common.management.ComponentMessageHolder;
import javax.jbi.management.DeploymentException;
import com.sun.jbi.common.management.ManagementMessageBuilder;
import javax.jbi.messaging.MessagingException;
import com.sun.jbi.common.Util;

import javax.management.ObjectInstance;
import javax.management.ObjectName;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class TransformationEngineSUManager
    implements ServiceUnitManager, TEResources
{
    /**
     *    
     */
    private static final int BUFFER = 2048;

    /**
     *    
     */
    private ComponentContext mContext = null;

    /**
     *    
     */
    private Logger mLogger = null;

    /**
     *    
     */
    private ManagementMessageBuilder mBuildManagementMessage;

    /**
     *    
     */
    private ServiceManager mServiceManager = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     *    
     */
    private String [] mAppsDeployed;

    /**
     * Creates a new TransformationEngineSUManager object.
     */
    public TransformationEngineSUManager()
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        mServiceManager = ServiceManager.getInstance();
        
    }
    
    /**
     * Sets the ComponentContext object on the TransformationEngineSUManager object.
     *
     * @param ctx ComponentContext object required for Deployment
     *
     */
    public void setContext(ComponentContext ctx)
    {
        mContext = ctx;
        mLogger = TransformationEngineContext.getInstance().getLogger("");
        mBuildManagementMessage = Util.createManagementMessageBuilder();
    }

    /**
     * Returns a boolean value indicating whether the Service Unit is currently
     * deployed.
     *
     * @param serviceUnitID - Id of the Service Unit
     *
     * @return boolean value indicating whether the Sercvice Unit is currently
     *         deployed
     */
    public boolean isDeployed(String serviceUnitID)
    {
        return mServiceManager.getDeploymentStatus(serviceUnitID);
    }

    /**
     * Returns a description of a deployed sub-assembly.
     *
     * @param serviceUnitID - ID of the Service Unit
     *
     * @return Description for the given Service Unit
     */
    public String getDeploymentInfo(String serviceUnitID)
    {
        return null;
    }

    // ***************************************************************//

    /**
     * Returns a list of all application sub-assemblies (Service Unit's)
     * currently deployed in to the named component.  This method is a
     * convenience wrapper for the same operation in the DeployerMBean.
     *
     * @return array of Service Unit ID strings.
     */
    public String [] getDeployments()
    {
        if ((mContext == null) || (mContext.getInstallRoot() == null))
        {
            return null;
        }

        mAppsDeployed =
            DeploymentLister.getFolderNameListing(new File(mContext
                    .getInstallRoot() + File.separatorChar
                    + mTranslator.getString(TEResources.SYSTEM)
                    + File.separatorChar
                    + mTranslator.getString(TEResources.DEPLOYMENT)));

        return mAppsDeployed;
    }

    //***************************************************************//
    //******* Implementation of ServiceUnitManager interface ********//
    //***************************************************************//

    /**
     * Initiate a Service Deployment.
     *
     * @param serviceUnitID - ID of the Service Unit being deployed
     * @param serviceUnitRootPath - Full path to the Service Unit root.
     *
     * @return NOT YET DOCUMENTED
     *
     * @throws DeploymentException Deployer Exception
     */
    public String deploy(
        String serviceUnitID,
        String serviceUnitRootPath)
        throws DeploymentException
    {
        String retMsg = null;

        mLogger.finer("TransformationEngineSUManager::deploy serviceUnitRootPath = "
            + serviceUnitRootPath);

        if (mServiceManager.getDeploymentStatus(serviceUnitID))
        {
            mLogger.severe(serviceUnitID
                + mTranslator.getString(TEResources.ALREADY_DEPLOYED_ON_TE));
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_30001", serviceUnitID,
                    "Duplicate Service Unit ID {1} not supported", null));
        }

        mServiceManager.setComponentContext(mContext);
        
        /**
         * You have to call initialize deployments here because, thats where
         * the schema check and data check are made, you want to fail the
         * deployment if the artifact fails validation. The same stuff is done
         * in init also during component starts , but if you dont do it here u
         * cannot fail deployments because of invalid data
         */
        try
        {
            initializeDeployment(serviceUnitID, serviceUnitRootPath);

            /*
             * Do not start deployments here.
             * The Deployment service will call init and start separately
             * after finidhing the deploy method
             */
        }
        catch (DeploymentException de)
        {
            throw de;
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_212", serviceUnitID,
                    mTranslator.getString(TE_CANNOT_CREATE_XML_MESSAGE, serviceUnitID),
                    e));
        }

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("deploy");
            compMsgHolder.setTaskResult(mTranslator.getString(
                    TEResources.SUCCESS));

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception ex)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.TE_CANNOT_CREATE_XML_MESSAGE) + ex.getMessage());
        }


        return retMsg;
    }
    
    /**
     * Initializes the deployments, parses and loads the config file.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    private void initializeDeployment(
        String serviceUnitID,
        String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        
        DeployHelper dh =
            new DeployHelper(serviceUnitID, serviceUnitRootPath, mContext);

        if (dh.isValid())
        {
            dh.doDeploy();
        }
        else
        {
            mLogger.severe(dh.getError());

            Exception e = dh.getException();

            if (e != null)
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "TESE_218", "", dh.getError(), e));
            }
            else
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "TESE_223", "", dh.getError(), null));
            }
        }
    }

    /**
     * Initialize the deployment. This is the first phase of a two-phase start,
     * where the component must prepare  to receive service requests related
     * to the deployment (if any).
     *
     * @param serviceUnitID service unit ID
     * @param serviceUnitRootPath service unit Root Path
     *
     * @throws DeploymentException deployement exception
     */
    public void init(
        String serviceUnitID,
        String serviceUnitRootPath)
        throws DeploymentException
    {
        mLogger.finer("init called for " + serviceUnitID);
        try
        {
            initializeDeployment(serviceUnitID, serviceUnitRootPath);

            /*
             * Do not start deployments here.
             * The Deployment service will call init and start separately
             * after finidhing the deploy method
             */
        }
        catch (DeploymentException de)
        {
            throw de;
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "TESE_316", serviceUnitID,
                    mTranslator.getString(TE_CANNOT_CREATE_XML_MESSAGE, serviceUnitID),
                    e));
        }
    }

    /**
     * Shut down the deployment. This causes the deployment to return to the
     * state it was in after deploy() and before init().
     *
     * @param serviceUnitID service unit ID
     *
     * @throws DeploymentException deployment exception
     */
    public void shutDown(String serviceUnitID)
        throws DeploymentException
    {
        mLogger.finer("shutDown called for " + serviceUnitID);
    }

    /**
     * Start the deployment. This is the second phase of a two-phase start,
     * where the component can now  initiate service requests related to the
     * deployment.
     *
     * @param serviceUnitID service unit ID
     *
     * @throws DeploymentException deployment exception
     */
    public void start(String serviceUnitID)
        throws DeploymentException
    {
        mLogger.finer("start called for " + serviceUnitID);

        boolean status = mServiceManager.getDeploymentStatus(serviceUnitID);

        if (!status)
        {
            throw new DeploymentException(serviceUnitID
                + " Is Not Deployed. Check.");
        }

        try
        {
            mServiceManager.startServices(serviceUnitID);
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "start", "FAILED", "TESE_332",
                    " ",
                    "Unable to start services for SU ID: " + serviceUnitID, e));
        }
    }

    /**
     * Stop the deployment. This causes the component to cease generating
     * service requests related to the deployment.  This returns the
     * deployment to a state equivalent to after init() was called
     *
     * @param serviceUnitID service unit ID
     *
     * @throws DeploymentException deployment exception
     */
    public void stop(String serviceUnitID)
        throws DeploymentException
    {
        mLogger.finer("stop called for " + serviceUnitID);

        boolean status = mServiceManager.getDeploymentStatus(serviceUnitID);

        if (!status)
        {
            throw new DeploymentException(serviceUnitID
                + " Is Not Deployed. Check.");
        }

        try
        {
            mServiceManager.stopServices(serviceUnitID);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "stop", "FAILED", "TESE_383",
                    " ", "Unable to stop services for SU ID:." + serviceUnitID,
                    e));
        }
    }

    /**
     * Cancel a Service Deployment.  If the deployment is in use (has
     * dependencies), then will operation may fail.
     *
     * @param serviceUnitID - ID of the Service Unit being undeployed
     * @param serviceUnitRootPath - Full path to the Service Unit root.
     *
     * @return NOT YET DOCUMENTED
     *
     * @throws DeploymentException deployment exception
     */
    public String undeploy(
        String serviceUnitID,
        String serviceUnitRootPath)
        throws DeploymentException
    {
        String retMsg = null;

        boolean status = mServiceManager.getDeploymentStatus(serviceUnitID);

	if (!status)
	{
    		throw new DeploymentException(serviceUnitID
        	+ " Is Not Deployed. Check.");
	}

	try
	{
   	     mServiceManager.destroyServices(serviceUnitID);
	}
	catch (Exception e)
	{
    	    e.printStackTrace();
    	    throw new DeploymentException(createExceptionMessage(
                   mContext.getComponentName(), "undeploy", "FAILED", "TESE_441",
                   " ", "Unable to destroy services for SU ID:." + serviceUnitID,
                   e));
	}

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("undeploy");
            compMsgHolder.setTaskResult(mTranslator.getString(
                    TEResources.SUCCESS));

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception ex)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.CANNOT_CREATE_XML_MESSAGE) + ex.getMessage());
        }

        return retMsg;
    }

    /**
     * helper method to create XML exception string.
     *
     * @param compid Component id.
     * @param oper operation like deploy or undeploy
     * @param status success r failure
     * @param loctoken some failure string token like SEQ_300001
     * @param locparam parameters for error message.
     * @param locmessage error message.
     * @param exObj stack trace of exception.
     *
     * @return XML string.
     */
    private String createExceptionMessage(
        String compid,
        String oper,
        String status,
        String loctoken,
        String locparam,
        String locmessage,
        Throwable exObj)
    {
        String retMsg = null;

        String [] locParams = new String[1];
        locParams[0] = locparam;

        ComponentMessageHolder msgMap =
            new ComponentMessageHolder("EXCEPTION_MSG");

        msgMap.setComponentName(compid);
        msgMap.setTaskName(oper);
        msgMap.setTaskResult(status);
        msgMap.setLocToken(1, loctoken);
        msgMap.setLocParam(1, locParams);
        msgMap.setLocMessage(1, locmessage);
        msgMap.setExceptionObject(exObj);

        try
        {
            retMsg = mBuildManagementMessage.buildComponentMessage(msgMap);
        }
        catch (Exception e)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.CANNOT_CREATE_MGMT_XML_MSG) + e.getMessage());
        }

        return retMsg;
    }
}
