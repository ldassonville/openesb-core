#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)file09999.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "filebinding00007.ksh- undeploy Stop, shutdown and uninstall file binding test"

. ./regress_defs.ksh


# undeploy

# Stop file binding
$JBI_ANT -Djbi.component.name=SunFileBinding stop-component

# Shutdown
$JBI_ANT -Djbi.component.name=SunFileBinding shut-down-component

# Uninstall
$JBI_ANT -Djbi.component.name=SunFileBinding uninstall-component
#comment out this step as system shared-library are expected by other tests
#$JBI_ANT -Djbi.shared.library.name=SunWSDLSharedLibrary uninstall-shared-library

# Stop app server
# $AS8BASE/bin/asadmin stop-domain $JBI_DOMAIN_NAME

echo Completed uninstalling file binding
