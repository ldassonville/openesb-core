/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.util.ConfigData;
import com.sun.jbi.binding.file.util.ConfigReader;
import com.sun.jbi.binding.file.util.StringTranslator;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessagingException;


/**
 * This is the Lifecycle class of the File Binding.  The init and the start()
 * methods of this class  are invoked by  the framework when the component  is
 * started
 *
 * @author Sun Microsystems, Inc.
 */
public class FileBindingLifeCycle
    implements javax.jbi.component.ComponentLifeCycle, FileBindingResources
{
    /**
     * Binding context.
     */
    private ComponentContext mContext = null;

    /**
     * Binding Channel Object
     */
    private DeliveryChannel mChannel;

    /**
     * Deployment registry.
     */
    private DeploymentRegistry mRegistry;

    /**
     * Manages the endpoints.
     */
    private EndpointManager mManager;

    /**
     * Resolver object.
     */
    private FileBindingResolver mResolver;

    /**
     * Deployer object.
     */
    private FileBindingSUManager mSUManager;

    /**
     * File receiver object for receiving messages from NMS.
     */
    private FileReceiver mFileReceiver;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * Helper for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Thread for file receiver.
     */
    private Thread mReceiverThread;

    /**
     * Flag to hold the result of init
     */
    private boolean mInitSuccess = false;

    /**
     * Get the ObjectName for any MBean that is provided for managing this
     * binding. In this case, there is none, so a null is returned.
     *
     * @return ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Sets the resolver.
     *
     * @param resolver resolver object.
     */
    public void setResolver(FileBindingResolver resolver)
    {
        mResolver = resolver;
    }

    /**
     * Sets the SU Manager.
     *
     * @param manager su manager.
     */
    public void setSUManager(FileBindingSUManager manager)
    {
        mSUManager = manager;
    }

    /**
     * Initialize the File BC. This performs initialization required by the
     * File BC but does not make it ready to process messages.  This method is
     * called immediately after installation of the File BC.  It is also
     * called when the JBI framework is starting up, and any time  the BC is
     * being restarted after previously being shut down through  a call to
     * shutdown().
     *
     * @param jbiContext the JBI environment context
     *
     * @throws javax.jbi.JBIException if the BC is unable to initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        FileBindingContext fbccontext = FileBindingContext.getInstance();
        fbccontext.setContext(jbiContext);

        Logger logger = jbiContext.getLogger("", null);
        fbccontext.setLogger(logger);

        // jbiContext.
        mLog = fbccontext.getLogger();
        mTranslator = new StringTranslator();
        mLog.info(mTranslator.getString(FBC_INIT_START));

        if (null == jbiContext)
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                    FBC_INVALID_BINDINGCONTEXT));
        }

        mContext = jbiContext;

        try
        {
            mManager = new EndpointManager();
            mSUManager.setValues(mManager, mResolver);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new javax.jbi.JBIException(mTranslator.getString(
                    FBC_INIT_FAILED), e);
        }

        try
        {
            mRegistry = DeploymentRegistry.getInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /* This a guard variable to check if init is a success
         * Nothing should be done in start in case init is a failure
         */
        mInitSuccess = true;
    }

    /**
     * Shutdown the BC. This performs cleanup before the BC is terminated. Once
     * this has been called, init() must be called before the BC can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the BC is unable to shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.info(mTranslator.getString(FBC_SHUTDOWN_BEGIN));
        mSUManager = null;
        mRegistry.clearRegistry();
        mLog.info(mTranslator.getString(FBC_SHUTDOWN_END));
    }

    /**
     * Start the File BC. This makes the File BC ready to process messages.
     * This method is called after init() completes when the JBI framework is
     * starting up, and when the BC is being restarted after a previous call
     * to shutdown(). If stop() was called previously but shutdown() was not,
     * start() can be called without a call to init().
     *
     * @throws javax.jbi.JBIException if the BC is unable to start.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info(mTranslator.getString(FBC_START_BEGIN));

        if (!mInitSuccess)
        {
            mLog.severe(mTranslator.getString(FBC_INIT_FAILED));

            return;
        }

        String compRoot = mContext.getInstallRoot();
        String schemaFile =
            compRoot + File.separatorChar + ConfigData.SCHEMA_FILE;

        if (compRoot == null)
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_COMPROOT));

            return;
        }

        try
        {
            mChannel = mContext.getDeliveryChannel();

            //mChannel.setResolver(mResolver);
        }
        catch (MessagingException me)
        {
            mLog.severe(mTranslator.getString(FBC_BINDINGCHANNEL_FAILED)
                + me.getMessage());

            return;
        }

        mFileReceiver = new FileReceiver(mChannel);
        mReceiverThread = new Thread(mFileReceiver);
        mReceiverThread.start();
        mLog.info(mTranslator.getString(FBC_START_END));
    }

    /**
     * Stop the BC. This makes the BC stop accepting messages for processing.
     * After a call to this method, start() can be called again without first
     * calling init().
     *
     * @throws javax.jbi.JBIException if the BC is unable to stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLog.info(mTranslator.getString(FBC_STOP_BEGIN));

        try
        {
            if (mReceiverThread != null)
            {
                mFileReceiver.stopReceiving();
                mReceiverThread.interrupt();
                mReceiverThread.join();
            }

            if (mChannel != null)
            {
                mChannel.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mLog.info(mTranslator.getString(FBC_STOP_END));
    }
}
