/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.EndpointBean;
import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.FileBindingResources;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Reads the configuration XML file and loads the data into the EndpointBean
 * objects. The parsing and reading information may not be state of the art
 * but serves the purpose of getting  the data from XML. Simple.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigReader
    extends UtilBase
    implements FileBindingResources
{
    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * i18n.
     */
    private StringTranslator mTranslator;

    /**
     * The list of endpoints as configured in the config file. This list
     * contains all the encpoints and their attibutes.
     */
    private EndpointBean [] mEndpointInfoList = null;

    /**
     * The total number of end points in the config file.
     */
    private int mTotalEndpoints = 0;

    /**
     * Creates a new ConfigReader object.
     */
    public ConfigReader()
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        setValid(true);
    }

    /**
     * Returns the Bean object corresponding to the endpoint.
     *
     * @param endpoint endpoint name.
     *
     * @return endpoint Bean object.
     */
    public EndpointBean getBean(String endpoint)
    {
        /*Search for the bean corresponding to the service and endpoint name
         */
        for (int j = 0; j < mEndpointInfoList.length; j++)
        {
            String tmp = mEndpointInfoList[j].getUniqueName();

            if (tmp.trim().equals(endpoint))
            {
                return mEndpointInfoList[j];
            }
        }

        return null;
    }

    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list
     */
    public EndpointBean [] getEndpoint()
    {
        return mEndpointInfoList;
    }

    /**
     * Returns the total number of endopoints in the config file.
     *
     * @return int number of endpoints.
     */
    public int getEndpointCount()
    {
        return mTotalEndpoints;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            loadServicesList();
        }
        catch (Exception genException)
        {
            mLog.severe(mTranslator.getString(FBC_LOAD_CONFIG_FAILED));
            genException.printStackTrace();
            setException(genException);
            setError(getError() + mTranslator.getString(FBC_LOAD_CONFIG_FAILED)
                + "\n" + genException.getMessage());
            setValid(false);
        }
    }

    /**
     * Utility method for setting the Bean object from the XML Nodes.
     *
     * @param node The node which needs to be
     * @param sb The end point bean object which has to be updated
     * @param tagName the tag name which needs to be read.
     */
    private void setEndpointBeanByTagName(
        Node node,
        EndpointBean sb,
        String tagName)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(tagName);

        if (namelist == null)
        {
            /* This means the tag is not present
             */
            return;
        }

        Element name = (Element) namelist.item(0);
        String sValue = null;

        try
        {
            sValue =
                ((Node) (name.getChildNodes().item(0))).getNodeValue().trim();
        }
        catch (NullPointerException ne)
        {
            sb.setValue(tagName, sValue);

            return;
        }

        sb.setValue(tagName, sValue);
    }

    /**
     * Sets the interface.
     *
     * @param nd Node
     * @param eb Endpoint Bean.
     */
    private void setInterface(
        Node nd,
        EndpointBean eb)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.INTERFACE);

        if (namelist == null)
        {
            /* This means the tag is not present
             */
            return;
        }

        Node node = (Node) namelist.item(0);

        try
        {
            eb.setValue(ConfigData.INTERFACE_NAMESPACE,
                getValue(node, ConfigData.NAMESPACE_URI));
            eb.setValue(ConfigData.INTERFACE_LOCALNAME,
                getValue(node, ConfigData.LOCAL_PART));
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Returns the operation name corresponding to the endpoint node.
     *
     * @param nd Node
     *
     * @return operation name
     */
    private String getOperationName(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.NAME);

        Node node = (Node) namelist.item(0);

        return getValue(node, ConfigData.LOCAL_PART);
    }

    /**
     * Returns the operation namespace for the operation node.
     *
     * @param nd node to be parsed.
     *
     * @return namespace of operation.
     */
    private String getOperationNamespace(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.NAME);
        Node node = (Node) namelist.item(0);

        return getValue(node, ConfigData.NAMESPACE_URI);
    }

    /**
     * Util method to get all operations loaded into Beans.
     *
     * @param node Endpoint node that has to be parsed.
     * @param eb endpoint bean object where info is to be loaded.
     *
     * @throws Exception exception.
     */
    private void setOperations(
        Node node,
        EndpointBean eb)
        throws Exception
    {
        try
        {
            Element ele = (Element) node;
            NodeList list = ele.getElementsByTagName(ConfigData.OPERATION);

            if (list.getLength() == 0)
            {
                setError(mTranslator.getString(FBC_NO_OPERATIONS,
                        eb.getUniqueName()));

                return;
            }

            for (int i = 0; i < list.getLength(); i++)
            {
                Node nd = (Node) list.item(i);
                String namespace = null;
                String name = null;
                String mep = null;
                String input = null;
                String output = null;
                String ext = null;
                String prefix = null;
                namespace = getOperationNamespace(nd);
                name = getOperationName(nd);
                mep = getValue(nd, ConfigData.MEP);
                input = getValue(nd, ConfigData.INPUT_MESSAGE_TYPE);
                output = getValue(nd, ConfigData.OUTPUT_MESSAGE_TYPE);
                prefix = getValue(nd, ConfigData.OUTPUTPREFIX);
                ext = getValue(nd, ConfigData.OUTPUTEXTENSION);

                if ((namespace == null) || (namespace.trim().equals("")))
                {
                    setError(mTranslator.getString(FBC_OPERATION_NAMESPACE_NULL));
                }

                if ((name == null) || (name.trim().equals("")))
                {
                    setError(mTranslator.getString(FBC_OPERATION_NAME_NULL));
                }

                if ((mep == null) || (mep.trim().equals("")))
                {
                    setError(mTranslator.getString(FBC_MEP_NULL));
                }

                if ((input == null) || (input.trim().equals("")))
                {
                    input = ConfigData.DEFAULT_MESSAGE_TYPE;
                }

                if ((output == null) || (output.trim().equals("")))
                {
                    output = ConfigData.DEFAULT_MESSAGE_TYPE;
                }

                if (!isValid())
                {
                    return;
                }

                eb.addOperation(namespace, name, mep, input, output, ext, prefix);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    /**
     * Sets the role of the endpoint.
     *
     * @param eb Endpoint Bean.
     */
    private void setRole(EndpointBean eb)
    {
        String role = (String) eb.getValue(ConfigData.ENDPOINT_TYPE);

        if (role == null)
        {
            eb.setRole(ConfigData.CONSUMER);
        }
        else
        {
            if (role.trim().equalsIgnoreCase(ConfigData.PROVIDER_STRING))
            {
                eb.setRole(ConfigData.PROVIDER);
            }
            else
            {
                eb.setRole(ConfigData.CONSUMER);
            }
        }
    }

    /**
     * Sets the service.
     *
     * @param nd Node
     * @param eb Endpoint Bean.
     */
    private void setService(
        Node nd,
        EndpointBean eb)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.SERVICE);

        if (namelist == null)
        {
            /* This means the tag is not present
             */
            return;
        }

        Node node = (Node) namelist.item(0);

        try
        {
            eb.setValue(ConfigData.SERVICE_NAMESPACE,
                getValue(node, ConfigData.NAMESPACE_URI));
            eb.setValue(ConfigData.SERVICE_LOCALNAME,
                getValue(node, ConfigData.LOCAL_PART));
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(FBC_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Util method to extract test node value from an element.
     *
     * @param n node.
     * @param name name of the element from which info is extracted.
     *
     * @return Value.
     */
    private String getValue(
        Node n,
        String name)
    {
        String s = null;

        try
        {
            Element ele = (Element) n;
            NodeList list = ele.getElementsByTagName(name);
            Element found = (Element) list.item(0);
            s = (String) found.getFirstChild().getNodeValue();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return s;
    }

    /**
     * Checks the required atributes.
     *
     * @param eb Bean object
     * @param counter index of endpoint in the list.
     */
    private void checkRequired(
        EndpointBean eb,
        int counter)
    {
        if (eb.getValue(ConfigData.SERVICE_NAMESPACE).trim().equals(""))
        {
            setError(getError() + "\n" + counter + " "
                + mTranslator.getString(FBC_INVALID_SERVICE, ""));
            setValid(false);
        }

        if (eb.getValue(ConfigData.SERVICE_LOCALNAME).trim().equals(""))
        {
            setError(getError() + "\n" + counter + " "
                + mTranslator.getString(FBC_INVALID_SERVICE, ""));
            setValid(false);
        }

        if (eb.getValue(ConfigData.ENDPOINTNAME).trim().equals(""))
        {
            setError(getError() + "\n" + counter + " "
                + mTranslator.getString(FBC_INVALID_ENDPOINT,
                    eb.getValue(ConfigData.SERVICE_LOCALNAME)));
            setValid(false);
        }

        if (eb.getValue(ConfigData.INPUTDIR).trim().equals(""))
        {
            setError(getError() + "\n" + counter + " "
                + mTranslator.getString(FBC_INVALID_INPUTFOLDER,
                    eb.getValue(ConfigData.ENDPOINTNAME)));
            setValid(false);
        }

        if (eb.getValue(ConfigData.OUTPUTDIR).trim().equals(""))
        {
            setError(getError() + "\n" + counter + " "
                + mTranslator.getString(FBC_INVALID_OUTPUTFOLDER,
                    eb.getValue(ConfigData.ENDPOINTNAME)));
            setValid(false);
        }

        if (eb.getValue(ConfigData.PROCESSEDDIR).trim().equals(""))
        {
            setError(getError() + "\n" + counter + " "
                + mTranslator.getString(FBC_INVALID_PROCESSEDFOLDER,
                    eb.getValue(ConfigData.ENDPOINTNAME)));
            setValid(false);
        }
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        NodeList list = mDoc.getElementsByTagName(ConfigData.ENDPOINT);
        mTotalEndpoints = list.getLength();
        mEndpointInfoList = new EndpointBean[mTotalEndpoints];

        try
        {
            for (int i = 0; i < mTotalEndpoints; i++)
            {
                Node node = list.item(i);
                EndpointBean sb = new EndpointBean();

                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    setService(node, sb);
                    setInterface(node, sb);
                    setEndpointBeanByTagName(node, sb, ConfigData.ENDPOINTNAME);
                    setEndpointBeanByTagName(node, sb, ConfigData.ENDPOINT_TYPE);
                    setEndpointBeanByTagName(node, sb, ConfigData.INPUTDIR);
                    setEndpointBeanByTagName(node, sb, ConfigData.OUTPUTDIR);
                    setEndpointBeanByTagName(node, sb, ConfigData.PROCESSEDDIR);
                    setEndpointBeanByTagName(node, sb, ConfigData.INPUTPATTERN);
                    setOperations(node, sb);
                }

                setRole(sb);
                sb.setDeploymentType("XML");
                checkRequired(sb, i);

                if (!isValid())
                {
                    setError(mTranslator.getString(FBC_LOAD_CONFIG_FAILED)
                        + "\n" + getError());

                    return;
                }

                mEndpointInfoList[i] = sb;
            }
        }
        catch (Exception ee)
        {
            mLog.severe(mTranslator.getString(FBC_LOAD_CONFIG_FAILED));
            ee.printStackTrace();
            setException(ee);
            setError(getError() + mTranslator.getString(FBC_LOAD_CONFIG_FAILED)
                + "\n" + ee.getMessage());
            setValid(false);
        }
    }
}
