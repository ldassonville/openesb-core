/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WorkThreadPool.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.framework;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.FileBindingResources;
import com.sun.jbi.binding.file.util.StringTranslator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.logging.Logger;


/**
 * This class manages a pool of worker threads. The Work Manager uses this
 * class to execute the command in different threads.
 *
 * @author Sun Microsystems, Inc.
 */
class WorkThreadPool
    implements FileBindingResources
{
    /**
     * Default Maximum Thread count.
     */
    private static final int MAX_THREAD_COUNT = 10;

    /**
     * Default Minimum Thread count.
     */
    private static final int MIN_THREAD_COUNT = 1;

    /**
     * Handle to the busy thread pool.
     */
    private ArrayList mBusyThreadPool;

    /**
     * Handle to the free thread pool.
     */
    private ArrayList mFreeThreadPool;

    /**
     * Contains a list of threads on which we are waiting for response.
     */
    private ArrayList mThreadWaitList;

    /**
     * Internal handle to the logger instance.
     */
    private Logger mLog;

    /**
     * Internal variable which indicates that workethread pool has been
     * instructed to stop all its threads.
     */
    private String mState;

    /**
     * i18n
     */
    private StringTranslator mTranslator;

    /**
     * Maximum Thread count.
     */
    private int mMaxThreadCount = MAX_THREAD_COUNT;

    /**
     * Minimum Thread count.
     */
    private int mMinThreadCount = MIN_THREAD_COUNT;

    /**
     * Creates a new instance of WorkThreadPool.
     *
     * @param minThreadCount - minimum thread limit
     * @param maxThreadCount - maximum thread limit
     */
    WorkThreadPool(
        int minThreadCount,
        int maxThreadCount)
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mMaxThreadCount = maxThreadCount;
        mMinThreadCount = minThreadCount;
        mThreadWaitList = new ArrayList();
        mTranslator = new StringTranslator();
        mState = "NEW";
    }

    /**
     * Creates a new instance of WorkThreadPool.
     */
    WorkThreadPool()
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        mThreadWaitList = new ArrayList();
        mState = "NEW";
    }

    /**
     * Returns the count of threads in busy thread pool
     *
     * @return int count of number of busy threads
     */
    public int getBusyThreads()
    {
        return mBusyThreadPool.size();
    }

    /**
     * Sets the log file.
     *
     * @param logFile log file.
     */
    public void setLogger(String logFile)
    {
        mLog = mLog.getLogger(logFile);
    }

    /**
     * Sets the maximum thread count
     *
     * @param count max thread.
     */
    public void setMaxThreads(int count)
    {
        mMaxThreadCount = count;
    }

    /**
     * Sets the minimum thread count.
     *
     * @param count min thread count.
     */
    public void setMinThreads(int count)
    {
        mMinThreadCount = count;
    }

    /**
     * This method exits when all the threds complete.
     */
    public synchronized void exitWhenBusyThreadsDone()
    {
        while (mBusyThreadPool.size() != 0)
        {
            try
            {
                wait();
            }
            catch (Exception e)
            {
                mLog.severe(mTranslator.getString(FBC_THREADS_BUSYTHREADS,
                        String.valueOf(mBusyThreadPool.size())));

                continue;
            }
        }
    }

    /**
     * Gets a free thread from the free thread pool.
     *
     * @return a worker thread instance if a free thread exists; otherwise
     *         null.
     *
     * @throws IllegalStateException when the thread pool is not running.
     */
    synchronized WorkThread getFreeThread()
    {
        WorkThread workerThread = null;

        if (!mState.equals("START"))
        {
            throw new IllegalStateException("Thread pool is not running");
        }

        if (mFreeThreadPool.size() > 0)
        {
            workerThread = (WorkThread) mFreeThreadPool.get(0);
            mFreeThreadPool.remove(0);
            mBusyThreadPool.add(workerThread);
        }
        else if ((mBusyThreadPool.size()) < mMaxThreadCount)
        {
            workerThread = new WorkThread(this);
            mBusyThreadPool.add(workerThread);
            new Thread(workerThread).start();
        }
        else
        {
            try
            {
                mThreadWaitList.add(Thread.currentThread());
                wait();
                mThreadWaitList.remove(Thread.currentThread());

                if (mFreeThreadPool.size() > 0)
                {
                    workerThread = (WorkThread) mFreeThreadPool.get(0);
                    mFreeThreadPool.remove(0);
                    mBusyThreadPool.add(workerThread);
                }
            }
            catch (InterruptedException interupException)
            {
                workerThread = null;
            }
            catch (Exception exception)
            {
                mLog.warning("Details : " + exception.toString());
                workerThread = null;
            }
        }

        return workerThread;
    }

    /**
     * Cleans up the free and busy threads.
     *
     * @throws IllegalStateException exception.
     */
    synchronized void cleanup()
    {
        if (!(mState.equals("INIT") || mState.equals("STOP")))
        {
            throw new IllegalStateException("Thread Pool is still active");
        }

        mFreeThreadPool.clear();
        mBusyThreadPool.clear();
    }

    /**
     * Initializes the instance.
     *
     * @throws IllegalStateException
     */
    void init()
    {
        if (!mState.equals("NEW"))
        {
            throw new IllegalStateException(
                "Threadpool has already been initialized");
        }

        mFreeThreadPool = new ArrayList(mMaxThreadCount);
        mBusyThreadPool = new ArrayList(mMaxThreadCount);

        for (int i = 0; i < mMinThreadCount; i++)
        {
            mFreeThreadPool.add(new WorkThread(this));
        }

        mState = "INIT";
    }

    /**
     * Release the thread to the free pool. This method is used by worker
     * threads to notify that it has completed processing its command.
     *
     * @param thread - worker thread instance.
     *
     * @throws IllegalStateException
     * @throws NoSuchElementException
     */
    synchronized void releaseThread(WorkThread thread)
    {
        if (!mState.equals("START"))
        {
            throw new IllegalStateException("Thread pool is not running");
        }

        int threadIndex = mBusyThreadPool.indexOf(thread);

        if (threadIndex != -1)
        {
            mBusyThreadPool.remove(threadIndex);

            if (mFreeThreadPool.size() < mMinThreadCount)
            {
                mFreeThreadPool.add(thread);

                // Notify the manager to indicate that a thread has been released
                // This is ignored if no one are waiting for a free thread.
                try
                {
                    notifyAll();
                }
                catch (IllegalMonitorStateException exp)
                {
                    mLog.severe("Details :" + exp.toString());
                }
            }
            else
            {
                thread.stop();
            }

            if (mBusyThreadPool.size() == 0)
            {
                try
                {
                    notify();
                }
                catch (IllegalMonitorStateException exp)
                {
                    mLog.severe("Details :" + exp.toString());
                }
            }
        }
        else
        {
            throw new NoSuchElementException("Thread " + thread.getName()
                + " cannot be found");
        }
    }

    /**
     * Start the free threads.
     *
     * @throws IllegalStateException
     */
    void start()
    {
        WorkThread workerThread;

        if (!(mState.equals("INIT") || mState.equals("STOP")))
        {
            throw new IllegalStateException(
                "Thread pool has already been started");
        }

        for (Iterator iter = mFreeThreadPool.iterator(); iter.hasNext();)
        {
            workerThread = (WorkThread) iter.next();
            new Thread(workerThread).start();
        }

        for (Iterator iter = mBusyThreadPool.iterator(); iter.hasNext();)
        {
            workerThread = (WorkThread) iter.next();
            new Thread(workerThread).start();
        }

        mState = "START";
    }

    /**
     * Stops the free and busy threads.
     *
     * @throws IllegalStateException exception.
     */
    synchronized void stop()
    {
        mLog.fine(mTranslator.getString(FBC_THREADS_POOL_STOP));

        WorkThread workerThread;

        if (!mState.equals("START"))
        {
            throw new IllegalStateException("Threadpool has not been started");
        }

        for (Iterator iter = mFreeThreadPool.iterator(); iter.hasNext();)
        {
            workerThread = (WorkThread) iter.next();
            workerThread.stop();
        }

        for (Iterator iter = mBusyThreadPool.iterator(); iter.hasNext();)
        {
            workerThread = (WorkThread) iter.next();
            workerThread.stop();
        }

        Thread waitThread = null;

        for (Iterator iter = mThreadWaitList.iterator(); iter.hasNext();)
        {
            waitThread = (Thread) iter.next();
            waitThread.interrupt();
        }

        mState = "STOP";
    }
}
