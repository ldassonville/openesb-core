/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigFileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.FileBindingResources;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;

import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * This class is uses to validate the configuration file supplied during
 * deployment conforms to the schema.
 *
 * @author Sun Microsystems, Inc.
 */
public final class ConfigFileValidator
    extends DefaultHandler
    implements FileBindingResources
{
    /**
     * Parser setting.
     */
    static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";

    /**
     * Parser setting.
     */
    static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    /**
     * Parser setting.
     */
    static final String JAXP_SCHEMA_SOURCE =
        "http://java.sun.com/xml/jaxp/properties/schemaSource";

    /**
     * Document object generated after parsing.
     */
    private Document mDocument;

    /**
     * Factory object for document.
     */
    private DocumentBuilderFactory mFactory = null;

    /**
     * Object to store exceptions
     */
    private Exception mException;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Stores the error message , if any.
     */
    private String mErrorMsg = "";

    /**
     * Name of the file to parse.
     */
    private String mFileName;

    /**
     * Name of the schema file.
     */
    private String mSchemaFile;

    /**
     * Helper class for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Flag to denote validity.
     */
    private boolean mValid = true;

    /**
     * Creates a new ConfigFileValidator object.
     *
     * @param schema schema file name
     * @param xmlfile xml file name
     */
    public ConfigFileValidator(
        String schema,
        String xmlfile)
    {
        mFactory = DocumentBuilderFactory.newInstance();
        mFactory.setNamespaceAware(true);
        mFactory.setValidating(false);
        mSchemaFile = schema;
        mFileName = xmlfile;
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }

    /**
     * Returns the document object obtained as a result of parsing.
     *
     * @return document object
     */
    public Document getDocument()
    {
        return mDocument;
    }

    /**
     * Returns the error message if any.
     *
     * @return String error message.
     */
    public String getError()
    {
        return mErrorMsg;
    }

    /**
     * Get the exception message if any.
     *
     * @return exception message if any occured.
     */
    public Exception getException()
    {
        return mException;
    }

    /**
     * Returns true if the file conforms to schema.
     *
     * @return valid / invalid
     */
    public boolean isValid()
    {
        return mValid;
    }

    /**
     * Sets the parser as validating or non-validating. The parser is
     * non-validating if this method is not called before the parse method.
     */
    public void setValidating()
    {
        setAttributes();
    }

    /**
     * Handler method for the parser.
     *
     * @param se sax parse exception object.
     */
    public void error(SAXParseException se)
    {
        mLog.severe(se.getMessage());
        setError(getError() + "\n\t" + "Line:" + se.getLineNumber()
            + ", Column:" + se.getColumnNumber() + ":" + se.getMessage());
        setException(se);
        mValid = false;
    }

    /**
     * Handler method for the parser.
     *
     * @param se SAXPrse exception.
     */
    public void fatalError(SAXParseException se)
    {
        mLog.severe(getError() + "\n\t" + "Line:" + se.getLineNumber()
            + ",Column:" + se.getColumnNumber() + ":" + se.getMessage());
        setError(se.getMessage());
        setException(se);
        mValid = false;
    }

    /**
     * This method has to be invoked to check the validity of the input
     * document.
     */
    public void validate()
    {
        parse();
        mLog.fine(mTranslator.getString(FBC_CONFIG_FILE_CHECK) + isValid());
    }

    /**
     * Handler method for the parser.
     *
     * @param se SAXParseexception.
     */
    public void warning(SAXParseException se)
    {
        mLog.warning(se.getLineNumber() + ":" + se.getColumnNumber() + ":"
            + se.getMessage());
    }

    /**
     * Sets the attributes of the parser.
     */
    private void setAttributes()
    {
        mFactory.setValidating(true);

        try
        {
            mFactory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            mFactory.setAttribute(JAXP_SCHEMA_SOURCE, mSchemaFile);
            mFactory.setNamespaceAware(true);
        }
        catch (IllegalArgumentException iae)
        {
            setError(iae.getMessage());
            setException(iae);
            mValid = false;
            iae.printStackTrace();
        }
    }

    /**
     * Used to set error message if occurs during parsing.
     *
     * @param msg error message that occurred.
     */
    private void setError(String msg)
    {
        mErrorMsg = msg;
    }

    /**
     * Sets the exception.
     *
     * @param e any exception
     */
    private void setException(Exception e)
    {
        mException = e;
    }

    /**
     * Parses the input XML file.
     */
    private void parse()
    {
        DocumentBuilder builder = null;

        try
        {
            builder = mFactory.newDocumentBuilder();
        }
        catch (ParserConfigurationException pce)
        {
            mLog.severe(pce.getMessage());
            setError(pce.getMessage());
            setException(pce);
            mValid = false;
        }

        builder.setErrorHandler(this);

        try
        {
            mDocument = builder.parse(mFileName);
        }
        catch (SAXException se)
        {
            mLog.severe(mTranslator.getString(FBC_CANNOT_PARSE));
            mLog.severe(se.getMessage());
            setError(mTranslator.getString(FBC_CANNOT_PARSE) + "\n"
                + se.getMessage());
            setException(se);
            se.printStackTrace();
            mValid = false;
        }
        catch (IOException ioe)
        {
            mLog.severe(mTranslator.getString(FBC_CANNOT_PARSE));
            mLog.severe(ioe.getMessage());
            setError(mTranslator.getString(FBC_CANNOT_PARSE) + "\n"
                + ioe.getMessage());
            setException(ioe);
            ioe.printStackTrace();
            mValid = false;
        }
    }
}
