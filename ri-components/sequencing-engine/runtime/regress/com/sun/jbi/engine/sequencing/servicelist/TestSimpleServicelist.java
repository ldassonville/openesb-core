/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSimpleServicelist.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import com.sun.jbi.engine.sequencing.MessageRegistry;
import com.sun.jbi.engine.sequencing.PatternRegistry;
import com.sun.jbi.engine.sequencing.SequencingEngineContext;
import com.sun.jbi.engine.sequencing.exception.ServicelistException;
import com.sun.jbi.engine.sequencing.framework.Servicelist;
import com.sun.jbi.engine.sequencing.framework.threads.Command;
import com.sun.jbi.engine.sequencing.servicelist.ServiceBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistReader;
import com.sun.jbi.engine.sequencing.util.MessageExchangeHelper;

import junit.framework.*;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSimpleServicelist extends TestCase
{
    /**
     * Creates a new TestSimpleServicelist object.
     *
     * @param testName   
     */
    public TestSimpleServicelist(java.lang.String testName)
    {
        super(testName);
    }

    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestSimpleServicelist.class);

        return suite;
    }

    /**
     * Test of cancelExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testCancelExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of execute method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testExecute()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of executeService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testExecuteService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getCurrentExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetCurrentExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getCurrentService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetCurrentService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDeploymentId method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetDeploymentId()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getInboundExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetInboundExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getListName method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetListName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServicelistBean method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetServicelistBean()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getState method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testGetState()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of hasTimedOut method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testHasTimedOut()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processResponse method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testProcessResponse()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of sendListError method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testSendListError()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of sendListResponse method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testSendListResponse()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setInboundExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testSetInboundExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setMessageExchange method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testSetMessageExchange()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setServicelistBean method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testSetServicelistBean()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setState method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testSetState()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of startTimer method, of class
     * com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist.
     */
    public void testStartTimer()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
