/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWorkManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.framework.threads;

import junit.framework.*;

import java.util.Hashtable;
import java.util.logging.Logger;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestWorkManager extends TestCase
{
    /**
     * Creates a new TestWorkManager object.
     *
     * @param testName
     */
    public TestWorkManager(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestWorkManager.class);

        return suite;
    }

    /**
     * Test of cease method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testCease()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getBusyThreads method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testGetBusyThreads()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getWorkManager method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testGetWorkManager()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of init method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testInit()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processCommand method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testProcessCommand()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setLogger method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testSetLogger()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setMaxThreads method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testSetMaxThreads()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setMinThreads method, of class
     * com.sun.jbi.engine.sequencing.framework.threads.WorkManager.
     */
    public void testSetMinThreads()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
