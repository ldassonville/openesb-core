/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServicelistReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import com.sun.jbi.engine.sequencing.util.*;

import junit.framework.*;

import org.w3c.dom.*;

import java.io.File;

import java.util.LinkedList;
import java.util.logging.Logger;

import javax.xml.parsers.*;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServicelistReader extends TestCase
{
    /**
     *    
     */
    private ServicelistReader mConfigReader;

    /**
     *    
     */
    private ServicelistValidator mValidator;

    /**
     * Creates a new TestServicelistReader object.
     *
     * @param testName
     */
    public TestServicelistReader(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestServicelistReader.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        mConfigReader = new ServicelistReader();
        System.out.println("testInit");

        String srcroot = System.getProperty("junit.srcroot");

        try
        {
            mValidator =
                new ServicelistValidator(srcroot
                    + "/engine/sequencing/schema/servicelist.xsd",
                    srcroot
                    + "/engine/sequencing/deploy/artifact/servicelist.xml");
            mValidator.validate();
            mConfigReader.init(mValidator.getDocument());
        }
        catch (Exception jbiException)
        {
            jbiException.printStackTrace();
            fail("Cannot Init Config Reader");
        }
    }

    /**
     * Test of getError method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistReader.
     */
    public void testGetError()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceCount method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistReader.
     */
    public void testGetServiceCount()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceListBean method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistReader.
     */
    public void testGetServiceListBean()
    {
        // Add your test code below by replacing the default call to fail.
        System.out.println("testGetBean");

        String ep = "servicelist:AdaptionService";
        ServicelistBean eb = mConfigReader.getServicelistBean();
        assertNotNull("Servicelist Bean is Null ", eb);
    }

    /**
     * Test of init method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistReader.
     */
    public void testInit()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of loadServices method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistReader.
     */
    public void testLoadServices()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
