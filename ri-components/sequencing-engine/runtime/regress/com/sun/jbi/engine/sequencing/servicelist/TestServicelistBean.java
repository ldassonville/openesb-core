/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServicelistBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import junit.framework.*;

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Enumeration;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServicelistBean extends TestCase
{
    /**
     * Creates a new TestServicelistBean object.
     *
     * @param testName   
     */
    public TestServicelistBean(java.lang.String testName)
    {
        super(testName);
    }

    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestServicelistBean.class);

        return suite;
    }

    /**
     * Test of addService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testAddService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of clearService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testClearService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDeploymentId method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testGetDeploymentId()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getListdescription method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testGetListdescription()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getOperation method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testGetOperation()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testGetService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceCount method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testGetServiceCount()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServicename method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testGetServicename()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of removeService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testRemoveService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setDeploymentId method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testSetDeploymentId()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setListdescription method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testSetListdescription()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setOperation method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testSetOperation()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testSetService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setServicename method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistBean.
     */
    public void testSetServicename()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
