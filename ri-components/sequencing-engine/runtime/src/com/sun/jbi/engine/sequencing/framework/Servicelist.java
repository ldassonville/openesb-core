/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Servicelist.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.framework;

import com.sun.jbi.engine.sequencing.framework.threads.Command;
import com.sun.jbi.engine.sequencing.servicelist.ServiceBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;

import javax.transaction.Transaction;


/**
 * This is an interface for all servicelist implementations.
 *
 * @author Sun Microsystems, Inc.
  */
public interface Servicelist
    extends Command
{
    /**
     * Should return the current service that is being executed in this list.
     *
     * @return service bean
     */
    ServiceBean getCurrentService();

    /**
     * Returns the deployment id cooresponding to this list.
     *
     * @return deployment id
     */
    String getDeploymentId();

    /**
     * Returns the name of the service list.
     *
     * @return list name
     */
    String getListName();

    /**
     * sets the message exchange.
     *
     * @param msgex message exchage
     */
    void setMessageExchange(javax.jbi.messaging.MessageExchange msgex);

    /**
     * Sets the service list bean.
     *
     * @param listbean list bean
     */
    void setServicelistBean(ServicelistBean listbean);

    /**
     * returns the service list bean.
     *
     * @return list bean
     */
    ServicelistBean getServicelistBean();

    /**
     * Sets the transaction .
     *
     * @param tran transaction.
     */
    void setTransactionContext(Transaction tran);

    /**
     * sets the type.
     *
     * @param state state
     */
    void setType(int state);    
    
}
