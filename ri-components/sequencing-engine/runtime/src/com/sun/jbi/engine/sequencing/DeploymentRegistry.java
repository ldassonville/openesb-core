/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;


/**
 * This is a registry which maintains the list of deployed services and  the
 * bean object.
 *
 * @author Sun Microsystems, Inc.
 */
public final class DeploymentRegistry
    implements SequencingEngineResources
{
    /**
     * Singleton reference.
     */
    private static DeploymentRegistry sMe;

    /**
     * List of all registered channels.
     */
    private HashMap mDeployments;

    /**
     * Logger object
     */
    private Logger mLog;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Private constructor.
     */
    private DeploymentRegistry()
    {
        mDeployments = new HashMap();
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized ChannelRegistry reference
     */
    public static synchronized DeploymentRegistry getInstance()
    {
        if (sMe == null)
        {
            sMe = new DeploymentRegistry();
        }

        return sMe;
    }

    /**
     * Returns all the deployments to SE.
     *
     * @return Array of deployment Ids.
     */
    public synchronized String [] getAllDeployments()
    {
        List deps = new ArrayList();
        Collection col = mDeployments.values();
        Iterator iter = col.iterator();

        while (iter.hasNext())
        {
            ServicelistBean eb = (ServicelistBean) iter.next();
            String depid = eb.getDeploymentId();
            deps.add(depid);
        }

        String [] deployments = new String[deps.size()];
        Iterator depiter = deps.iterator();
        int i = 0;

        while (depiter.hasNext())
        {
            deployments[i] = (String) depiter.next();
            i++;
        }

        return deployments;
    }

    /**
     * Util method to find if a service has been deployed.
     *
     * @param servicename Service name
     *
     * @return true if deployed, false otherwise.
     */
    public boolean isDeployed(String servicename)
    {
        if (mDeployments.get(servicename) == null)
        {
            return false;
        }

        return true;
    }

    /**
     * Gets the status of the asa id.
     *
     * @param asId sub-assembly id
     *
     * @return true if deployed , false otherwise.
     */
    public boolean getDeploymentStatus(String asId)
    {
        Set keyset = mDeployments.keySet();
        Iterator iter = keyset.iterator();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            ServicelistBean eb = (ServicelistBean) mDeployments.get(ser);

            if (eb.getDeploymentId().equals(asId))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the servicelist bean associated with the ASA id.
     *
     * @param asId ASA ID.
     *
     * @return list of serviclist bean.
     */
    public ServicelistBean getService(String asId)
    {
        Set keyset = mDeployments.keySet();
        Iterator iter = keyset.iterator();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            ServicelistBean sb = (ServicelistBean) mDeployments.get(ser);

            if (sb.getDeploymentId().trim().equals(asId))
            {
                return sb;
            }
        }

        return null;
    }

    /**
     * Removes all the entries from the registry.
     */
    public void clearRegistry()
    {
        if (mDeployments == null)
        {
            return;
        }

        mDeployments = new HashMap();
    }

    /**
     * Removes a service from the registry.
     *
     * @param service service name
     */
    public synchronized void deregisterService(String service)
    {
        mLog.info(mTranslator.getString(SEQ_DEREGISTER_SERVICE, service));

        ServicelistBean eb = (ServicelistBean) mDeployments.remove(service);

        if (eb == null)
        {
            mLog.severe(mTranslator.getString(SEQ_DEREGISTER_SERVICE_FAILED,
                    service));
        }
    }

    /**
     * Gets the Service list associated with the service name .
     *
     * @param servicename service name
     *
     * @return servicelist bean
     */
    public ServicelistBean findService(String servicename)
    {
        ServicelistBean sb;
        sb = (ServicelistBean) mDeployments.get(servicename);

        return sb;
    }

    /**
     * List all endpoints currently present in the registry.
     *
     * @return an iterator over all endpoints in the registry.
     */
    public Iterator listAllServices()
    {
        if (mDeployments == null)
        {
            return null;
        }

        Set tmp = mDeployments.keySet();

        if (tmp == null)
        {
            return null;
        }

        return tmp.iterator();
    }

    /**
     * Registers an endpoint with the registry.  Duplicates are allowed and no
     * validation is performed at this point.
     *
     * @param service the endpoint to register
     * @param listbean list bean
     */
    public synchronized void registerService(
        String service,
        ServicelistBean listbean)
    {
        mLog.info(mTranslator.getString(SEQ_REGISTER_SERVICE, service));
        mDeployments.put(service, listbean);
    }
}
