/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import javax.jbi.servicedesc.ServiceEndpoint;


/**
 * Class ServiceBean.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceBean
    implements java.io.Serializable
{
    /**
     * Default timeout
     */
    private static final long DEFAULT_TIMEOUT = 5000;

    /**
     * Service reference.
     */
    private ServiceEndpoint mServiceReference;

    /**
     * Field mDescription
     */
    private java.lang.String mDescription;

    /**
     * Endpoint name
     */
    private String mEndpointName;

    /**
     * Interface name.
     */
    private String mInterfaceName;

    /**
     * Interface namespace.
     */
    private String mInterfaceNamespace;

    /**
     * Message exchange pattern.
     */
    private String mMep;

    /**
     * Field mName
     */
    private java.lang.String mName;

    /**
     * Namespace URI
     */
    private String mNamespace;

    /**
     * Field mOperation
     */
    private java.lang.String mOperation;

    /**
     * Operation namespace
     */
    private String mOperationNamespace;

    /**
     * Field mServiceId
     */
    private java.lang.String mServiceId;

    /**
     * Keeps track of state for field: mTimeout
     */
    private boolean mHasTimeout;

    /**
     * Field mTimeout
     */
    private long mTimeout = DEFAULT_TIMEOUT;

    /**
     * Creates a new ServiceBean object.
     */
    public ServiceBean()
    {
        super();
    }

    /**
     * Sets the value of field 'description'.
     *
     * @param description the value of field 'description'.
     */
    public void setDescription(java.lang.String description)
    {
        this.mDescription = description;
    }

    /**
     * Returns the value of field 'description'.
     *
     * @return the value of field 'description'.
     */
    public java.lang.String getDescription()
    {
        return this.mDescription;
    }

    /**
     * Sets the endpoint name.
     *
     * @param name endpoint name
     */
    public void setEndpointName(String name)
    {
        mEndpointName = name;
    }

    /**
     * Gets the endpoint name.
     *
     * @return endpoint name
     */
    public String getEndpointName()
    {
        return mEndpointName;
    }

    /**
     * Setter for property mInterfaceName.
     *
     * @param mInterfaceName New value of property mInterfaceName.
     */
    public void setInterfaceName(java.lang.String mInterfaceName)
    {
        this.mInterfaceName = mInterfaceName;
    }

    /**
     * Getter for property mInterfaceName.
     *
     * @return Value of property mInterfaceName.
     */
    public java.lang.String getInterfaceName()
    {
        return mInterfaceName;
    }

    /**
     * Setter for property mInterfaceNamespace.
     *
     * @param mInterfaceNamespace New value of property mInterfaceNamespace.
     */
    public void setInterfaceNamespace(java.lang.String mInterfaceNamespace)
    {
        this.mInterfaceNamespace = mInterfaceNamespace;
    }

    /**
     * Getter for property mInterfaceNamespace.
     *
     * @return Value of property mInterfaceNamespace.
     */
    public java.lang.String getInterfaceNamespace()
    {
        return mInterfaceNamespace;
    }

    /**
     * Sets the MEP.
     *
     * @param mep mep
     */
    public void setMep(String mep)
    {
        mMep = mep;
    }

    /**
     * Gets the MEP.
     *
     * @return mep
     */
    public String getMep()
    {
        return mMep;
    }

    /**
     * Sets the value of field 'name'.
     *
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this.mName = name;
    }

    /**
     * Returns the value of field 'name'.
     *
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this.mName;
    }

    /**
     * Sets the value of field 'namespace'.
     *
     * @param namespace the value of field 'namespace'.
     */
    public void setNamespace(java.lang.String namespace)
    {
        this.mNamespace = namespace;
    }

    /**
     * Gets the value of field 'namespace'.
     *
     * @return namespace name
     */
    public String getNamespace()
    {
        return mNamespace;
    }

    /**
     * Sets the value of field 'operation'.
     *
     * @param operation the value of field 'operation'.
     */
    public void setOperation(java.lang.String operation)
    {
        this.mOperation = operation;
    }

    /**
     * Returns the value of field 'operation'.
     *
     * @return the value of field 'operation'.
     */
    public java.lang.String getOperation()
    {
        return this.mOperation;
    }

    /**
     * Sets the value of field 'operation'.
     *
     * @param operationnamespace the value of field 'operation'.
     */
    public void setOperationNamespace(java.lang.String operationnamespace)
    {
        this.mOperationNamespace = operationnamespace;
    }

    /**
     * Returns the value of field 'operation'.
     *
     * @return the value of field 'operation'.
     */
    public java.lang.String getOperationNamespace()
    {
        return this.mOperationNamespace;
    }

    /**
     * Sets the service reference.
     *
     * @param ref service reference
     */
    public void setServiceReference(ServiceEndpoint ref)
    {
        mServiceReference = ref;
    }

    /**
     * Gets the service reference.
     *
     * @return service ref
     */
    public ServiceEndpoint getServiceReference()
    {
        return mServiceReference;
    }

    /**
     * Sets the value of field 'serviceid'.
     *
     * @param serviceid the value of field 'serviceid'.
     */
    public void setServiceid(java.lang.String serviceid)
    {
        this.mServiceId = serviceid;
    }

    /**
     * Returns the value of field 'serviceid'.
     *
     * @return the value of field 'serviceid'.
     */
    public java.lang.String getServiceid()
    {
        return this.mServiceId;
    }

    /**
     * Sets the value of field 'timeout'.
     *
     * @param timeout the value of field 'timeout'.
     */
    public void setTimeout(long timeout)
    {
        this.mTimeout = timeout;
        this.mHasTimeout = true;
    }

    /**
     * Returns the value of field 'timeout'.
     *
     * @return the value of field 'timeout'.
     */
    public long getTimeout()
    {
        return this.mTimeout;
    }

    /**
     * Method deleteTimeout.
     */
    public void deleteTimeout()
    {
        this.mHasTimeout = false;
    }

    /**
     * Method hasTimeout.
     *
     * @return true if it has timed out.
     */
    public boolean hasTimeout()
    {
        return this.mHasTimeout;
    }
}
