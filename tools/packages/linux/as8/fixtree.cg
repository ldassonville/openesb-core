#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)fixtree.cg - ver 1.1 - 01/04/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#required variables:  $SRCROOT, $TOOLROOT, $FORTE_PORT
#optional variables:  see below

#### set bootstrap vars:
TOOLROOT = $TOOLROOT:nameof:env
%ifnot TOOLROOT   %exit ${CG_INFILE}: TOOLROOT is not set.  Cannot proceed.

FORTE_PORT = $FORTE_PORT:nameof:env
%ifnot FORTE_PORT %exit ${CG_INFILE}: FORTE_PORT is not set.  Cannot proceed.

JAVA_HOME = $JAVA_HOME:nameof:env
%ifnot JAVA_HOME %exit ${CG_INFILE}: JAVA_HOME is not set.  Cannot proceed.
#### end bootstrap

#set remaining env defs: SRCROOT, AS8BASE, AS8DIST, and set up $DOMAIN_LIST:
%include $TOOLROOT/packages/$FORTE_PORT/as8/fixtree.defs

#check that domain name args are valid:
%call check_domain_names
%if $BAD_DOMAIN_NAMES %eecho ${CG_INFILE}: bad domain name arguments - ABORT
%if $BAD_DOMAIN_NAMES %halt 1

#check that instance name args are valid:
%call check_instance_names
%if $BAD_INSTANCE_NAMES %eecho ${CG_INFILE}: bad instance name arguments - ABORT
%if $BAD_INSTANCE_NAMES %halt 1

#first patch the appserver install with our locations:
CG_ROOT=	$AS8BASE
CG_MODE=0775

#use JV variables in appserver patch, in case this is a cygwin port:
AS8BASE =   $JV_AS8BASE
JAVA_HOME = $JV_JAVA_HOME

#we only patch clean installs:
%if $IS_CLEAN_INSTALL %include $AS8BASE/patch/as8patch.cg

admin_name	= admin
admin_passwd	= adminadmin
admin_master_passwd	= changeit
admin_passwdfile	= $JV_AS_ADMIN_PWFILE

#######
#create password file:
#######
ECHO_TXT = << EOF
AS_ADMIN_PASSWORD=$admin_passwd
AS_ADMIN_ADMINPASSWORD=$admin_passwd
AS_ADMIN_MASTERPASSWORD=$admin_master_passwd
EOF

#emit passwords file:
echo passwords

#######
#create imq password file:
#######
ECHO_TXT = << EOF
imq.imqcmd.password=admin
EOF

#emit passwords file:
echo imq-passwords

#restore original values defined in fixtree.defs:
AS8BASE =   $SV_AS8BASE
JAVA_HOME = $SV_JAVA_HOME

#if cygwin, add shell-script wrappers for key commands:
%if $IS_CYGWIN %include create_sh_wrappers.cg

#now create the domains:
%include create_domains.cg
