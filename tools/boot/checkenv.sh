#!/usr/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)checkenv.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo
echo "Checking the JDK installation..."
$JAVA_HOME/bin/java -version
if [ $? -ne 0 ]; then
  echo "CONFIGURATION ERROR: Check the java settings in the build_setup file and rerun this script"
  exit 1
else
  echo JDK installation found
fi
echo

echo "Checking the MAVEN 2 installation..."
$M2_HOME/bin/mvn -version
if [ $? -ne 0 ]; then
  echo "CONFIGURATION ERROR: Check the maven settings in the build_setup file and rerun this script"
  exit 1
else
  echo MAVEN 2 installation found
fi
echo


echo "Checking the GLASSFISH installation..."
ls $AS8BASE/LICENSE.txt
if [ $? -ne 0 ]; then
  echo "CONFIGURATION ERROR: Check the glassfish settings in the build_setup file and rerun this script"
  exit 1
else
  echo GLASSFISH installation found
fi
echo

if [ $FORTE_PORT = "cygwin" ]; then
  od -c $SRCROOT/m2.ant | grep '\\r' > /dev/null
  if [ $? -eq 0 ]; then
    echo "CONFIGURATION ERROR: You are creating files with DOS file endings.  Y ou will need to uninstall cygwin and then reinstall it.  Make sure that when you reinstall cygwin you select Unix/binary as the default text file type."
    exit 1
  else
    echo "cygwin is correctly creating files in Unix/binary file type."
  fi
fi
echo

#incase the users want to use the ant distribution that ships with glassfish
#make sure it is executable
echo "Making sure glassfish's ant binary is executable..."
chmod +x $AS8BASE/lib/ant/bin/ant
echo "Done"
echo
