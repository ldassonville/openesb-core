<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="java.util.*" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.auth.*" %>

<%! 
  public void jspInit()
  {
    WikiEngine wiki = WikiEngine.getInstance( getServletConfig() );
    AuthenticationManager mgr = wiki.getAuthenticationManager();
    if ( mgr.isContainerAuthenticated() )
    {   
      postURL = "j_security_check";
    }
    else
    {
      postURL = "Login.jsp";
    }
  }
  String postURL;
%>

<form action="<%=postURL%>"  
      class="wikiform"
   onsubmit="return WikiForm.submitOnce( this );"
     method="post" accept-charset="<wiki:ContentEncoding />" >

  <h3>Sign In to <wiki:Variable var="applicationname" /></h3>

  <input type="hidden" name="page" value="<wiki:Variable var="pagename" />" />
  <div align="center">
  <table>
    <tr>
      <td colspan="2">
        <wiki:Messages div="error" prefix="Could not log in: " />
      </td>
    </tr>
    <tr>
      <td><label for="j_username">Login</label></td>
      <td><input type="text" size="24"
                 value="<wiki:Variable var='uid' default='' />" 
                 name="j_username" id="j_username" /></td>
    </tr>
    <tr>
      <td><label for="j_password">Password</label></td>
      <td><input type="password"  size="24"
                 name="j_password" id="j_password" /></td>
    </tr>
    <tr>
      <td />
      <td align="center">
        <input type="submit" value="login" name="action" style="display:none;"/>
        <input type="button" value="login" name="proxy1" onclick="this.form.action.click();" />
      </td>
    </tr>
  </table>
  </div>
  <wiki:Permission permission='editProfile'>
  <p>Don't have a password? 
    <a href="javascript://nop/" title="Register New User"
       onclick="TabbedSection.onclick('registercontent');">
      Register with <wiki:Variable var="applicationname" /> now!
    </a>
  </p>
  </wiki:Permission>

</form>
