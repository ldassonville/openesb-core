<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"

    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <lafPayroll>

            <xsl:for-each select="safPayroll/safTimecard">

                <xsl:text>&#xA;</xsl:text>

                <xsl:text>    </xsl:text>

                <lafTimecard>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <EmployeeId><xsl:value-of select="Id"/></EmployeeId>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <FirstName><xsl:value-of select="FName"/></FirstName>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <MiddleName><xsl:value-of select="MName"/></MiddleName>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <LastName><xsl:value-of select="SurName"/></LastName>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <NumberOfHoursWorked><xsl:value-of select="Hours"/>     </NumberOfHoursWorked>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <CurrentAddress><xsl:value-of select="Location"/></CurrentAddress>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>        </xsl:text>

                    <ContactNumber><xsl:value-of select="Phone"/></ContactNumber>

                    <xsl:text>&#xA;</xsl:text>

                    <xsl:text>    </xsl:text>

                </lafTimecard>

            </xsl:for-each>

            <xsl:text>&#xA;</xsl:text>

        </lafPayroll> 

    </xsl:template>

</xsl:stylesheet>

