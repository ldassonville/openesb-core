#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)packaging00012.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#this tests runs walkdir on m2 open-esb contents
testname=packaging00012
theDir=m2-new

echo Test $testname

. ./regress_defs.ksh

cd $SVC_BLD/$theDir

echo creating $theDir-toc.txt
walkdir > $SVC_BLD/$theDir-toc.txt
[ $? -ne 0 ] && bldmsg -error walkdir $theDir FAILED && exit 1

cd $SVC_REGRESS
echo diffing reference against $theDir-toc.txt
diff testdat/$theDir-toc.txt $SVC_BLD/$theDir-toc.txt
[ $? -eq 0 ] && echo there are no diffs.
